export const GRID_COLUMNS = 12;
export const FOOTER_HEIGHT = 80;
export const MARGIN_HORIZONTAL = 30;
export const GRID_GUTTERS = 30 / (1440 - MARGIN_HORIZONTAL * 2); // gutter-width / (layout-size - margins)
