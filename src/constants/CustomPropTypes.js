import PropTypes from 'prop-types';

import { Colors } from 'theme';
import { SMALL, MEDIUM, LARGE } from 'theme/Breakpoints';

import * as Header from './Header';
import Icons from './Icons';
import * as Themes from './Themes';

export default {
  animation: PropTypes.shape({
    interpolate: PropTypes.func.isRequired,
  }),
  breakpoint: PropTypes.oneOf([SMALL, MEDIUM, LARGE]),
  color: PropTypes.oneOf(Object.values(Colors)),
  headerTheme: PropTypes.oneOf(Object.values(Header)),
  icon: PropTypes.oneOf(Object.values(Icons)),
  theme: PropTypes.oneOf(Object.values(Themes)),
};
