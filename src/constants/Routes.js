// TODO:  Remover esse arquivo quando não houver mais rotas apontando para ele

import pathToRegexp from 'path-to-regexp';

export const publicPath = process.env.PUBLIC_PATH || '';

export const toPath = (route, params = {}) => pathToRegexp.compile(route)(params);

const Routes = {
  HOME: `${publicPath}/`,

  // authentication
  LOGIN: `${publicPath}/login`,
  LOGIN_LEGACY: `${publicPath}/login/legacy-account`,
  FORGOT_PASSWORD: `${publicPath}/forgot-password`,

  // main pages
  BRIEFING: `${publicPath}/briefing`,
  CHAT: `${publicPath}/chat`,
  PROFILE: `${publicPath}/profiles`,
  SETTINGS: `${publicPath}/settings`,
};

export const BriefingRoutes = {
  CATEGORY: Routes.BRIEFING,
  BRIEFING: `${Routes.BRIEFING}/briefing/:category/:subcategory`,
  PUBLISH: `${Routes.BRIEFING}/briefing/publish`,
  SUBCATEGORY: `${Routes.BRIEFING}/briefing/:category`,
};

export const ChatRoutes = {
  MESSAGES: Routes.CHAT,
};

export default Routes;
