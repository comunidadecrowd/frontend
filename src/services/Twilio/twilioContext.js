import React, { createContext } from 'react';

export const TwilioContext = createContext();

export const withTwilio = (Component) => (props) => {

  return (
    <TwilioContext.Consumer>
      {
        Twilio => <Component {...props} Twilio={Twilio} />
      }
    </TwilioContext.Consumer>
  )

}