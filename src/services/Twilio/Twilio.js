import { Client as TwilioChatClient } from 'twilio-chat';
import APIS from 'apis';

import EventEmitter from 'events';

import APP_STORAGE from 'APP_STORAGE';

export default class Twilio {

  constructor(fromNavigation) {

    this.emitter = new EventEmitter();
    // this.emitter.removeAllListeners()

    this.Chat;

    if (!global.TwilioChat) {
      this.initChat(fromNavigation);
    } else {
      this.Chat = global.TwilioChat;
      this.registerChatListeners();
    }

    

  }

  initChat = async (fromNavigation) => {

    if (!fromNavigation) {

      const { userID, _FIREBASEUSERTOKEN } = global.CrowdStorage.User;

      APIS.Chat.getToken(
        userID,
        _FIREBASEUSERTOKEN,
        async ({ requestAccessToken: { token } }) => {

          this.Chat = await TwilioChatClient.create(token);
          global.TwilioChat = await this.Chat;

          await this.registerChatListeners()

          if (_FIREBASEUSERTOKEN !== null) {
            this.Chat.setPushRegistrationId('fcm', _FIREBASEUSERTOKEN);
          }

        }
      )

    } else {

      const getUserOnStorage = await APP_STORAGE.getItem('User');
      
      APIS.Chat.getToken(
        getUserOnStorage.userID,
        getUserOnStorage._FIREBASEUSERTOKEN,
        async ({ requestAccessToken: { token } }) => {

          this.Chat = await TwilioChatClient.create(token);
          global.TwilioChat = await this.Chat;

          await this.registerChatListeners()

          if (getUserOnStorage._FIREBASEUSERTOKEN !== null) {
            this.Chat.setPushRegistrationId('fcm', getUserOnStorage._FIREBASEUSERTOKEN);
          }

        }
      )

    }

  }

  registerChatListeners = () => {

    this.Chat.on('channelAdded', (channelTwilio) => this.emitter.emit('channelAdded', channelTwilio))
    this.Chat.on('channelUpdated', (channelTwilio) => this.emitter.emit('channelUpdated', channelTwilio))
    this.Chat.on('connectionStateChanged', (obj) => global.emitters.emit('twilioConnectionStateChanged', obj))
    this.Chat.on('tokenAboutToExpire', () => this.renewUserToken())
    this.Chat.on('tokenExpired', () => this.renewUserToken())

  }

  getChannel = async (channelSid, setListeners = false, callback = false) => {

    const Channel = await this.Chat.getChannelBySid(channelSid)

    // TODO:  Alex repensar nessa lógica com callbacks
    if (setListeners) {
      if (!callback) {
        return await this.registerChannelListeners(Channel, channelSid);
      } else {
        await this.registerChannelListeners(Channel, channelSid);
        return await callback(Channel)
      }
    } else {
      if (!callback) {
        return await Channel
      } else {
        return callback(Channel)
      }
    }

  }

  registerChannelListeners = (Channel, channelSid) => {

    Channel.on('messageAdded', (res) => this.emitter.emit(`${channelSid}_messageAdded`, res))
    Channel.on('typingStarted', () => this.emitter.emit(`${channelSid}_channelTypingStarted`));
    Channel.on('typingEnded', () => this.emitter.emit(`${channelSid}_channelTypingEnded`));
    Channel.on('memberUpdated', (res) => this.emitter.emit(`${channelSid}_memberUpdated`, res));

  }

  getChannelMessages = async (channelSid, callback) => {

    return await this.getChannel(channelSid).then((Channel) => callback(Channel.getMessages(100))) // FIX:   Alex && Bruno alterar para um valor dinâmico

  }

  getChannelMembers = async (channelSid, callback) => {

    return await this.getChannel(channelSid).then((Channel) => callback(Channel.getMembers()))

  }

  setAllMessagesConsumed = async (channelSid) => {

    return await this.getChannel(channelSid).then((Channel) => Channel.setAllMessagesConsumed())

  }

  updateLastConsumedMessageIndex = async (channelSid, indexFromTwilio) => {

    return await this.getChannel(channelSid).then((Channel) => Channel.updateLastConsumedMessageIndex(indexFromTwilio))

  }

  checkFriendOnlineStatus = async (friendUserId, callback) => {

    await this.Chat.getUser(friendUserId).then((user) => {

      callback(user.online)

      user.on('updated', (event) => {

        const { updateReasons, user: { online } } = event;

        if (updateReasons.includes('online')) {

          this.emitter.emit(`${friendUserId}_onlineStatus`, online);

        }
      })

    })

  }

  renewUserToken = () => {

    APIS.Chat.getToken(
      userID,
      _FIREBASEUSERTOKEN,
      ({ requestAccessToken: { token } }) => this.Chat.updateToken(token)
    )

  }

  // sendChatMessage = (channelSid) // TODO:  Alex -> Repensar sobre como usar o sendMessage sem precisar toda hora dar o getChannel

}
