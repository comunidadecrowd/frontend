import callAPI from './callAPI';
const service = 'briefing';

export default {

  insertBriefing: (obj, callback) => {
    const { userID, title, description, excerpt } = obj;
    const gql = `
      mutation{
        insertBriefing(
          ownerId: ${userID},
          title: ${JSON.stringify(title)},
          description: ${JSON.stringify(description)},
          excerpt: ${JSON.stringify(excerpt)}
          status: 200
        ) {
          ownerId,
          title,
          description,
          excerpt,
          status
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  approveMatch: (obj, callback) => {
    const { userId, briefingId, status } = obj;
    const gql = `
      mutation{
        approveMatch(
          briefingId: ${briefingId},
          userId: ${userId}
        ) {
          briefingId,
          channelSid,
          matchId,
          matchUserId,
          messageSid,
          status
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  refuseMatch: (obj, callback) => {
    const { userId, briefingId } = obj;
    const gql = `
      mutation{
        refuseMatch(
          briefingId: ${briefingId},
          userId: ${userId}
        ) {
          briefingId,
          channelSid,
          matchId,
          matchUserId,
          messageSid,
          status
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  updateMatch: (obj, callback) => {
    const { userId, briefingId, status } = obj;
    const gql = `
      mutation{
        updateMatch(
          briefingId: ${briefingId},
          userId: ${userId},
          status: ${status}
        ) {
          briefingId,
          channelSid,
          matchId,
          matchUserId,
          messageSid,
          status
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

}
