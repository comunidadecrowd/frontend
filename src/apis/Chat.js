import callAPI from './callAPI';
const service = 'chat';

export default {

  getToken: (userID, deviceToken, callback) => {

    if(!userID) {
      return console.error("on APIS.Chat.getToken => userID is NULL or UNDEFINED") 
    }

    // TODO:  Alex: Depois refaforar o platform para Android, iOS e Web

    const gql = `{
      requestAccessToken(
        uid: ${userID},
        deviceToken: "${deviceToken}",
        platform: "outros",
      ) {
        token
      }
    }`;

    callAPI(service, gql).then(res => res && callback(res))
    
  }

}