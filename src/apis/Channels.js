// import { request } from 'graphql-request'; /* // NOTE:  01/08 Após o refact verificar se ainda precisa manter esse método antigo
import callAPI from './callAPI';

// const API_URL = __DEV__ ? 'https://hm.crowd.br.com/v1/channels/graphql' : 'https://v1.crowd.br.com/channels/graphql'; /* // NOTE:  01/08 Após o refact verificar se ainda precisa manter esse método antigo
const service = 'channels';

export default {

  listUserChannels: (userId, callback) => {
    const gql = `
      query listUserChannels($userId: ID!) {
        listUserChannels(userId: "${userId}") {
          status,
          friendlyName,
          channelSid,
          unreadMessagesCount,
          createdBy,
          dateUpdated
          members {
            userId,
            profileImage,
            name,
            title,
            category
          }
          attributes {
            type
          }
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  /* // NOTE:  01/08 Após o refact verificar se ainda precisa manter esse método antigo
    channelDetails: (obj, callback) => {
      const { channelSid, userId } = obj;
      const query = `
        query channelDetails($channelSid: String!, $userId: ID!) {
          channelDetails(channelSid: "${channelSid}", userId: "${userId}") {
            status,
            friendlyName,
            channelSid,
            unreadMessagesCount,
            createdBy,
            dateUpdated
            members {
              userId,
              profileImage,
              name,
              title,
              category
            }
            attributes {
              type,
              blocked
            }
          }
        }
      `;

      return request(API_URL, query)
        .then((data) => callback(data))
        .catch(error => global.emitters.emit('apisRequestError', { api: 'Channels.channelDetails', error}));
    },
  */

  channelDetails: (obj, callback) => {
    const { channelSid, userId } = obj;
    const gql = `
        query channelDetails($channelSid: String!, $userId: ID!) {
          channelDetails(channelSid: "${channelSid}", userId: "${userId}") {
            status,
            friendlyName,
            channelSid,
            unreadMessagesCount,
            createdBy,
            dateUpdated
            members {
              userId,
              profileImage,
              name,
              title,
              category
            }
            attributes {
              type
            }
          }
        }
      `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  createChat: (obj, callback) => {
    const { userId, friendUserId } = obj;
    const gql = `
      mutation{
        createChat(
          channelType: "private",
          createdBy: "${userId}",
          invitedUserId: "${friendUserId}"
        ) {
          channelSid,
          createdBy,
          dateCreated,
          dateUpdated,
          friendlyName,
          lastConsumedMessageIndex,
          attributes {
            type
          },
          members {
            attributes
            category
            channelSid
            dateCreated
            dateUpdated
            email
            idCustomer
            idusuario
            lastConsumedMessageIndex
            lastConsumptionTimestamp
            name
            profileImage
            role
            title
            trade
            updatedDb
            userId
          },
          membersCount,
          notificationLevel,
          status,
          uniqueName,
          unreadMessagesCount
        }
      }
    `;

    console.log('gql', gql)

    callAPI(service, gql).then(res => res && callback(res))
  }

}
