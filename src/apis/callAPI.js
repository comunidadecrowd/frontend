import { GraphQLClient } from 'graphql-request';
import { API_URL } from 'utils/Helpers';

export default callAPI = async (service, gql) => {

  const
    _JWT = (global.CrowdStorage.hasOwnProperty('User') && global.CrowdStorage.User.hasOwnProperty('_JWT')) ? global.CrowdStorage.User._JWT : await (async () => {

      const getUser = await APP_STORAGE.getItem('User')
      return getUser.hasOwnProperty('_JWT') ? getUser._JWT : null;

    })(),
    endpoint = API_URL + service + '/graphql',
    noToken = !(/(sendSms|phoneLogin)/).test(endpoint),
    gqlClient = new GraphQLClient(endpoint, noToken ? {} : {
      headers: {
        authorization: `Bearer ${_JWT}`,
      }
    })

  try {
    const req = await gqlClient.request(gql);
    console.log('$$$ endpoint && response', `${endpoint}`, req);
    console.log('$$$ ====');
    return req;
  } catch (error) {
    global.emitters.emit('apisRequestError', { endpoint, error })
    console.log('$$$ req ERROR', error);
  }

}