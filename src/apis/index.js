import User from './User.js';
import Chat from './Chat.js';
import Propose from './Propose.js';
import Briefing from './Briefing.js';
import Channels from './Channels.js';
import Contact from './Contact.js';

export default APIS = { User, Chat, Propose, Briefing, Channels, Contact }