import callAPI from './callAPI';
const service = 'user';

export default {

  userCredentials: (userId, callback) => {
    const gql = `
      query userCredentials($userId: ID!) {
        userCredentials(userId: "${parseInt(userId)}") {
          name,
          title,
          category,
          profileImage,
          city,
          countryCode,
          diary,
          hourly,
          industryList,
          languageList {
            languageId
            proficiency
          },
          skillList {
            label
            skillId
          },
          stateId,
          summary
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))

  },

  blockUser: (obj, callback) => {
    const
      { channelSid, blockedUserId, isBlocked, userId } = obj,
      gql = `
        mutation {
          blockUser(
            channelSid: "${channelSid}",
            blockedUserId: "${blockedUserId}",
            isBlocked: ${isBlocked},
            userId: "${userId}"
          )
        }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  listUsersBlocked: (userId, callback) => {
    const gql = `
      query {
        listBlocked(userId: "${userId}")
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  reportUser: (obj, callback) => {
    const
      { channelSid, reportedUserId, isReported, userId } = obj,
      gql = `
        mutation {
          reportUser(
            channelSid: "${channelSid}",
            reportedUserId: "${reportedUserId}",
            isReported: ${isReported},
            userId: "${userId}"
          )
        }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  listUsersReported: (userId, callback) => {
    const gql = `
      query {
        listReported(userId: "${userId}")
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  sendSMStoUser: async (obj, callback) => {

    const { code, phoneInputMask } = obj;

    let transformedCell = `${code}${phoneInputMask.replace(/[()-\s]+/g, "")}`;

    const gql = `{
      sendSms(phone: "${transformedCell}" ) {
        success
      }
    }`;

    callAPI(service, gql).then(res => res && callback(res))

  },

  validateUserCode: (obj, callback) => {

    const { phoneInputMask, codeValue } = obj;

    let transformedCell = `+55${phoneInputMask.replace(/[()-\s]+/g, "")}`;

    const gql = `{
      phoneLogin(
        phone: "${transformedCell}",
        code: "${codeValue}"
      ) 
      {
        jwt
        success
        userId
      }
    }`;

    callAPI(service, gql).then(res => res && callback(res))

  },

  sendCredentials: (obj, callback) => {

    const
      {
        userID,
        credentials,
        firebaseUserToken,
        summary,
        countryCode,
        stateId,
        city,
        diary,
        hourly
      } = obj;

    let inputCredentials = `userId: "${userID}"`;

    if (credentials) {
      const { name, title, category } = credentials;

      if (name) inputCredentials += `\n , name: "${name}"`;
      if (title) inputCredentials += `\n , title: "${title}"`;
      if (category) inputCredentials += `\n , category: "${category}"`;
    }

    if (firebaseUserToken) inputCredentials += `\n , firebaseUserToken: "${firebaseUserToken}"`;

    if (summary) inputCredentials += `\n , summary: "${summary}"`;

    if (countryCode) inputCredentials += `\n , countryCode: "${countryCode}"`;

    if (stateId) inputCredentials += `\n , stateId: "${stateId}"`;

    if (city) inputCredentials += `\n , city: "${city}"`;

    if (diary) inputCredentials += `\n , diary: [${diary}]`;

    if (hourly) inputCredentials += `\n , hourly: "${hourly}"`;

    const gql = `
      mutation {
        inputCredentials(
          ${inputCredentials}
        ) {
          profileImage,
          email,
          phone,
          firebaseUserToken
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))

  }

}

