import callAPI from './callAPI';
const service = 'propose';

export default {

  listProposes: (cSID, callback) => {
    const gql = `
      query listProposes($channelSid: ID!) {
        listProposeByChannelSid(channelSid: "${cSID}") {
          body {
            title,
            price,
            daysTerm
          },
          proposeId,
          status,
          userId,
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  
  },

  lastStatusProposeById: (proposeId, callback) => {
    const gql = `
      query lastStatusProposeById($proposeId: ID!) {
        lastProposeByProposeId(proposeId: ${proposeId}) {
          body {
            title,
            price,
            daysTerm
          },
          proposeId,
          status,
          userId,
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  createPropose: (obj, callback) => {
    
    const {cSID, userID, title, value: price, term: daysTerm} = obj;
    
    let transformPrice = (priceParam) => {
      
      let 
        replaces = priceParam.replace(/[R$-.-,]+/g,""),
        toLength = (replaces.length - 2);

      return parseFloat(`${replaces.substr(0, toLength)}.${replaces.substr(toLength)}`);

    }     
    
    const gql = `
      mutation{
        insertPropose(
          body: {
              title: "${title}",
              price: ${transformPrice(price)},
              daysTerm: ${Number(daysTerm)}
          }, 
          channelSid: "${cSID}",
          status: 200,
          userId: "${userID}"
        ) {
          body {
            title,
            price,
            daysTerm
          },
          proposeId,
          status,
          userId
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  reSendPropose: (obj, callback) => {
    const { channelSid, proposeId, userID, title, price, daysTerm } = obj;
    const gql = `
      mutation{
        insertPropose(
          body: {
            title: "${title}",
            price: ${price},
            daysTerm: ${Number(daysTerm)}
          }, 
          channelSid: "${channelSid}",
          proposeId: "${proposeId}",
          userId: "${userID}",
          status: 210
        ) {
          body {
            title,
            price,
            daysTerm
          },
          proposeId,
          status,
          userId
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  contractPropose: (obj, callback) => {
    const { proposeId, contractorUserId } = obj;
    const gql = `
      mutation hirePropose($proposeId: ID!, $contractorUserId: ID!) {
        contractPropose(
          proposeId: ${proposeId},
          contractorUserId: ${contractorUserId}
        ) {
          dueDate,
          body {
            title,
            price,
          },
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  requestApprovalPropose: (obj, callback) => {
    const { proposeId } = obj;
    const gql = `
      mutation{
        requestApprovalPropose(
          proposeId: "${proposeId}"
        ) {
          dueDate,
          proposeId,
          channelSid,
          status,
          body {
            title,
            price,
            daysTerm
          }
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  },

  approvePropose: (obj, callback) => {
    const { proposeId, contractorUserId } = obj;
    const gql = `
      mutation {
        approvePropose(
          proposeId: ${proposeId},
          approverUserId: ${contractorUserId}
        ) {
          dueDate,
          proposeId,
          channelSid,
          status,
          body {
            title,
            price,
            daysTerm,
          }
        }
      }
    `;

    callAPI(service, gql).then(res => res && callback(res))
  }

}