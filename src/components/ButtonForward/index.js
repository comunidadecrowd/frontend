import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Animated, Text, TouchableHighlight, View } from 'react-native';

import { Component } from 'components/BaseComponent';

import CustomPropTypes from 'constants/CustomPropTypes';

import { Colors, Duration, TextStyles } from 'theme';

import withAnimation from 'utils/withAnimation';

class ButtonForward extends Component {
  static propTypes = {
    activating: CustomPropTypes.animation.isRequired,
    children: PropTypes.node.isRequired,
    disabled: PropTypes.bool,
    isLastStep: PropTypes.bool,
    style: TouchableHighlight.propTypes.style,

    // functions
    onPress: PropTypes.func.isRequired,
    startActivatingTiming: PropTypes.func.isRequired,
  };

  static defaultProps = {
    disabled: false,
    isLastStep: false,
    style: null,
  };

  _handleLayout = event => {
    const { width } = event.nativeEvent.layout;

    this.setState({ rootWidth: width - 1 });
  };

  componentDidUpdate(prevProps) {
    const { startActivatingTiming, disabled } = this.props;

    if (prevProps.disabled !== disabled) {
      startActivatingTiming({
        toValue: !disabled ? 1 : 0,
        duration: Duration.FAST,
      });
    }
  }

  template(css) {
    const { activating, children, onPress, style, disabled, isLastStep } = this.props;

    const label = children.toLowerCase();

    return (
      <TouchableHighlight
        onPress={onPress}
        onMouseEnter={this._handleMouseEnter}
        onMouseLeave={this._handleMouseLeave}
        disabled={disabled}
        style={css(['root', style])}
      >
        <Animated.View
          style={css([
            'container',
            {
              paddingLeft: activating.interpolate({
                inputRange: [0, 1],
                outputRange: [32, 23],
              }),
            },
          ])}
        >
          <View
            style={css([
              'arrow',
              !disabled && 'arrowActive',
              isLastStep && !disabled && 'arrowActiveLast',
            ])}
          />
          <Text
            style={css([
              'label',
              !disabled && 'labelActive',
              isLastStep && !disabled && 'labelActiveLast',
            ])}
          >
            {label}
          </Text>
        </Animated.View>
      </TouchableHighlight>
    );
  }

  styles() {
    return {
      root: {
        alignSelf: 'center',
        backgroundColor: 'transparent',
        padding: 8,

        ...Platform.select({
          web: {
            outline: 'none',
          },
        }),
      },

      container: {
        paddingLeft: 32,
      },

      arrow: {
        position: 'absolute',
        borderLeftColor: Colors.GRAY_DARK,
        borderTopColor: Colors.TRANSPARENT,
        borderBottomColor: Colors.TRANSPARENT,
        borderTopWidth: 4,
        borderBottomWidth: 4,
        borderLeftWidth: 8,
        left: 8,
        top: 9,
        zIndex: 1,
      },

      arrowActive: {
        borderLeftColor: Colors.BLUE,
      },

      arrowActiveLast: {
        borderLeftColor: Colors.GREEN,
      },

      label: {
        ...TextStyles.h6,
        color: Colors.GRAY_DARK,
      },

      labelActive: {
        color: Colors.BLUE,
      },

      labelActiveLast: {
        color: Colors.GREEN,
      },
    };
  }
}

export default withAnimation('activating', 0)(ButtonForward);
