import React from 'react';
import { View, TouchableOpacity, StatusBar, Text } from 'react-native';
import { Link } from 'react-router-native';
import { Navigation } from 'react-native-navigation';

import LogoShort from 'assets/svgs/LogoShort';

import { PureComponent } from 'components/BaseComponent';
import Icon, { Icons } from 'components/Icon';
import { GlobalsContext } from 'components/GlobalsProvider';
import { ThemeContext } from 'components/ThemeProvider';

import { FULL_HEADER, MODAL_HEADER, NO_HEADER } from 'constants/Header';
import { MARGIN_HORIZONTAL } from 'constants/Layout';
import Routes, { BriefingRoutes } from 'constants/Routes';
import { THEME_BLACK, THEME_WHITE } from 'constants/Themes';

import { Colors, TextStyles } from 'theme';

class Header extends PureComponent {
  _handleBack = () => {
    Navigation.popTo(BriefingRoutes.CATEGORY);
  };

  template(css) {
    return (
      <GlobalsContext.Consumer>
        {({ headerTheme, headerOptions }) => (
          <ThemeContext.Consumer>
            {({ theme }) => (
              <View>
                <StatusBar barStyle="light-content" />
                {(!headerTheme || headerTheme === NO_HEADER) && <View style={css('noHeader')} />}
                {headerTheme === FULL_HEADER && (
                  <View style={css('root')}>
                    {!!headerOptions &&
                      !!headerOptions.title && (
                        <Text style={css('title')}>{headerOptions.title}</Text>
                      )}
                  </View>
                )}
                {headerTheme === MODAL_HEADER && (
                  <View style={css('root')}>
                    <Link to={Routes.HOME} style={css('logo', 'logoMedium')}>
                      <View>
                        <LogoShort
                          width={54}
                          height={12}
                          color={theme === THEME_WHITE ? Colors.BLACK : Colors.WHITE}
                        />
                      </View>
                    </Link>
                    {!!headerOptions &&
                      !!headerOptions.title && (
                        <Text style={css('title')}>{headerOptions.title}</Text>
                      )}
                    <TouchableOpacity onPress={this._handleBack} style={css('close')}>
                      <Icon
                        icon={Icons.CLOSE}
                        style={css([
                          'closeIcon',
                          theme === THEME_WHITE && 'iconBlack',
                          theme === THEME_BLACK && 'iconWhite',
                        ])}
                      />
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            )}
          </ThemeContext.Consumer>
        )}
      </GlobalsContext.Consumer>
    );
  }

  styles() {
    return {
      root: {
        backgroundColor: Colors.BLACK,
        paddingTop: 32,
        paddingBottom: 16,
        paddingHorizontal: MARGIN_HORIZONTAL,
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'center',
        height: 72,
      },

      noHeader: {
        backgroundColor: Colors.BLACK,
        height: 24,
      },

      logo: {
        alignItems: 'center',
        display: 'none',
      },

      logoMedium: {
        display: 'flex',
      },

      menu: {
        fontSize: 45,
        marginLeft: -8,
      },

      profile: {
        fontSize: 45,
      },

      close: {
        position: 'absolute',
        right: MARGIN_HORIZONTAL,
        top: 30,
      },

      closeIcon: {
        fontSize: 30,
      },

      iconBlack: {
        color: Colors.BLACK,
      },

      iconWhite: {
        color: Colors.WHITE,
      },

      title: {
        ...TextStyles.h6,
        color: Colors.WHITE,
      },
    };
  }
}

export default Header;
