/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ThemeProvider, { ThemeContext } from 'components/ThemeProvider';
import GlobalsProvider, { GlobalsContext } from 'components/GlobalsProvider';
import { FULL_HEADER, MODAL_HEADER, NO_HEADER } from 'constants/Header';
import { BriefingRoutes, ChatRoutes } from 'constants/Routes';
import { THEME_BLACK, THEME_WHITE } from 'constants/Themes';
import { noop } from 'lodash';
import { Navigation } from 'react-native-navigation';
import withLocalization from 'utils/withLocalization';

const defaultTheme = THEME_BLACK;
const defaultHeader = NO_HEADER;

const mapRoutesToTheme = {
  [BriefingRoutes.BRIEFING]: THEME_WHITE,
};

const mapRoutesToHeader = t => ({
  [BriefingRoutes.SUBCATEGORY]: {
    theme: MODAL_HEADER,
  },
  [ChatRoutes.MESSAGES]: {
    theme: FULL_HEADER,
    options: {
      title: t('chat:MESSAGES'),
    },
  },
});

class ThemeSwitcher extends Component {
  constructor(props) {
    super(props);
    Navigation.events().bindComponent(this);
  }

  _changeTheme = () => {
    const { changeTheme, componentId } = this.props;

    const themeForRoute = mapRoutesToTheme[componentId] || defaultTheme;

    changeTheme(themeForRoute);
  };

  _changeHeader = () => {
    const { changeHeader, componentId, t } = this.props;

    const headerForRoute = mapRoutesToHeader(t)[componentId] || defaultHeader;

    changeHeader(headerForRoute);
  };

  componentDidAppear() {
    this._changeTheme();
    this._changeHeader();
  }

  componentDidMount() {
    this._changeTheme();
    this._changeHeader();
  }

  render() {
    const { children } = this.props;

    return children;
  }
}

ThemeSwitcher.propTypes = {
  componentId: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,

  // functions
  changeHeader: PropTypes.func,
  changeTheme: PropTypes.func,
  t: PropTypes.func.isRequired,
};

ThemeSwitcher.defaultProps = {
  // functions
  changeHeader: noop,
  changeTheme: noop,
};

const Theme = props => (
  <ThemeProvider>
    <ThemeContext.Consumer>
      {themeProps => (
        <GlobalsProvider>
          <GlobalsContext.Consumer>
            {headerProps => <ThemeSwitcher {...themeProps} {...headerProps} {...props} />}
          </GlobalsContext.Consumer>
        </GlobalsProvider>
      )}
    </ThemeContext.Consumer>
  </ThemeProvider>
);

Theme.propTypes = {
  // functions
  children: PropTypes.node.isRequired,
  t: PropTypes.func.isRequired,
};

export default withLocalization(Theme);
