import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes } from 'react-native';

import { PureComponent } from 'components/BaseComponent';

import { MARGIN_HORIZONTAL } from 'constants/Layout';

import { Colors } from 'theme/Colors';

class ControlsBar extends PureComponent {
  static propTypes = {
    children: PropTypes.node,
    style: ViewPropTypes.style,
  };

  static defaultProps = {
    children: null,
    style: null,
  };

  template(css) {
    const { children, style } = this.props;

    return (
      <View style={css('root')}>
        <View style={css(['bar', style], 'hidden')}>
          <View style={css('background')} />
          {children}
        </View>
      </View>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
        // position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
      },

      bar: {
        height: 50,
        flexDirection: 'row',
        alignItems: 'center',
      },

      background: {
        backgroundColor: Colors.BLACK,
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: -MARGIN_HORIZONTAL,
        right: -MARGIN_HORIZONTAL,
      },

      hidden: { display: 'none' },
    };
  }
}

export default ControlsBar;
