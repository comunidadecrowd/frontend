import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes, Text } from 'react-native';
import { getCountryCallingCode } from 'libphonenumber-js';
import { noop } from 'lodash';
import RNPickerSelect from 'react-native-picker-select';

import { Component } from 'components/BaseComponent';
import Emoji from 'components/Emoji';

import Countries from 'constants/Countries';

import withLocalization from 'utils/withLocalization';

class SelectCountry extends Component {
  static propTypes = {
    flagStyle: Emoji.propTypes.style,
    initialValue: PropTypes.string,
    labelStyle: Text.propTypes.style,
    style: ViewPropTypes.style,

    // functions
    onChange: PropTypes.func,
    t: PropTypes.func.isRequired,
  };

  static defaultProps = {
    flagStyle: null,
    initialValue: 'BR',
    labelStyle: null,
    style: null,

    // functions
    onChange: noop,
  };

  state = {
    value: this.props.initialValue,
    items: Countries.map(item => ({
      label: `${this.props.t(`countries:${item}`)} (+${getCountryCallingCode(item)})`,
      value: item,
      phonePrefix: getCountryCallingCode(item),
    })).sort((a, b) => {
      // Sort by name
      if (a.value === this.props.initialValue) return -1;
      if (b.value === this.props.initialValue) return 1;
      return a.label.localeCompare(b.label);
    }),
  };

  _handleChange = value => {
    const { onChange } = this.props;

    if (value) {
      this.setState({ value });

      onChange && onChange(value);
    }
  };

  template(css) {
    const { style, flagStyle, labelStyle } = this.props;
    const { value, items } = this.state;
    const selectedItem = items.filter(item => item.value === value)[0];

    return (
      <View style={css(['root', style])}>
        <Emoji emoji={`flag-${value.toLowerCase()}`} style={flagStyle} />
        <RNPickerSelect value={value} items={items} onValueChange={this._handleChange}>
          <Text style={css(['label', labelStyle])}>
            (+
            {selectedItem.phonePrefix})
          </Text>
        </RNPickerSelect>
      </View>
    );
  }

  styles() {
    return {
      root: {
        flexDirection: 'row',
      },

      label: {
        borderWidth: 1,
        color: 'white',
        marginLeft: 4,
      },
    };
  }
}

export default withLocalization(SelectCountry);
