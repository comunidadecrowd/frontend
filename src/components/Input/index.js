import React from 'react';
import PropTypes from 'prop-types';
import { Platform, View, TextInput, Animated, ViewPropTypes, Text } from 'react-native';
import { noop } from 'lodash';

import { Component } from 'components/BaseComponent';

import CustomPropTypes from 'constants/CustomPropTypes';

import { Colors, Duration, TextStyles } from 'theme';

import withAnimation from 'utils/withAnimation';

const IS_WEB = Platform.OS === 'web';

class Input extends Component {
  
  static propTypes = {
    component: PropTypes.any,
    error: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    focusing: CustomPropTypes.animation.isRequired,
    id: PropTypes.string,
    initialValue: PropTypes.string,
    label: PropTypes.string,
    noDecoration: PropTypes.bool,
    shouldKeepFocusWhenHasValue: PropTypes.bool,
    style: ViewPropTypes.style,
    styleInput: TextInput.propTypes.style,
    styleLabel: Animated.Text.propTypes.style,
    styleLabelHelper: ViewPropTypes.style,
    styleWrapper: ViewPropTypes.style,
    type: PropTypes.oneOf(['text', 'password', 'tel', 'email', 'money', 'only-numbers']),

    // functions
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onChangeText: PropTypes.func,
    onFocus: PropTypes.func,
    startFocusingTiming: PropTypes.func.isRequired,
    setRef: PropTypes.func,
  };

  static defaultProps = {
    component: null,
    error: '',
    id: null,
    initialValue: '',
    label: '',
    noDecoration: false,
    shouldKeepFocusWhenHasValue: false,
    style: null,
    styleInput: null,
    styleLabel: null,
    styleLabelHelper: null,
    styleWrapper: null,
    type: 'text',

    // functions
    onBlur: noop,
    onChange: noop,
    onChangeText: noop,
    onFocus: noop,
    setRef: noop,
  };

  _labelArrow = null;
  inputComponent = Animated.createAnimatedComponent(this.props.component || TextInput);
  
  state = {
    value: this.props.initialValue,
    focusingDelta: 0,
    rootWidth: 0,
    labelHelperWidth: 0,
  };

  _handleLayout = event => {
    const { width } = event.nativeEvent.layout;

    this.setState({ rootWidth: width - 1 });
  };

  _handleLabelHelperLayout = event => {
    const { width } = event.nativeEvent.layout;

    this.setState({ labelHelperWidth: width });
  };

  _handleFocus = event => {
    const { startFocusingTiming, onFocus } = this.props;

    startFocusingTiming({
      callback: cb => {
        this.setState({ focusingDelta: cb.value });
      },
      toValue: 1,
      duration: Duration.MEDIUM,
    });

    onFocus && onFocus(event);
  };

  _handleBlur = event => {
    const { startFocusingTiming, shouldKeepFocusWhenHasValue, onBlur } = this.props;
    const { value } = this.state;

    // Only remove focus style when there's no value
    if (value.replace(/\n/g, '') !== '' && shouldKeepFocusWhenHasValue) return;

    startFocusingTiming({
      callback: cb => {
        this.setState({ focusingDelta: cb.value });
      },
      toValue: 0,
      duration: Duration.MEDIUM,
    });

    onBlur && onBlur(event);
  };

  _handleChange = eventOrValue => {
    const { onChange } = this.props;
    const value = eventOrValue.target ? eventOrValue.target.value : eventOrValue;

    this.setState({ value });

    onChange && onChange(value);
  };

  _handleChangeNative = value => {
    const { onChange } = this.props;

    this.setState({ value });

    onChange && onChange(value);
  };

  template(css) {
    const {
      label,
      type,
      id,
      focusing,
      style,
      styleWrapper,
      styleLabel,
      styleInput,
      styleLabelHelper,
      setRef,
      onBlur,
      onFocus,
      onChange,
      onChangeText,
      noDecoration,
      error,
      ...props
    } = this.props;
    const { value, rootWidth, labelHelperWidth } = this.state;
    const inputRestProps = Object.keys(props)
      .filter(item => Object.keys(TextInput.propTypes).includes(item))
      .reduce(
        (accumulator, current) => ({
          ...accumulator,
          [current]: props[current],
        }),
        {}
      );

    const focusOffset = labelHelperWidth * 1.5;

    const AnimatedTextInput = this.inputComponent;

    return (
      <View style={css(['root', style])} onLayout={this._handleLayout}>
        <View style={css(['wrapper', !!error && 'wrapperError', styleWrapper])}>
          {!noDecoration && (
            <View style={css('labelHelperWrapper')} onLayout={this._handleLabelHelperLayout}>
              <Animated.View
                style={css([
                  'labelHelper',
                  {
                    opacity: focusing,
                    transform: [
                      {
                        translateX: focusing.interpolate({
                          inputRange: [0, 1],
                          outputRange: [-focusOffset / 2, 0],
                        }),
                      },
                    ],
                  },
                  styleLabelHelper,
                ])}
              />
            </View>
          )}
          {!value && (
            <Animated.Text
              accessibilityRole="text"
              htmlFor={id}
              style={css([
                'label',
                !noDecoration && {
                  paddingLeft: focusing.interpolate({
                    inputRange: [0, 1],
                    outputRange: [8, focusOffset],
                  }),
                },
                styleLabel,
              ])}
            >
              {label}
            </Animated.Text>
          )}
          <AnimatedTextInput
            style={css([
              'input',
              !!error && 'inputError',
              styleInput,
              !noDecoration && {
                paddingLeft: focusing.interpolate({
                  inputRange: [0, 1],
                  outputRange: [8, focusOffset],
                }),
              },
            ])}
            type={type}
            id={id}
            value={value}
            onBlur={this._handleBlur}
            onFocus={this._handleFocus}
            onChange={IS_WEB ? this._handleChange : null}
            onChangeText={!IS_WEB ? this._handleChangeNative : null}
            secureTextEntry={type === 'password'}
            ref={!!setRef ? setRef : null}
            {...inputRestProps}
          />
          <Animated.View
            style={css([
              'border',
              {
                width: focusing.interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, rootWidth],
                }),
              },
            ])}
          />
        </View>
        {!!error && typeof error === 'string' && <Text style={css('error')}>{error}</Text>}
      </View>
    );
  }

  styles() {
    return {
      wrapper: {
        backgroundColor: Colors.BLACK,
        borderBottomColor: Colors.GRAY,
        borderBottomWidth: 1,
      },

      wrapperError: {
        borderBottomColor: Colors.YELLOW,
      },

      labelHelperWrapper: { position: 'absolute' },

      labelHelper: {
        borderLeftColor: Colors.BLUE,
        borderLeftWidth: 10,
        borderTopColor: Colors.TRANSPARENT,
        borderTopWidth: 5,
        borderBottomColor: Colors.TRANSPARENT,
        borderBottomWidth: 5,
        top: 13,
        left: 0,
        zIndex: 1,
      },

      label: {
        ...TextStyles.input,
        // color: Colors.GRAY, // TODO:  Bruno analisar para o placeholder (cor cinza quando sem valor) funcionar corretamente
        color: '#fff',
        position: 'absolute',
        top: 8,

        ...Platform.select({
          web: {
            cursor: 'pointer',
          },
        }),
      },

      input: {
        ...TextStyles.input,
        color: Colors.WHITE,
        paddingTop: 8,
        paddingBottom: 16,
        position: 'relative',
        borderWidth: 0,

        ...Platform.select({
          web: {
            outline: 'none',
            boxShadow: 'none',
          },
        }),
      },

      inputError: { color: Colors.YELLOW },

      border: {
        position: 'absolute',
        bottom: -1,
        left: 0,
        borderBottomColor: Colors.BLUE,
        borderBottomWidth: 1,
      },

      error: {
        ...TextStyles.small,
        color: Colors.YELLOW,
        marginTop: 8,
      },
    };
  }

}

export default withAnimation('focusing', 0)(Input);
