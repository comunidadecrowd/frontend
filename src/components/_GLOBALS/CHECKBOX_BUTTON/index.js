import React, { PureComponent } from 'react';

import {
  _Container,
  _CheckboxButton,
  _CheckboxLabel,
  _Checkbox
} from './styles';

class CHECKBOX_BUTTON extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { active, label, backtickColor, onPressFn } = this.props;

    return (
      <_Container>
        <_CheckboxButton
          isLast={false}
          onPress={onPressFn}
          activeOpacity={1}
        >
          <_Checkbox active={active} backtickColor={backtickColor} />
          <_CheckboxLabel active={active} content={label} fontSize={14} />
        </_CheckboxButton>
      </_Container>
    );
  }
}

export default CHECKBOX_BUTTON;
