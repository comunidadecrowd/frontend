import React from 'react';
import styled from 'styled-components/native';
import { TEXT } from 'GLOBALS/TEXT';
import { Colors } from 'theme';
import IconBacktick from 'components/Icon/IconBacktick';

export const _Container = styled.View`
  padding: 0 0 20px;
`;

export const _CheckboxButton = styled.TouchableOpacity`
  flex-direction: row;
  /*height: 35px;*/
  /*margin: ${({ isLast }) => !isLast ? '0 0 10px' : '0' };*/
  align-items: center;
`;

export const _CheckboxContainer = styled.View`
  width: 21px;
  height: 21px;
  border-width: 1px;
  border-color: ${({ active, backtickColor }) => active ? backtickColor ? backtickColor : Colors.TECHNOLOGY : Colors.GRAY_DEFAULT};
  align-items: center;
  justify-content: center;
`;

export const _CheckboxIcon = styled(IconBacktick)`
`;

export const _Checkbox = (props) => {

  const { active, backtickColor } = props;

  return (
    <_CheckboxContainer active={active} backtickColor={backtickColor}>
      {active && <_CheckboxIcon
        width={10}
        height={8}
        color={backtickColor ? backtickColor : Colors.WHITE}
      />}
    </_CheckboxContainer>
  )
}

export const _CheckboxLabel = styled(TEXT)`
  color: #9B9B9B;
  margin-left: 15px;
`;
