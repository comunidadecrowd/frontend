import React, { Fragment, PureComponent } from 'react';
import { Platform } from 'react-native';
import { Animated } from 'react-native';

import { _ButtonContainer, _ButtonLabel, _ButtonArrowLeft, _ButtonArrowRight } from './styles';

const isiOS = Platform.OS === 'ios';

class BUTTON extends PureComponent {
  
  constructor(props) {
    super(props);
    this.state = {
      animations: {
        buttonLabel: new Animated.Value(0),
      }
    }
  }

  onPressCallback = () => {
    const { onPressFn, noPadding } = this.props;
    const { animations: { buttonLabel } } = this.state;

    Animated.timing(buttonLabel, {
      toValue: 1,
      duration: 50,
    }).start(() => {
      setTimeout(() => onPressFn(), 50);
      setTimeout(() => {
        Animated.timing(buttonLabel, {
          toValue: 0,
          duration: 250,
        }).start();
      }, 1000);
    });
  }

  render() {
    
    const { content, color, disabled, isAndroid, noPadding, normalText, verticalAlign, bg, needFlex } = this.props;
    const { animations: { buttonLabel } } = this.state;
    
    const animatedInterpolate = buttonLabel.interpolate({
      inputRange: [0, 1],
      outputRange: !noPadding ? [10, 0] : [0, -5]
    });
    
    const styleLabelAnimate = {
      marginHorizontal: 10
    };
    
    const styleLabelAnimated = {
      marginHorizontal: animatedInterpolate
    };
    
    return (
      <_ButtonContainer
        activeOpacity={isAndroid ? .5 : 1 }
        onPress={!disabled ? this.onPressCallback : () => false}
        verticalAlign={verticalAlign}
        bg={bg}
        needFlex={needFlex}
      >
      {
        isiOS ? (
          <Fragment>
            <_ButtonArrowLeft color={color ? color : undefined} disabled={disabled} />
            <Animated.View style={[styleLabelAnimate, styleLabelAnimated]}>
              <_ButtonLabel {...this.props}>{normalText ? content : content.toLowerCase() }</_ButtonLabel>
            </Animated.View>
            <_ButtonArrowRight color={color ? color : undefined} disabled={disabled} />
          </Fragment>
        ) : (
          // {/* // FIX:  Bruno -> Ajustar setas para Android */}
          // {/* <_ButtonArrowLeft color={color ? color : undefined} disabled={disabled} /> */}
          // {/* <Animated.View style={[styleLabelAnimate, styleLabelAnimated]}> */}
            <_ButtonLabel {...this.props} lineHeight={36}>{normalText ? content : content.toLowerCase() }</_ButtonLabel>
          // {/* </Animated.View> */}
          // {/* <_ButtonArrowRight color={color ? color : undefined} disabled={disabled} /> */}
        )
      }
      </_ButtonContainer>
    );
  }
}

export default BUTTON;
