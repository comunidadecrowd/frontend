import {Platform} from 'react-native';
import styled from 'styled-components/native';
import { FontRatioNormalize } from 'theme';
import { Colors } from 'theme/Colors';
import { isSmallIOS } from 'utils/Helpers';

const isiOS = Platform.OS === 'ios';

export const _ButtonContainer = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: ${({verticalAlign}) => verticalAlign ? '0' : '15px 0' };
  background: ${({bg}) => bg ? bg : 'rgba(0,0,0,0)'};
  ${({ needFlex }) => needFlex ? (() => (
    `flex: 1`
  )) : null };
`;

export const _ButtonLabel = styled.Text`
  color: ${({ color, disabled }) => disabled ? Colors.DISABLED : color ? color : Colors.WHITE};
  font-size: ${({ fontSize }) => fontSize ? FontRatioNormalize(fontSize) : FontRatioNormalize(22)};
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  ${({ lineHeight }) => lineHeight ? (() => (
    `line-height: ${lineHeight}`
  )) : null };
  top: ${isSmallIOS ? '1px' : '0'};
`;

export const _ButtonArrowLeft = styled.View`
  top: 2px;
  border: 5px solid ${({ color, disabled }) => disabled ? Colors.DISABLED : color ? color : Colors.WHITE};
  border-left-color: ${({ color, disabled }) => disabled ? Colors.DISABLED : color ? color : Colors.WHITE};
  border-right-color: transparent;
  border-top-color: transparent;
  border-bottom-color: transparent;
  border-top-width: 5px;
  border-left-width: 10px;
  border-bottom-width: 5px;
`;

export const _ButtonArrowRight = styled.View`
  top: 2px;
  border: 5px solid ${({ color, disabled }) => disabled ? Colors.DISABLED : color ? color : Colors.WHITE};
  border-left-color: transparent;
  border-right-color: ${({ color, disabled }) => disabled ? Colors.DISABLED : color ? color : Colors.WHITE};
  border-top-color: transparent;
  border-bottom-color: transparent;
  border-top-width: 5px;
  border-right-width: 10px;
  border-bottom-width: 5px;
`;
