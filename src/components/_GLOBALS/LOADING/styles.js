import styled from 'styled-components/native';

export const _Container = styled.View`
  width: ${({full}) => full ? '100%' : 'auto' };
  height: ${({full}) => full ? '100%' : 'auto' };
  align-items: ${({full}) => full ? 'center' : 'flex-start' };
  justify-content: ${({full}) => full ? 'center' : 'flex-start'};
  /* background: green; */
`;