import React, { Component } from 'react';

import loadingAnimation from 'assets/animations/icons/loading.json';
import loadingAnimationWhite from 'assets/animations/icons/loading_white.json';

import Lottie from 'components/Lottie';
import { TEXT } from 'GLOBALS/TEXT';

import {
  _Container
} from './styles';

class LOADING extends Component {

  componentDidMount() {
    const { loadingStatus } = this.props;
    loadingStatus && loadingStatus === 'onPause' ? this.animation.play(0,0) : this.animation.play();
  }

  componentDidUpdate(prevProps) {

    if(prevProps.loadingStatus !== this.props.loadingStatus) {
      this.props.loadingStatus === 'onPause' ? this.animation.play(0,0) : this.animation.play();
    }
  }

  render() {

    const { full, height, style, isWhite, message, messageColor, loadingStatus } = this.props;

    return (
      <_Container full={full} style={style}>
        <Lottie
          ref={animation => this.animation = animation}
          style={{ height: height ? height : 52 }}
          source={isWhite ? loadingAnimationWhite : loadingAnimation}
          autoplay={loadingStatus && loadingStatus === 'onPause' ? false : true}
          loop={loadingStatus && loadingStatus === 'onPause' ? false : true}
        />
        {
          message &&
          <TEXT
            euclid
            content={message}
            color={messageColor}
            style={{ marginTop: 10 }}
          />
        }
      </_Container>
    )
  }
}

export default LOADING