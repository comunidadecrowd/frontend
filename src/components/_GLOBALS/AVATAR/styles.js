import styled from 'styled-components/native';

export const _Container = styled.View`
  width: ${({size}) => size === 'big' ? 67 : 52};
  height: ${({size}) => size === 'big' ? 67 : 52};
  border: 1px solid ${({ color }) => color ? color : '#ED4B68'};
  border-radius: 52;
  overflow: hidden;
`;

export const _Img = styled.Image`
  width: 100%;
  height: 100%;
`;