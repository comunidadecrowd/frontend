import React from 'react';
import { _Container, _Img } from './styles';

export const AVATAR = ({ color, uri, size }) => {
  return (
    <_Container size={size ? size : 'normal'} color={color}>
      <_Img source={uri} />
    </_Container>
  )
};