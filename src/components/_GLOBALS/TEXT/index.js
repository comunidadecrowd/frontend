import React, { Fragment } from 'react';
import { _TEXT, _WrapperForLoading } from './styles';

import LOADING from 'GLOBALS/LOADING';

export const TEXT = ({
  withLoading,
  loadingHeight,
  align,
  bold,
  euclid,
  content,
  fontSize,
  lineHeight,
  color,
  paddingTop,
  paddingRight,
  paddingBottom,
  paddingLeft,
  style,
  loadingStyles,
  loadingAlign,
  loadingStatus
}) => {
  return (
    withLoading
      ? (
        <_WrapperForLoading>
          {
            loadingAlign && loadingAlign === 'left'
              ? (
                <Fragment>
                  <LOADING
                    loadingStatus={loadingStatus}
                    isWhite
                    height={loadingHeight ? loadingHeight : 25}
                    style={loadingStyles ? loadingStyles : {}}
                  />
                  <_TEXT
                    align={align}
                    bold={bold}
                    euclid={euclid}
                    color={color}
                    fontSize={fontSize}
                    lineHeight={lineHeight}
                    paddingTop={paddingTop}
                    paddingRight={paddingRight}
                    paddingBottom={paddingBottom}
                    paddingLeft={paddingLeft}
                    style={style}>
                    {content}
                  </_TEXT>
                </Fragment>
              ) : (
                <Fragment>
                  <_TEXT
                    align={align}
                    bold={bold}
                    euclid={euclid}
                    color={color}
                    fontSize={fontSize}
                    lineHeight={lineHeight}
                    paddingTop={paddingTop}
                    paddingRight={paddingRight}
                    paddingBottom={paddingBottom}
                    paddingLeft={paddingLeft}
                    style={style}>
                    {content}
                  </_TEXT>
                  <LOADING
                    isWhite
                    loadingStatus={loadingStatus}
                    height={loadingHeight ? loadingHeight : 25}
                    style={loadingStyles ? loadingStyles : { position: 'absolute', right: -30, top: 2 }}
                  />
                </Fragment>
              )
          }
        </_WrapperForLoading>
      ) : (
        <_TEXT
          align={align}
          bold={bold}
          euclid={euclid}
          color={color}
          fontSize={fontSize}
          lineHeight={lineHeight}
          paddingTop={paddingTop}
          paddingRight={paddingRight}
          paddingBottom={paddingBottom}
          paddingLeft={paddingLeft}
          style={style}>
          {content}
        </_TEXT>
      )
  );
}