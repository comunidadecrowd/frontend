import {Platform} from 'react-native';
import styled from 'styled-components/native';
import { FontRatioNormalize } from 'theme';

const isiOS = Platform.OS === 'ios';

export const _TEXT = styled.Text`
  color: ${({ color }) => color ? color : '#FFF'};
  font-size: ${({ fontSize }) => fontSize ? FontRatioNormalize(fontSize) : FontRatioNormalize(18)};
  padding-top: ${({ paddingTop }) => paddingTop ? paddingTop : 'auto'};
  padding-right: ${({ paddingRight }) => paddingRight ? paddingRight : 'auto'};
  padding-bottom: ${({ paddingTop }) => paddingTop ? paddingTop : 'auto'};
  padding-left: ${({ paddingLeft }) => paddingLeft ? paddingLeft : 'auto'};
  font-family: ${({ euclid, bold }) => (euclid && !bold) ? (isiOS ? 'Euclid Flex' : 'euclid-flex-medium') : (euclid && bold || bold) ? (isiOS ? 'InterUI-Bold' : 'inter-ui-bold' ) : (isiOS ? 'Inter UI' : 'inter-ui-regular' )};
  text-align: ${({ align }) => align ? align : 'left'};
  ${({ lineHeight }) => lineHeight ? (() => (
    `line-height: ${lineHeight}`
  )) : null}
`;

export const _WrapperForLoading = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;