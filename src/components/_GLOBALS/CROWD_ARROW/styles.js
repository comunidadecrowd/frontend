import styled from 'styled-components/native';

export const _ArrowRight = styled.View`
  top: 2px;
  width: ${({ size }) => size ? size - 3 : 5}px;
  height: ${({ size }) => size ? size - 3 : 5}px;
  border: ${({size}) => size ? size : 5}px solid ${({color}) => color ? color : '#FFF'};
  border-left-color: ${({color}) => color ? color : '#FFF'};
  border-right-color: transparent;
  border-top-color: transparent;
  border-bottom-color: transparent;
  border-top-width: ${({size}) => size ? size : 5}px;
  border-left-width: ${({size}) => size ? size * 2 : 10}px;
  border-bottom-width: ${({size}) => size ? size : 5}px;
`;

export const _ArrowLeft = styled.View`
  top: 2px;
  width: ${({ size }) => size ? size - 3 : 5}px;
  height: ${({ size }) => size ? size - 3 : 5}px;
  border: ${({size}) => size ? size : 5}px solid ${({color}) => color ? color : '#FFF'};
  border-left-color: transparent;
  border-right-color: ${({color}) => color ? color : '#FFF'};
  border-top-color: transparent;
  border-bottom-color: transparent;
  border-top-width: ${({size}) => size ? size : 5}px;
  border-right-width: ${({size}) => size ? size * 2 : 10}px;
  border-bottom-width: ${({size}) => size ? size : 5}px;
`;

export const _ArrowDown = styled.View`
  top: 2px;
  width: ${({ size }) => size ? size - 3 : 10}px;
  height: ${({ size }) => size ? size - 3 : 10}px;
  border: ${({size}) => size ? size : 5}px solid ${({color}) => color ? color : '#FFF'};
  border-left-color: transparent;
  border-right-color: transparent;
  border-top-color: ${({color}) => color ? color : '#FFF'};
  border-bottom-color: transparent;
  border-top-width: ${({size}) => size ? size * 2 : 10}px;
  border-right-width: ${({size}) => size ? size : 5}px;
  border-bottom-width: ${({size}) => size ? size * 2 : 10}px;
`;