import React, { Component } from 'react';
import { _ArrowLeft, _ArrowRight, _ArrowDown } from './styles';

class Arrow extends Component {
  render () {
    const { orientation, style } = this.props;
    if (!orientation || orientation == 'left')
      return (
        <_ArrowLeft
          {...this.props}
          color={style && style.color ? style.color : undefined}
        />
      );

    if (orientation == 'right')
      return (
        <_ArrowRight
          {...this.props}
          color={style && style.color ? style.color : undefined}
        />
      );

    if (orientation == 'down')
      return (
        <_ArrowDown
          {...this.props}
          color={style && style.color ? style.color : undefined}
        />
      );
  }
}

export default Arrow;