import styled from 'styled-components/native';
import { Dimensions } from 'react-native';
import { TEXT } from 'GLOBALS/TEXT';
import { Colors } from 'theme';
import Icon from 'components/Icon';

const { width, height } = Dimensions.get('window');

export const _HeaderContainer = styled.View`
  flex-direction: row;
  align-content: center;
  justify-content: ${({ withButtonsNavigation }) => withButtonsNavigation ? 'center' : 'flex-start'};
  width: ${(width - 40)}px;
  height: 70px;
  padding-top: ${({ paddingTop }) => paddingTop ? typeof paddingTop !== 'boolean' ? paddingTop : 29 : 0}px;
`;


export const _ButtonLeft = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  align-items: center;
  justify-content: center;
  opacity: ${({ hide }) => hide ? 0 : 1};
`;

export const _IconPrev = styled(Icon)`
  color: ${Colors.WHITE};
  font-size: 34px;
  margin-left: -10px;
`;

export const _WrapperTitle = styled.View`
  padding-top: 5px;
  flex-grow: 2;
`;

export const _TitleHeader = styled(TEXT)`
  opacity: ${({ hide }) => !hide ? 1 : 0};
`;

export const _ButtonRight = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  align-items: center;
  justify-content: center;
  opacity: ${({ hide }) => hide ? 0 : 1};
`;

export const _IconNext = styled(Icon)`
  color: ${Colors.WHITE};
  font-size: 34px;
  margin-right: -10px;
`;
