import React from 'react';
import { Icons } from 'components/Icon';
import LogoShort from 'assets/svgs/LogoShort';

import {
  _HeaderContainer,
  _ButtonLeft,
  _ButtonRight,
  _WrapperTitle,
  _TitleHeader,
  _IconNext,
  _IconPrev,
} from './styles';

export const HEADER = ({
  paddingTop,
  withButtonsNavigation,
  buttonLeft,
  buttonRight,
  iconRight,
  logoShort,
  title,
  showMenu
}) => {
  return (
    <_HeaderContainer paddingTop={paddingTop} withButtonsNavigation={withButtonsNavigation}>
      {!logoShort && <_ButtonLeft onPress={buttonLeft ? buttonLeft : () => false} hide={!buttonLeft}>
        <_IconPrev icon={Icons.CHEVRON_LEFT} />
      </_ButtonLeft>}
      {!title && logoShort ?
        <LogoShort />
        : <_WrapperTitle>
          <_TitleHeader content={title ? title : 'No title'} align={'center'} euclid hide={!title} />
        </_WrapperTitle>
      }
      {!logoShort && <_ButtonRight onPress={buttonRight ? buttonRight : () => false} hide={!buttonRight}>
        <_IconNext icon={!iconRight ? Icons.CHEVRON_RIGHT : iconRight} disabled={false} />
      </_ButtonRight>}
    </_HeaderContainer>
  );
}
