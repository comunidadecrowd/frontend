import React, { PureComponent } from 'react';

import {
  _Container,
  _RadioButton,
  _RadioLabel,
  _Radio
} from './styles';

class RADIO_BUTTON extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      active,
      label,
      labelInactive,
      activeColor,
      inactiveColor,
      alignRight,
      height,
      onPressFn
    } = this.props;

    return (
      <_Container>
        <_RadioButton
          isLast={false}
          onPress={onPressFn}
          activeOpacity={1}
          height={height}
        >
          {!alignRight && <_RadioLabel
            active={active}
            content={active ? label : labelInactive}
            fontSize={14}
          />}
          <_Radio
            active={active}
            activeColor={activeColor}
            inactiveColor={inactiveColor}
          />
          {alignRight && <_RadioLabel
            active={active}
            activeColor={activeColor}
            inactiveColor={inactiveColor}
            content={active ? label : labelInactive}
            fontSize={14}
            alignRight
          />}
        </_RadioButton>
      </_Container>
    );
  }
}

export default RADIO_BUTTON;
