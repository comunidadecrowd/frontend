import React from 'react';
import styled from 'styled-components/native';
import { TEXT } from 'GLOBALS/TEXT';
import { Colors } from 'theme';

export const _Container = styled.View``;

export const _RadioButton = styled.TouchableOpacity`
  flex-direction: row;
  /*height: 35px;*/
  /*margin: ${({ isLast }) => !isLast ? '0 0 10px' : '0' };*/
  height: ${({ height }) => !height ? 'auto' : `${height}px`};
  align-items: center;
`;

export const _RadioContainer = styled.View`
  width: 21px;
  height: 21px;
  border-width: 1px;
  border-radius: 21px;
  border-color: ${({ active, activeColor, inactiveColor }) => active ? activeColor ? activeColor : Colors.SUCCESS : inactiveColor ? inactiveColor : Colors.DISABLED};
  align-items: center;
  justify-content: center;
`;

export const _RadioIcon = styled.View`
  width: 6px;
  height: 6px;
  border-radius: 6px;
  background: ${({ active, activeColor, inactiveColor }) => active ? activeColor ? activeColor : Colors.SUCCESS : inactiveColor ? inactiveColor : Colors.DISABLED};
`;

export const _Radio = (props) => {

  const { active, activeColor, inactiveColor, height } = props;

  return (
    <_RadioContainer
      active={active}
      activeColor={activeColor}
      inactiveColor={inactiveColor}
      height={height}
    >
      <_RadioIcon
        active={active}
        activeColor={activeColor}
        inactiveColor={inactiveColor}
      />
    </_RadioContainer>
  )
}

export const _RadioLabel = styled(TEXT)`
  color: ${({ active, activeColor, inactiveColor }) => active ? activeColor ? activeColor : Colors.SUCCESS : inactiveColor ? inactiveColor : Colors.DISABLED};
  padding: ${({ alignRight }) => !alignRight ? '0 20px 0 0' : '0 0 0 20px'};
`;
