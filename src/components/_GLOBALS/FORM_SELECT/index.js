import React, { PureComponent } from 'react';

import {
  _Container,
  _SelectList,
  _SelectItem,
  _Button,
  _Label,
} from './styles';

class FORM_SELECT extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidUpdate() {
    const { labelProp, inputValue, data } = this.props;

    if (this.selectListRef) this.selectListRef.scrollToIndex({
        animated: true,
        //index: 3
        index: inputValue && (data.findIndex((item) => item[labelProp] === inputValue) !== -1 || data.findIndex((item) => item[labelProp] === inputValue) > 6) ? data.findIndex((item) => item[labelProp] === inputValue) - 7 : 0
      });
  }

  _getItemLayout = (data, index) => {
    return {
      length: 35,
      offset: 35 * index,
      index
    }; 
  };

  _keyExtractor = (item, key) => key.toString();

  _renderItem = ({ item }) => {
    const { valueProp, labelProp, inputValue, handleSelect } = this.props;

    return (
      <_SelectItem isSelected={item[labelProp] === inputValue}>
        <_Button onPress={() => handleSelect(item[labelProp])} activeOpacity={0.6}>
          <_Label content={item[labelProp]} />
        </_Button>
      </_SelectItem>
    ); 
  };

  render() {
    const { data } = this.props;

    return (
      <_Container>
        <_SelectList
          ref={ref => this.selectListRef = ref}
          data={data}
          renderItem={this._renderItem}
          keyExtractor={this._keyExtractor}
          getItemLayout={this._getItemLayout}
        />
      </_Container>
    );
  }
}

export default FORM_SELECT;
