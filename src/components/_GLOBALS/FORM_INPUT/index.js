import React, { Component } from 'react';
import { Animated, Easing } from 'react-native';
import withLocalization from 'utils/withLocalization';

import CROWD_ARROW from 'GLOBALS/CROWD_ARROW';

import {
  _Container,
  _InputsWrapper,
  _FormInputs,
	_FormInputMoney
} from './styles';

const CROWD_ARROWAnimated = Animated.createAnimatedComponent(CROWD_ARROW);
const _InputsWrapperAnimated = Animated.createAnimatedComponent(_InputsWrapper);

class FORM_INPUT extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.animations = {
      input: new Animated.Value(0),
      arrow: new Animated.Value(0),
    }

  }

  handlerInput = (context, value = null) => {

    const
      { onChange } = this.props,
      { input, arrow } = this.animations,
      easing = Easing.in(),
      duration = 300;


    if (context === 'change') {
      onChange( value );
    }

    if(context === 'blur') {

      Animated.timing(input, {
        toValue: value && value.trim().length < 3 ? 2 : 3,
        easing,
        duration
      }).start();

      Animated.timing(arrow, {
        toValue: 0,
        easing,
        duration
      }).start();
    
    }
    
    if(context === 'focus') {

      Animated.timing(input, {
        toValue: 1,
        easing,
        duration
      }).start();

      Animated.timing(arrow, {
        toValue: 1,
        easing,
        duration
      }).start();

    }

  }

  render() {
    const
      { t, inputValue, placeholder, type } = this.props,
      { input, arrow } = this.animations,
      inputsWrapperInterpolate = {
        borderBottomColor: input.interpolate({
          inputRange: [0, 1, 2, 3],
          outputRange: ['#444', '#84C9F2', '#FFC80B', '#BCDEA1']
        }),
        left: input.interpolate({
          inputRange: [0, 1, 2, 3],
          outputRange: [-15, 0, -15, -15]
        })
      },
      arrowInterpolate = arrow.interpolate({
        inputRange: [0, 1],
        outputRange: [-10, 0]
      });

    return (
      <_Container>
        <_InputsWrapperAnimated
          style={inputsWrapperInterpolate}
        >
          <CROWD_ARROWAnimated
            orientation={"right"}
            size={5}
            style={{ top: 0, color: "#84C9F2", left: arrowInterpolate }}
          />
          { type === 'money' ?
						<_FormInputMoney
							type={'money'}
							placeholder={placeholder}
							placeholderTextColor={'#9B9B9B'}
							value={inputValue}
							onFocus={() => this.handlerInput('focus')}
							onBlur={() => this.handlerInput('blur')}
							onChangeText={(value) => this.handlerInput('change', value)}
							options={{
								unit: 'R$ '
							}}
						/>
					: <_FormInputs
							placeholder={placeholder}
							placeholderTextColor={'#9B9B9B'}
							value={inputValue}
							keyboardType={'default'}
							onFocus={() => this.handlerInput('focus')}
							onBlur={() => this.handlerInput('blur')}
							onChangeText={(value) => this.handlerInput('change', value)}
						/>}
        </_InputsWrapperAnimated>
      </_Container>
    );
  }
}

export default withLocalization(FORM_INPUT);
