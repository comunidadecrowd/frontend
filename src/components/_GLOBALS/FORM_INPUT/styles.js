import styled from 'styled-components/native';
import { Dimensions } from 'react-native';
import { TextInputMask } from 'react-native-masked-text';

const { width, height } = Dimensions.get('window');

export const _Container = styled.View`
  max-width: 100%;
  height: 115px;
  justify-content: space-between;
  overflow: hidden;
`;

export const _FormWrapper = styled.View`
  width: 100%;
  height: 115px;
  justify-content: space-between;
`;

export const _InputsWrapper = styled.View`
  width: ${width - 80 }px;
  /*width: 100%;*/
  height: 50px;
  flex-direction: row;
  align-items: center;
  padding-right: 20px;
  border-bottom-width: 1px;
`;

export const _FormInputMoney = styled(TextInputMask)`
  width: 100%;
  height: 100%;
  padding: 0 5px;
  font-size: 18px;
  font-family: 'Inter UI';
  color: #fff;
`;

export const _FormInputs = styled.TextInput`
  width: 100%;
  height: 100%;
  padding: 0 5px;
  font-size: 18px;
  font-family: 'Inter UI';
  color: #fff;
`;
