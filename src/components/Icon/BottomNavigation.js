/* eslint-disable */
import React from 'react';
import Svg, { G, Path, Polyline, Polygon } from 'react-native-svg';

export const IconMessages = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-2.000000, -1.000000)">
          <G transform="translate(2.000000, 1.000000)">
            <Path d="M0.5,0.5 L0.5,10.5 L14.5,10.5 L14.5,0.5 L0.5,0.5 Z" stroke={props.color} />
            <Polyline
              stroke={props.color}
              points="11 12.6276247 11 15.510498 23.5474854 15.510498 23.5474854 7 15.9993286 7"
            />
            <Polygon
              fill={props.color}
              transform="translate(20.500000, 17.000000) rotate(-90.000000) translate(-20.500000, -17.000000) "
              points="22.5 14.5 18.5 16.9647261 22.5 19.5 22.5 16.8920898"
            />
            <Polygon
              fill={props.color}
              transform="translate(3.500000, 12.000000) rotate(-90.000000) translate(-3.500000, -12.000000) "
              points="5.5 9.5 1.5 11.9647261 5.5 14.5 5.5 11.8920898"
            />
          </G>
        </G>
      </G>
    </Svg>
  );
};

export const IconBriefing = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-3.000000, -1.000000)">
          <G transform="translate(3.000000, 1.000000)">
            <G transform="translate(0.000000, 1.000000)" stroke={props.color}>
              <Polyline points="3 7 6.10864258 7 10 7" />
              <Path d="M3,10 L7,10" />
              <Polyline points="13.5349121 0.989501953 13.5349121 13.2873253 10.424576 16.5048828 0.555175781 16.5048828 0.555175781 1.57519531 6.02026367 1.57519531" />
            </G>
            <Polygon
              fill={props.color}
              transform="translate(9.500000, 2.000000) rotate(-270.000000) translate(-9.500000, -2.000000) "
              points="11.5 -0.5 7.5 1.96472606 11.5 4.5"
            />
          </G>
        </G>
      </G>
    </Svg>
  );
};

export const IconMainData = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(0.000000, -2.000000)">
          <G transform="translate(0.000000, 2.000000)">
            <Path d="M0.5,2.5 L16.5,2.5" stroke={props.color} />
            <Path d="M0.5,15.5 L16.5,15.5" stroke={props.color} />
            <Polygon
              fill="#000000"
              transform="translate(13.000000, 2.500000) rotate(-180.000000) translate(-13.000000, -2.500000) "
              points="15 0 11 2.46472606 15 5"
            />
            <Polygon
              fill={props.color}
              transform="translate(13.000000, 2.500000) rotate(-180.000000) translate(-13.000000, -2.500000) "
              points="15 0 11 2.46472606 15 5"
            />
            <Polygon
              fill={props.color}
              transform="translate(13.000000, 15.500000) rotate(-180.000000) translate(-13.000000, -15.500000) "
              points="15 13 11 15.4647261 15 18"
            />
            <G transform="translate(8.500000, 9.500000) scale(-1, 1) translate(-8.500000, -9.500000) translate(0.000000, 7.000000)">
              <Path d="M0.5,2.5 L16.5,2.5" stroke={props.color} />
              <Polygon
                fill={props.color}
                transform="translate(13.000000, 2.500000) rotate(-180.000000) translate(-13.000000, -2.500000) "
                points="15 0 11 2.46472606 15 5"
              />
            </G>
          </G>
        </G>
      </G>
    </Svg>
  );
};
