import React from 'react';
import { G, Path, Svg } from 'react-native-svg';

const IconPlus = props => (
  <Svg {...props}>
    <G stroke="none" fill="none" fillRule="evenodd">
      <G transform="translate(-4.000000, -16.000000)" stroke="#FFFFFF">
        <G transform="translate(4.000000, 16.000000)">
          <G>
            <Path d="M9.5,0 L9.5,19" />
            <Path d="M0,9 L19,9" />
          </G>
        </G>
      </G>
    </G>
  </Svg>
);

export default IconPlus;
