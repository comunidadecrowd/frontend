import React from 'react';
import Svg, { Polygon } from 'react-native-svg';

export const UserBubble = props => (
  <Svg {...props}>
    <Polygon points="0,15 7,7 0,0" fill="#FFFFFF" stroke={props.color ? props.color : '#000000'} strokeWidth="1" />
  </Svg>
);

export const FriendBubble = props => (
  <Svg {...props}>
    <Polygon points="15,15 15,0 3,7" fill="#F2F2F2" stroke="none" />
  </Svg>
);
