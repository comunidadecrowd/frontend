import React from 'react';
import Svg, { G, Path, Polygon } from 'react-native-svg';

export const IconCloseButton = props => {
  return (
    <Svg width={props.width} height={props.height}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-3.000000, -4.000000)" stroke={props.color}>
          <G transform="translate(3.000000, 4.000000)">
            <Path d="M0.5,16.5 L16.5,0.5" />
            <Path d="M16.5,16.5 L0.5,0.5" />
          </G>
        </G>
      </G>
    </Svg>
  );
};

export const IconSearchActive = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-30.000000, -81.000000)" fill={props.color}>
          <Polygon points="30 81 38 86.0010428 30 91" />
        </G>
      </G>
    </Svg>
  );
};
