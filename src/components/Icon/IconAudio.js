import React from 'react';
import { G, Path, Polygon, Svg } from 'react-native-svg';

const IconAudio = props => (
  <Svg {...props}>
    <G stroke="none" fill="none" fillRule="evenodd">
      <G transform="translate(-6.000000, -16.000000)">
        <G transform="translate(6.000000, 18.000000)">
          <G>
            <Path d="M4,16.5 L11,16.5" stroke="#FFFFFF" />
            <Path d="M7.5,10 L7.5,16" stroke="#FFFFFF" />
            <Path
              d="M7.33333333,0 L7.33333333,9.53468938"
              stroke="#FFFFFF"
              strokeWidth="4"
              strokeLinecap="round"
              strokeLinejoin="round"
            />
            <Polygon fill="#FFFFFF" points="0 7 4 5 0 3" />
            <Polygon
              fill="#FFFFFF"
              transform="translate(13.000000, 5.000000) rotate(-180.000000) translate(-13.000000, -5.000000) "
              points="11 7 15 5 11 3"
            />
          </G>
        </G>
      </G>
    </G>
  </Svg>
);

export default IconAudio;
