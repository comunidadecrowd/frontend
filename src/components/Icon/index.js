import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text } from 'react-native';

import Glyphs from 'constants/Icons';

import { iconsfont } from 'theme';

export const Icons = Glyphs;

const Icon = ({ icon, style }) => <Text style={[styles.icon, style]}>{icon}</Text>;

Icon.propTypes = {
  icon: PropTypes.oneOf(Object.values(Icons)).isRequired,
  style: Text.propTypes.style,
};

Icon.defaultProps = {
  style: null,
};

const styles = StyleSheet.create({
  icon: iconsfont,
});

export default Icon;
