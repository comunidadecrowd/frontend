/* eslint-disable */
import React from 'react';
import Svg, { G, Path, Polygon } from 'react-native-svg';

export const IconUserNotFound = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-49.000000, -364.000000)" fill={props.color}>
          <G transform="translate(49.000000, 364.000000)">
            <Path d="M11.2,3.11111111 C11.2,4.82318841 9.71228716,6.22222222 7.59944777,6.22222222 C5.48660838,6.22222222 4,4.82318841 4,3.11111111 C4,1.39903382 5.48660838,0 7.59944777,0 C9.71228716,0 11.2,1.39903382 11.2,3.11111111 Z" />
            <Polygon points="16 21 0 21 7.89522408 9.33333333" />
          </G>
        </G>
      </G>
    </Svg>
  );
};
