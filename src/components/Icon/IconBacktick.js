/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { G, Polyline, Svg } from 'react-native-svg';

const IconBacktick = ({ color, ...props }) => (
  <Svg {...props} viewBox="0 0 10 8" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <G id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <G id="archive" transform="translate(0.000000, 1.000000)" stroke={color}>
        <Polyline id="Path" points="0 2.53141156 3.99894848 6 10 0"></Polyline>
      </G>
    </G>
  </Svg>
);

IconBacktick.propTypes = {
  color: PropTypes.string.isRequired,
};

export default IconBacktick;
