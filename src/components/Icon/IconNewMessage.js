/* eslint-disable */
import React from 'react';
import Svg, { G, Path, Polygon, Rect } from 'react-native-svg';

export const IconNewMessage = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G fill={props.color}>
          <Path d="M610.951,62.122 C610.951,62.122 617.706,30.304 605.921,21.077 L582.869,3.084 C571.113,-6.085 541.824,8.114 541.824,8.114 L523.831,31.166 L592.958,85.174 L610.951,62.122 Z" />
          <Polygon points="253.702 376.86 252.265 449.954 322.858 430.869 583.933 96.7 560.909 78.678 305.239 405.891 279.284 415.405 282.187 387.897 537.857 60.685 514.805 42.663" />
          <Polygon points="0.877 631.755 546.998 631.755 546.998 200.607 518.254 229.35 518.254 603.012 29.62 603.012 29.62 114.377 403.281 114.377 432.024 85.634 0.877 85.634" />
        </G>
      </G>
    </Svg>
  );
};
