/* eslint-disable */
import React from 'react';
import Svg, { G, Path, Polyline, Polygon } from 'react-native-svg';

export const IconBackButton = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-8.000000, -3.000000)" stroke={props.color}>
          <Polyline
            transform="translate(11.964905, 11.918091) rotate(-180.000000) translate(-11.964905, -11.918091) "
            points="8 19.8361816 15.9298096 11.9180908 8 4"
          />
        </G>
      </G>
    </Svg>
  );
};

export const IconCallUser = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-277.000000, -30.000000)" stroke={props.color}>
          <G transform="translate(277.000000, 30.000000)">
            <Path d="M4.7205,0.625 C4.7205,0.625 0.3355,2.88375 0.6405,5.45125 C0.94425,8.06375 2.9405,15.1475 10.8405,19.08875 C13.5755,20.505 16.8755,16.21 16.8755,16.21 L12.92425,12.4025 C12.92425,12.4025 10.8855,14.75 9.973,14.395 C9.973,14.395 7.108,12.66875 4.938,7.93125 C4.503,7.045 7.49925,5.40625 7.49925,5.40625 L4.7205,0.625 Z" />
          </G>
        </G>
      </G>
    </Svg>
  );
};

export const IconOpenMenu = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(0.000000, -1.000000)">
          <G transform="translate(0.000000, 1.000000)">
            <Polygon
              fill={props.color}
              points="24.9864502 0.0491 16.9762 4.99719238 24.9864502 10.0869141"
            />
            <Path d="M0,5 L11.5,5" stroke={props.color} />
            <Path d="M0,11 L16.9711914,11" stroke={props.color} />
            <Path d="M0,17 L16.9711914,17" stroke={props.color} />
          </G>
        </G>
      </G>
    </Svg>
  );
};

export const IconStar = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-145.000000, -305.000000)" fill={props.color}>
          <G transform="translate(145.000000, 305.000000)">
            <Polygon points="11.1099843 6.84614643 18 6.84614643 12.4491564 11.1260731 14.5549922 18 8.95270582 13.7964696 3.37531115 18 5.45791463 11.1260731 0 6.84614643 6.79542731 6.84614643 8.95270582 0" />
          </G>
        </G>
      </G>
    </Svg>
  );
};
