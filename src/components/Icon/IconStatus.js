import React from 'react';
import { Circle, G, Svg } from 'react-native-svg';

import { Colors } from 'theme';

const IconStatus = ({ userStatus, ...props }) => {
  
  const statusColor = userStatus === 'Online' ? '#BCDEA1' : userStatus === 'Offline' ? '#9B9B9B' : Colors.ERROR;

  return (
    <Svg {...props} style={{ marginRight: 5 }}>
      <G stroke="none" fill="none" fillRule="evenodd">
        <G transform="translate(-8.000000, -8.000000)" fill={statusColor}>
          <G transform="translate(8.000000, 8.000000)">
            <Circle cx="3" cy="3" r="3" />
          </G>
        </G>
      </G>
    </Svg>
  );
};

export default IconStatus;
