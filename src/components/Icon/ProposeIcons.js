/* eslint-disable */
import React from 'react';
import Svg, { G, Path, Polyline, Polygon } from 'react-native-svg';

export const Trash = props => (
  <Svg {...props} viewBox="0 0 16 21" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <G id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <G id="archive">
        <G id="icon/trash">
          <Polyline id="Stroke-1" stroke="#FFFFFF" points="14.1604263 8.1997176 14.1604263 17.0365526 11.7100579 19.7651652 5.1731728 19.7651652 2.34184737 17.0365526 2.34184737 8.1819166"></Polyline>
          <Polygon id="Fill-4" fill="#FFFFFF" points="8.4858947 0.26086957 5.84210526 0.26086957 8.5654737 4.7826087 11.3673158 0.26086957"></Polygon>
          <Path d="M10.2631579,9.3043478 L10.2631579,17.2742442" id="Path-2" stroke="#FFFFFF"></Path>
          <Path d="M5.84210526,9.3043478 L5.84210526,17.2637568" id="Path-2-Copy" stroke="#FFFFFF"></Path>
          <Path d="M0.31578947,5.91304348 L15.7894737,5.91304348" id="Path-2-Copy" stroke="#FFFFFF"></Path>
        </G>
      </G>
    </G>
  </Svg>
);

export const Restore = ({ color, ...props }) => (
  <Svg {...props} viewBox="0 0 19 24" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <G id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
      <G id="archive2">
        <G id="Group-5" transform="translate(0.000000, 6.000000)" stroke={color}>
          <Polyline id="Stroke-1" points="17 3.02151508 17 13.7020844 13.8900215 17 5.5934846 17 2 13.7020844 2 3"></Polyline>
          <Path d="M12.5,4 L12.5,14" id="Path-2"></Path>
          <Path d="M6.5,4 L6.5,14" id="Path-2-Copy"></Path>
          <Path d="M0,0.5 L19,0.5" id="Path-2-Copy"></Path>
        </G>
        <Polygon id="Fill-4" fill={color} transform="translate(9.499500, 2.000000) rotate(-180.000000) translate(-9.499500, -2.000000) " points="9.392 4.4408921e-16 7 4.4408921e-16 9.464 4 11.999 4.4408921e-16"></Polygon>
      </G>
    </G>
  </Svg>
);
