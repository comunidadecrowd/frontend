/* eslint-disable */
import React from 'react';
import Svg, { G, Polygon } from 'react-native-svg';

export const IconArrows = props => {
  return (
    <Svg {...props}>
      <G stroke="none" fill="none" fillRule="evenodd">
        {!props.inverted ? (
          <G transform="translate(-19.000000, -17.000000)" fill={props.color}>
            <G transform="translate(19.000000, 7.000000)">
              <Polygon points="0 10 0 23 12 16.5004841" />
            </G>
          </G>
        ) : (
          <G transform="translate(-123.000000, -18.000000)" fill={props.color}>
            <G transform="translate(19.000000, 7.000000)">
              <Polygon points="116 11 104 17.5004841 116 24" />
            </G>
          </G>
        )}
      </G>
    </Svg>
  );
};
