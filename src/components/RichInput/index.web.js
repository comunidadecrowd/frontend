import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes } from 'react-native';
import { noop } from 'lodash';
import { Editor, EditorState, RichUtils } from 'draft-js';
import { stateToMarkdown } from 'draft-js-export-markdown';

class RichInput extends Component {
  static propTypes = {
    style: ViewPropTypes.style,

    // functions
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    setRef: PropTypes.func,
  };

  static defaultProps = {
    style: null,

    // functions
    onBlur: noop,
    onChange: noop,
    onFocus: noop,
    setRef: noop,
  };

  state = { editorState: EditorState.createEmpty() };
  _editor = null;

  toggleBold = () => this._toggleInlineStyle('BOLD');
  toggleItalic = () => this._toggleInlineStyle('ITALIC');
  toggleUnderline = () => this._toggleInlineStyle('UNDERLINE');

  _handleChange = editorState => {
    const { onChange } = this.props;

    this.setState({ editorState });

    if (onChange) {
      const currentContent = this.state.editorState.getCurrentContent();

      onChange(!!currentContent.getPlainText() ? stateToMarkdown(currentContent) : '');
    }
  };

  _toggleInlineStyle = inlineStyle => {
    this._handleChange(RichUtils.toggleInlineStyle(this.state.editorState, inlineStyle));
  };

  componentDidMount() {
    const { setRef } = this.props;

    setRef && setRef(this);
  }

  componentWillUnmount() {
    const { setRef } = this.props;

    setRef && setRef(undefined);
  }

  render() {
    const { style, onFocus, onBlur } = this.props;

    return (
      <View style={style}>
        <Editor
          editorState={this.state.editorState}
          onChange={this._handleChange}
          spellCheck={true}
          onFocus={onFocus}
          onBlur={onBlur}
        />
      </View>
    );
  }
}

export default RichInput;
