import styled from 'styled-components/native';
import { TEXT } from 'GLOBALS/TEXT';

export const _Message = styled(TEXT)`
  margin: 10px 0 0;
`;