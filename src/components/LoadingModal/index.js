import React from 'react';
import { View, Modal } from 'react-native';

import loadingAnimation from 'assets/animations/icons/loading.json';
import loadingAnimationGreen from 'assets/animations/icons/loading_green.json';

import Lottie from 'components/Lottie';
import { PureComponent } from 'components/BaseComponent';

import { Colors } from 'theme/Colors';

import {
  _Message
} from './styles'

export default class LoadingModal extends PureComponent {

  _animation = null;

  componentDidMount() {
    this._animation.play && this._animation.play();
  }

  template(css) {

    const { message, messageColor, isGreen } = this.props;

    return (
      <Modal transparent={true} animationType={'none'} visible>
        <View style={css('modalBackground')}>
          <View style={css('activityIndicatorWrapper')}>
            <Lottie
              ref={animation => this._animation = animation}
              style={css('animation')}
              source={isGreen ? loadingAnimationGreen : loadingAnimation}
              autoplay
              loop
            />
            {
              message && 
              <_Message
                euclid 
                content={message}
                color={isGreen ? "#BCDEA1" : messageColor} 
              />
            }
          </View>
        </View>
      </Modal>
    );
  }

  styles() {
    
    const { opacity } = this.props;
    
    return {
      modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: opacity ? opacity : `${Colors.BLACK}80`,
      },

      activityIndicatorWrapper: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
      },

      animation: {
        height: 52,
      },
    };
  }
}
