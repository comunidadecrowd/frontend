import React, { Component } from 'react';
import PropTypes from 'prop-types';
import createReactContext from 'create-react-context';

import { THEME_BLACK } from 'constants/Themes';

export const ThemeContext = createReactContext({ theme: THEME_BLACK });

export default class ThemeProvider extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  state = {
    theme: THEME_BLACK,
  };

  changeTheme = theme => {
    this.setState({ theme });
  };

  render() {
    return (
      <ThemeContext.Provider
        value={{
          ...this.state,
          changeTheme: this.changeTheme,
        }}
      >
        {this.props.children}
      </ThemeContext.Provider>
    );
  }
}

export const withTheme = WrappedComponent => {
  const WithTheme = props => (
    <ThemeContext.Consumer>
      {context => <WrappedComponent {...context} {...props} />}
    </ThemeContext.Consumer>
  );

  return WithTheme;
};
