import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableHighlight, ViewPropTypes } from 'react-native';
import { noop } from 'lodash';

import Icon, { Icons } from 'components/Icon';

class RichInputControls extends Component {
  static propTypes = {
    style: ViewPropTypes.style,
    iconStyle: Icon.propTypes.style,

    // functions
    toggleBold: PropTypes.func,
    toggleItalic: PropTypes.func,
    toggleUnderline: PropTypes.func,
  };

  static defaultProps = {
    style: null,
    iconStyle: null,

    // functions
    toggleBold: noop,
    toggleItalic: noop,
    toggleUnderline: noop,
  };

  render() {
    const { style, iconStyle, toggleBold, toggleItalic, toggleUnderline } = this.props;

    return (
      <View style={style}>
        <TouchableHighlight onPress={toggleBold}>
          <View>
            <Icon icon={Icons.BOLD} style={iconStyle} />
          </View>
        </TouchableHighlight>
        <TouchableHighlight onPress={toggleUnderline}>
          <View>
            <Icon icon={Icons.UNDERLINE} style={iconStyle} />
          </View>
        </TouchableHighlight>
        <TouchableHighlight onPress={toggleItalic}>
          <View>
            <Icon icon={Icons.ITALIC} style={iconStyle} />
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

export default RichInputControls;
