import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, withRouter } from 'react-router-native';
import { compose } from 'recompose';

import { Component } from 'components/BaseComponent';
import { withGlobalsContext } from 'components/GlobalsProvider';
import Home from 'components/Home';
import Settings from 'components/Settings';

import Routes from 'constants/Routes';

import Chat from 'routes/Chat';
import Briefing from 'routes/Briefing';
import Login from 'routes/Login';
import Profile from 'routes/Profile';

class Router extends Component {
  static propTypes = {
    location: PropTypes.any.isRequired,

    // functions
    toggleMainScroll: PropTypes.func.isRequired,
  };

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.props.toggleMainScroll(true);
    }
  }

  template() {
    return (
      <Switch>
        <Route exact path={Routes.HOME} component={Home} />
        <Route path={Routes.LOGIN} component={Login} />
        <Route path={Routes.BRIEFING} component={Briefing} />
        <Route path={Routes.CHAT} component={Chat} />
        <Route path={Routes.SETTINGS} component={Settings} />
        <Route path={Routes.PROFILE} component={Profile} />
      </Switch>
    );
  }
}

export default compose(
  withRouter,
  withGlobalsContext
)(Router);
