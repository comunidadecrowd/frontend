import React from 'react';
import PropTypes from 'prop-types';
import { Text } from 'react-native';
import NodeEmoji from 'node-emoji';

const Emoji = ({ style, emoji }) => <Text style={style}>{NodeEmoji.get(emoji)}</Text>;

Emoji.propTypes = {
  emoji: PropTypes.string.isRequired,
  style: Text.propTypes.style,
};

Emoji.defaultProps = {
  style: null,
};

export default Emoji;
