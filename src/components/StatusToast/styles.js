import styled from 'styled-components/native';
import { TEXT } from 'GLOBALS/TEXT';

export const _Container = styled.View`
  height: ${({ statusBarHeight }) => `${statusBarHeight + 30}px`};
  width: 100%;
  position: absolute;
  background: ${({styleOfBar}) => styleOfBar === 'light' ? '#fff' : '#333'};
  z-index: 9999;
`;

export const _StatusBarSpace = styled.View`
  height: ${({ statusBarHeight }) => statusBarHeight};
`;

export const _ToastMessageWrapper = styled.View`
  height: 30px;
  align-items: center; 
  justify-content: center;
`;

export const _ToastMessage = styled(TEXT)``;