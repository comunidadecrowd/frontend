import React, { Component } from 'react';
import { Animated, Easing } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { Colors } from 'theme';

import { Navigation } from 'react-native-navigation';
import { isIphoneX } from 'utils/Helpers';

import {
  _Container,
  _StatusBarSpace,
  _ToastMessageWrapper,
  _ToastMessage
} from './styles';

const _ContainerAnimated = Animated.createAnimatedComponent(_Container);

export default class StatusToast extends Component {

  constructor(props) {

    super(props);

    this.animations = {
      container: new Animated.Value(0),
      text: new Animated.Value(0)
    }

    this.styleOfBar = [
      '/settings/myProfile'
    ].includes(this.props.componentId) ? 'light' : 'dark';

    this.state = {
      userConnectionInfo: null,
      statusToast: {
        color: this.styleOfBar === 'dark' ? '#fff' : '#000',
        message: ''
      },
      hasStatusToastOnUI: false
    }

    this.navigationEventListener = Navigation.events().bindComponent(this);

  }

  async componentDidMount() {

    const {
      withTwilio,
      componentId,
      fromNavigation
    } = this.props;

    if (withTwilio && !fromNavigation && componentId !== '/chat/channel') {
      this.openToast(Colors.WARNING, 'Aguardando Rede...');
    }

  }

  componentDidAppear() {

    const { withTwilio } = this.props;

    withTwilio ? this.twilioConnectionInfo() : this.userNetInfo();

  }

  userNetInfo = () => {

    NetInfo.fetch().then(({ type }) => {
      this.setState({ userConnectionInfo: type })
      if (!(/(none|unknown)/).test(type)) return;
      this.openToast(Colors.WARNING, 'Aguardando Rede...');
    })

    this.NetInfoListener = NetInfo.addEventListener(({ type }) => {

      if (this.state.userConnectionInfo === type) return;

      this.setState({ userConnectionInfo: type })

      if ((/(none|unknown)/).test(type)) {
        this.openToast(Colors.WARNING, 'Aguardando Rede...');
      }

    });

    this.registerListeners();

  }

  // REFACT:  07/08 - UPDATE RN .59
  /*
    userNetInfo = () => {

      NetInfo.getConnectionInfo().then(({ type }) => {

        this.setState({ userConnectionInfo: type })

        if (!(/(none|unknown)/).test(type)) return;

        this.openToast(Colors.WARNING, 'Aguardando Rede...');

      })

      NetInfo.addEventListener('connectionChange', ({ type }) => {

        if (this.state.userConnectionInfo === type) return;

        this.setState({ userConnectionInfo: type })

        if ((/(none|unknown)/).test(type)) {
          this.openToast(Colors.WARNING, 'Aguardando Rede...');
        } else {
          this.openToast(Colors.SUCCESS, 'Conectado', 500);
        }

      })

      this.registerListeners();

    }
  */

  twilioConnectionInfo = () => {

    const { emitters } = global;

    emitters.on('twilioConnectionStateChanged', (obj) => {

      obj === 'connected' ? this.openToast(Colors.SUCCESS, 'Conectado', 500) : this.openToast(Colors.WARNING, 'Aguardando Rede...');

    })

  }

  registerListeners = () => {

    const { emitters } = global;

    emitters.on('apisRequestError', (obj) => {

      let message = obj.error.message.includes('Network request failed') ? 'Verifique sua internet' : 'Tente novamente'
      this.openToast(Colors.ERROR, message);

    })

  }

  openToast(color, message, closeToastTimer = false) {

    this.setState({ statusToast: { color, message } })

    const
      { container, text } = this.animations,
      { hasStatusToastOnUI } = this.state,
      toValue = 1,
      duration = 300,
      useNativeDriver = true;

    if (!hasStatusToastOnUI) {

      Animated.sequence([

        Animated.timing(container, {
          toValue,
          duration,
          easing: Easing.exp,
          useNativeDriver
        }),
        Animated.timing(text, {
          toValue,
          easing: Easing.linear,
          duration: (duration / 2),
          useNativeDriver
        })

      ]).start(() => {
        if (closeToastTimer) this.closeToast(closeToastTimer)
        this.setState({ hasStatusToastOnUI: true })
      })

    } else {
      if (closeToastTimer) this.closeToast(closeToastTimer)
    }

  }

  closeToast = (closeToastTimer = false) => {

    const
      { container, text } = this.animations,
      toValue = 2,
      duration = 300,
      useNativeDriver = true;

    Animated.sequence([

      Animated.delay(closeToastTimer ? closeToastTimer : 0),

      Animated.timing(text, {
        toValue,
        easing: Easing.linear,
        duration: (duration / 2),
        useNativeDriver
      }),
      Animated.timing(container, {
        toValue,
        duration,
        easing: Easing.exp,
        useNativeDriver
      })

    ]).start(() => {
      this.setState({ hasStatusToastOnUI: false })
    })

  }

  componentWillUnmount() {
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
    // NetInfo.removeEventListener('connectionChange'); // REFACT:  07/08 - UPDATE RN .59
    this.NetInfoListener = () => null
  }

  componentDidDisappear() {
    // NetInfo.removeEventListener('connectionChange'); // REFACT:  07/08 - UPDATE RN .59
    this.NetInfoListener = () => null
    // TODO:  Alex, analisar se vale a pena fechar o Toast e reiniciar os States 

  }

  render() {

    const
      { statusBarHeight } = this.props,
      { container, text } = this.animations,
      { statusToast: { color, message } } = this.state,
      _ContainerAnimatedInterpolate = container.interpolate({
        inputRange: [0, 1, 2],
        outputRange: [isIphoneX ? -70 : -50, 0, isIphoneX ? -70 : -50]
      }),
      TextInterpolate = text.interpolate({
        inputRange: [0, 1, 2],
        outputRange: [0, 1, 0]
      });

    return (
      <_ContainerAnimated
        statusBarHeight={statusBarHeight}
        styleOfBar={this.styleOfBar}
        style={{
          transform: [
            { translateY: _ContainerAnimatedInterpolate },
            { perspective: 1000 }
          ]
        }}
      >
        <_StatusBarSpace statusBarHeight={statusBarHeight} />
        <_ToastMessageWrapper>
          <Animated.View style={{ opacity: TextInterpolate }}>
            <_ToastMessage
              content={message}
              color={color}
              fontSize={12}
            />
          </Animated.View>
        </_ToastMessageWrapper>
      </_ContainerAnimated>
    )

  }

}