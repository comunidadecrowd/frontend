import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes, Text } from 'react-native';

import { Component } from 'components/BaseComponent';
import Input from 'components/Input';

import { TextStyles, Colors } from 'theme';

class InputCode extends Component {
  static propTypes = {
    error: PropTypes.string,
    length: PropTypes.number,
    style: ViewPropTypes.style,

    // functions
    onChange: PropTypes.func,
    onFinish: PropTypes.func,
  };

  static defaultProps = {
    error: '',
    length: 6,
    style: null,

    // functions
    onChange: null,
    onFinish: null,
  };

  state = {
    values: [],
  };

  inputs = [];

  _serializedValue = () => {
    const { values } = this.state;

    return values.join('');
  };

  _handleChange = (index, value) => {
    const { onChange } = this.props;
    const values = this.state.values;

    values[index] = value ? parseInt(value, 10) : null;

    this.setState({ values });

    onChange && onChange(this._serializedValue());
  };

  _handleKeyPress = (index, event) => {
    const { onFinish, length } = this.props;
    const key = event.nativeEvent.key;
    const nextIndex = key === 'Backspace' ? index - 1 : index + 1;

    if (this.inputs[nextIndex]) {
      this.inputs[nextIndex]._component.focus();
    } else if (nextIndex >= length) {
      requestAnimationFrame(() => {
        this.inputs[index]._component.blur();
        onFinish && onFinish(this._serializedValue());
      });
    }
  };

  template(css) {
    const { length, style, error } = this.props;

    return (
      <View style={style}>
        <View style={css('root')}>
          {[...Array(length)].map((_, index) => (
            <Input
              key={index}
              style={css('inputWrapper')}
              styleInput={css('input')}
              noDecoration
              maxLength={1}
              onChange={value => this._handleChange(index, value)}
              onKeyPress={event => this._handleKeyPress(index, event)}
              onKey
              keyboardType="number-pad"
              setRef={element => {
                this.inputs[index] = element;
              }}
              error={!!error}
            />
          ))}
        </View>
        {!!error && <Text style={css('error')}>{error}</Text>}
      </View>
    );
  }

  styles() {
    return {
      root: {
        flexDirection: 'row',
        justifyContent: 'space-between',
      },

      inputWrapper: {
        width: 40,
      },

      input: {
        ...TextStyles.code,
        textAlign: 'center',
      },

      error: {
        ...TextStyles.small,
        color: Colors.YELLOW,
        marginTop: 8,
      },
    };
  }
}

export default InputCode;
