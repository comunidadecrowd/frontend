import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Animated, Text, TouchableOpacity, View, ViewPropTypes } from 'react-native';

import { Component } from 'components/BaseComponent';

import CustomPropTypes from 'constants/CustomPropTypes';

import { Colors, Duration, TextStyles } from 'theme';

import withAnimation from 'utils/withAnimation';

class Button extends Component {
  static propTypes = {
    arrowLeftStyle: ViewPropTypes.style,
    arrowRightStyle: ViewPropTypes.style,
    arrowsStyle: ViewPropTypes.style,
    children: PropTypes.node.isRequired,
    disabled: PropTypes.bool,
    hovering: CustomPropTypes.animation.isRequired,
    labelStyle: Text.propTypes.style,
    style: ViewPropTypes.style,

    // functions
    onPress: PropTypes.func.isRequired,
    startHoveringTiming: PropTypes.func.isRequired,
  };

  static defaultProps = {
    arrowLeftStyle: null,
    arrowRightStyle: null,
    arrowsStyle: null,
    disabled: false,
    labelStyle: null,
    style: null,
  };

  _handleLayout = event => {
    const { width } = event.nativeEvent.layout;

    this.setState({ rootWidth: width - 1 });
  };

  _handleHover = isHovering => {
    const { startHoveringTiming, disabled } = this.props;

    if (disabled) return;

    startHoveringTiming({
      toValue: Number(isHovering),
      duration: Duration.FAST,
    });
  };

  _handleMouseEnter = () => this._handleHover(true);
  _handleMouseLeave = () => this._handleHover(false);

  template(css) {
    const {
      hovering,
      children,
      onPress,
      style,
      labelStyle,
      arrowsStyle,
      arrowLeftStyle,
      arrowRightStyle,
      disabled,
    } = this.props;

    const label = children.toLowerCase();

    return (
      <TouchableOpacity
        onPress={onPress}
        onMouseEnter={this._handleMouseEnter}
        onMouseLeave={this._handleMouseLeave}
        disabled={disabled}
        style={css(['root', style])}
      >
        <Animated.View
          style={css('container', {
            paddingHorizontal: hovering.interpolate({
              inputRange: [0, 1],
              outputRange: [32, 23],
            }),
          })}
        >
          <View style={css(['arrow', 'arrowLeft', arrowsStyle, arrowLeftStyle])} />
          <Text style={css(['label', labelStyle])}>{label}</Text>
          <View style={css(['arrow', 'arrowRight', arrowsStyle, arrowRightStyle])} />
        </Animated.View>
      </TouchableOpacity>
    );
  }

  styles() {
    return {
      root: {
        alignSelf: 'center',
        backgroundColor: 'transparent',
        padding: 8,

        ...Platform.select({
          web: {
            outline: 'none',
          },
        }),
      },

      container: {
        paddingHorizontal: 32,
      },

      arrow: {
        position: 'absolute',
        borderTopColor: Colors.TRANSPARENT,
        borderBottomColor: Colors.TRANSPARENT,
        borderTopWidth: 5,
        borderBottomWidth: 5,
        top: 12,
        zIndex: 1,
      },

      arrowLeft: {
        borderLeftColor: Colors.WHITE,
        borderLeftWidth: 10,
        left: 0,
      },

      arrowRight: {
        borderRightColor: Colors.WHITE,
        borderRightWidth: 10,
        right: 0,
      },

      label: {
        ...TextStyles.button,
        color: Colors.WHITE,
      },
    };
  }
}

export default withAnimation('hovering', 0)(Button);
