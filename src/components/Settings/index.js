/* eslint-disable */

import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity } from 'react-native';
import { Link, withRouter } from 'react-router-native';
import { Navigation } from 'react-native-navigation';

import { PureComponent } from 'components/BaseComponent';
import Row from 'components/Grid/Row';
import Column from 'components/Grid/Column';
import Home from 'components/Home';
import Icon, { Icons } from 'components/Icon';

import { MARGIN_HORIZONTAL } from 'constants/Layout';
import Routes, { BriefingRoutes, ChatRoutes } from 'constants/Routes';

import { Colors } from 'theme/Colors';

class Settings extends PureComponent {
  _handleClick = route => {
    Navigation.push(Routes.SETTINGS, {
      component: {
        name: route,
        id: route,
        options: {
          bottomTabs: {
            visible: false,
          },
          topBar: {
            visible: false,
          },
        },
      },
    });
  };

  template(css) {
    return (
      <SafeAreaView>
        <View style={css('root')}>
          <TouchableOpacity onPress={() => this._handleClick(Routes.PROFILE)}>
            <Row>
              <View style={css(['avatar'])}>
                <Icon icon={Icons.PERFIL} style={css('avatarPlaceholder')} />
              </View>
              <Column>
                <Text style={{ color: Colors.WHITE }}>Tiago Cruz</Text>
                <Text style={{ color: Colors.WHITE }}>Designer de Branding</Text>
              </Column>
              <Icon icon={Icons.CHEVRON_RIGHT} />
            </Row>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this._handleClick(Routes.LOGIN)}>
            <Text style={{ color: Colors.WHITE }}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this._handleClick(BriefingRoutes.CATEGORY)}>
            <Text style={{ color: Colors.WHITE }}>Briefing</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this._handleClick(BriefingRoutes.PUBLISH)}>
            <Text style={{ color: Colors.WHITE }}>Publish</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this._handleClick(ChatRoutes.MESSAGES)}>
            <Text style={{ color: Colors.WHITE }}>Chat</Text>
          </TouchableOpacity>
        </View>
        <Home />
      </SafeAreaView>
    );
  }

  styles() {
    return {
      root: { padding: MARGIN_HORIZONTAL },
      avatar: {
        borderWidth: 2,
        borderColor: Colors.WHITE,
        borderRadius: 28,
        height: 56,
        width: 56,
        overflow: 'hidden',
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 16,
      },
      avatarPlaceholder: {
        color: Colors.WHITE,
        fontSize: 40,
      },
    };
  }
}

export default withRouter(Settings);
