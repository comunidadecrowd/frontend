import React from 'react';
import { View, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import { Component } from 'components/BaseComponent';

import CustomPropTypes from 'constants/CustomPropTypes';
import { MARGIN_HORIZONTAL, GRID_GUTTERS, GRID_COLUMNS } from 'constants/Layout';

import { MEDIUM, LARGE } from 'theme/Breakpoints';

import withDimensions from 'utils/withDimensions';

class Column extends Component {
  static propTypes = {
    breakpoint: CustomPropTypes.breakpoint.isRequired,
    children: PropTypes.node.isRequired,
    push: PropTypes.number,
    pushMedium: PropTypes.number,
    pushLarge: PropTypes.number,
    size: PropTypes.number,
    sizeMedium: PropTypes.number,
    sizeLarge: PropTypes.number,
    style: ViewPropTypes.style,
    windowWidth: PropTypes.number.isRequired,
    withMargin: PropTypes.bool,
  };

  static defaultProps = {
    push: null,
    pushMedium: null,
    pushLarge: null,
    size: 12,
    sizeMedium: null,
    sizeLarge: null,
    style: null,
    withMargin: true,
  };

  state = {
    columnWidth: 0,
    columnWidthMedium: 0,
    columnWidthLarge: 0,
    push: 0,
    pushMedium: 0,
    pushLarge: 0,
  };

  _getGutterWidth = () => {
    const { windowWidth } = this.props;
    const margins = MARGIN_HORIZONTAL * 2;

    return (windowWidth - margins) * GRID_GUTTERS;
  };

  _getColumnSize = (size, rightGutter) => {
    if (!size) return null;

    const { windowWidth } = this.props;

    const margins = MARGIN_HORIZONTAL * 2;
    const gutter = this._getGutterWidth();
    const gutters = gutter * (GRID_COLUMNS - 1);
    const columnSize = (windowWidth - margins - gutters) / GRID_COLUMNS;
    const columnsSize = columnSize * size + gutter * (size - 1);

    return columnsSize + (rightGutter ? gutter : 0);
  };

  _setWidth = () => {
    const {
      breakpoint,
      withMargin,
      size,
      sizeMedium,
      sizeLarge,
      push,
      pushMedium,
      pushLarge,
    } = this.props;

    let responsiveSize = size;
    let responsivePush = push;

    if (breakpoint === MEDIUM) {
      responsiveSize = sizeMedium || size;
      responsivePush = pushMedium || push;
    } else if (breakpoint === LARGE) {
      responsiveSize = sizeLarge || sizeMedium || size;
      responsivePush = pushLarge || pushMedium || push;
    }

    this.setState({
      marginLeft: this._getColumnSize(responsivePush, true),
      marginRight: withMargin ? this._getGutterWidth() : null,
      width: this._getColumnSize(responsiveSize),
    });
  };

  componentDidMount() {
    this._setWidth();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.windowWidth !== this.props.windowWidth) {
      this._setWidth();
    }
  }

  template(css) {
    const { style, children } = this.props;
    const { width, marginLeft, marginRight } = this.state;

    return (
      <View
        style={css([
          'root',
          style,
          {
            marginLeft,
            marginRight,
            width,
          },
        ])}
      >
        {children}
      </View>
    );
  }

  styles() {
    return {
      root: {
        maxWidth: `100%`,
      },
    };
  }
}

export default withDimensions(Column);
