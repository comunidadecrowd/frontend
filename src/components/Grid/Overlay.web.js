import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import PropTypes from 'prop-types';

import { GRID_COLUMNS, GRID_GUTTERS, MARGIN_HORIZONTAL } from 'constants/Layout';

import { Colors } from 'theme';

class Overlay extends Component {
  static propTypes = {
    shouldStartVisible: PropTypes.bool,
  };

  static defaultProps = {
    shouldStartVisible: false,
  };

  state = { visible: this.props.shouldStartVisible || false };

  componentDidMount() {
    window.addEventListener('keypress', this._toggleVisible);
  }

  componentWillUnmount() {
    window.removeEventListener('keypress', this._toggleVisible);
  }

  _toggleVisible = event => {
    const key = event.keyCode || event.charCode || 0;

    // "Alt" + "."
    if (key === 8805) {
      this.setState(state => ({ visible: !state.visible }));
    }
  };

  render() {
    const { visible } = this.state;

    if (!visible) return null;

    return (
      <View style={styles.root}>
        {[...Array(GRID_COLUMNS)].map((_, index) => (
          <View
            key={index}
            style={[styles.column, index < GRID_COLUMNS - 1 && styles.columnMargin]}
          />
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  root: {
    position: 'fixed',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginHorizontal: MARGIN_HORIZONTAL,
    zIndex: 100,
  },
  column: {
    backgroundColor: Colors.GRAY,
    opacity: 0.5,
    flex: 1,
  },
  columnMargin: { marginRight: `${GRID_GUTTERS * 100}%` },
});

export default Overlay;
