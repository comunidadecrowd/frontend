import React from 'react';
import { View, ViewPropTypes } from 'react-native';
import PropTypes from 'prop-types';

import { PureComponent } from 'components/BaseComponent';

import { MARGIN_HORIZONTAL } from 'constants/Layout';

class Container extends PureComponent {
  static propTypes = {
    children: PropTypes.node.isRequired,
    style: ViewPropTypes.style,
  };

  static defaultProps = {
    style: null,
  };

  template(css) {
    const { style, children, ...props } = this.props;

    return (
      <View style={css(['root', style])} {...props}>
        {children}
      </View>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
        marginHorizontal: MARGIN_HORIZONTAL,
      },
    };
  }
}

export default Container;
