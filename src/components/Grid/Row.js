import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ViewPropTypes } from 'react-native';

const Row = ({ style, children, ...props }) => (
  <View style={[styles.root, style]} {...props}>
    {children}
  </View>
);

const styles = StyleSheet.create({
  root: {
    display: 'flex',
    flexDirection: 'row',
  },
});

Row.propTypes = {
  children: PropTypes.node.isRequired,
  style: ViewPropTypes.style,
};

Row.defaultProps = {
  style: null,
};

export default Row;
