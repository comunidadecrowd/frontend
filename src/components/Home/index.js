import React from 'react';
import PropTypes from 'prop-types';
import { SafeAreaView, Text, View, Button } from 'react-native';
import { withRouter } from 'react-router-native';
import firebase from 'react-native-firebase';

import { PureComponent } from 'components/BaseComponent';

import { ChatRoutes } from 'constants/Routes';

import { Colors } from 'theme/Colors';

import { getToken, deleteToken } from 'utils/auth';

class Home extends PureComponent {
  static propTypes = {
    history: PropTypes.any.isRequired,
  };

  state = {
    user: null,
    token: null,
  };

  _signOut = () => {
    firebase.auth().signOut();

    deleteToken();

    this.setState({
      user: null,
      token: null,
    });
  };

  componentDidMount() {
    const { history } = this.props;

    firebase.auth().onAuthStateChanged(user => {
      this.setState({ user });
    });

    getToken().then(token => {
      this.setState({ token });
    });

    process.env.NODE_ENV !== 'production' && history.push(ChatRoutes.MESSAGES);
  }

  template() {
    const { user, token } = this.state;
    const textStyle = {
      color: Colors.WHITE,
      marginHorizontal: 'auto',
    };

    return (
      <SafeAreaView>
        {user && (
          <View>
            <Text style={textStyle}>FIREBASE USER: {JSON.stringify(user)}</Text>
          </View>
        )}
        {token && (
          <View>
            <Text style={textStyle}>LEGACY TOKEN: {JSON.stringify(token)}</Text>
          </View>
        )}
        {(user || token) && <Button title="Sign Out" color="red" onPress={this._signOut} />}
      </SafeAreaView>
    );
  }
}

export default withRouter(Home);
