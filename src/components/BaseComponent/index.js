import React from 'react';
import { Dimensions, StyleSheet } from 'react-native';
import { flatten, debounce } from 'lodash';

import { breakpoints, SMALL, MEDIUM, LARGE } from 'theme/Breakpoints';
import { emToPx } from 'utils/number';

const hoc = Component =>
  class BaseComponent extends Component {
    constructor(props) {
      super(props);

      this.breakpoint = this._getBreakpoint();
      this.styleSheet = this.styles && StyleSheet.create(this.styles(props));
      this._setBreakpoint = debounce(this._setBreakpoint.bind(this), 100);
    }
    _parseStyles = styles => {
      if (!styles) return null;

      if (Array.isArray(styles)) {
        return styles.map(style => {
          if (typeof style === 'string') {
            return this.styleSheet[style];
          }

          return style;
        });
      } else if (typeof styles === 'string') {
        return this.styleSheet[styles];
      }

      return styles;
    };

    _parseResponsiveStyles = (stylesSmall, stylesMedium, stylesLarge) => {
      const parsedStyles = [
        this._parseStyles(stylesSmall),
        this.breakpoint !== SMALL && this._parseStyles(stylesMedium),
        this.breakpoint === LARGE && this._parseStyles(stylesLarge),
      ];

      return flatten(parsedStyles).filter(item => !!item || item === 0);
    };

    _getBreakpoint = () => {
      const { width } = Dimensions.get('window');

      let breakpoint;

      if (width >= emToPx(breakpoints[LARGE])) {
        breakpoint = LARGE;
      } else if (width >= emToPx(breakpoints[MEDIUM])) {
        breakpoint = MEDIUM;
      } else {
        breakpoint = SMALL;
      }

      return breakpoint;
    };

    _setBreakpoint() {
      const breakpoint = this._getBreakpoint();

      if (breakpoint !== this.breakpoint) {
        this.breakpoint = breakpoint;
        this.forceUpdate();
      }
    }

    componentDidMount() {
      if (super.componentDidMount) super.componentDidMount();
      Dimensions.addEventListener('change', this._setBreakpoint);
    }

    componentWillUnmount() {
      if (super.componentWillUnmount) super.componentWillUnmount();
      Dimensions.removeEventListener('change', this._setBreakpoint);
    }

    render() {
      if (!this.template) throw new TypeError("Component doesn't define `template`");
      return this.template(this._parseResponsiveStyles);
    }
  };

export const PureComponent = hoc(React.PureComponent);
export const Component = hoc(React.Component);
