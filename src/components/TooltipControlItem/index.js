import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableHighlight } from 'react-native';
import { noop } from 'lodash';

import { Component } from 'components/BaseComponent';
import Icon, { Icons } from 'components/Icon';

import { Colors } from 'theme';

class TooltipControlItem extends Component {
  static propTypes = {
    isActive: PropTypes.bool,
    style: TouchableHighlight.propTypes.style,

    // functions
    onPress: PropTypes.func,
  };

  static defaultProps = {
    isActive: false,
    style: null,
    styleLarge: null,

    // functions
    onPress: noop,
  };

  state = { isHovering: false };

  _handleMouseEnter = () => this._handleHover(true);
  _handleMouseLeave = () => this._handleHover(false);

  _handleHover(isHovering) {
    this.setState({ isHovering });
  }

  template(css) {
    const { onPress, style, isActive } = this.props;
    const { isHovering } = this.state;

    return (
      <TouchableHighlight
        onPress={onPress}
        onMouseEnter={this._handleMouseEnter}
        onMouseLeave={this._handleMouseLeave}
        style={style}
      >
        <View>
          <Icon
            icon={Icons.TOOLTIP}
            style={css(['icon', isHovering && 'iconHover', isActive && 'iconActive'])}
          />
        </View>
      </TouchableHighlight>
    );
  }

  styles() {
    return {
      icon: {
        fontSize: 30,
        color: Colors.GRAY,
      },

      iconHover: { color: Colors.BLUE },

      iconActive: { color: Colors.BLUE },
    };
  }
}

export default TooltipControlItem;
