import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';

class Avatar extends Component {
  propTypes = {
    image: PropTypes.string.isRequired,
    color: PropTypes.string,
  };

  defaultProps = {
    image: 'https://randomuser.me/api/portraits/men/81.jpg', // URL avatarDefault da CROWD
    color: '#ED4B68',
  };

  render() {
    const { image, color } = this.props;

    return (
      <WrapperAvatar color={color}>
        <Img source={{ uri: image }} />
      </WrapperAvatar>
    );
  }
}

export default Avatar;

const WrapperAvatar = styled.View`
  width: 60px;
  height: 60px;
  border: 1px solid ${({ color }) => color};
  border-radius: 50px;
  overflow: hidden;
`;

const Img = styled.Image`
  width: 60px;
  height: 60px;
`;
