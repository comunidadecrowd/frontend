# Includes

Commonly Used Components

## Folders / Files

### Tree

+ComponentExample1  
  +components  
  index.js  
+ComponentExample2  
  ...  

### Files Description

Every component has a root folder with the exact component name (ie. ComponentExample1)  
`components` is a folder to keep the sub components which are used to render the main component.  
`index.js` has a main component engine.  
