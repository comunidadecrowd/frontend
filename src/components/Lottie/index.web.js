import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { View, ViewPropTypes } from 'react-native';
import lottie from 'lottie-web';

class Lottie extends Component {
  static propTypes = {
    autoplay: PropTypes.bool,
    options: PropTypes.object,
    source: PropTypes.oneOfType([PropTypes.object, PropTypes.string]).isRequired,
    style: ViewPropTypes.style,
  };

  static defaultProps = {
    autoplay: null,
    options: null,
    style: null,
  };

  _animation = null;
  _element = null;

  componentDidMount() {
    const { options, source } = this.props;

    const container = ReactDOM.findDOMNode(this._element); // eslint-disable-line react/no-find-dom-node

    this._animation = lottie.loadAnimation({
      container,
      path: source,
      renderer: 'svg',
      animationData: source,
      ...options,
    });
  }

  render() {
    const { options, source, style, autoplay, ...props } = this.props;

    return (
      <View
        style={style}
        ref={element => {
          this._element = element;
        }}
        {...props}
      />
    );
  }
}

export default Lottie;
