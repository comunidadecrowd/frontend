import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter, matchPath } from 'react-router-native';
import { View } from 'react-native';

import { FOOTER_HEIGHT } from 'constants/Layout';
import Routes, { BriefingRoutes, ChatRoutes } from 'constants/Routes';

import { GlobalsContext } from 'components/GlobalsProvider';

import { Colors } from 'theme/Colors';

import { PureComponent } from './BaseComponent';
import Icon, { Icons } from './Icon';

class Footer extends PureComponent {
  static propTypes = {
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
  };

  template(css) {
    const {
      location: { pathname },
    } = this.props;

    const isChat = !!matchPath(pathname, Routes.CHAT);
    const isBriefing = !!matchPath(pathname, Routes.BRIEFING);
    const isSettings = !!matchPath(pathname, Routes.SETTINGS);
    const isLogin = !!matchPath(pathname, Routes.LOGIN);

    if (isLogin) return null;

    return (
      <GlobalsContext.Consumer>
        {({ isFooterVisible }) => (
          <View style={css(['root', !isFooterVisible && 'rootHidden'], 'rootMedium')}>
            <Link to={ChatRoutes.MESSAGES} style={css('item')}>
              <Icon icon={Icons.CHAT} style={css(['icon', isChat && 'iconActive'])} />
            </Link>
            <Link to={BriefingRoutes.CATEGORY} style={css('item')}>
              <Icon icon={Icons.BRIEFING} style={css(['icon', isBriefing && 'iconActive'])} />
            </Link>
            <Link to={Routes.SETTINGS} style={css('item')}>
              <Icon icon={Icons.CONFIG} style={css(['icon', isSettings && 'iconActive'])} />
            </Link>
          </View>
        )}
      </GlobalsContext.Consumer>
    );
  }

  styles() {
    return {
      root: {
        flexDirection: 'row',
        alignItems: 'center',
        left: 0,
        right: 0,
        height: FOOTER_HEIGHT,
        borderTopColor: Colors.GRAY_DARK,
        borderTopWidth: 1,
        backgroundColor: Colors.BLACK,
      },

      rootHidden: {
        bottom: -100,
      },

      rootMedium: {
        display: 'none',
      },

      item: {
        flex: 1,
        alignItems: 'center',
        marginHorizontal: 16,
      },

      icon: {
        color: Colors.GRAY_DARK,
        fontSize: 40,
      },

      iconActive: {
        color: Colors.BLUE,
      },
    };
  }
}

export default withRouter(Footer);
