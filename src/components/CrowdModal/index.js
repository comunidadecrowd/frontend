import React from 'react';
import PropTypes from 'prop-types';
import { Dimensions, View, TouchableOpacity } from 'react-native';
import { noop } from 'lodash';

import { PureComponent } from 'components/BaseComponent';

import { IconCloseButton } from 'components/Icon/SearchView';
const { width, height } = Dimensions.get('window');

export default class CrowdModal extends PureComponent {
  static propTypes = {
    handleCloseModal: PropTypes.func.isRequired,
    children: PropTypes.node.isRequired,
  };

  static defaultProps = {
    children: noop,
  };

  template(css) {
    const { handleCloseModal } = this.props;

    return (
      <View style={css('modalBackground')}>
        <View style={css('modal')}>
          <TouchableOpacity style={css('iconClose')} onPress={handleCloseModal}>
            <IconCloseButton width={20} height={20} color="#FFF" />
          </TouchableOpacity>
          <View style={css('modalContent')}>{this.props.children}</View>
        </View>
      </View>
    );
  }

  styles() {
    return {
      modalBackground: {
        width: width - 30,
        height: height - 180,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        zIndex: 2,
        // backgroundColor: `${Colors.BLACK}80`,
      },

      modal: {
        width: '100%',
        height: '100%',
        backgroundColor: '#000',
        padding: 15,
        alignItems: 'center',
      },

      iconClose: {
        position: 'absolute',
        top: 15,
        right: 15,
        zIndex: 1,
      },

      modalContent: {
        width: '100%',
        height: '100%',
        alignItems: 'center',
      },
    };
  }
}
