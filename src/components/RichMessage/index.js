import React, { Fragment } from 'react';
import {Platform} from 'react-native';
import PropTypes from 'prop-types';
import { noop } from 'lodash';

import { Component } from 'components/BaseComponent';
import ButtonSubmit from 'components/ButtonSubmit';
import { FontRatioNormalize } from 'theme';

import ProposeMessage from './components/ProposeMessage';

const isiOS = Platform.OS === 'ios';

class RichMessage extends Component {
  static propTypes = {
    template: PropTypes.string,
    item: PropTypes.shape({
      content: PropTypes.string,
      attributes: PropTypes.shape({}),
    }),
    from: PropTypes.string,
    onPress: PropTypes.func,
  };

  static defaultProps = {
    template: 'default', // possible values: type[sendPropose, accetPropose[etc]]
    item: {},
    from: 'user',
    onPress: noop,
  };

  template(css) {
    const { template, item, from, onPress } = this.props;

    if (template === 'propose') {
      return (
        <Fragment>
          <ProposeMessage data={item.attributes} />
          {from === 'friend' && (
            <ButtonSubmit
              noAnimation
              noPadding
              style={css('sendPropose')}
              textSize={FontRatioNormalize(14)}
              onPress={onPress}
              labelColor="black"
              activeColor="black"
            >
              contratar proposta
            </ButtonSubmit>
          )}
        </Fragment>
      );
    }
  }
  styles() {
    return {
      sendPropose: {
        width: '100%',
      },
      text: {
        color: '#000000',
        fontFamily: isiOS ? 'Inter UI' : 'inter-ui-regular',
        fontSize: 12,
        fontWeight: 'bold',
      },
    };
  }
}

export default RichMessage;
