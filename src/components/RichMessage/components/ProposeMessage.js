import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, Platform } from 'react-native';

const isiOS = Platform.OS === 'ios';

const styles = {
  main: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  label: {
    paddingBottom: 5,
    paddingTop: 5,
    color: '#CCCCCC',
  },
  attributes: {
    borderBottomWidth: 1,
    borderBottomColor: 'lightblue',
    paddingBottom: 5,
    marginBottom: 15,
  },
  text: {
    color: '#000000',
    fontFamily: isiOS ? 'Inter UI' : 'inter-ui-regular',
    fontSize: 14,
  },
};

const ProposeMessage = props => {
  const { title, value, term } = props.data.body;

  return (
    <View style={styles.main}>
      <Text style={styles.label}>Titulo</Text>
      <View style={styles.attributes}>
        <Text style={styles.text}>{title}</Text>
      </View>
      <Text style={styles.label}>Valor</Text>
      <View style={styles.attributes}>
        <Text style={styles.text}>R$ {value}</Text>
      </View>
      <Text style={styles.label}>Prazo</Text>
      <View style={styles.attributes}>
        <Text style={styles.text}>{term}</Text>
      </View>
    </View>
  );
};

ProposeMessage.propTypes = {
  data: PropTypes.shape({
    body: PropTypes.shape({
      title: PropTypes.string,
      value: PropTypes.number,
      term: PropTypes.number,
    }),
  }).isRequired,
};

export default ProposeMessage;
