import React, { Component } from 'react';
import PropTypes from 'prop-types';
import createReactContext from 'create-react-context';

import { FULL_HEADER } from 'constants/Header';

export const GlobalsContext = createReactContext({
  headerTheme: FULL_HEADER,
  isFooterVisible: true,
});

export default class GlobalsProvider extends Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  state = {
    headerTheme: FULL_HEADER,
    isFooterVisible: true,
    isMainScrollEnabled: true,
  };

  _changeHeader = ({ theme, options }) => {
    this.setState({
      headerTheme: theme,
      headerOptions: options,
    });
  };

  _toggleFooter = visible => {
    this.setState(state => ({
      isFooterVisible: visible === undefined ? !state : visible,
    }));
  };

  _toggleMainScroll = isEnabled => {
    this.setState(state => ({
      isMainScrollEnabled: isEnabled === undefined ? !state : isEnabled,
    }));
  };

  render() {
    return (
      <GlobalsContext.Provider
        value={{
          ...this.state,
          changeHeader: this._changeHeader,
          toggleFooter: this._toggleFooter,
          toggleMainScroll: this._toggleMainScroll,
        }}
      >
        {this.props.children}
      </GlobalsContext.Provider>
    );
  }
}

export const withGlobalsContext = WrappedComponent => {
  const WithGlobalsContext = props => (
    <GlobalsContext.Consumer>
      {context => <WrappedComponent {...context} {...props} />}
    </GlobalsContext.Consumer>
  );

  return WithGlobalsContext;
};
