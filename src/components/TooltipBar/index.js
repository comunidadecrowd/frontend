import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Dimensions, View, Text, TouchableHighlight, ViewPropTypes } from 'react-native';
import { withRouter } from 'react-router-native';
import { compose } from 'recompose';
// import { BlurView } from 'react-native-blur'; // NOTE:  O react-native-blur foi removido do projeto em 04/06 devido a problemas no build da Release do Android

import { Component } from 'components/BaseComponent';
import Button from 'components/Button';
import { withGlobalsContext } from 'components/GlobalsProvider';
import Icon, { Icons } from 'components/Icon';
import TooltipControlItem from 'components/TooltipControlItem';

import CustomPropTypes from 'constants/CustomPropTypes';
import { MARGIN_HORIZONTAL } from 'constants/Layout';
import { BriefingRoutes } from 'constants/Routes';

import { Colors } from 'theme/Colors';
import { TextStyles } from 'theme/TextStyles';

import withLocalization from 'utils/withLocalization';
import withDimensions from 'utils/withDimensions';

const { width, height } = Dimensions.get('window');

class TooltipBar extends Component {
  static propTypes = {
    currentIndex: PropTypes.number,
    history: PropTypes.any.isRequired,
    openIndex: PropTypes.number,
    shouldStartOpen: PropTypes.bool,
    style: ViewPropTypes.style,
    tips: PropTypes.arrayOf(
      PropTypes.shape({
        icon: CustomPropTypes.icon,
        title: PropTypes.string,
        description: PropTypes.string,
      })
    ),
    windowHeight: PropTypes.number.isRequired,

    // functions
    t: PropTypes.func.isRequired,
    toggleFooter: PropTypes.func.isRequired,
  };

  static defaultProps = {
    currentIndex: 0,
    openIndex: null,
    shouldStartOpen: false,
    style: null,
    tips: [],
  };

  state = { openTipIndex: this.props.shouldStartOpen ? 0 : null };

  _toggleFooter = () => {
    const isOpen = this.state.openTipIndex !== null;

    this.props.toggleFooter(!isOpen);
  };

  _handleOpenTip = index => {
    this.setState({ openTipIndex: this.state.openTipIndex === index ? null : index });
  };

  _handleClose = () => {
    const { history } = this.props;
    const { openTipIndex } = this.state;

    if (openTipIndex === null) {
      history.push(BriefingRoutes.CATEGORY);
    } else {
      this.setState({ openTipIndex: null });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.openIndex !== prevProps.openIndex) {
      this.setState({ openTipIndex: this.props.openIndex });
    }

    if (this.state.openTipIndex !== prevState.openTipIndex) {
      this._toggleFooter();
    }
  }

  componentDidMount() {
    this._toggleFooter();
  }

  template(css) {
    const { tips, style, currentIndex, t, windowHeight } = this.props;
    const { openTipIndex } = this.state;

    return (
      <View style={css(['root', openTipIndex !== null && 'rootOpen', style], 'rootMedium')}>
        {/* // NOTE:  O react-native-blur foi removido do projeto em 04/06 devido a problemas no build da Release do Android
          <BlurView
          style={css('absolute')}
          // viewRef={this.state.viewRef}
          blurType="dark"
          blurAmount={10}
        /> */}
        <View style={css(['sidebar', openTipIndex !== null && 'sidebarOpen'])}>
          <View style={css(['background', 'hidden'], 'visible')} />
          <TouchableHighlight
            onPress={this._handleClose}
            style={css(openTipIndex === null && 'invisible', 'visible')}
          >
            <View>
              <Icon icon={Icons.CLOSE} style={css('closeIcon')} />
            </View>
          </TouchableHighlight>
          <View style={css(['control', openTipIndex !== null && 'controlOpen'], 'controlMedium')}>
            {tips.map((item, index) => (
              <TooltipControlItem
                key={index}
                style={css(['tooltipIcon', index !== currentIndex && 'hidden'], 'visible')}
                isActive={openTipIndex === index}
                onPress={() => this._handleOpenTip(index)}
              />
            ))}
          </View>
        </View>
        {openTipIndex !== null && (
          <View
            style={css(
              ['detailsWrapper', { minHeight: windowHeight - 64 }],
              'detailsWrapperMedium'
            )}
          >
            <View style={css('background')} />
            <View style={css('details')}>
              <Icon icon={tips[openTipIndex].icon} style={css('detailsIcon')} />
              <Text style={css(['content', 'contentTitle', TextStyles.h3])}>
                {tips[openTipIndex].title}
              </Text>
              <Text style={css(['content', TextStyles.body])}>
                {tips[openTipIndex].description}
              </Text>
              <Button style={css('button', 'hidden')} onPress={this._handleClose}>
                {t('briefing:TOOLTIP_INSERT_TEXT')}
              </Button>
            </View>
          </View>
        )}
      </View>
    );
  }
  styles() {
    return {
      root: {
        display: 'flex',
        position: 'absolute',
        top: 0,
        bottom: 0,
        right: -MARGIN_HORIZONTAL,
        zIndex: 2,
        //backgroundColor: Colors.BLACK,

        ...Platform.select({
          web: {
            pointerEvents: 'none',
          },
        }),
      },

      rootOpen: {
        left: -MARGIN_HORIZONTAL,
        paddingHorizontal: MARGIN_HORIZONTAL,
      },

      rootMedium: { left: 'auto' },
      absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      },
      sidebar: {
        display: 'flex',
        width: 92,
        top: 32,
        right: 0,
        bottom: 200,
        flexDirection: 'column',
        position: 'absolute',
        alignItems: 'center',
        zIndex: 1,
        backgroundColor: 'transparent',
        ...Platform.select({
          web: {
            pointerEvents: 'auto',
          },
        }),
      },

      sidebarOpen: {
        top: 0,
        backgroundColor: 'transparent',
      },

      detailsWrapper: {
        position: 'relative',
        right: 0,
        top: 0,
        bottom: 0,
        left: 0,

        ...Platform.select({
          web: {
            pointerEvents: 'auto',
          },
        }),
      },

      detailsWrapperMedium: {
        left: 'auto',
        width: 420,
        right: 92,
      },

      background: {
        backgroundColor: Colors.BLACK,
        position: 'absolute',
        top: -33,
        bottom: -32,
        left: 0,
        right: 0,
      },

      control: { marginTop: 0 },

      controlOpen: { marginTop: 32 },

      controlMedium: { marginTop: 200 },

      closeIcon: {
        fontSize: 30,
        color: Colors.WHITE,
      },

      tooltipIcon: {
        marginBottom: 88,
      },

      detailsIcon: {
        fontSize: 58,
        color: Colors.WHITE,
        marginLeft: -16,
        marginBottom: 4,
      },

      details: {
        position: 'relative',
        paddingTop: 48,
        flex: 1,
        justifyContent: 'flex-start',
      },

      content: {
        color: Colors.WHITE,
        position: 'relative',
      },

      contentTitle: { marginBottom: 32 },

      button: { marginTop: 32 },

      visible: {
        display: 'flex',
        opacity: 1,
      },

      hidden: { display: 'none' },

      invisible: { opacity: 0 },
    };
  }
}

export default compose(
  withLocalization,
  withDimensions,
  withRouter,
  withGlobalsContext
)(TooltipBar);
