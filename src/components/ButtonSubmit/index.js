import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Animated, Text, TouchableOpacity, View, ViewPropTypes } from 'react-native';

import { Component } from 'components/BaseComponent';

import CustomPropTypes from 'constants/CustomPropTypes';

import { Colors, Duration, TextStyles } from 'theme';

import withAnimation from 'utils/withAnimation';

class ButtonSubmit extends Component {
  static propTypes = {
    activating: CustomPropTypes.animation.isRequired,
    children: PropTypes.node.isRequired,
    disabled: PropTypes.bool,
    style: ViewPropTypes.style,
    type: PropTypes.string, //default or submit
    activeColor: PropTypes.string,
    labelColor: PropTypes.string,
    textSize: PropTypes.number,
    noPadding: PropTypes.bool,
    noAnimation: PropTypes.bool,
    // functions
    onPress: PropTypes.func.isRequired,
    startActivatingTiming: PropTypes.func.isRequired,
  };

  static defaultProps = {
    style: null,
    disabled: false,
    type: 'default',
    activeColor: Colors.GREEN,
    labelColor: Colors.GREEN,
    textSize: 24,
    noPadding: false,
    noAnimation: false,
  };

  _handleLayout = event => {
    const { width } = event.nativeEvent.layout;

    this.setState({ rootWidth: width - 1 });
  };

  componentDidUpdate(prevProps) {
    const { startActivatingTiming, disabled } = this.props;

    if (prevProps.disabled !== disabled) {
      startActivatingTiming({
        toValue: !disabled ? 1 : 0,
        duration: Duration.FAST,
      });
    }
  }

  template(css) {
    const { activating, noPadding, children, onPress, style, disabled } = this.props;

    const label = children.toLowerCase();

    return (
      <TouchableOpacity
        onPress={onPress}
        onMouseEnter={this._handleMouseEnter}
        onMouseLeave={this._handleMouseLeave}
        disabled={disabled}
        style={css(['root', style])}
      >
        <Animated.View
          style={css([
            'container',
            {
              paddingHorizontal: activating.interpolate({
                inputRange: [0, 1],
                outputRange: [noPadding ? 0 : 32, 23],
              }),
            },
          ])}
        >
          <View style={css(['arrow', 'arrowLeft', !disabled && 'arrowActive'])} />
          <Text style={css(['label', !disabled && 'labelActive'])}>{label}</Text>
          <View style={css(['arrow', 'arrowRight', !disabled && 'arrowActive'])} />
        </Animated.View>
      </TouchableOpacity>
    );
  }

  styles() {
    const {
      noPadding,
      type,
      disabled,
      labelColor,
      activeColor,
      textSize,
      noAnimation,
    } = this.props;

    return {
      root: {
        alignSelf: 'center',
        backgroundColor: 'transparent',
        padding: noPadding ? 0 : 8,

        ...Platform.select({
          web: {
            outline: 'none',
          },
        }),
      },

      container: {
        paddingHorizontal: noAnimation ? 10 : 32,
      },

      arrow: {
        position: 'absolute',
        borderLeftColor: Colors.GRAY_DARK,
        borderRightColor: Colors.GRAY_DARK,
        borderTopColor: Colors.TRANSPARENT,
        borderBottomColor: Colors.TRANSPARENT,
        borderTopWidth: 5,
        borderBottomWidth: 5,
        top: 12,
        zIndex: 1,
      },

      arrowActive: {
        borderLeftColor: activeColor,
        borderRightColor: activeColor,
      },

      arrowLeft: {
        borderLeftWidth: 10,
        left: 0,
      },

      arrowRight: {
        borderRightWidth: 10,
        right: 0,
      },

      label: {
        ...TextStyles.button,
        fontSize: textSize,
        color: type == 'submit' && !disabled ? Colors.GREEN : Colors.GRAY_DARK,
        textAlign: 'center',
      },

      labelActive: {
        color: labelColor,
      },
    };
  }
}

export default withAnimation('activating', 0)(ButtonSubmit);
