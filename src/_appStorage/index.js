import AsyncStorage from '@react-native-community/async-storage';

// NOTE:  23/05/19 - Alex | Expondo o objeto Global CrowdStorage para a Aplicação
const createCrowdStorage = async () => {
  
  if (global.CrowdStorage) return;
  const keys = await AsyncStorage.getAllKeys();

  global.CrowdStorage = {};

  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];

    global.CrowdStorage[key] = JSON.parse(await AsyncStorage.getItem(key));
  }

  console.log('# CrowdStorage', global.CrowdStorage)

};

export {createCrowdStorage};

export default APP_STORAGE = {

  init: () => {

    const hasUserOnStorage = global.CrowdStorage.User;
    return !hasUserOnStorage ? APP_STORAGE.setItem('User', { hasLogin: false }) : APP_STORAGE.getItem('User')

  },

  getItem: async (item, withLog = false) => {

    return await AsyncStorage
      .getItem(item)
      .then((response) => !response ? console.log(`- | APP_STORAGE.getItem: ${item} is NULL`) : withLog ? console.log(`- | APP_STORAGE.getItem: ${item}`, JSON.parse(response)) : JSON.parse(response))
      .catch(e => console.error(`- | APP_STORAGE.getItem: ERROR -> when ${item}`, e));

  },

  setItem: async (item, value, callback = false) => {

    return AsyncStorage
      .setItem(item, JSON.stringify(value))
      .then(() => {
        console.log(`- | APP_STORAGE.setItem: ${item} has been CREATED`);
        return callback ? typeof callback === 'function' ? callback() : callback : false;
      })
      .catch(e => console.error(`- | APP_STORAGE.setItem: ERROR -> when ${item}`, e));
  
  },

  updateItem: async (item, value, callback = false) => {

    return AsyncStorage
      .setItem(item, JSON.stringify(value))
      .then(() => {
        console.log(`- | APP_STORAGE.updateItem: ${item} has been UPDATED`)
        return callback ? callback() : false;
      })
      .catch(e => console.error(`- | APP_STORAGE.updateItem: ERROR -> when ${item}`, e));

  },

  deleteItem: async (item) => {
    return await AsyncStorage.removeItem(item).then(console.log(`- | APP_STORAGE.deleteItem: ${item} deleted on Storage`));
  },

  getAll: async (withRegex = false, onlyNamespace = false) => {

    if(withRegex) {

      let 
        getAllKeys = await AsyncStorage.getAllKeys(),
        withFilter = getAllKeys.filter(item => item.match(withRegex));

      if(onlyNamespace) {
        return withFilter
      }

      return await AsyncStorage.multiGet(withFilter)

    } else {
      return await AsyncStorage.getAllKeys().then((response) => console.log(`- | APP_STORAGE.getAll:`, response));
    }

  }, 

  deleteAll: async (withRegex = false, callback = false) => {

    if(withRegex) {
      
      let 
        getAllKeys = await AsyncStorage.getAllKeys(),
        withFilter = getAllKeys.filter(item => item.match(withRegex));

        withFilter.map((item, index) => AsyncStorage.removeItem(item).then(index === (withFilter.length-1) ? callback ? (() => {
          console.log(`- | APP_STORAGE.deleteAll: All itens with REGEX deleted on Storage`) 
          callback()
        })() : console.log(`- | APP_STORAGE.deleteAll: All itens with REGEX deleted on Storage`) : false ))  

    } else {
      
      let getAllKeys = await AsyncStorage.getAllKeys();
  
      getAllKeys.map((item, index) => AsyncStorage.removeItem(item).then(index === (getAllKeys.length-1) ? callback ? (() => {
        console.log(`- | APP_STORAGE.deleteAll: All itens deleted on Storage`)
        callback()
      })() : console.log(`- | APP_STORAGE.deleteAll: All itens deleted on Storage`) : false ))
    
    }

  
  }

}
