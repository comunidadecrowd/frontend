export const euclid = {
  fontFamily: 'Euclid Flex',
  fontWeight: 'normal',
  fontStyle: 'normal',
};

export const interui = {
  fontFamily: 'Inter UI',
  fontWeight: 'normal',
  fontStyle: 'normal',
};

export const iconsfont = {
  fontFamily: 'iconfont',
  fontWeight: 'normal',
  fontStyle: 'normal',
};
