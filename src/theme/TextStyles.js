import { euclid, interui } from './Fonts';

/**
 * To compensate the difference between fontSize and lineHeight that crops the text on iOS, with an
 * extra value to make room for accents of uppercased letters.
 */
const compensateLineHeight = (fontSize, lineHeight) => {
  return { paddingTop: (fontSize - lineHeight) * 3 };
};

export const TextStyles = {
  h1: {
    ...euclid,
    fontSize: 100,
    lineHeight: 120,
    letterSpacing: 0,
  },
  h2: {
    ...euclid,
    fontSize: 66,
    lineHeight: 60,
    letterSpacing: 0,
    ...compensateLineHeight(66, 60),
  },
  h3: {
    ...euclid,
    fontSize: 44,
    lineHeight: 40,
    letterSpacing: 0,
    ...compensateLineHeight(44, 40),
  },
  h4: {
    ...euclid,
    fontSize: 36,
    lineHeight: 33,
    letterSpacing: 0,
    ...compensateLineHeight(36, 33),
  },
  h5: {
    ...euclid,
    fontSize: 24,
    lineHeight: 30,
    letterSpacing: 0,
  },
  h6: {
    ...euclid,
    fontSize: 18,
    lineHeight: 23,
    letterSpacing: 0,
  },
  body: {
    ...interui,
    fontSize: 16,
    lineHeight: 26,
    letterSpacing: 0,
  },
  small: {
    ...interui,
    fontSize: 14,
    lineHeight: 25,
    letterSpacing: 0,
  },
  input: {
    ...interui,
    fontSize: 16,
  },
  code: {
    ...interui,
    fontSize: 20,
  },
  button: {
    ...euclid,
    lineHeight: 30,
    letterSpacing: 0,
  },
  p: {
    fontFamily: 'Inter UI',
    fontSize: 14,
    color: '#fff',
  },
};
