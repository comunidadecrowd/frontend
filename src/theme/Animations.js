export const Duration = {
  INSTANT: 0,
  SLOWER: 800,
  SLOW: 600,
  MEDIUM: 400,
  FAST: 200,
};

export const Ease = {
  DEFAULT: 'cubic-bezier(.25, .1, .25, 1)',
  HARD: 'cubic-bezier(.5,0,.1,1)',
  LINEAR: 'linear',
};
