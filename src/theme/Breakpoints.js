export const SMALL = 'Small';
export const MEDIUM = 'Medium';
export const LARGE = 'Large';

export const Breakpoints = [49, 78];

export const breakpoints = {
  [MEDIUM]: Breakpoints[0],
  [LARGE]: Breakpoints[1],
};
