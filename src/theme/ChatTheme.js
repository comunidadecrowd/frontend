/* eslint-disable */
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

import { IconArrows } from 'components/Icon/IconArrows';

export class Main extends Component {
  render() {
    return (
      <View
        {...this.props}
        style={[
          {
            flex: 1,
            backgroundColor: this.props.theme === 'black' ? '#000' : '#fff',
          },
          this.props.style,
        ]}
      >
        {this.props.children}
      </View>
    );
  }
}

export const H6 = props => {
  return (
    <Text
      {...props}
      style={[
        TextStyles.h6,
        {
          color: props.color ? props.color : TextStyles.h6.color,
          fontSize: props.size ? props.size : TextStyles.h6.fontSize,
          textAlign: props.center ? 'center' : undefined,
        },
        props.style,
      ]}
    >
      {props.upper ? props.children.toUpperCase() : props.children}
    </Text>
  );
};

export const H5 = props => {
  return (
    <Text
      {...props}
      style={[
        TextStyles.h5,
        {
          color: props.color ? props.color : TextStyles.h5.color,
          fontSize: props.size ? props.size : TextStyles.h5.fontSize,
          fontWeight: props.medium ? '600' : TextStyles.h5.fontWeight,
          textAlign: props.center ? 'center' : undefined,
        },
        props.style,
      ]}
    >
      {props.upper ? props.children.toUpperCase() : props.children}
    </Text>
  );
};

export const H3E = props => {
  return (
    <Text
      {...props}
      style={[
        TextStyles.h3E,
        {
          color: props.color ? props.color : TextStyles.h3E.color,
          fontSize: props.size ? props.size : TextStyles.h3E.fontSize,
          textAlign: props.center ? 'center' : undefined,
        },
        props.style,
      ]}
    >
      {props.upper ? props.children.toUpperCase() : props.children}
    </Text>
  );
};

export const P = props => {
  return (
    <Text
      {...props}
      style={[
        TextStyles.p,
        {
          color: props.color ? props.color : TextStyles.p.color,
          fontSize: props.size ? props.size : TextStyles.p.fontSize,
          textTransform: props.upper ? 'uppercase' : undefined,
          fontWeight: props.medium ? '600' : undefined,
          textAlign: props.center ? 'center' : undefined,
        },
        props.style,
      ]}
    >
      {props.upper ? props.children.toUpperCase() : props.children}
    </Text>
  );
};

export const Span = props => {
  return (
    <Text
      {...props}
      style={[
        TextStyles.span,
        {
          color: props.color ? props.color : TextStyles.span.color,
          fontSize: props.size ? props.size : TextStyles.span.fontSize,
          textAlign: props.center ? 'center' : undefined,
        },
        props.style,
      ]}
    >
      {props.upper ? props.children.toUpperCase() : props.children}
    </Text>
  );
};

export const Button = props => {
  const { button } = styles;
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={[button, { backgroundColor: props.bgColor || '#000' }, props.style]}
    >
      <IconArrows width={12} height={13} color={props.color} />
      <P
        upper={props.upper}
        size={props.fontSize || 24}
        color={props.color}
        style={{ marginHorizontal: 10 }}
      >
        {props.children}
      </P>
      <IconArrows width={12} height={13} color={props.color} inverted />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    width: '100%',
    height: 30,
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const TextStyles = {
  h6: {
    fontFamily: 'Euclid Flex',
    fontSize: 18,
    color: '#fff',
  },
  h5: {
    fontFamily: 'Inter UI',
    fontSize: 14,
    fontWeight: '900',
    color: '#fff',
  },
  h3E: {
    fontFamily: 'Euclid Flex',
    fontSize: 24,
    fontWeight: '600',
    color: '#fff',
  },
  p: {
    fontFamily: 'Inter UI',
    fontSize: 14,
    color: '#fff',
  },
  span: {
    fontFamily: 'Inter UI',
    fontSize: 12,
    color: '#4A4A4A',
  },
};