export const euclid = {
  fontFamily: 'Euclid Flex, sans-serif',
  fontWeight: 'normal',
  fontStyle: 'normal',
};

export const interui = {
  fontFamily: 'Inter UI, sans-serif',
  fontWeight: 'normal',
  fontStyle: 'normal',
};

export const iconsfont = {
  fontFamily: 'iconfont',
  fontWeight: 'normal',
  fontStyle: 'normal',
};
