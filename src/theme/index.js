import { Duration, Ease } from './Animations';
import { Breakpoints, breakpoints, LARGE, MEDIUM, SMALL } from './Breakpoints';
import { Colors } from './Colors';
import { interui, euclid, iconsfont } from './Fonts';
import { FontRatioNormalize } from './FontRatioNormalize';
import { TextStyles } from './TextStyles';

module.exports = {
  // Animations.js
  Duration,
  Ease,

  // Breakpoints.js
  Breakpoints,
  breakpoints,
  LARGE,
  MEDIUM,
  SMALL,

  // Colors.js
  Colors,

  // Fonts.js
  FontRatioNormalize,
  interui,
  euclid,
  iconsfont,

  // TextStyles.js
  TextStyles,
};
