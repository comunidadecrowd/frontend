import i18n from 'i18next';

import content, { NAMESPACE } from 'localization';

i18n.init({
  fallbackLng: 'pt-BR',
  resources: {
    ...content,
    ns: [NAMESPACE],
    defaultNS: NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
  },
});

export default i18n;
