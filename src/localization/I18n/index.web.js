import i18next from 'i18next';
import lngDetector from 'i18next-browser-languagedetector';

import content, { NAMESPACE } from 'constants/Localization';

i18next.use(lngDetector).init({
  fallbackLng: 'pt-BR',
  resources: {
    ...content,
    ns: [NAMESPACE],
    defaultNS: NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
  },
});

export default i18next;
