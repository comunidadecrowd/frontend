import React from 'react';
import { View, Platform, StatusBar, SafeAreaView } from 'react-native';

import { Navigation } from 'react-native-navigation';
import Routes from './Routes';

import InitPages from 'pages';
import Welcome from 'pages/Welcome';
import AndroidSplashScreen from 'pages/AndroidSplashScreen';
import Login, { Legacy, Register } from 'pages/Login';
import Chat, { Channel, Channels } from 'pages/Chat';
import Briefing, { Category, SubCategory, Publish } from 'pages/Briefing';
import Account, { MyProfile, MyPreferences, MySettings, MyCredentials, NetworkingAccounts } from 'pages/Settings';

import Twilio, { TwilioContext } from 'services/Twilio';

import StatusToast from 'components/StatusToast';
import { isIphoneX } from 'utils/Helpers';

const statusBarHeight = Platform.OS === 'ios' ? isIphoneX ? 40 : 20 : StatusBar.currentHeight;

export function RegisterPages() {

  Navigation.registerComponent(Routes.ANDROIDSPLASHSCREEN, () => createPage(AndroidSplashScreen));

  Navigation.registerComponent(Routes.WELCOME, () => createPage(Welcome));

  Navigation.registerComponent(Routes.LOGIN._, () => createPage(Login));
  Navigation.registerComponent(Routes.LOGIN.REGISTER, () => createPage(Register));
  Navigation.registerComponent(Routes.LOGIN.LEGACY, () => createPage(Legacy));

  Navigation.registerComponent(Routes.CHAT._, () => createPage(Chat, true));
  Navigation.registerComponent(Routes.CHAT.CHANNEL, () => createPage(Channel, true));
  Navigation.registerComponent(Routes.CHAT.CHANNELS, () => createPage(Channels));

  Navigation.registerComponent(Routes.BRIEFING._, () => createPage(Briefing));
  Navigation.registerComponent(Routes.BRIEFING.CATEGORY, () => createPage(Category));
  Navigation.registerComponent(Routes.BRIEFING.SUBCATEGORY, () => createPage(SubCategory));
  Navigation.registerComponent(Routes.BRIEFING.PUBLISH, () => createPage(Publish));

  Navigation.registerComponent(Routes.SETTINGS._, () => createPage(Account));

  Navigation.registerComponent(Routes.SETTINGS.MY_PROFILE, () => createPage(MyProfile));

  Navigation.registerComponent(Routes.SETTINGS.MY_PREFERENCES, () => createPage(MyPreferences));

  Navigation.registerComponent(Routes.SETTINGS.MY_SETTINGS, () => createPage(MySettings));

  Navigation.registerComponent(Routes.SETTINGS.MY_CREDENTIALS, () => createPage(MyCredentials));

  Navigation.registerComponent(Routes.SETTINGS.NETWORKING_ACCOUNTS, () => createPage(NetworkingAccounts));

}

const createPage = (Component, withTwilio = false) => {

  class EnhancedApp extends React.Component {

    constructor(props) {
      super(props);
      Navigation.events().bindComponent(this);
    }

    render() {

      const { componentId, fromNavigation } = this.props;

      return (
        <>
          {
            isIphoneX ? (
              <SafeAreaView style={{ flex: 1, backgroundColor: "rgba(0,0,0,0)" }}>
                <StatusToast statusBarHeight={statusBarHeight} componentId={componentId} withTwilio={withTwilio} fromNavigation={fromNavigation} />
                <View style={{ flex: 1, backgroundColor: '#000' }}>
                  <InitPages componentId={componentId}>
                    {
                      withTwilio ? (
                        <TwilioContext.Provider value={new Twilio(fromNavigation)}>
                          <Component {...this.props} />
                        </TwilioContext.Provider>
                      ) : (
                          <Component {...this.props} />
                        )
                    }
                  </InitPages>
                </View>
              </SafeAreaView>
            ) : (
                <>
                  <StatusToast statusBarHeight={statusBarHeight} componentId={componentId} withTwilio={withTwilio} fromNavigation={fromNavigation} />
                  <View style={{ flex: 1, backgroundColor: '#000' }}>
                    <InitPages componentId={componentId}>
                      {
                        withTwilio ? (
                          <TwilioContext.Provider value={new Twilio(fromNavigation)}>
                            <Component {...this.props} />
                          </TwilioContext.Provider>
                        ) : (
                            <Component {...this.props} />
                          )
                      }
                    </InitPages>
                  </View>
                </>
              )
          }
        </>
      );

    }

  }

  return EnhancedApp;

};
