export const publicPath = process.env.PUBLIC_PATH || '';

const Routes = {
  
  ANDROIDSPLASHSCREEN: `${publicPath}/androidsplashscreen`,
  
  WELCOME: `${publicPath}/welcome`,

  LOGIN: {
    _: `${publicPath}/login`,
    REGISTER: `${publicPath}/login/register`,
    LEGACY: `${publicPath}/login/legacy`,
  },
  
  CHAT: {
    _: `${publicPath}/chat`,
    CHANNEL: `${publicPath}/chat/channel`,
    CHANNELS: `${publicPath}/chat/channels`
  } ,

  BRIEFING: {
    _: `${publicPath}/briefing`,
    CATEGORY: `${publicPath}/briefing/:category`,
    SUBCATEGORY: `${publicPath}/briefing/:category/:subcategory`,
    PUBLISH: `${publicPath}/briefing/publish`,
  },

  SETTINGS: {
    _: `${publicPath}/settings`,
    MY_PROFILE: `${publicPath}/settings/myProfile`,
    MY_PREFERENCES: `${publicPath}/settings/myPreferences`,
    MY_SETTINGS: `${publicPath}/settings/mySettings`,
    MY_CREDENTIALS: `${publicPath}/settings/myCredentials`,
    NETWORKING_ACCOUNTS: `${publicPath}/settings/networkingAccounts`,
    FORGOT_PASSWORD: `${publicPath}/forgot-password`,
  }

};

export default Routes;
