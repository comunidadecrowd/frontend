import { Colors } from 'theme'; // TODO:  Unificar todos os itens de Tema em um lugar só

import { Navigation } from 'react-native-navigation';
import { RegisterPages } from './RegisterPages';
import Routes from './Routes';
import { navigateFromTo } from 'utils/navigateFromTo';
import EventEmitter from 'events';

import APP_STORAGE from 'APP_STORAGE';

export async function initApp(initContext) {

  global.emitters = new EventEmitter();
  
  RegisterPages();

  // Subscribe
  const commandListener = Navigation.events().registerCommandListener(async (name, params) => {
    if ( name === 'popTo' && params.componentId === '/chat' ) {
      let getUserOnStorage = await APP_STORAGE.getItem('User');
      if (getUserOnStorage.userID && getUserOnStorage.name && getUserOnStorage.title && getUserOnStorage.category)
        return navigateFromTo(Routes.BRIEFING.PUBLISH, Routes.CHAT._, null, {
          // alreadyImportedContacts: getUserOnStorage.alreadyImportedContacts, // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
          userID: getUserOnStorage.userID
        });
    }
  });

  Navigation.events().registerAppLaunchedListener(async () => {

    Navigation.setDefaultOptions({
      layout: {
        backgroundColor: '#000'
      },
      statusBar: {
        visible: true,
        style: 'light',
        blur: false
      },
      bottomTabs: {
        backgroundColor: '#000'
      },
      bottomTab: {
        selectedIconColor: Colors.BLUE
      },
      topBar: {
        visible: false
      },
      animations: {
        setRoot: {
          waitForRender: true,
        },
        pop: {
          enabled: true,
          waitForRender: true
        },
        push: {
          enabled: true,
          waitForRender: true
        }
      }
    })

    if(initContext === 'needLogin') {

      Navigation.setRoot({
        root: {
          stack: {
            children: [
              {
                component: {
                  name: Routes.WELCOME, 
                  id: Routes.WELCOME,
                  options: {
                    bottomTabs: {
                      visible: false,
                    }
                  }
                }
              }
            ],
          }
        }
      })

    } else if(initContext === 'needRegister') {
      
      Navigation.setRoot({
          root: {
            stack: {
              children: [
                {
                  component: {
                    name: Routes.LOGIN.REGISTER,
                    id: Routes.LOGIN.REGISTER,
                    options: {
                      bottomTabs: {
                        visible: false,
                      }
                    }
                  }
                }
              ],
            }
          }
        })

    } else {

      Navigation.setRoot({
        root: {
          bottomTabs: {
            children: [
              {
                stack: {
                  children: [
                    {
                      component: {
                        name: Routes.CHAT._,
                        id: Routes.CHAT._,
                        options: {
                          bottomTab: {
                            icon: require('./assets/icons/bottom_chat.png'),
                            iconInsets: { top: 7, left: 0, bottom: -7, right: 0 }
                          }
                        }
                      }
                    }
                  ]
                }
              },
              {
                stack: {
                  children: [
                    {
                      component: {
                        name: Routes.BRIEFING._,
                        id: Routes.BRIEFING._,
                        options: {
                          bottomTab: {
                            icon: require('./assets/icons/bottom_briefing.png'),
                            iconInsets: { top: 5, left: 0, bottom: -5, right: 0 }
                          }
                        }
                      }
                    }
                  ]
                }
              },
              {
                stack: {
                  children: [
                    {
                      component: {
                        name: Routes.SETTINGS.MY_PROFILE,
                        id: Routes.SETTINGS.MY_PROFILE,
                        options: {
                          bottomTab: {
                            icon: require('./assets/icons/bottom_settings.png'),
                            iconInsets: { top: 7, left: 0, bottom: -7, right: 0 }
                          },
                          statusBar: {
                            style: 'dark'
                          }
                        }
                      }
                    }
                  ]
                }
              }
            ]
          }
        },
      });
    
    }

  });

};
