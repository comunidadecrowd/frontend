import { emToPx } from './number';

describe('emToPx', () => {
  test('Should return 768px if 48em is set', () => {
    expect(emToPx(48)).toEqual(768);
  });
});
