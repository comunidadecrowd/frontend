import AsyncStorage from '@react-native-community/async-storage';
import { APP_NAME, AUTH_TOKEN } from 'constants/App';

export const saveToken = token =>
  new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.setItem(`@${APP_NAME}:${AUTH_TOKEN}`, token);
      resolve();
    } catch (error) {
      reject(error.message);
    }
  });

export const deleteToken = () =>
  new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.removeItem(`@${APP_NAME}:${AUTH_TOKEN}`);
      resolve();
    } catch (error) {
      reject(error.message);
    }
  });

export const getToken = () =>
  new Promise(async (resolve, reject) => {
    try {
      const token = await AsyncStorage.getItem(`@${APP_NAME}:${AUTH_TOKEN}`);

      resolve(token);
    } catch (error) {
      reject(error.message);
    }
  });

export const isSignedIn = async () => {
  try {
    await AsyncStorage.getItem(`@${APP_NAME}:${AUTH_TOKEN}`);
  } catch (error) {
    // Error retrieving data
  }
};
