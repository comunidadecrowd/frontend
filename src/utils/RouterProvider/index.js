import { Platform } from 'react-native';
import { NativeRouter } from 'react-router-native';
import { BrowserRouter } from 'react-router-dom';

const isWeb = Platform.OS === 'web';

export default (isWeb ? BrowserRouter : NativeRouter);
