import { Navigation } from 'react-native-navigation';

export const navigateFromTo = (routeFrom, routeTo, customOptions = null, passProps = null, bottomTabsVisible = false, topBarVisible = false) => {
  
  Navigation.push(routeFrom, {
    component: {
      name: routeTo,
      id: routeTo,
      options: customOptions ? customOptions : {
        bottomTabs: {
          visible: bottomTabsVisible,
        },
        topBar: {
          visible: topBarVisible,
        },
      },
      passProps
    }
  });

}