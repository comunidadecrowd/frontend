/* eslint-disable */
// @flow

/**
 * This HOC is an animation helper. It creates an animation instance that can be used by the components
 * without it needing to deal with animation states and clearings listeners.
 */

import React, { Component } from 'react';
import { Animated, Easing } from 'react-native';

import { capitalize } from './string';

export default function animate(
  animationName: string = 'animating',
  initialValue: number = 0
): Function {
  return WrappedComponent =>
    class AnimateHOC extends Component<void, { animatedValue: any }> {
      listeners: Array<mixed>;

      constructor(props: any) {
        super(props);

        this.state = {
          animatedValue: new Animated.Value(initialValue),
        };

        this.listeners = [];
      }

      animationBase(
        animationFunction: Function,
        animationProperties: any,
        callback?: Function
      ): Animated.Animation {
        const { animatedValue } = this.state;

        const callBackToString = callback
          ? callback
              .toString()
              .replace(/\n/g, '')
              .replace(/\s{2}/g, ' ')
          : null;

        // Prevent from duplicating listeners
        if (callBackToString && !this.listeners.includes(callBackToString)) {
          animatedValue.addListener(callback);
          this.listeners = [...this.listeners, callBackToString];
        }

        return animationFunction(animatedValue, animationProperties);
      }

      timingAnimation(
        toValue: number = 1,
        duration: number = 500,
        delay: number = 0,
        callback?: Function,
        easing: Function = Easing.linear
      ): Animated.Animation {
        return this.animationBase(
          Animated.timing,
          {
            delay,
            duration,
            easing,
            toValue,
          },
          callback
        );
      }

      springAnimation(
        toValue: number = 1,
        friction: number = 7,
        tension: number = 40,
        callback?: Function
      ): Animated.Animation {
        return this.animationBase(
          Animated.spring,
          {
            friction,
            tension,
            toValue,
          },
          callback
        );
      }

      stopAnimations() {
        const { animatedValue } = this.state;

        animatedValue.stopAnimation();
      }

      resetAnimations() {
        const { animatedValue } = this.state;

        animatedValue.resetAnimation();
      }

      componentWillUnmount() {
        const { animatedValue } = this.state;

        animatedValue.removeAllListeners();
        this.stopAnimations();
      }

      render() {

        const { animatedValue } = this.state;

        const animationProps = {};

        animationProps[animationName] = animatedValue;

        type timingProps = {
          toValue: number,
          duration?: number,
          delay?: number,
          callback?: Function,
          onFinish?: Function,
          easing?: Function,
        };

        animationProps[`${animationName}Timing`] = ({
          toValue,
          duration,
          delay,
          callback,
          easing,
        }): timingProps => this.timingAnimation(toValue, duration, delay, callback, easing);

        animationProps[`start${capitalize(animationName)}Timing`] = ({
          toValue,
          duration,
          delay,
          callback,
          onFinish,
          easing,
        }): timingProps =>
          animationProps[`${animationName}Timing`]({
            callback,
            delay,
            duration,
            easing,
            toValue,
          }).start(
            onFinish
              ? cb =>
                  onFinish({
                    ...cb,
                    value: toValue,
                  })
              : null
          );

        type springProps = {
          toValue: number,
          friction?: number,
          tension?: number,
          callback?: Function,
          onFinish?: Function,
        };

        animationProps[`${animationName}Spring`] = ({
          toValue,
          friction,
          tension,
          callback,
        }): springProps => this.springAnimation(toValue, friction, tension, callback);

        animationProps[`start${capitalize(animationName)}Spring`] = ({
          toValue,
          friction,
          tension,
          callback,
          onFinish,
        }): springProps =>
          animationProps[`${animationName}Spring`]({
            callback,
            friction,
            tension,
            toValue,
          }).start(
            onFinish
              ? cb =>
                  onFinish({
                    ...cb,
                    value: toValue,
                  })
              : null
          );

        animationProps[`stop${capitalize(animationName)}`] = () => this.stopAnimations();
        animationProps[`reset${capitalize(animationName)}`] = () => this.resetAnimations();

        return <WrappedComponent {...animationProps} {...this.props} />;
      }
    };
}
