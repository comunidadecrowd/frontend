// @flow
import { AsYouType } from 'libphonenumber-js';

// Convert em value into a pixel value
export const emToPx = (value: number): number => value * 16;

export const formatPhone = (countryCode: string) => {
  const phoneMask = new AsYouType(countryCode.toUpperCase());

  return (str: string) => {
    const clean = str.replace(/[^\d]+/gi, '');
    const r = phoneMask.input(clean);

    phoneMask.reset();

    return r;
  };
};
