import { createContext } from 'react';

const MessagesContextApi = createContext();

export { MessagesContextApi }