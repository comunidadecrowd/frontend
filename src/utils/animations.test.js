import { lerpColor } from './animations';

describe('lerpColor', () => {
  test('Should return black if delta is 0', () => {
    const input = lerpColor('#000000', '#FFFFFF', 0);

    expect(input).toEqual('#000000');
  });

  test('Should return white if delta is 1', () => {
    const input = lerpColor('#000000', '#FFFFFF', 1);

    expect(input).toEqual('#FFFFFF');
  });

  test('Should return medium gray if delta is 0.5', () => {
    const input = lerpColor('#000000', '#FFFFFF', 0.5);

    expect(input).toEqual('#7F7F7F');
  });

  test('Should return null in case of short hexadecimals', () => {
    const input = lerpColor('#000', '#FFF', 0.5);

    expect(input).toBeNull();
  });
});
