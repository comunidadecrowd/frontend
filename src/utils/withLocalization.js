import { translate } from 'react-i18next';
export default WrappedComponent => translate('ns')(WrappedComponent);