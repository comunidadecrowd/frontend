import { Dimensions } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const removeAllKeysOnStorage = allKeys =>
  allKeys.map(item => AsyncStorage.removeItem(item).then(() => console.log(`- | ${item} removed`)));

const setScrollHeight = (setOfComponentes = []) => {
  const { height } = Dimensions.get('window');
  return setOfComponentes.length >= 1
    ? height - setOfComponentes.reduce((sum, cur) => sum + cur)
    : height;
};

const truncateText = (textToChange, numberToMax) => {
  let changeText = textToChange.substring(0, numberToMax - 3),
    newText = changeText + '...';
  if (textToChange.length <= numberToMax) {
    return textToChange;
  }
  return newText;
};

export { removeAllKeysOnStorage, setScrollHeight, truncateText };
