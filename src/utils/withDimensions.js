/* eslint-disable */
// @flow

/**
 * This HOC adds breakpoint and window width and height as props and watch for their changes.
 */

import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import { debounce } from 'lodash';

import { breakpoints, SMALL, MEDIUM, LARGE } from '../theme';

import { emToPx } from './number';

export default (WrappedComponent: any) => {
  class WithDimensions extends Component<void> {
    _getBreakpoint: Function;
    _setWindowWidth: Function;
    windowWidth: number;
    windowHeight: number;
    breakpoint: null | SMALL | MEDIUM | LARGE;

    constructor(props: any) {
      super(props);
      const { width, height } = Dimensions.get('window');

      this.breakpoint = this._getBreakpoint();
      this.windowWidth = width;
      this.windowHeight = height;

      this._setWindowWidth = debounce(this._setWindowWidth.bind(this), 10);
      this._getBreakpoint = this._getBreakpoint.bind(this);
    }

    _getBreakpoint() {
      const { width } = Dimensions.get('window');

      let breakpoint;

      if (width >= emToPx(breakpoints[LARGE])) {
        breakpoint = LARGE;
      } else if (width >= emToPx(breakpoints[MEDIUM])) {
        breakpoint = MEDIUM;
      } else {
        breakpoint = SMALL;
      }

      return breakpoint;
    }

    _setWindowWidth() {
      const { width, height } = Dimensions.get('window');

      this.breakpoint = this._getBreakpoint();
      this.windowWidth = width;
      this.windowHeight = height;
      this.forceUpdate();
    }

    componentDidMount() {
      if (super.componentDidMount) super.componentDidMount();
      Dimensions.addEventListener('change', this._setWindowWidth);
    }

    componentWillUnmount() {
      if (super.componentWillUnmount) super.componentWillUnmount();
      Dimensions.removeEventListener('change', this._setWindowWidth);
    }

    render() {
      return (
        <WrappedComponent
          from="whitDimensions"
          breakpoint={this.breakpoint}
          windowWidth={this.windowWidth}
          windowHeight={this.windowHeight}
          {...this.props}
        />
      );
    }
  }

  return WithDimensions;
};
