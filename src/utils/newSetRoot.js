import { Navigation } from 'react-native-navigation';
import Routes from 'Routes';

export const newSetRoot = (passProps = null) => {

  const icons = {
    chat: require('../assets/icons/bottom_chat.png'),
    briefing: require('../assets/icons/bottom_briefing.png'),
    settings: require('../assets/icons/bottom_settings.png')
  }

  return (
    Navigation.setRoot({
      root: {
        bottomTabs: {
          children: [
            {
              stack: {
                children: [
                  {
                    component: {
                      name: Routes.CHAT._,
                      id: Routes.CHAT._,
                      passProps: {
                        ...passProps,
                        fromNavigation: true
                      },
                      options: {
                        bottomTab: {
                          icon: icons.chat,
                          iconInsets: { top: 7, left: 0, bottom: -7, right: 0 }
                        }
                      }
                    }
                  }
                ]
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: Routes.BRIEFING._,
                      id: Routes.BRIEFING._,
                      passProps: {
                        ...passProps,
                        fromNavigation: true
                      },
                      options: {
                        bottomTab: {
                          icon: icons.briefing,
                          iconInsets: { top: 5, left: 0, bottom: -5, right: 0 }
                        }
                      }
                    }
                  }
                ]
              }
            },
            {
              stack: {
                children: [
                  {
                    component: {
                      name: Routes.SETTINGS.MY_PROFILE,
                      id: Routes.SETTINGS.MY_PROFILE,
                      passProps: {
                        ...passProps,
                        fromNavigation: true
                      },
                      options: {
                        bottomTab: {
                          icon: icons.settings,
                          iconInsets: { top: 7, left: 0, bottom: -7, right: 0 }
                        },
                        statusBar: {
                          style: 'dark'
                        }
                      }
                    }
                  }
                ]
              }
            }
          ]
        }
      }
    })
  )
}
