import { Dimensions, PixelRatio, Platform } from 'react-native';
import { Colors } from 'theme';

export const categoryColor = category => {
  switch (category) {
    case 'technology':
      return Colors.TECHNOLOGY;
      break;
    case 'media':
      return Colors.MEDIA;
      break;
    case 'design':
      return Colors.DESIGN;
      break;
    case 'content':
      return Colors.CONTENT;
      break;
    default:
      return Colors.GRAY_DARK;
      break;
  }
};

const { width, height } = Dimensions.get('window');
export const isSmallIOS = (Platform.OS === 'ios' && PixelRatio.get() === 2 && width === 320 && height === 568)

export const isIphoneX = (() => {

  /** 
   * IphoneX Sizes
   * 
   * X / XS = { pixelRatio: 3, width: 375, height: 812 }
   * XS MAX = { pixelRatio: 3, width: 414, height: 896 }
   * XR = { pixelRatio: 2, width: 414, height: 896 }
  */

  return (Platform.OS === 'ios' && PixelRatio.get() >= 2 && width >= 375 && height >= 812)

})()

export const API_URL = __DEV__ ? 'https://hm.crowd.br.com/v1/' : 'https://v1.crowd.br.com/';
export const STORAGE_URL = `https://storage.googleapis.com/crowd-${__DEV__ ? 'hm' : 'producao'}`;