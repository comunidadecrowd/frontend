// @flow

import { isYesterday, isToday, format } from 'date-fns';

export function distanceToNow(date: string, t: Function): string {
  if (isYesterday(date)) return t('common:YESTERDAY');

  if (isToday(date)) {
    return format(date, 'HH:MM');
  }

  return format(date, 'DD/MM');
}
