// @flow

export function lerpColor(a: string, b: string, amount: number): string | null {
  if (a.length < 7 || b.length < 7) return null;

  /* eslint-disable no-mixed-operators, no-bitwise */
  const ah = parseInt(a.replace(/#/g, ''), 16);
  const ar = ah >> 16;
  const ag = (ah >> 8) & 0xff;
  const ab = ah & 0xff;
  const bh = parseInt(b.replace(/#/g, ''), 16);
  const br = bh >> 16;
  const bg = (bh >> 8) & 0xff;
  const bb = bh & 0xff;
  const rr = ar + Math.min(1, amount) * (br - ar);
  const rg = ag + Math.min(1, amount) * (bg - ag);
  const rb = ab + Math.min(1, amount) * (bb - ab);

  return (
    '#' +
    (((1 << 24) + (rr << 16) + (rg << 8) + rb) | 0)
      .toString(16)
      .toUpperCase()
      .slice(1)
  ); // eslint-disable-line prefer-template
  /* eslint-enable no-mixed-operators, no-bitwise */
}
