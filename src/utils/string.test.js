import { capitalize } from './string';

describe('capitalize', () => {
  test('Should return a string with first char capitalized', () => {
    expect(capitalize('crowd')).toEqual('Crowd');
  });
});
