import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes } from 'react-native';
import Svg, { Path, Polygon, G } from 'react-native-svg';

import CustomPropTypes from 'constants/CustomPropTypes';

import { Colors } from 'theme/Colors';

const LogoShort = ({ style, width, height, color }) => (
  <View style={style}>
    <Svg width={width} height={height}>
      <G transform="translate(-30.000000, -36.000000)" fill={color}>
        <G transform="translate(30.000000, 36.000000)">
          <Path d="M29.039,0.9614 C29.494,1.0924 29.867,1.2524 30.16,1.4404 C30.452,1.6264 30.71,1.8134 30.935,2.0004 C31.16,2.1884 31.322,2.3464 31.42,2.4764 L31.574,2.6764 L30.27,3.9814 C30.243,3.9444 30.206,3.8944 30.16,3.8304 C30.113,3.7654 30.008,3.6554 29.845,3.4994 C29.682,3.3434 29.505,3.2054 29.313,3.0884 C29.122,2.9704 28.87,2.8604 28.558,2.7604 C28.245,2.6594 27.926,2.6094 27.601,2.6094 C26.705,2.6094 25.956,2.9114 25.354,3.5124 C24.752,4.1144 24.451,4.8644 24.451,5.7604 C24.451,6.6554 24.752,7.4054 25.354,8.0064 C25.956,8.6084 26.705,8.9104 27.601,8.9104 C27.948,8.9104 28.282,8.8624 28.602,8.7664 C28.919,8.6704 29.184,8.5574 29.393,8.4264 C29.604,8.2944 29.789,8.1614 29.949,8.0264 C30.109,7.8944 30.226,7.7764 30.296,7.6804 L30.404,7.5394 L31.641,8.8434 C31.604,8.8974 31.551,8.9664 31.477,9.0544 C31.403,9.1414 31.241,9.2944 30.989,9.5154 C30.737,9.7364 30.467,9.9304 30.18,10.0984 C29.892,10.2644 29.516,10.4184 29.049,10.5564 C28.583,10.6934 28.101,10.7634 27.601,10.7634 C26.139,10.7634 24.904,10.2744 23.899,9.2944 C22.893,8.3164 22.391,7.1374 22.391,5.7604 C22.391,4.3824 22.893,3.2044 23.899,2.2294 C24.904,1.2524 26.139,0.7634 27.601,0.7634 C28.104,0.7634 28.585,0.8304 29.039,0.9614" />
          <Polygon points="0.1552 0.9683 8.4612 5.7643 0.1552 10.5583" />
          <Polygon points="54.6469 0.9683 46.3399 5.7643 54.6469 10.5583" />
        </G>
      </G>
    </Svg>
  </View>
);

LogoShort.propTypes = {
  color: CustomPropTypes.color,
  height: PropTypes.number,
  style: ViewPropTypes.style,
  width: PropTypes.number,
};

LogoShort.defaultProps = {
  color: Colors.WHITE,
  height: 11,
  style: null,
  width: 55,
};

export default LogoShort;
