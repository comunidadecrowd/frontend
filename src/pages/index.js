import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { I18nextProvider } from 'react-i18next';
import i18n from 'localization/I18n';
import RouterProvider from 'utils/RouterProvider';
import Theme from 'components/Theme';
import { ThemeContext } from 'components/ThemeProvider';
import { THEME_BLACK } from 'constants/Themes';
import { Colors } from 'theme';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.WHITE,
  },

  containerBlack: { backgroundColor: Colors.BLACK },
});

const InitPages = ({ componentId, children }) => {
  return (
    // TODO:  Alex -> Rever a necessidades desses RouterProvider, Theme, ThemeContext.Consumer
    <I18nextProvider i18n={i18n}>
      <RouterProvider>
        <Theme componentId={componentId}>
          <ThemeContext.Consumer>
            {({ theme }) => (
              <View style={[styles.container, theme === THEME_BLACK && styles.containerBlack]}>
                {children}
              </View>
            )}
          </ThemeContext.Consumer>
        </Theme>
      </RouterProvider>
    </I18nextProvider>
  );
}


InitPages.propTypes = {
  children: PropTypes.node.isRequired,
  componentId: PropTypes.string.isRequired,
};

export default React.memo(InitPages);
