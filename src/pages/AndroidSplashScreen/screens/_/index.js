import React, { PureComponent } from 'react';
import APP_STORAGE from 'APP_STORAGE';
import splashScreen from './splash.png';

import { Navigation } from 'react-native-navigation';
import Routes from '../../../../Routes';

import { 
  _Container,
  _Splash
} from './styles';

class AndroidSplashScreen extends PureComponent {

  async componentDidMount() {

    const getInit = await APP_STORAGE.init().then(res => res)

    this.timeout = setTimeout(() => {
    
      if(!getInit || !getInit.hasLogin) {

        Navigation.setRoot({
          root: {
            stack: {
              children: [
                {
                  component: {
                    name: Routes.WELCOME, 
                    id: Routes.WELCOME,
                    options: {
                      bottomTabs: {
                        visible: false,
                      }
                    }
                  }
                }
              ]
            }
          }
        })

      } else if (getInit.hasLogin && !getInit.hasRegister) {

        Navigation.setRoot({
          root: {
            stack: {
              children: [
                {
                  component: {
                    name: Routes.LOGIN.REGISTER,
                    id: Routes.LOGIN.REGISTER,
                    options: {
                      bottomTabs: {
                        visible: false,
                      }
                    }
                  }
                }
              ],
            }
          }
        })

      } else {

        Navigation.setRoot({
          root: {
            bottomTabs: {
              children: [
                {
                  stack: {
                    children: [
                      {
                        component: {
                          name: Routes.CHAT._,
                          id: Routes.CHAT._,
                          options: {
                            bottomTab: {
                              icon: require('../../../../assets/icons/bottom_chat.png')
                            }
                          }
                        }
                      }
                    ]
                  }
                },
                {
                  stack: {
                    children: [
                      {
                        component: {
                          name: Routes.BRIEFING._,
                          id: Routes.BRIEFING._,
                          options: {
                            bottomTab: {
                              icon: require('../../../../assets/icons/bottom_briefing.png')
                            }
                          }
                        }
                      }
                    ]
                  }
                },
                {
                  stack: {
                    children: [
                      {
                        component: {
                          name: Routes.SETTINGS.MY_PROFILE,
                          id: Routes.SETTINGS.MY_PROFILE,
                          options: {
                            bottomTab: {
                              icon: require('../../../../assets/icons/bottom_settings.png')
                            }
                          }
                        }
                      }
                    ]
                  }
                }
              ]
            }
          },
        });
      
      }

    }, 1000);
    
  }

  componentDidDisappear() { 
    clearTimeout(this.timeout);
  }
  componentWillUnmount() { 
    clearTimeout(this.timeout);
  }

  render() {
    return(
      <_Container> 
        <_Splash source={splashScreen} />
      </_Container>  
    )
  }

}

export default AndroidSplashScreen