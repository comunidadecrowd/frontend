import styled from 'styled-components/native';

export const _Container = styled.View`
  flex: 1;
  background: #000;
`;

export const _Splash = styled.Image`
  width: 100%;
  height: 100%;
`;