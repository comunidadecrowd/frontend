import React, { Component } from 'react';
import Routes from 'Routes';
import { Navigation } from 'react-native-navigation';

import { HEADER } from 'GLOBALS/HEADER';
import MenuList from './../_/UI/MenuList';

import {
  _Container,
  _WrapperHeader,
  _LineTopList,
} from './styles';

class NetworkingAccounts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menuList: [
        {
          key: '0',
          label: 'LinkedIn',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Conectado',
              labelInactive: 'Desconectado',
              active: true,
            },
          },
        },
        {
          key: '1',
          label: 'Facebook',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Conectado',
              labelInactive: 'Desconectado',
              active: true,
            },
          },
        },
        {
          key: '2',
          label: 'Twitter',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Conectado',
              labelInactive: 'Desconectado',
              active: false,
            },
          },
        },
        {
          key: '3',
          label: 'Instagram',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Conectado',
              labelInactive: 'Desconectado',
              active: true,
            },
          },
        },
        {
          key: '4',
          label: 'Gmail',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Conectado',
              labelInactive: 'Desconectado',
              active: true,
            },
          },
        },
        {
          key: '5',
          label: 'Slack',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Desconectado',
              labelInactive: 'Desconectado',
              active: false,
            },
          },
        },
        {
          key: '6',
          label: 'Youtube',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Conectado',
              labelInactive: 'Desconectado',
              active: true,
            },
          },
        },
        {
          key: '7',
          label: 'Behance',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Conectado',
              labelInactive: 'Desconectado',
              active: true,
            },
          },
        },
      ],
    };
  }

  render() {
    const { menuList } = this.state;

    return (
      <_Container>
        <_WrapperHeader>
          <HEADER
            withButtonsNavigation
            buttonLeft={() => Navigation.popTo(Routes.SETTINGS.MY_PROFILE)}
            title={'Conexões'}
          />
        </_WrapperHeader>

        <_LineTopList />
        <MenuList items={menuList} />
      </_Container>
    );
  }
}

export default NetworkingAccounts;
