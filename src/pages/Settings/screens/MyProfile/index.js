import React, { PureComponent, Fragment } from 'react';
import { Animated, Easing, Alert } from 'react-native';
import withLocalization from 'utils/withLocalization';

import Routes from 'Routes';
import APIS from 'apis';
import { Navigation } from 'react-native-navigation';
import { newSetRoot } from 'utils/newSetRoot';
import { navigateFromTo } from 'utils/navigateFromTo';
import { countryList } from 'constants/CountryList';
import { Icons } from 'components/Icon';

import { HEADER } from 'GLOBALS/HEADER';

import ProfileHeader from './UI/ProfileHeader';
import Summary from './UI/Summary';
import Skills from './UI/Skills';
import Country from './UI/Country';
import City from './UI/City';
import State from './UI/State';
import Diary from './UI/Diary';
import Hourly from './UI/Hourly';
import ModalSummary from './UI/ModalSummary';
import ModalCountry from './UI/ModalCountry';
import ModalState from './UI/ModalState';
import ModalCity from './UI/ModalCity';
import ModalDiary from './UI/ModalDiary';
import ModalHourly from './UI/ModalHourly';

import {
  _Container,
  _ScrollView,
  _WrapperHeader,
  _WrapperContent,
  _StaticsInfos,
  _StaticItem,
  _StaticLabel,
  _ValuePerHour,
  _ProposesCount,
  _JobsCount,
  _ButtonsActions,
  _WrapperButton,
  _StorageButtonsWrapper,
  _StorageButtons
} from './styles';

class MyProfile extends PureComponent {

  constructor(props) {

    super(props);

    this.user;

    this.state = {
      userID: global.CrowdStorage.User ? global.CrowdStorage.User.userID : null,
      credentials: {
        name: global.CrowdStorage.User ? global.CrowdStorage.User.name : '',
        title: global.CrowdStorage.User ? global.CrowdStorage.User.title : '',
        category: global.CrowdStorage.User ? global.CrowdStorage.User.category : ''
      },
      profileImage: global.CrowdStorage.User ? global.CrowdStorage.User.profileImage : null,
      summary: '',
      skillList: [],
      countryCode: '',
      stateId: '',
      city: '',
      diary: [],
      hourly: '',
      submitting: false
    }

    this.devUsers = [
      "+5511957973662",
      "+5511965757319",
      "+5511981450225",
      "+5511981864840",
      "+5511983502851",
      "+5511994805660",
      "+5511998970206",
      "+5512981491039"
    ]

  }

  async componentDidMount() {
    
    let getUserOnStorage = await APP_STORAGE.getItem('User');
    this.user = getUserOnStorage;

    if (getUserOnStorage && (this.state.credentials.name === '' || this.state.credentials.title === '' || this.state.credentials.category === '' || !this.state.userID )) {
      console.log('# getUserOnStorage =>', getUserOnStorage);
      this.setState({
        userID: getUserOnStorage.userID,
        credentials: {
          name: getUserOnStorage.name,
          title: getUserOnStorage.title,
          category: getUserOnStorage.category
        },
        profileImage: getUserOnStorage.profileImage
      });
    }

    this.getUserCredentials();
  }

  getUserCredentials = () => {
    const { userID } = this.state;

    APIS.User.userCredentials(userID, ({ userCredentials }) => {
      console.log('# userCredentials', userCredentials);

      const hasDecimal = userCredentials.hourly ? userCredentials.hourly.toString().split('.') : '';

      this.setState({
        credentials: {
          name: userCredentials.name,
          title: userCredentials.title,
          category: userCredentials.category
        },
        profileImage: userCredentials.profileImage,
        summary: userCredentials.summary,
        skillList: userCredentials.skillList,
        countryCode: userCredentials.countryCode ? countryList.find(country => country.code === userCredentials.countryCode).country : null,
        stateId: userCredentials.stateId,
        city: userCredentials.city,
        diary: userCredentials.diary || [],
        hourly: userCredentials.hourly && hasDecimal.length === 1 ? `${userCredentials.hourly.toLocaleString('pt-BR')},00` : userCredentials.hourly && hasDecimal[1].length === 1 ? `${userCredentials.hourly.toLocaleString('pt-BR')}0` : userCredentials.hourly ? userCredentials.hourly.toLocaleString('pt-BR') : '',
        industryList: userCredentials.industryList,
        languageList: userCredentials.languageList
      });
    });
  }

  onUpdateUser = (obj) => {
    this.setState(obj); 
  }

  backPage = (newDataToUser) => {
    const { credentials: { name, title, category } } = this.state;

    if (newDataToUser && (
      name !== newDataToUser.name
      || title !== newDataToUser.title
      || category !== newDataToUser.category
    )) this.setState({
        credentials: {
          name: newDataToUser.name,
          title: newDataToUser.title,
          category: newDataToUser.category
        },
      }, () => Navigation.popTo(Routes.SETTINGS.MY_PROFILE));
  }

  clearStorage = (context) => {

    context === 'chat' ? APP_STORAGE.deleteAll(/^(CHAT_|CHANNEL_|BlockedList|ReportedList)/, Alert.alert('Todos os canais apagados.\nFeche o app e abra novamente.') ) : APP_STORAGE.deleteAll(false, Alert.alert('Todo o Storage apagado.\nFeche o app e abra novamente.')) 

    // console.log('*** clearStorage -> context', context)
  }
  
  render() {
    const
      { t } = this.props,
      {
        credentials,
        credentials: { name, title, category },
        profileImage,
        summary,
        skillList,
        countryCode,
        stateId,
        city,
        diary,
        hourly,
        userID
      } = this.state;

    console.log('# render MyProfile');

    return (
      <_Container>
        <_ScrollView>
          <_WrapperHeader>
            <HEADER
              //withButtonsNavigation
              //buttonLeft={() => Navigation.popTo(Routes.SETTINGS._)}
              buttonRight={() => navigateFromTo(Routes.SETTINGS.MY_PROFILE, Routes.SETTINGS.MY_CREDENTIALS, null, {
                backPage: this.backPage
              })}
              iconRight={Icons['ESCREVER']}
            />
          </_WrapperHeader>
          <ProfileHeader
            userName={name}
            userId={userID}
            title={title}
            category={category}
            profileImage={profileImage}
            state={stateId || ''}
            city={city || ''}
          />
          {/*<_StaticsInfos>
            <_StaticItem>
              <_StaticLabel content={'Valor/Hora'} fontSize={14} />
              <_ValuePerHour content={'R$ 45,00'} fontSize={14} align={'center'} />
            </_StaticItem>
            <_StaticItem>
              <_StaticLabel content={'Propostas'} fontSize={14} />
              <_ProposesCount content={'50'} fontSize={14} align={'center'} />
            </_StaticItem>
            <_StaticItem>
              <_StaticLabel content={'Jobs'} fontSize={14} />
              <_JobsCount content={'50'} fontSize={14} align={'center'} />
            </_StaticItem>
          </_StaticsInfos>
          <_ButtonsActions>
            <_WrapperButton>
              <BUTTON
                onPressFn={() => navigateFromTo(Routes.SETTINGS.MY_PROFILE, Routes.SETTINGS.MY_PREFERENCES)}
                content={'preferências'}
                fontSize={14}
                noPadding
              />
            </_WrapperButton>
            <_WrapperButton>
              <BUTTON
                onPressFn={() => navigateFromTo(Routes.SETTINGS.MY_PROFILE, Routes.SETTINGS.NETWORKING_ACCOUNTS)}
                content={'redes sociais'}
                fontSize={14}
                noPadding
              />
            </_WrapperButton>
          </_ButtonsActions>*/}

          <_WrapperContent>

            <Summary
              content={summary || ''}
              openSummaryModal={() => this.summaryModal.snapTo({ index: 1 })}
            />

            <Skills skills={skillList} />

            <Country
              content={countryCode || ''}
              openCountryModal={() => this.countryModal.snapTo({ index: 1 })}
            />

            {countryCode === 'BRASIL' && <State
              content={stateId || ''}
              openStateModal={() => this.stateModal.snapTo({ index: 1 })}
            />}

            <City
              content={city || ''}
              openCityModal={() => this.cityModal.snapTo({ index: 1 })}
            />

            <Diary
              diary={diary || ''}
              modal={this.diaryModal}
              openDiaryModal={() => this.diaryModal.snapTo({ index: 1 })}
            />

            <Hourly
              content={hourly ? `R$ ${hourly}` : ''}
              openHourlyModal={() => this.hourlyModal.snapTo({ index: 1 })}
            />

          </_WrapperContent>
          
          {
            this.user && this.devUsers.includes(this.user.phone) &&
            <_StorageButtonsWrapper>
              {
                ['chat', 'all'].map((item) => {
                  return(
                    <_StorageButtons 
                      key={item}
                      bg={item === 'chat' ? 'rgba(255, 255, 255, .50)' : 'rgba(255, 255, 255, .10)'}
                      fontSize={15}
                      onPressFn={() => this.clearStorage(item)}
                      content={item === 'chat' ? 'limpar chats' : 'clear app'}
                      needFlex
                    /> 
                  )
                })
              }
            </_StorageButtonsWrapper> 
          }

        </_ScrollView>
        

        { /* MODAIS COM INPUTS */ }
        <ModalSummary
          getRef={ref => this.summaryModal = ref}
          userID={userID}
          defaultValue={summary}
          onSubmit={this.onUpdateUser}
        />
        {/*<ModalSkills />*/}
        <ModalCountry
          getRef={ref => this.countryModal = ref}
          userID={userID}
          defaultValue={countryCode}
          onSubmit={this.onUpdateUser}
        />
        <ModalState
          getRef={ref => this.stateModal = ref}
          userID={userID}
          defaultValue={stateId}
          onSubmit={this.onUpdateUser}
        />
        <ModalCity
          getRef={ref => this.cityModal = ref}
          userID={userID}
          defaultValue={city}
          onSubmit={this.onUpdateUser}
        />
        <ModalDiary
          getRef={ref => this.diaryModal = ref}
          userID={userID}
          defaultValue={diary}
          onSubmit={this.onUpdateUser}
        />
        <ModalHourly
          getRef={ref => this.hourlyModal = ref}
          userID={userID}
          defaultValue={hourly}
          onSubmit={this.onUpdateUser}
        />
      </_Container>
    )

  }

}

export default withLocalization(MyProfile);
