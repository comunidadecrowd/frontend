import React, { memo } from 'react';
import withLocalization from 'utils/withLocalization';
import TitleButton from '../_COMMONS/TitleButton'
import ComponentWrapper from '../_COMMONS/ComponentWrapper'

import {
  _WrapperDiaryItems,
  _DiaryItem,
  _DiaryLabel
} from './styles';


const Diary = ({ t, diary, modal, openDiaryModal }) => {
  return (
    <ComponentWrapper isList>
      <TitleButton
        content={t('settings:myProfile:diary:title')}
        onPressFN={openDiaryModal}
      />
      <_WrapperDiaryItems>
        {modal && diary ? diary.map((checkbox, index) => (
          <_DiaryItem key={index} isLastOnes={diary.length > 2 && diary.length % 2 !== 0 ? (index === (diary.length - 1)) : ((index === (diary.length - 1)) || (index === (diary.length - 2)))}>
            <_DiaryLabel
              content={modal.props.checkboxItems[checkbox].label.length > 12 ? modal.props.checkboxItems[checkbox].label.slice(0, 9) + '...' : modal.props.checkboxItems[checkbox].label}
              fontSize={14}
            />
          </_DiaryItem>))
          : undefined}
      </_WrapperDiaryItems>
    </ComponentWrapper>
  )
}

export default memo(withLocalization(Diary))