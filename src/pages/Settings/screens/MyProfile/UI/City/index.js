import React, { memo } from 'react';
import withLocalization from 'utils/withLocalization';
import TitleButton from '../_COMMONS/TitleButton'

import ComponentWrapper from '../_COMMONS/ComponentWrapper'

const City = ({ t, content, openCityModal }) => {
  return(
    <ComponentWrapper content={content}>
        <TitleButton 
          content={t('settings:myProfile:city:title')}
          onPressFN={openCityModal}
        />
      </ComponentWrapper>
  )
}

export default memo(withLocalization(City))