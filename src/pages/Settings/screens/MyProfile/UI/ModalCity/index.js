import React, { Component } from 'react';
import { Dimensions, Keyboard } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { IconCloseButton } from 'components/Icon/SearchView';

import APIS from 'apis';
import BUTTON from 'GLOBALS/BUTTON';
import FORM_INPUT from 'GLOBALS/FORM_INPUT';
import { Colors } from 'theme';

import {
  _ContainerInteractable,
  _ButtonClose,
  _WrapperInputs,
  _WrapperButtonSubmit,
} from './styles';

const { width, height } = Dimensions.get('window');

class ModalCity extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: null,
      submiting: false,
    };
  }

  saveRef = (ref) => {
    const { getRef } = this.props;
    getRef(ref);
    this.modal = ref;
  };

  closeModal = () => {
    const
      { defaultValue } = this.props,
      { city } = this.state;

    this.modal.snapTo({ index: 0 });
    if (city === '') this.setState({ city: defaultValue });
    Keyboard.dismiss();
  };

  handleOnChange = (city) => {
    this.setState({ city });
  }

  onSave = () => {
    const
      { onSubmit, userID } = this.props,
      { city } = this.state;

    this.setState({ submiting: true }, Keyboard.dismiss());
    APIS.User.sendCredentials({
      userID,
      city 
    }, ({ inputCredentials }) => {
      onSubmit({ city });
      this.setState({ submiting: false }, this.closeModal());
    });
  };

  render() {
    const { t, defaultValue } = this.props;
    const { city, submiting } = this.state;

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        snapPoints={[{ y: height }, { y: 5 }]}
        ref={ref => this.saveRef(ref)}
      >
        <_ButtonClose onPress={!submiting ? this.closeModal : () => false} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>

        <_WrapperInputs>
          <FORM_INPUT
            name={'city'}
            placeholder={t('settings:myProfile:city:placeholder')}
            inputValue={city === null || city === defaultValue ? defaultValue : city}
            onChange={this.handleOnChange}
          />
        </_WrapperInputs>

        <_WrapperButtonSubmit>
          <BUTTON
            onPressFn={this.onSave}
            color={Colors.SUCCESS}
            content={t('common:buttons:save')}
            disabled={submiting || city === null || city === '' || city === defaultValue}
          />
        </_WrapperButtonSubmit>
      </_ContainerInteractable>
    );
  }
}

export default withLocalization(ModalCity);
