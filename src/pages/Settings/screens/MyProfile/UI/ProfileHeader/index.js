import React, { Component } from 'react';
import { Colors } from 'theme/Colors';
import { categoryColor, STORAGE_URL } from 'utils/Helpers';
import IconStatus from 'components/Icon/IconStatus';
import {
  _Container,
  _UserImage,
  _BgGradient,
  _UserInfos,
  _UserName,
  _StatusInfos,
  _JobTitle,
  _UserLocalization,
  _UserStars,
  _StarIcon,
  _StartsPoints,
} from './styles';

import DefaultProfileImage from './img/default.png';

class ProfileHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {

    const { userId, userName, profileImage, title: jobTitle, category, state, city } = this.props; 
    const hasStateAndCity = (state && state !== '') && (city && city !== '')

    // REFACT:  mock props begin;
    const props = {
      isOnline: true,
      points: 4.98
    }
    // REFACT:  mock props end;

    return (
      <_Container>
        <_UserImage
          source={(profileImage === null || profileImage === '') ? DefaultProfileImage : {
            uri: `${STORAGE_URL}.appspot.com/users/${userId}/profile/${profileImage}`
          }} />
        <_BgGradient width="100%" height="100%" />
        <_UserInfos>
          <_UserName
            euclid
            content={userName ? userName : 'UserName'}
            fontSize={24}
            color={categoryColor(category)}
          />
          <_JobTitle
            content={jobTitle}
            color={'#9B9B9B'}
            fontSize={14}
          />
          {
            hasStateAndCity === true &&
            <_UserLocalization bold content={`${city}, ${state}`} fontSize={14} />
          }
          {/* TODO: Voltar o elemento de Rating quando houver a função 
            <_UserStars>
              <_StarIcon width={18} height={18} color={Colors.WHITE} />
              <_StartsPoints
                bold
                content={props.points}
                fontSize={18}
              />
            </_UserStars> 
          */}
        </_UserInfos>
      </_Container>
    );
  }
}

export default ProfileHeader;
