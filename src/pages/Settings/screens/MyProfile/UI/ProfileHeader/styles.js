import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

import { TEXT } from 'GLOBALS/TEXT';
import ImageGradient from './ImageGradient';
import { IconStar } from 'components/Icon/ChatHeader';

const { width, height } = Dimensions.get('window');

export const _Container = styled.View`
  width: ${width}px;
  height: ${height / 2};
`;

export const _UserImage = styled.Image`
  width: 100%;
  height: 100%;
`;

export const _BgGradient = styled(ImageGradient)`
  position: absolute;
`;

export const _UserInfos = styled.View`
  align-items: center;
  justify-content: center;
  position: absolute;
  width: 100%;
  bottom: 0;
  padding-bottom: 29px;
`;

export const _StatusInfos = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const _UserName = styled(TEXT)``;

export const _JobTitle = styled(TEXT)``;

export const _UserLocalization = styled(TEXT)`
  margin: 10px 0 5px;
`;

export const _UserStars = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const _StarIcon = styled(IconStar)`
  margin-right: 8px;
`;

export const _StartsPoints = styled(TEXT)`
  margin-bottom: 29px;
`;
