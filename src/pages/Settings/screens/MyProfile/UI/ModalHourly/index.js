import React, { Component } from 'react';
import { Dimensions, Keyboard } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { IconCloseButton } from 'components/Icon/SearchView';

import APIS from 'apis';
import BUTTON from 'GLOBALS/BUTTON';
import FORM_INPUT from 'GLOBALS/FORM_INPUT';
import { Colors } from 'theme';

import {
  _ContainerInteractable,
  _ButtonClose,
  _WrapperInputs,
  _WrapperButtonSubmit,
} from './styles';

const { width, height } = Dimensions.get('window');

class ModalHourly extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hourly: null,
      submiting: false,
    };
  }

  saveRef = (ref) => {
    const { getRef } = this.props;
    getRef(ref);
    this.modal = ref;
  };

  closeModal = () => {
    const
      { defaultValue } = this.props,
      { hourly } = this.state;

    this.modal.snapTo({ index: 0 });
    if (hourly === '') this.setState({ hourly: defaultValue });
    Keyboard.dismiss();
  };

  handleOnChange = (hourly) => {
		console.log('# ModalHourly | handleOnChange/hourly', hourly);

    this.setState({ hourly });
  };

  onSave = () => {
    const
      { onSubmit, userID } = this.props,
      { hourly } = this.state,
			priceFloat = parseFloat(hourly.replace('R$ ', '').replace('.', '').replace(',', '.')),
			hasDecimal = priceFloat ? priceFloat.toString().split('.') : '';

    this.setState({ submiting: true }, Keyboard.dismiss());
    APIS.User.sendCredentials({
      userID,
      hourly: priceFloat 
    }, ({ inputCredentials }) => {
      onSubmit({
        hourly: priceFloat && hasDecimal.length === 1 ? `${priceFloat.toLocaleString('pt-BR')},00` : priceFloat && hasDecimal[1].length === 1 ? `${priceFloat.toLocaleString('pt-BR')}0` : priceFloat ? priceFloat.toLocaleString('pt-BR') : '',
			});
      this.setState({ submiting: false }, this.closeModal());
    });
  };

  render() {
    const { t, defaultValue } = this.props;
    const { hourly, submiting } = this.state;

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        snapPoints={[{ y: height }, { y: 5 }]}
        ref={ref => this.saveRef(ref)}
      >
        <_ButtonClose onPress={!submiting ? this.closeModal : () => false} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>

        <_WrapperInputs>
          <FORM_INPUT
            name={'hourly'}
            placeholder={t('settings:myProfile:hourly:placeholder')}
            inputValue={hourly === null || hourly === defaultValue ? defaultValue : hourly}
            onChange={this.handleOnChange}
						type={'money'}
          />
        </_WrapperInputs>

        <_WrapperButtonSubmit>
          <BUTTON
            onPressFn={this.onSave}
            color={Colors.SUCCESS}
            content={t('common:buttons:save')}
            disabled={submiting || hourly === null || hourly === '' || hourly === defaultValue}
          />
        </_WrapperButtonSubmit>
      </_ContainerInteractable>
    );
  }
}

export default withLocalization(ModalHourly);
