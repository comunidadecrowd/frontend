import React, { memo } from 'react';
import withLocalization from 'utils/withLocalization';
import TitleButton from '../_COMMONS/TitleButton'
import ComponentWrapper from '../_COMMONS/ComponentWrapper'

const State = ({ t, content, openStateModal }) => {
  return(
    <ComponentWrapper content={content}>
        <TitleButton 
          content={t('settings:myProfile:state:title')}
          onPressFN={openStateModal}
        />
      </ComponentWrapper>
  )
}

export default memo(withLocalization(State))