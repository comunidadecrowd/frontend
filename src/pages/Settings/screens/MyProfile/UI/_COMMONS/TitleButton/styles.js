import styled from 'styled-components/native';
import { Colors } from 'theme';
import { TEXT } from 'GLOBALS/TEXT';
import Icon from 'components/Icon';

export const _WrapperTitle = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  margin-bottom: 5px;
  height: 33px;
`;

export const _Title = styled(TEXT)`
  flex-grow: 2;
  color: ${Colors.BLACK};
`;

export const _Button = styled.View``;

export const _Icon = styled(Icon)`
  font-size: 30px;
`;