import React, {memo} from 'react';
import { Icons } from 'components/Icon';

import {
  _WrapperTitle,
  _Title,
  _Button,
  _Icon
} from './styles';

const TitleButton = ({content, onPressFN, icon}) => {
  return (
    <_WrapperTitle onPress={onPressFN || null} activeOpacity={1}>
      <_Title
        content={content.toUpperCase()}
        fontSize={14}
        bold
      />
      {
        onPressFN && 
        <_Button>
          <_Icon icon={icon ? Icons[icon] : Icons['ESCREVER']} />
        </_Button>
      }
    </_WrapperTitle>
  )
}

export default memo(TitleButton)