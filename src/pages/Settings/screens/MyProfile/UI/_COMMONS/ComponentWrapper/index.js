import React, {Fragment} from 'react';

import {
  _Container,
  _Content
} from './styles';

const ComponentWrapper = ({bg, content, children, isList}) => {
  return (
    <_Container bg={bg}>
      {
        isList ? (
          <Fragment>
            {children}    
          </Fragment>
        ) : (
          <Fragment>
            {children}
            {
              content !== '' && 
              <_Content
                content={content}
                fontSize={14}
              />
            }
          </Fragment>
        )
      }
    </_Container>  
  )
}

export default React.memo(ComponentWrapper)