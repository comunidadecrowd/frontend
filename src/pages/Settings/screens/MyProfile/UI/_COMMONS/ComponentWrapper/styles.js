import styled from 'styled-components/native';
import { Colors } from 'theme';
import { TEXT } from 'GLOBALS/TEXT';

export const _Container = styled.View`
  background: ${({bg}) => bg ? bg : Colors.TRANSPARENT };
  margin: 0 0 25px;
`;

export const _Content = styled(TEXT)`
  color: ${Colors.BLACK};
`;
