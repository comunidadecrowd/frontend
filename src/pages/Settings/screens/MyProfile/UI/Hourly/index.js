import React, { memo } from 'react';
import withLocalization from 'utils/withLocalization';
import TitleButton from '../_COMMONS/TitleButton'
import ComponentWrapper from '../_COMMONS/ComponentWrapper'

const Hourly = ({ t, content, openHourlyModal }) => {
  return(
    <ComponentWrapper content={content}>
        <TitleButton 
          content={t('settings:myProfile:hourly:title')}
          onPressFN={openHourlyModal}
        />
      </ComponentWrapper>
  )
}

export default memo(withLocalization(Hourly))