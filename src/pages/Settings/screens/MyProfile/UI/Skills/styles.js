import styled from 'styled-components/native';
import { Colors } from 'theme';
import { TEXT } from 'GLOBALS/TEXT';

export const _Content = styled.View`
  flex-direction: row;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export const _SkillItem = styled.View`
  width: 48.5%;
  align-items: center;
  background: ${Colors.BLACK};
  padding: 10px 7px;
  margin-bottom: ${({isLastOnes}) => isLastOnes ? '0' : '7px'};
`;

export const _SkillLabel = styled(TEXT)``;
