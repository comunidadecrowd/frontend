import React, { memo } from 'react';
import withLocalization from 'utils/withLocalization';

import TitleButton from '../_COMMONS/TitleButton'
import ComponentWrapper from '../_COMMONS/ComponentWrapper'

import {
  _Content,
  _SkillItem,
  _SkillLabel
} from './styles';

const Skills = ({ t, skills }) => {
  return(
    <ComponentWrapper isList>
        <TitleButton
          content={t('settings:myProfile:skills:title')}
        />
        <_Content>
          {skills.map((skill, index) => (
            <_SkillItem key={index} isLastOnes={skills.length > 2 && skills.length % 2 !== 0 ? (index === (skills.length - 1)) : ((index === (skills.length - 1)) || (index === (skills.length - 2)))}>
              <_SkillLabel content={skill.label} fontSize={14} />
            </_SkillItem>
          ))}
        </_Content>
      </ComponentWrapper>
  )
}

export default memo(withLocalization(Skills))