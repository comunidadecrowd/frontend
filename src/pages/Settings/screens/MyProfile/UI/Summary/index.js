import React, { memo } from 'react';
import withLocalization from 'utils/withLocalization';

import TitleButton from '../_COMMONS/TitleButton'
import ComponentWrapper from '../_COMMONS/ComponentWrapper'

const Summary = ({ t, content, openSummaryModal }) => {
  return(
    <ComponentWrapper content={content}>
        <TitleButton 
          content={t('settings:myProfile:summary:title')}
          onPressFN={openSummaryModal}
        />
      </ComponentWrapper>
  )
}

export default memo(withLocalization(Summary))