import React, { memo } from 'react';
import withLocalization from 'utils/withLocalization';

import TitleButton from '../_COMMONS/TitleButton'
import ComponentWrapper from '../_COMMONS/ComponentWrapper'

const Country = ({ t, content, openCountryModal }) => {
  return(
    <ComponentWrapper content={content}>
        <TitleButton 
          content={t('settings:myProfile:country:title')}
          onPressFN={openCountryModal}
        />
      </ComponentWrapper>
  )
}

export default memo(withLocalization(Country))