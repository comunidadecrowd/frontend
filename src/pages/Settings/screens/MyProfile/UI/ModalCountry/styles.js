import styled from 'styled-components/native';
import Interactable from 'react-native-interactable';
import { Dimensions } from 'react-native';

import { Colors, FontRatioNormalize } from 'theme';
const { width, height } = Dimensions.get('window');

export const _ContainerInteractable = styled(Interactable.View)`
  align-items: center;
  justify-content: space-between;
  position: absolute;
  width: ${width - 40};
  height: 300px;
  background: ${Colors.BLACK};
  top: ${(height - 300) / 2}px;
  left: 20px;
  z-index: 1;
`;

export const _ButtonClose = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  position: absolute;
  top: 20px;
  right: 0;
`;

export const _WrapperSelect = styled.View`
  width: ${width - 40}px;
  height: 200px;
  margin: 40px 0 30px;
  padding: 0 10px;
`;

export const _WrapperButtonSubmit = styled.View`
  bottom: 29px;
`;
