import React, { Component } from 'react';
import { Dimensions, Keyboard } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { IconCloseButton } from 'components/Icon/SearchView';

import APIS from 'apis';
import BUTTON from 'GLOBALS/BUTTON';
import FORM_INPUT from 'GLOBALS/FORM_INPUT';
import FORM_SELECT from 'GLOBALS/FORM_SELECT';
import { Colors } from 'theme';
import { countryList } from 'constants/CountryList';

import {
  _ContainerInteractable,
  _ButtonClose,
  _WrapperSelect,
  _WrapperButtonSubmit,
} from './styles';

const { width, height } = Dimensions.get('window');

class ModalCountry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      country: null,
      submiting: false,
    };
  }

  saveRef = (ref) => {
    const { getRef } = this.props;
    getRef(ref);
    this.modal = ref;
  };

  closeModal = () => {
    const
      { defaultValue } = this.props,
      { country } = this.state;

    this.modal.snapTo({ index: 0 });
    if (country === '') this.setState({ country: defaultValue });
    Keyboard.dismiss();
  };

  handleOnChange = (country) => {
    this.setState({ country });
  };

  onSave = () => {
    const
      { onSubmit, userID } = this.props,
      { country } = this.state;

    this.setState({ submiting: true }, Keyboard.dismiss());
    APIS.User.sendCredentials({
      userID,
      countryCode: countryList.find(item => item.country === country).code
    }, ({ inputCredentials }) => {
      onSubmit({ countryCode: country });
      this.setState({ submiting: false }, this.closeModal());
    });
  };

  render() {
    const { t, defaultValue } = this.props;
    const { country, submiting } = this.state;

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        snapPoints={[{ y: height }, { y: 5 }]}
        ref={ref => this.saveRef(ref)}
      >
        <_ButtonClose onPress={!submiting ? this.closeModal : () => false} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>

        <_WrapperSelect>
          <FORM_SELECT
            data={countryList.sort((a, b) => a.country < b.country ? -1 : a.country > b.country ? 1 : 0)}
            valueProp={'code'}
            labelProp={'country'}
            inputValue={country === null || country === defaultValue ? defaultValue : country}
            handleSelect={this.handleOnChange}
          />
        </_WrapperSelect>

        <_WrapperButtonSubmit>
          <BUTTON
            onPressFn={this.onSave}
            color={Colors.SUCCESS}
            content={t('common:buttons:save')}
            disabled={submiting || country === null || country === '' || country === defaultValue}
          />
        </_WrapperButtonSubmit>
      </_ContainerInteractable>
    );
  }
}

export default withLocalization(ModalCountry);
