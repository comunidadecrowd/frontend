import styled from 'styled-components/native';
import Interactable from 'react-native-interactable';
import { Dimensions } from 'react-native';

import { Colors, FontRatioNormalize } from 'theme';
const { width, height } = Dimensions.get('window');

export const _ContainerInteractable = styled(Interactable.View)`
  align-items: center;
  position: absolute;
  width: ${width - 40};
  height: 200px;
  background: ${Colors.BLACK};
  top: ${(height - 200) / 2}px;
  left: 20px;
  overflow: hidden;
  z-index: 1;
`;

export const _ButtonClose = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  position: absolute;
  top: 20px;
  right: 0;
`;

export const _WrapperInputs = styled.View`
  width: ${width - 40}px;
  margin: 40px 0 0;
  padding: 0 29px;
`;

export const _WrapperButtonSubmit = styled.View`
  bottom: 29px;
`;
