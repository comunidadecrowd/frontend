import React, { Component } from 'react';
import { Dimensions, Keyboard } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { IconCloseButton } from 'components/Icon/SearchView';

import APIS from 'apis';
import BUTTON from 'GLOBALS/BUTTON';
import FORM_INPUT from 'GLOBALS/FORM_INPUT';
import { Colors } from 'theme';

import {
  _ContainerInteractable,
  _ButtonClose,
  _WrapperInputs,
  _WrapperButtonSubmit,
} from './styles';

const { width, height } = Dimensions.get('window');

class ModalState extends Component {
  constructor(props) {
    super(props);
    this.state = {
      state: null,
      submiting: false,
    };
  }

  saveRef = (ref) => {
    const { getRef } = this.props;
    getRef(ref);
    this.modal = ref;
  };

  closeModal = () => {
    const
      { defaultValue } = this.props,
      { state } = this.state;

    this.modal.snapTo({ index: 0 });
    if (state === '') this.setState({ state: defaultValue });
    Keyboard.dismiss();
  };

  handleOnChange = (state) => {
    this.setState({ state });
  }

  onSave = () => {
    const
      { onSubmit, userID } = this.props,
      { state: stateId } = this.state;

    this.setState({ submiting: true }, Keyboard.dismiss());
    APIS.User.sendCredentials({
      userID,
      stateId 
    }, ({ inputCredentials }) => {
      onSubmit({ stateId });
      this.setState({ submiting: false }, this.closeModal());
    });
  };

  render() {
    const { t, defaultValue } = this.props;
    const { state, submiting } = this.state;

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        snapPoints={[{ y: height }, { y: 5 }]}
        ref={ref => this.saveRef(ref)}
      >
        <_ButtonClose onPress={!submiting ? this.closeModal : () => false} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>

        <_WrapperInputs>
          <FORM_INPUT
            name={'state'}
            placeholder={t('settings:myProfile:state:placeholder')}
            inputValue={state === null || state === defaultValue ? defaultValue : state}
            onChange={this.handleOnChange}
          />
        </_WrapperInputs>

        <_WrapperButtonSubmit>
          <BUTTON
            onPressFn={this.onSave}
            color={Colors.SUCCESS}
            content={t('common:buttons:save')}
            disabled={submiting || state === null || state === '' || state === defaultValue}
          />
        </_WrapperButtonSubmit>
      </_ContainerInteractable>
    );
  }
}

export default withLocalization(ModalState);
