import styled from 'styled-components/native';
import Interactable from 'react-native-interactable';
import { Dimensions } from 'react-native';

import { Colors, FontRatioNormalize } from 'theme';
const { width, height } = Dimensions.get('window');

export const _ContainerInteractable = styled(Interactable.View)`
  align-items: center;
  position: absolute;
  width: ${width - 40};
  height: 350px;
  background: ${Colors.BLACK};
  top: ${(height - 350) / 2}px;
  left: 20px;
  z-index: 1;
`;

export const _ButtonClose = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  position: absolute;
  top: 20px;
  right: 0;
`;

export const _WrapperCheckbox = styled.View`
  width: ${width - 40}px;
  margin: 40px 0 20px;
  padding: 0 29px;
`;

export const _WrapperButtonSubmit = styled.View`
  bottom: 29px;
`;
