import React, { Component } from 'react';
import { Dimensions, Keyboard } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { IconCloseButton } from 'components/Icon/SearchView';

import APIS from 'apis';
import BUTTON from 'GLOBALS/BUTTON';
import CHECKBOX_BUTTON from 'GLOBALS/CHECKBOX_BUTTON';
import { Colors } from 'theme';

import {
  _ContainerInteractable,
  _ButtonClose,
  _WrapperCheckbox,
  _WrapperButtonSubmit,
} from './styles';

const { width, height } = Dimensions.get('window');

class ModalDiary extends Component {
  constructor(props) {
    super(props);
    this.state = {
			diary: this.props.defaultValue || [],
      submiting: false,
      checkboxItems: [
        {
          label: 'Meio Período',
          checked: false
        },
        {
          label: 'Período Integral',
          checked: false
        },
        {
          label: 'Remoto',
          checked: false
        },
        {
          label: 'Alocado',
          checked: false
        },
        {
          label: 'Freelancer',
          checked: false
        },
        {
          label: 'Contrato',
          checked: false
        },
      ]
    };
  }

  componentDidUpdate() {
		const
			{ defaultValue } = this.props,
			{ checkboxItems, diary } = this.state;
		
		if (defaultValue && defaultValue.length && !diary.length) {
			defaultValue.map(checkbox => {
        if (!checkboxItems[checkbox].checked) {
					checkboxItems[checkbox].checked = true;
					this.setState({ checkboxItems });
				}
			});
		}
  }

  _handleChecked = (label) => {
    const { defaultValue } = this.props;
    const { checkboxItems, diary } = this.state;

    checkboxItems.find(checkbox => checkbox.label === label).checked = !checkboxItems.find(checkbox => checkbox.label === label).checked;

    this.setState({
			checkboxItems,
      diary: checkboxItems.map((checkbox, key) => {
        if (checkbox.checked) return key;
      })
		});
  }

  saveRef = (ref) => {
    const { getRef } = this.props;
    getRef(ref);
    this.modal = ref;
  };

  closeModal = () => {
    const
      { defaultValue } = this.props,
      { diary } = this.state;

    this.modal.snapTo({ index: 0 });
    if (!diary.length) this.setState({ diary: defaultValue });
    Keyboard.dismiss();
  };

  onSave = () => {
    const
      { onSubmit, userID } = this.props,
      { checkboxItems, diary } = this.state;

    this.setState({ submiting: true }, Keyboard.dismiss());
    APIS.User.sendCredentials({
      userID,
      diary: diary.filter(checkbox => checkbox !== null && checkbox !== undefined)
    }, ({ inputCredentials }) => {
      onSubmit({ diary: diary.filter(checkbox => checkbox !== null && checkbox !== undefined) });
      this.setState({ submiting: false }, this.closeModal());
    });
  };

  checkDisabled = () => {
    const
      { defaultValue } = this.props,
      { submiting, diary } = this.state;

    let
      diarySelected = diary.filter(diary => diary !== null && diary !== undefined),
      findSelectedDefault = diarySelected ? diarySelected.map(d => defaultValue.find(value => value === d)) : null;

    console.log('# findSelectedDefault', findSelectedDefault, findSelectedDefault.find(f => !parseInt(f)));

    if (submiting || !diary || !diary.length) return true;
    return false
  };

  render() {
    const { t, defaultValue } = this.props;
    const { checkboxItems, diary, submiting } = this.state;

    console.log('# render ModalDiary');

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        snapPoints={[{ y: height }, { y: 5 }]}
        ref={ref => this.saveRef(ref)}
        checkboxItems={checkboxItems}
      >
        <_ButtonClose onPress={!submiting ? this.closeModal : () => false} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>

        <_WrapperCheckbox>
          { checkboxItems.map((checkbox, key) =>
            <CHECKBOX_BUTTON
              key={key}
              onPressFn={() => this._handleChecked(checkbox.label)}
              active={checkbox.checked}
              label={checkbox.label}
            />)
          }
        </_WrapperCheckbox>

        <_WrapperButtonSubmit>
          <BUTTON
            onPressFn={this.onSave}
            color={Colors.SUCCESS}
            content={t('common:buttons:save')}
            disabled={this.checkDisabled()}
          />
        </_WrapperButtonSubmit>
      </_ContainerInteractable>
    );
  }
}

export default withLocalization(ModalDiary);
