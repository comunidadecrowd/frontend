import styled from 'styled-components/native';
import { Dimensions, Platform, StatusBar } from 'react-native';
import BUTTON from 'GLOBALS/BUTTON';
import { TEXT } from 'GLOBALS/TEXT';
import { Colors } from 'theme';

const { width } = Dimensions.get('window');
const statusBarHeight = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;

export const _Container = styled.View``;

export const _ScrollView = styled.ScrollView`
  width: ${width}px;
  margin-top: ${`-${statusBarHeight}px`}; /* // TODO:  Alex || Bruno -> Investigar melhor o porque nessa tela fica com essa diferença */
  /*background: ${Colors.WHITE};*/
`;

export const _WrapperHeader = styled.View`
  position: absolute;
  padding: 26px 20px 0;
  z-index: 3;
`;

export const _WrapperContent = styled.View`
  padding: 20px 30px 0;
  background: ${Colors.WHITE};
`;

export const _StaticsInfos = styled.View`
  flex-direction: row; 
  justify-content: space-between;
  padding: 40px 29px 25px;
  background: ${Colors.BLACK};
`;

export const _StaticItem = styled.View`
  justify-content: center;
`;

export const _StaticLabel = styled(TEXT)`
  color: #9B9B9B;
`;

export const _ValuePerHour = styled(TEXT)``;

export const _ProposesCount = styled(TEXT)``;

export const _JobsCount = styled(TEXT)``;

export const _ButtonsActions = styled.View`
  flex-direction: row;
  justify-content: space-between;
  padding: 0 29px 15px;
  background: ${Colors.BLACK};
`;

export const _WrapperButton = styled.View`
  width: ${(width - 58) / 2}px;
  justify-content: center;
`;

// NOTE:  26/06 - Botões criados para limpar o Storage do Device que auxiliam em evitar uma nova instalação do APP.

export const _StorageButtonsWrapper = styled.View`
  flex-direction: row;
  width: 100%;
`

export const _StorageButtons = styled(BUTTON)``