import styled from 'styled-components/native';
import { Dimensions, View, Platform } from 'react-native';
import { TEXT } from 'GLOBALS/TEXT';
import BUTTON from 'GLOBALS/BUTTON';

const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  height: 100%;
  align-items: center;
  padding-top: 23px;
`;

export const _FormInputs = styled.TextInput`
  height: 100%;
  padding: 0 5px;
  width: 100%;
  font-size: 18px;
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  color: #fff;
`;

export const _CategoryItem = styled.TouchableOpacity`
  flex-direction: row;
  height: 35px;
  margin: ${({isLast}) => !isLast ? '0 0 10px' : '0' };
  align-items: center;
`;

export const _CategoryRadio = (props) => {

  const { categoryActive } = props;

  return (
    <View style={{
      width: 21,
      height: 21,
      borderWidth: 1,
      borderRadius: 21,
      borderColor: categoryActive ? "#BCDEA1" : '#666',
      marginRight: 15,
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <View style={{
      width: 6,
      height: 6,
      borderRadius: 6,
      backgroundColor: categoryActive ? "#BCDEA1" : '#666',
    }}/>
    </View>
  )
}

export const _CategoryText = styled(TEXT)`
  font-size: 18px;
  color: ${({categoryActive}) => categoryActive ? "#BCDEA1" : '#fff'};
`;

export const _SendButton = styled(BUTTON)``;