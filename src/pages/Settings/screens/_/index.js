import React, { Component } from 'react';
import withLocalization from 'utils/withLocalization';
import Routes from 'Routes';

import { Colors } from 'theme';
import { HEADER } from 'GLOBALS/HEADER';
import MenuList from './UI/MenuList';

import {
  _Container,
} from './styles';

class Account extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userID: null,
      name: '',
      title: '',
      category: '',
      profileImage: '',
      menuList: [
        {
          key: '0',
          label: APP_STORAGE.getItem('User').name,
          attributes: {
            type: 'profile',
            linkTo: Routes.SETTINGS.MY_PROFILE,
            userID: APP_STORAGE.getItem('User').userID,
            title: APP_STORAGE.getItem('User').title,
            category: APP_STORAGE.getItem('User').category,
            profileImage: APP_STORAGE.getItem('User').profileImage
          }
        },

        {
          key: '1',
          label: 'CROWD Match',
          attributes: {
            type: 'match',
            linkTo: null,
            radioButton: {
              label: 'Ativo',
              labelInactive: 'Inativo',
              active: false,
              activeColor: Colors.TECHNOLOGY,
              inactiveColor: Colors.DESIGN
            }
          }
        },
        {
          key: '2',
          label: 'Transações',
            attributes: {
            type: 'transactions',
            linkTo: null
          }
        },
        {
          key: '3',
          label: 'Configurações',
          attributes: {
            type: 'settings',
            linkTo: Routes.SETTINGS.MY_SETTINGS
          }
        },
        {
          key: '4',
          label: 'Ajuda',
          attributes: {
            type: 'help',
            linkTo: null
          }
        }
      ]
    };
  }

  async componentDidMount() {
    const { menuList } = this.state;
    let
      getUserOnStorage = await APP_STORAGE.getItem('User');

    this.userOnStorage = getUserOnStorage;
    this.userID = getUserOnStorage.userID;

    if (getUserOnStorage.userID) {
    
      menuList.find(item => item.attributes.type === 'profile').label = getUserOnStorage.name;
      menuList.find(item => item.attributes.type === 'profile').attributes.userID = getUserOnStorage.userID;
      menuList.find(item => item.attributes.type === 'profile').attributes.title = getUserOnStorage.title;
      menuList.find(item => item.attributes.type === 'profile').attributes.category = getUserOnStorage.category;
      menuList.find(item => item.attributes.type === 'profile').attributes.profileImage = getUserOnStorage.profileImage;

      this.setState({ menuList });
    
    }
  }

  render() {
    const { t, userID, name, title, category, profileImage } = this.props;
    const { menuList } = this.state;

    return (
      <_Container>
        <HEADER title={t('settings:account:titleHeader')} paddingTop />
        <MenuList items={menuList} />
      </_Container>
    );
  }
}

export default withLocalization(Account);
