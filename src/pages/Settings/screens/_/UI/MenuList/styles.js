import React from 'react';
import styled from 'styled-components';
import { Dimensions, View } from 'react-native';
import Svg, { Polygon } from 'react-native-svg';
import { TEXT } from 'GLOBALS/TEXT';
import { Colors } from 'theme';
import Icon from 'components/Icon';
import { AVATAR } from 'GLOBALS/AVATAR';

export const _MenuList = styled.FlatList`
  width: 100%;
  padding: 0 30px;
`;

export const _MenuItem = styled.View`
  width: 100%;
  border: 0px solid #444444;
  border-bottom-width: 2px;
`;

export const _Button = styled.TouchableOpacity`
  flex-direction: row;
  align-items: ${({ isProfile }) => !isProfile ? 'center' : 'flex-start'};
  justify-content: space-between;
  min-height: ${({ isProfile }) => !isProfile ? 58 : 58}px;
  padding-top: ${({ isProfile }) => !isProfile ? 0 : 10}px;
  padding-bottom: ${({ isProfile }) => !isProfile ? 0 : 20}px;
`;

export const _InfosProfile = styled.View`
  flex-direction: column;
  flex-grow: 2;
  padding-top: 7px;
  padding-left: 10px;
`;

export const _Avatar = styled(AVATAR)`
  width: 50px;
  height: 50px;
  background: gray;
`

export const _AvatarDefault = styled.View`
  width: ${({size}) => size === 'big' ? 42 : 52};
  height: ${({size}) => size === 'big' ? 42 : 52};
  align-items: center;
  justify-content: center;
  border: 1px solid ${({ color }) => color ? color : '#ED4B68'};
  border-radius: ${({size}) => size === 'big' ? 67 : 52};
  overflow: hidden;
`;

export const _Label = styled(TEXT)``;

export const _Title = styled(TEXT)`
  color: #4A4A4A;
`;

export const _IconNext = styled(Icon)`
  color: ${Colors.WHITE};
  font-size: 34px;
  margin-right: -10px;
`;

export const _RadioButton = styled.TouchableOpacity`
  flex-direction: row;
  /*height: 35px;*/
  /*margin: ${({ isLast }) => !isLast ? '0 0 10px' : '0' };*/
  align-items: center;
`;

export const _Radio = (props) => {

  const { active } = props;

  return (
    <View style={{
      width: 21,
      height: 21,
      borderWidth: 1,
      borderRadius: 21,
      borderColor: active ? `${Colors.TECHNOLOGY}` : `${Colors.DESIGN}`,
      alignItems: 'center',
      justifyContent: 'center',
    }}>
    <View style={{
      width: 6,
      height: 6,
      borderRadius: 6,
      backgroundColor: active ? `${Colors.TECHNOLOGY}` : `${Colors.DESIGN}`,
    }}/>
    </View>
  )
}

export const _RadioLabel = styled(TEXT)`
  color: #9B9B9B;
  padding-right: 20px;
`;

export const _CROWDIcon = (props) => (
  <Svg {...props}>
    <Polygon points="26,26 13,0 0,26" fill="#666" stroke="none" />
  </Svg>
);
