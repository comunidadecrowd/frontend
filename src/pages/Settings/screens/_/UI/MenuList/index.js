import React, { Component } from 'react';
import Routes from 'Routes';
import { navigateFromTo } from 'utils/navigateFromTo';

import { Icons } from 'components/Icon';
import { categoryColor, STORAGE_URL } from 'utils/Helpers';

import RADIO_BUTTON from 'GLOBALS/RADIO_BUTTON';

import {
  _MenuList,
  _MenuItem,
  _Button,
  _Label,
  _Title,
  _RadioButton,
  _Radio,
  _RadioLabel,
  _IconNext,
  _InfosProfile,
  _Avatar,
  _AvatarDefault,
  _CROWDIcon,
} from './styles';

class MenuList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: this.props.items
    };
  }

  _keyExtractor = (item, key) => item.key;

  _handleOnPress = ({ linkTo, radioButton }, key) => {
    const { onPressFn } = this.props;

    if (!linkTo && !radioButton) return;

    if (radioButton) return this._handleSelected(key);

    return navigateFromTo(Routes.SETTINGS._, linkTo);
  }

  _handleSelected = (key) => {
    const { items } = this.state;
    
    if (items.find(item => item.key === key).attributes.radioButton) {
      items.find(item => item.key === key).attributes.radioButton.active = !items.find(item => item.key === key).attributes.radioButton.active;
    }
    // items.find(item => item.key === key).attributes.radioButton.label = !items.find(item => item.key === key).attributes.radioButton.active ? items.find(item => item.key === key).attributes.radioButton.labelInactive : items.find(item => item.key === key).attributes.radioButton.label;

    this.setState({ items });
  }
  
  _renderItem = ({ item: { key, label, attributes } }) => {
    if ( attributes && attributes.type === 'profile' ) {
      return (
        <_MenuItem>
          <_Button isProfile onPress={() => this._handleOnPress(attributes, key)} activeOpacity={1}>
            {
              attributes.profileImage
                ? (
                  <_Avatar
                    uri={{ uri: `${STORAGE_URL}.appspot.com/users/${attributes.userID}/profile/${attributes.profileImage}` }}
                    color={categoryColor(attributes.category)}
                  />
                ) : (
                  <_AvatarDefault color={categoryColor(attributes.category)} size={'big'}>
                    <_CROWDIcon width={24} height={20} />
                  </_AvatarDefault>
                )
            }
            <_InfosProfile>
              <_Label
                content={label}
                color={categoryColor(attributes.category)}
                fontSize={16}
                bold
              />
              <_Title
                content={attributes.title}
                fontSize={14}
              />
            </_InfosProfile>
            <_IconNext icon={Icons.CHEVRON_RIGHT} />
          </_Button>
        </_MenuItem>
      );
    }

    return (
      <_MenuItem>
        <_Button onPress={attributes ? () => this._handleOnPress(attributes, key) : () => false} activeOpacity={1}>
          <_Label
            content={label}
            fontSize={14}
            bold
          />
          { attributes && attributes.radioButton ?
            <RADIO_BUTTON
              active={attributes.radioButton.active}
              label={attributes.radioButton.label}
              labelInactive={attributes.radioButton.labelInactive}
              activeColor={attributes.radioButton.activeColor}
              inactiveColor={attributes.radioButton.inactiveColor}
            />
            : <_IconNext icon={Icons.CHEVRON_RIGHT} />
          }
        </_Button>
      </_MenuItem>
    );
  }

  render() {
    const { items } = this.state;

    return (
      <_MenuList
        data={items}
        renderItem={this._renderItem}
        keyExtractor={this._keyExtractor}
      />
    );
  }
}

export default MenuList;
