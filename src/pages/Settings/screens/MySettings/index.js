import React, { Component } from 'react';
import Routes from 'Routes';
import { Navigation } from 'react-native-navigation';

import { HEADER } from 'GLOBALS/HEADER';
import MenuList from './../_/UI/MenuList';

import {
  _Container,
  _WrapperHeader,
  _LineTopList,
} from './styles';


class MySettings extends Component {
  constructor(props) {
    super(props);

    this.state = {}
  }

  render() {
    return (
      <_Container>
        <_WrapperHeader>
          <HEADER
            withButtonsNavigation
            buttonLeft={() => Navigation.popTo(Routes.SETTINGS._)}
            title={'Configurações'}
          />
        </_WrapperHeader>

        <_LineTopList />
        <MenuList
          items={[
            {
              key: '0',
              label: 'Mensagens Salvas',
              attributes: {
                linkTo: null
              }
            },
            {
              key: '1',
              label: 'Ligações Recentes',
              attributes: {
                linkTo: null
              }
            },
            {
              key: '2',
              label: 'Seus Arquivos',
              attributes: {
                linkTo: null
              }
            },
            {
              key: '3',
              label: 'Como quer pagar?',
              attributes: {
                linkTo: null
              }
            },
            {
              key: '4',
              label: 'Como quer receber?',
              attributes: {
                linkTo: null
              }
            },
            {
              key: '5',
              label: 'Notificações',
              attributes: {
                linkTo: null
              }
            },
            {
              key: '6',
              label: 'Importar Lista de Contatos',
              attributes: {
                linkTo: null
              }
            },
          ]}
        />
      </_Container>
    );
  }
}

export default MySettings;
