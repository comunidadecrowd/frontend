import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

import { TEXT } from 'GLOBALS/TEXT';
import BUTTON from 'GLOBALS/BUTTON';

const { width, height } = Dimensions.get('window');

export const _WrapperHeader = styled.View`
  padding: 29px 20px 0 0;
`;

export const _WrapperContent = styled.View`
  width: ${width - (29 * 2) }px;
  align-self: center;
  height: 100%;
  overflow: hidden;
`;

export const _Title = styled(TEXT)``;

export const _Main = styled.View`
  height: ${height - 100}px;
  /*padding: 0 29px;*/
  justify-content: space-around;
`;

export const _FormWrapper = styled.View`
  width: 100%;
  height: 115px;
  justify-content: space-between;
`;

export const _InputsWrapper = styled.View`
  width: ${width + 15 }px;
  height: 50px;
  flex-direction: row;
  align-items: center;
  border-bottom-width: 1px;
`;

export const _FormInputs = styled.TextInput`
  width: 100%;
  height: 100%;
  padding: 0 5px;
  font-size: 18px;
  font-family: 'Inter UI';
  color: #fff;
`;

export const _RadiosButton = styled.View``;

export const _SendButton = styled(BUTTON)``;

