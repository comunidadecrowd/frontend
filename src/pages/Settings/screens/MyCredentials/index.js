import React, { Component } from 'react';
import { KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback, Animated, Easing } from 'react-native';
import withLocalization from 'utils/withLocalization';

import APIS from 'apis';
import Routes from 'Routes';
import { Navigation } from 'react-native-navigation';
import { Colors } from 'theme';
import CROWD_ARROW from 'GLOBALS/CROWD_ARROW';
import { HEADER } from 'GLOBALS/HEADER';
import RADIO_BUTTON from 'GLOBALS/RADIO_BUTTON';

import {
  _WrapperHeader,
  _WrapperContent,
  _Title,
  _Main,
  _FormWrapper,
  _InputsWrapper,
  _FormInputs,
  _RadiosButton,
  _SendButton
} from './styles';


const CROWD_ARROWAnimated = Animated.createAnimatedComponent(CROWD_ARROW);
const _InputsWrapperAnimated = Animated.createAnimatedComponent(_InputsWrapper);

class MyCredentials extends Component {
  constructor(props) {
    super(props);

    this.animations = {
      inputs: Array.from(Array(2), () => new Animated.Value(0)),
      arrow: Array.from(Array(2), () => new Animated.Value(0)),
    }

    this.user = APP_STORAGE.getItem('User');

    this.state = {
      userID: this.props.hasOwnProperty('userID') ? this.props.userID : undefined,
      credentials: {
        name: global.CrowdStorage.User ? global.CrowdStorage.User.name : '',
        title: global.CrowdStorage.User ? global.CrowdStorage.User.title : '',
        category: global.CrowdStorage.User ? global.CrowdStorage.User.category : ''
      },
      profileImage: global.CrowdStorage.User ? global.CrowdStorage.User.profileImage : null,
      submitting: false
    }
  }

  async componentDidMount() {

    const { userID } = this.props;
    let getUserOnStorage = await APP_STORAGE.getItem('User');
    
    this.user = getUserOnStorage;

    if (getUserOnStorage && (
      this.state.credentials.name === ''
        || this.state.credentials.title === ''
        || this.state.credentials.title !== getUserOnStorage.title
        || this.state.credentials.category === '' 
        || this.state.credentials.category !== getUserOnStorage.category
    )) {
      this.setState({
        credentials: {
          name: getUserOnStorage.name,
          title: getUserOnStorage.title,
          category: getUserOnStorage.category
        },
        profileImage: getUserOnStorage.profileImage
      });
    }

    if (!userID) {

      this.setState({
        userID: getUserOnStorage.userID
      })

    }

  }

  handlerInput = (context, item, value = false) => {

    const
      { inputs, arrow } = this.animations,
      { credentials } = this.state,
      easing = Easing.in(),
      duration = 300;

    if (item === 'name') {

      if (context === 'change') {
        this.setState({
          credentials: {
            ...credentials,
            name: value.trim()
          }
        })
      }

      if(context === 'blur') {

        Animated.timing(inputs[0], {
          toValue: credentials.name.trim().length < 3 ? 2 : 3,
          easing,
          duration
        }).start();

        Animated.timing(arrow[0], {
          toValue: 0,
          easing,
          duration
        }).start();
      
      }
  
    } 

    if (item === 'professionalTitle') {

      if (context === 'change') {
        this.setState({
          credentials: {
            ...credentials,
            title: value.trim()
          }
        })
      }
  
      if(context === 'blur') {
  
        Animated.timing(inputs[1], {
          toValue: credentials.title.trim().length < 3 ? 2 : 3,
          easing,
          duration
        }).start();

        Animated.timing(arrow[1], {
          toValue: 0,
          easing,
          duration
        }).start();
      
      }
    
    }

    if(context === 'focus') {

      Animated.timing(inputs[item === 'name' ? 0 : 1], {
        toValue: 1,
        easing,
        duration
      }).start();

      Animated.timing(arrow[item === 'name' ? 0 : 1], {
        toValue: 1,
        easing,
        duration
      }).start();

    }

  }

  handlerRadioBox = (choice) => {

    const { credentials } = this.state;

    this.setState({
      credentials: {
        ...credentials,
        category: choice
      }
    })

  }

  sendCredentials = () => {

    const { backPage } = this.props;
    const objToSend = this.state;
    const {userID, credentials: { name, title, category }, submitting} = objToSend;

    this.setState({ submitting: true });
    APIS.User.sendCredentials(objToSend, (data) => {

      console.log('sendCredentials then data', data);

      const {inputCredentials: {email, phone}} = data;
      
      let newDataToUser = {
        ...this.user,
        hasRegister: this.user.hasRegister,
        // alreadyImportedContacts: this.user.alreadyImportedContacts, // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
        name, 
        title, 
        category,
        email,
        phone
      }

      this.user = newDataToUser;

      const callback = () => {
        this.user = newDataToUser;

        this.setState({ submitting: false }, () => backPage(newDataToUser));
      };

      APP_STORAGE.updateItem('User', newDataToUser, callback());

    })

  }

  render() {
    const
      { t } = this.props,
      { inputs, arrow } = this.animations,
      { credentials, credentials: { name, title, category }, profileImage,
        userID, submitting } = this.state,
      inputsWrapperInterpolate = inputs.map(item => {

        return {
          borderBottomColor: item.interpolate({
            inputRange: [0, 1, 2, 3],
            outputRange: ['#444', '#84C9F2', '#FFC80B', '#BCDEA1']
          }),
          left: item.interpolate({
            inputRange: [0, 1, 2, 3],
            outputRange: [-15, 0, -15, -15]
          })
        }

      }),
      arrowInterpolate = arrow.map(item => item.interpolate({
        inputRange: [0, 1],
        outputRange: [-10, 0]
      })),
      categories = ['design', 'technology', 'content', 'media'],
      formIsDisabled = (name.trim().length < 3 || title.trim().length < 3 || !category.length) || (name === this.user.name && title == this.user.title && category === this.user.category);

    return (
      <KeyboardAvoidingView behavior="padding">
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <_WrapperContent>
            <_WrapperHeader>
              <HEADER
                withButtonsNavigation
                buttonLeft={() => !submitting ? Navigation.popTo(Routes.SETTINGS.MY_PROFILE) : false}
              />
            </_WrapperHeader>
            <_Main>
              <_Title
                content={t(`login:register:title`)}
                fontSize={20}
                lineHeight={30}
                euclid
              />
              <_FormWrapper>
                {
                  ['name', 'professionalTitle'].map((item, index) => {
                    return (
                      <_InputsWrapperAnimated
                        key={index}
                        style={inputsWrapperInterpolate[index]}
                      >
                        <CROWD_ARROWAnimated
                          orientation={"right"}
                          size={5}
                          style={{ top: 0, color: "#84C9F2", left: arrowInterpolate[index] }}
                        />
                        <_FormInputs
                          placeholder={index === 0 ? t(`login:register:formInputs:name`) : t(`login:register:formInputs:professionalTitle`)}
                          placeholderTextColor={'#9B9B9B'}
                          value={index === 0 ? name : title}
                          keyboardType={'default'}
                          onFocus={() => this.handlerInput('focus', item)}
                          onBlur={() => this.handlerInput('blur', item)}
                          onChangeText={(value) => this.handlerInput('change', item, value)}
                        />
                      </_InputsWrapperAnimated>
                    )
                  })
                }
              </_FormWrapper>
              <_RadiosButton>
                {categories.map(item => 
                  <RADIO_BUTTON
                    key={item}
                    active={credentials.category === item ? credentials.category : null }
                    label={t(`login:register:formInputs:categories:${item}`)}
                    labelInactive={t(`login:register:formInputs:categories:${item}`)}
                    alignRight
                    height={35}
                    onPressFn={() => this.handlerRadioBox(item)}
                  />
                )} 
              </_RadiosButton>
              <_SendButton
                disabled={formIsDisabled || submitting}
                onPressFn={this.sendCredentials}
                content={t('login:register:sendButton')}
                color={!formIsDisabled ? "#BCDEA1" : undefined}
              />
            </_Main>
          </_WrapperContent>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }
}

export default withLocalization(MyCredentials);
