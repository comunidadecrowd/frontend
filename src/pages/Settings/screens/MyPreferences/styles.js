import styled from 'styled-components/native';
import { Dimensions } from 'react-native';

import { TEXT } from 'GLOBALS/TEXT';

const { width, height } = Dimensions.get('window');

export const _Container = styled.View``;

export const _WrapperHeader = styled.View`
  padding: 29px 20px 0;
`;

export const _CheckboxWrapper = styled.View`
  padding: 29px 29px 0;
`;

export const _TitleCheckbox = styled(TEXT)`
  margin-bottom: 32px;
`;

export const _LineTopList = styled.View`
  width: ${width - 58}px;
  height: 2px;
  margin-left: 29px;
  background: #444444;
`;
