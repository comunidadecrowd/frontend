import React, { Component } from 'react';
import Routes from 'Routes';
import { Navigation } from 'react-native-navigation';

import { Colors } from 'theme';
import { HEADER } from 'GLOBALS/HEADER';
import CHECKBOX_BUTTON from 'GLOBALS/CHECKBOX_BUTTON';
import MenuList from './../_/UI/MenuList';

import {
  _Container,
  _WrapperHeader,
  _LineTopList,
  _CheckboxWrapper,
  _TitleCheckbox,
} from './styles';


class MyPreferences extends Component {
  constructor(props) {
    super(props);

    this.state = {
      menuList: [
        {
          key: '0',
          label: 'Categorias',
          attributes: {
            linkTo: null
          }
        },
        {
          key: '1',
          label: 'Disponibilidade',
          attributes: {
            linkTo: null,
            radioButton: {
              label: 'Ativo',
              labelInactive: 'Inativo',
              active: true,
              activeColor: Colors.TECHNOLOGY,
              inactiveColor: Colors.DESIGN
            }
          }
        },
      ],
      checkboxItems: [
        {
          label: 'Meio Período',
          checked: false
        },
        {
          label: 'Período Integral',
          checked: true
        },
        {
          label: 'Remoto',
          checked: false
        },
        {
          label: 'Alocado',
          checked: true
        },
        {
          label: 'Freelancer',
          checked: false
        },
        {
          label: 'Contrato',
          checked: false
        },
      ]
    };
  }

  _handleChecked = (key) => {
    const { checkboxItems } = this.state;

    checkboxItems[key].checked = !checkboxItems[key].checked;
    this.setState({ checkboxItems: checkboxItems });
  }

  render() {
    const { menuList, checkboxItems } = this.state;

    return (
      <_Container>
        <_WrapperHeader>
          <HEADER
            withButtonsNavigation
            buttonLeft={() => Navigation.popTo(Routes.SETTINGS.MY_PROFILE)}
            title={'Preferências'}
          />
        </_WrapperHeader>

        <_LineTopList />
        <MenuList items={menuList} />
        
        <_CheckboxWrapper>
          <_TitleCheckbox
            content={'Tipos de Jobs que lhe interessam?'}
            fontSize={14}
            bold
          />
          { checkboxItems.map((checkbox, key) =>
            <CHECKBOX_BUTTON
              key={key}
              onPressFn={() => this._handleChecked(key)}
              active={checkbox.checked}
              label={checkbox.label}
            />)
          }
        </_CheckboxWrapper> 
      </_Container>
    );
  }
}

export default MyPreferences;
