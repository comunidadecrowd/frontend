import Account from './screens/_';
import MyProfile from './screens/MyProfile';
import MyPreferences from './screens/MyPreferences';
import MySettings from './screens/MySettings';
import MyCredentials from './screens/MyCredentials';
import NetworkingAccounts from './screens/NetworkingAccounts';

export default Account;
export { MyProfile, MyPreferences, MySettings, MyCredentials, NetworkingAccounts };
