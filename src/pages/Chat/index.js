import Chat from './screens/_';
import Channel from './screens/Channel';
import Channels from './screens/Channels';

export default Chat;
export { Channel, Channels };
