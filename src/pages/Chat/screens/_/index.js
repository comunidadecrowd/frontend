import React, { Component } from 'react';
// import { AppState, PushNotificationIOS } from 'react-native'; // REFACT:  15/06 - Refactor TWILIO desvinculado de UI

// import ImportContacts from './ImportContacts'; // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
import Channels from './../Channels';
// import LoadingModal from 'components/LoadingModal'; // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório

import firebase from 'react-native-firebase';
import { Navigation } from 'react-native-navigation';

// import { _Container } from './styles'; // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório

class Chat extends Component {

  constructor(props) {

    super(props);

    this.activeChannel;
    this.navigationEventListener = Navigation.events().bindComponent(this); // TODO:  Alex, rever se é necessário aribuir esse valor novamente, já que no RegisterPages -> constructor, já existe algo 'parecido' 

  }

  /* // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
    constructor(props) {

      super(props);

      this.userID;
      this.userOnStorage;
      this.activeChannel;

      this.state = {
        alreadyImportedContacts: undefined,
        init: this.props.alreadyImportedContacts ? true : false,
      }

    }
  */

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    constructor(props) {

      super(props);

      this.Chat = null;
      this.userID = undefined;

      this.state = {
        alreadyImportedContacts: undefined,
        init: this.props.alreadyImportedContacts ? true : false,
      }

      this.userOnStorage = undefined;
      this.activeChannel = undefined;

    }
  */

  async componentDidMount() {

    const
      { fromNavigation } = this.props,
      firebaseUserToken = fromNavigation ? this.props._FIREBASEUSERTOKEN : global.CrowdStorage.User._FIREBASEUSERTOKEN;

    if (firebaseUserToken !== null) firebase.messaging().onMessage((firebaseData) => this.handleNotifications(firebaseData));

  }

  /* // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
    async componentDidMount() {

      let getUserOnStorage = await APP_STORAGE.getItem('User');

      this.userOnStorage = getUserOnStorage;
      this.userID = getUserOnStorage.userID;

      if (getUserOnStorage.alreadyImportedContacts === false) {
        this.setState({
          alreadyImportedContacts: false,
          init: true,
          userID: getUserOnStorage.userID
        });
      } else {

        if (getUserOnStorage._FIREBASEUSERTOKEN !== null) {
          firebase.messaging().onMessage((firebaseData) => this.handleNotifications(firebaseData));
        }

        this.setState({
          alreadyImportedContacts: true,
          init: true
        })
      }

      this.navigationEventListener = Navigation.events().bindComponent(this);

    }
  */

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI 
    async componentDidMount() {

      let
        getUserOnStorage = await APP_STORAGE.getItem('User'),
        getAllChatsOnStorage = await APP_STORAGE.getAll(/^CHAT_/);

      this.userOnStorage = getUserOnStorage;
      this.userID = getUserOnStorage.userID;

      if (getUserOnStorage.alreadyImportedContacts === false) {
        this.setState({
          alreadyImportedContacts: false,
          init: true,
          userID: getUserOnStorage.userID
        });
      } else {
        APP_SWORKER.initChat(getUserOnStorage.userID).then((Chat) => {

          if (getAllChatsOnStorage.length) {

            this.Chat = Chat;
            // this.updateMessagesOnStorage(getAllChatsOnStorage);

          }

          if (getUserOnStorage._FIREBASEUSERTOKEN !== null) {
            firebase.messaging().onMessage((firebaseData) => this.handleNotifications(firebaseData));
          }

          this.setState({
            alreadyImportedContacts: true,
            init: true
          }, () => {
            APP_STORAGE.updateItem('User', {
              ...getUserOnStorage,
              _TOKEN: Chat.fpaToken,
            })
          });

        });
      }

      AppState.addEventListener('change', this.AppStateFN);
      this.navigationEventListener = Navigation.events().bindComponent(this);

    }
  */

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI 
    componentWillUnmount() {
      AppState.removeEventListener('change', this.AppStateFN);
    }
  */

  componentDidAppear() {
    this.activeChannel = undefined;
  }

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI 
    updateMessagesOnStorage = (getAllChatsOnStorage) => {

      getAllChatsOnStorage.map(async (channelStorage) => {

        let
          getChannelSID = channelStorage[0].replace("CHAT_", ""),
          getChannel;

        try {
          getChannel = await this.Chat.getChannelBySid(getChannelSID);
        } catch (error) {
          getChannel = null;
        }

        if (!getChannel) return;
        if (!getChannel.lastMessage) return;

        const
          { lastConsumedMessageIndex, lastMessage: { index: indexFromLastMessage } } = getChannel,
          getMessages = (indexFromLastMessage - lastConsumedMessageIndex) !== 0 ? await getChannel.getMessages((indexFromLastMessage - lastConsumedMessageIndex), (indexFromLastMessage + 1)) : null;

        if (!getMessages) return;

        const { items: friendMessages } = getMessages;

        let
          parseChannelStorage = JSON.parse(channelStorage[1]),
            lastMessageIsFromFriend = (parseChannelStorage.slice(-1).length === 1 && parseInt(parseChannelStorage.slice(-1)[0].state.author) !== parseInt(this.userID)),
          updateChannelOnStorage,
          comesFromMe = friendMessages.filter(({author}) => author === this.userID).length,
          previouslyChanged = parseChannelStorage.slice(-1)[0].state.attributes.crowdID === friendMessages.slice(-1)[0].state.attributes.crowdID;

        if (lastMessageIsFromFriend) {

          if (previouslyChanged) return;

          friendMessages.map(msg => {

            const { author, attributes, body, status } = msg.state;

            if (!attributes.type) attributes.type = 'default';
            
            parseChannelStorage.slice(-1).push({
              author,
              body,
              attributes,
              status
            })

          })

          APP_STORAGE.updateItem(channelStorage[0], parseChannelStorage)

        } else {

          if (comesFromMe) return;

          updateChannelOnStorage = [
            ...parseChannelStorage,
            {
              state: friendMessages.map(msg => {

                const { author, attributes, body, status } = msg.state;

                if (!attributes.type) attributes.type = 'default';

                return {
                  author,
                  body,
                  attributes,
                  status
                }

              })
            }
          ]

          APP_STORAGE.updateItem(channelStorage[0], updateChannelOnStorage)

        }

      })

    }
  */

  /* // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
    importContactsCompletedFN = () => {
      APP_STORAGE.updateItem('User', {
        ...this.userOnStorage,
        alreadyImportedContacts: true 
      }, this.setState({ alreadyImportedContacts: true }));
    }
  */

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI 
    AppStateFN = (appState) => {
      if (appState === 'active') APP_SWORKER.initChat(this.userOnStorage.userID)
    }
  */

  setActiveChannelFN = (activeChannel) => {
    this.activeChannel = activeChannel;
  }

  handleNotifications = (firebaseData) => {

    console.log("### handleNotifications -> firebaseData", firebaseData);

    const
      { data, data: { author, channel_id, twi_message_type, twi_body } } = firebaseData,
      isMessage = twi_message_type && twi_message_type.endsWith('new_message');

    let notification;

    if ((this.activeChannel && this.activeChannel === channel_id) || !isMessage) return;

    notification = new firebase.notifications.Notification()
      .setTitle(author)
      .setBody(twi_body.substr((author.length + 2), twi_body.length))
      .setSound("default");

    firebase.notifications().displayNotification(notification)

    // firebase.notifications().onNotification((notification) => {
    //   console.log("### firebase.notifications onNotification", notification);
    // });

    // firebase.notifications().onNotificationOpened((notificationOpen) => {
    //   console.log("### firebase.notifications onNotificationOpened", notificationOpen);
    // });

    // firebase.notifications().onNotificationDisplayed((notificationDisplayed) => {
    //   console.log("### firebase.notifications notificationDisplayed", notificationDisplayed);
    // });

  }

  componentWillUnmount() {
    // if (this.navigationEventListener) { // TODO:  Alex, verificar se remover o listener vai prejudicar o this.activeChannel
    //   this.navigationEventListener.remove();
    // }
  }

  render() {

    const
      { fromNavigation } = this.props,
      userID = fromNavigation ? this.props.userID : global.CrowdStorage.User.userID;

    return (
      <Channels
        fromNavigation={fromNavigation}
        userId={userID}
        setActiveChannel={(activeChannel) => this.setActiveChannelFN(activeChannel)}
      />

    );

  }

  /* // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
    render() {

      const { init } = this.state;

      const alreadyImportedContacts = this.props.hasOwnProperty(alreadyImportedContacts) ? this.props.alreadyImportedContacts : this.state.alreadyImportedContacts;
      const userID = this.props.hasOwnProperty(userID) ? this.props.userID : this.state.userID;

      return (
        <Fragment>
          {
            !init
              ? (
                <LoadingModal />
              ) : (
                <_Container hasChannels={alreadyImportedContacts}>
                  {
                    alreadyImportedContacts === false
                      ? (
                        <ImportContacts userID={userID} importContactsCompleted={this.importContactsCompletedFN.bind(this)} />
                      ) : (
                        <Channels
                          userId={this.userOnStorage.userID}
                          isFirstTime={this.props.alreadyImportedContacts === false ? true : false}
                          setActiveChannel={(activeChannel) => this.setActiveChannelFN(activeChannel)}
                        />
                      )
                  }
                </_Container>
              )
          }
        </Fragment>
      );

    }
  */

}

export default Chat
