import styled from 'styled-components/native';

export const _Container = styled.View`
  padding: ${({hasChannels}) => hasChannels ? '29px 0 0' : '29px' };
  height: 100%;
`;