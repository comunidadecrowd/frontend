import styled from 'styled-components/native';
import { Dimensions } from 'react-native';
import { Colors } from 'theme/Colors';
const { width, height } = Dimensions.get('window');

export const _Container = styled.View`
  height: 100%;
  background: ${({ theme }) => theme == 'white' ? Colors.WHITE : Colors.BLACK};
  align-content: space-between;
`;

export const _KeyboardAvoidingView = styled.KeyboardAvoidingView`
  align-content: space-between;
  justify-content: space-between;
`;

export const _BriefingButtonAnswer = styled.View`
  width: ${width};
  background: ${Colors.BLACK};
  position: absolute;
  left: 0;
  bottom: 0;
  z-index: 2;
`;
