import React, { Component } from 'react';
import { Colors } from 'theme/Colors';
import { categoryColor, STORAGE_URL } from 'utils/Helpers';
import IconStatus from 'components/Icon/IconStatus';

import {
  _Container,
  _UserImage,
  _BgGradient,
  _UserInfos,
  _UserName,
  _StatusInfos,
  _JobTitle,
  _UserLocalization,
  _UserStars,
  _StarIcon,
  _StartsPoints,
} from './styles';

import DefaultProfileImage from './img/default.png';

class ChatProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    
    const { userId, userName, profileImage, title: jobTitle, category } = this.props; 

    // REFACT:  mock props begin;
    const props = {
      isOnline: true,
      points: 4.98
    }
    // REFACT:  mock props end;

    return (
      <_Container>
        <_UserImage
          source={(profileImage === null || profileImage === '') ? DefaultProfileImage : {
            uri: `${STORAGE_URL}.appspot.com/users/${userId}/profile/${profileImage}`
          }} />
        <_BgGradient width="100%" height="100%" />
        <_UserInfos>
          <_StatusInfos>
            {/* TODO: Habilitar se na revisão da UI ainda foi necessário 
              <IconStatus
                width={6}
                height={6}
                userStatus={props.isOnline}
              />
             */}
            <_UserName
              euclid
              content={userName ? userName : 'UserName'}
              fontSize={24}
              color={categoryColor(category)}
            />
          </_StatusInfos>
          <_JobTitle
            content={jobTitle}
            color={'#9B9B9B'}
            fontSize={14}
          />
          {/* TODO: Habilitar quando o backEnd disponibilizar os dados dentro da mutation createChat
            <_UserLocalization bold content={'São Paulo, SP'} fontSize={14} /> 
          */}
          {/* TODO: Habilitar quando houver a função/método de Rating 
            <_UserStars>
              <_StarIcon width={18} height={18} color={Colors.WHITE} />
              <_StartsPoints
                bold
                content={props.points}
                fontSize={18}
              />
            </_UserStars> 
          */}
        </_UserInfos>
      </_Container>
    );
  }
}

export default ChatProfile;