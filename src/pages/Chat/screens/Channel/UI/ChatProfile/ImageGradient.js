import React from 'react';
import { Dimensions } from 'react-native';
import { Defs, G, LinearGradient, Polygon, Stop, Svg } from 'react-native-svg';

const { width: windowW, height: windowH } = Dimensions.get('window');

const ImageGradient = props => (
  <Svg {...props}>
    <Defs>
      <LinearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="gradient">
        <Stop stopColor="#000000" stopOpacity="0" offset="0%" />
        <Stop stopColor="#000000" stopOpacity="0.0625335577" offset="7.8289042%" />
        <Stop stopColor="#000000" stopOpacity="0.455857117" offset="66.714854%" />
        <Stop stopColor="#000000" offset="100%" />
      </LinearGradient>
    </Defs>
    <G stroke="none" fill="none" fill-rule="evenodd">
      <Polygon
        fill="url(#gradient)"
        points={`0 0 ${windowW} 0 ${windowW} ${windowH / 2} 262.820312 ${windowH / 2} 0 ${windowH / 2}`}
      />
    </G>
  </Svg>
);

export default ImageGradient;
