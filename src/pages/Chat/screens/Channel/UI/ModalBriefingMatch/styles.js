import styled from 'styled-components/native';
import Interactable from 'react-native-interactable';
import { Dimensions } from 'react-native';
import { isSmallIOS } from 'utils/Helpers';

import { Colors, FontRatioNormalize } from 'theme';

const { width, height } = Dimensions.get('window');

export const _ContainerInteractable = styled(Interactable.View)`
  align-items: center;
  position: absolute;
  width: ${width - 40};
  height: ${height - 200};
  background: ${Colors.BLACK};
  top: 110px;
  left: 20px;
  overflow: hidden;
  z-index: 1;
`;

export const _ButtonClose = styled.TouchableOpacity`
  position: absolute;
  top: 20px;
  right: 20px;
`;

export const _CarouselContainer = styled.View`
  margin: 40px 0 0;
`;

export const _CarouselTextWrapper = styled.View`
  min-height: 160px;
  padding: 30px 20px 0;
  color: ${Colors.WHITE};
`;

export const _CarouselText = styled.Text`
  text-align: center;
  color: ${Colors.WHITE};
  padding: 20px 20px 10px;
  font-size: ${FontRatioNormalize(16)};
`;

export const _ButtonActions = styled.View`
  align-items: center;
  padding: 0 40px;
`;

export const _ButtonActionsForIsSmallIOS = styled.View`
  flex-direction: row;
  width: ${width - 40};
  left: 20px;
`