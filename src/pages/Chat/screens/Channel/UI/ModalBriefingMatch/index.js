import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import withLocalization from 'utils/withLocalization';
import { IconCloseButton } from 'components/Icon/SearchView';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { FirstIcon, SecondIcon, ThirdIcon } from 'components/Icon/CarouselBriefingMatch';
import BUTTON from 'GLOBALS/BUTTON';
import { Colors } from 'theme/Colors';
import { isSmallIOS } from 'utils/Helpers';

import {
  _ContainerInteractable,
  _ButtonClose,
  _CarouselContainer,
  _CarouselTextWrapper,
  _CarouselText,
  _ButtonActions,
  _ButtonActionsForIsSmallIOS,
} from './styles';

const { width, height } = Dimensions.get('window');

class ModalBriefingMatch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSnapIndex: 0,
      sliderActiveItem: 0,
      entries: [
        'Você recebeu um briefing! Para conversar com o cliente e entender mais sobre o projeto, basta aceitar!',
        'Durante a conversa, você pode tirar dúvidas, negociar valores e prazos com o cliente. Não esqueça de alinhar as expectativas!',
        'Para enviar propostas comerciais na Crowd, você precisa certificar-se de indicar os valores em reais e os prazos em dias. Boa sorte.',
      ],
      submiting: false,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.state.currentSnapIndex !== nextState.currentSnapIndex ||
      this.state.sliderActiveItem !== nextState.sliderActiveItem ||
      this.state.submiting !== nextState.submiting ||
      this.props.openModalBriefingMatch !== nextProps.openModalBriefingMatch ||
      this.props.chatMessagesRef !== nextProps.chatMessagesRef
    );
  }

  saveRef = ref => {
    const { getRef } = this.props;
    getRef(ref);
    this.modal = ref;
  };

  closeModal = () => {
    this.modal.snapTo({ index: 0 });
  };

  onSubmitAcceptBriefingMatch = () => {
    const { userId, briefingId, channelSid, changeBriefingMatch, chatMessagesRef } = this.props;
    let { messages } = chatMessagesRef.state;

    this.setState({ submiting: true });
    APIS.Briefing.updateMatch(
      {
        userId,
        briefingId,
        status: 200,
      },
      ({ updateMatch }) => {
        this.setState({ submiting: false });
        messages.find(
          msg =>
            msg.state.attributes.type === 'briefing' &&
            msg.state.attributes.status <= 199 &&
            parseInt(msg.state.attributes.briefingId) === parseInt(briefingId)
        ).state.attributes.status = 200;
        chatMessagesRef.setState({ messages }, () => {
          changeBriefingMatch();
          this.closeModal();
        });
      }
    );
  };

  onSubmitRejectBriefingMatch = () => {
    const { userId, briefingId, changeBriefingMatch } = this.props;

    this.setState({ submiting: true });
    APIS.Briefing.updateMatch(
      {
        userId,
        briefingId,
        status: 300,
      },
      ({ updateMatch }) => {
        this.setState({ submiting: false });
        changeBriefingMatch(true);
        this.closeModal();
      }
    );
  };

  renderItem = ({ item, index }) => {
    return (
      <_CarouselTextWrapper key={index}>
        {index === 0 && (
          <FirstIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 1 && (
          <SecondIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 2 && (
          <ThirdIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        <_CarouselText>{item}</_CarouselText>
      </_CarouselTextWrapper>
    );
  };

  render() {
    const { t, openModalBriefingMatch } = this.props,
      { currentSnapIndex, sliderActiveItem, entries, submiting } = this.state,
      paginationDot = {
        width: 13,
        height: 13,
        borderRadius: 20,
        marginHorizontal: 10,
      };

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        ref={ref => this.saveRef(ref)}
        snapPoints={[{ y: height }, { y: 5 }]}
        currentSnapIndex={currentSnapIndex}
        openModalBriefingMatch={openModalBriefingMatch}
      >
        <_ButtonClose onPress={!submiting ? this.closeModal : () => false} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>

        <_CarouselContainer>
          <Carousel
            ref={ref => (this.carousel = ref)}
            data={entries}
            renderItem={this.renderItem}
            sliderWidth={width}
            itemWidth={width}
            swipeThreshold={10}
            horizontal
            firstItem={0}
            onSnapToItem={index => this.setState({ sliderActiveItem: index })}
          />

          <Pagination
            dotStyle={paginationDot}
            inactiveDotColor={'#FFFFFF'}
            activeDotIndex={sliderActiveItem}
            dotColor={'lightblue'}
            dotsLength={entries.length}
            carouselRef={this.carousel}
            inactiveDotScale={0.9}
          />

          {!isSmallIOS ? (
            <_ButtonActions>
              <BUTTON
                onPressFn={this.onSubmitAcceptBriefingMatch}
                color={Colors.SUCCESS}
                content={t('chat:typeMessages:briefingMatch:acceptBriefing')}
                disabled={submiting}
              />

              <BUTTON
                onPressFn={this.onSubmitRejectBriefingMatch}
                color={Colors.ERROR}
                content={t('chat:typeMessages:briefingMatch:rejectBriefing')}
                disabled={submiting}
              />
            </_ButtonActions>
          ) : (
            <_ButtonActionsForIsSmallIOS>
              {['rejectBriefingSmall', 'acceptBriefingSmall'].map((item, index) => {
                return (
                  <BUTTON
                    key={item}
                    bg={index === 0 ? '#411F20' : '#1C391B'}
                    content={t(`chat:typeMessages:briefingMatch:${item}`)}
                    onPressFn={
                      index === 0
                        ? this.onSubmitRejectBriefingMatch
                        : this.onSubmitAcceptBriefingMatch
                    }
                    fontSize={15}
                    disabled={submiting}
                    needFlex
                    color={index === 0 ? '#411F20' : '#1C391B'}
                    bg={index === 0 ? Colors.ERROR : Colors.SUCCESS}
                  />
                );
              })}
            </_ButtonActionsForIsSmallIOS>
          )}
        </_CarouselContainer>
      </_ContainerInteractable>
    );
  }
}

export default withLocalization(ModalBriefingMatch);
