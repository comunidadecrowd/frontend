import React, { Component, Fragment } from 'react';
import ChatTypeMessages from './../ChatTypeMessages';

import {
  _Container,
  _MessageWrapper,
  _UserImage,
  _FriendBubble,
} from './styles';

class FriendMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleOpenModalContractPropose = () => {
    const {
      userId: contractorUserId,
      message: { attributes },
      openModalContractPropose,
    } = this.props;

    if (attributes.type && attributes.type === 'propose') {
      const { proposeId, } = attributes;
      return openModalContractPropose(proposeId, contractorUserId);
    }
  };

  render() {
    const {
      friendData: { category, image },
      message: { attributes, body: content, status },
      index,
      messagesCount,
    } = this.props;

    return (
      <_Container isPropose={attributes && attributes.type === 'propose'}>
        {index === messagesCount - 1 && (
        <Fragment>
          <_UserImage
            category={category}
            source={image}
          />
          <_FriendBubble
            width={15}
            height={15}
          />
        </Fragment>)}
        {/* <View
          style={[
            userMessageWrapper,
            index !== payload.length - 1 && { marginBottom: 10 },
            props.style,
            attributes && attributes.type === 'propose' ? friendProposeWrapper : props.style,
          ]}
        > */}
        <_MessageWrapper
          from={'friend'}
          type={attributes.type === 'propose' ? attributes.type : 'answer'}
          lastMessage={index === messagesCount - 1}>
          {/* {status === 'typing' ? <TypingFormat /> : MessageType(item, content, onAccept)} */}
          <ChatTypeMessages
            from={'friend'}
            attributes={attributes}
            content={content}
            openModalContractPropose={this.handleOpenModalContractPropose}
          />
        </_MessageWrapper>
        {/* </View> */}
      </_Container>
    );
  }
}

export default FriendMessage;