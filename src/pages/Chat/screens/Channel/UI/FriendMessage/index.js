import React, { Component, Fragment } from 'react';
import ChatTypeMessages from './../ChatTypeMessages';
import { STORAGE_URL } from 'utils/Helpers';

import {
  _Container,
  _MessageWrapper,
  _UserImage,
  _FriendBubble,
} from './styles';

import DefaultProfileImage from './../ChatProfile/img/default.png';
const emojiRegex = require('emoji-regex');

class FriendMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleOpenModalContractPropose = (attributes) => {
    const {
      userId: contractorUserId,
      openModalContractPropose,
    } = this.props;
    const { type, proposeId, status } = attributes;

    if (type !== 'propose') return;

    if (type === 'propose' && status === 220)
      return this.handleOpenModalApprovePropose(attributes);

    if (type === 'propose' && status === 230)
      return this.handleOpenModalSendNFPropose(attributes);

    return openModalContractPropose(proposeId, contractorUserId);
  };

  handleOpenModalApprovePropose = (attributes) => {
    const {
      userId: contractorUserId,
      openModalApprovePropose,
    } = this.props;
    const { type, proposeId, status } = attributes;

    if (type === 'requestApproval' || type === 'propose' && status === 220)
      return openModalApprovePropose(proposeId, contractorUserId);

    if (type === 'propose' && status === 230)
      return this.handleOpenModalSendNFPropose(attributes);
  };

  handleOpenModalSendNFPropose = (attributes) => {
    const { openModalSendNFPropose, } = this.props;
    const { type, status } = attributes;

    if (type === 'approvedPropose' || type === 'propose' && status === 230)
      return openModalSendNFPropose();
  };

  handleOpenModalBriefingMatch = (attributes) => {
    const { openModalBriefingMatch, } = this.props;
    const { type, briefingId } = attributes;

    if (type === 'briefing')
      return openModalBriefingMatch(briefingId);
  };

  render() {
    /*const {
      friendUserId,
      profileImage,
      category,
      message: { payload, status },
      listProposesRef,
      chatMessagesRef,
    } = this.props;*/

    const {
      index,
      messagesLength,
      friendUserId,
      profileImage,
      category,
      messages,
      message: { state: { attributes, body, status } },
      listProposesRef,
      chatMessagesRef,
    } = this.props,
    regex = emojiRegex(),
    hasEmoji = regex.exec(body),
    onlyEmoji = body && !body.replace(/\s/g, '').match(/(\w){1,}|(\d){1,}/g) && hasEmoji && hasEmoji[0];

    return (
      <_Container
        from={'friend'}
        isPropose={attributes && (attributes.type === 'propose' || attributes.type === 'requestApprovalPropose' || attributes.type === 'approvedPropose')}
        isBriefing={attributes && attributes.type === 'briefing'}
        type={attributes && attributes.type ? attributes.type : 'default'}
        onlyEmoji={onlyEmoji}
      >
          {( index === (messagesLength - 1) || !messages.slice(index + 1).find(msg => parseInt(msg.state.author) === parseInt(friendUserId)) || parseInt(messages.slice(index + 1)[0].state.author) !== parseInt(friendUserId) && messages.slice(index + 1).find(msg => parseInt(msg.state.author) === parseInt(friendUserId)) ) && (!attributes || attributes.type !== 'briefing') &&
          <Fragment>
            <_UserImage
              category={category}
              source={(profileImage === null || profileImage === '') ? DefaultProfileImage : {
                uri: `${STORAGE_URL}.appspot.com/users/${friendUserId}/profile/${profileImage}`                
              }}
            />
            {!onlyEmoji && <_FriendBubble
              width={15}
              height={15}
            />}
          </Fragment>
        }
        <_MessageWrapper
          from={'friend'}
          type={attributes && attributes.type ? attributes.type : 'default'}
          onlyEmoji={onlyEmoji}>
      
          <ChatTypeMessages
            from={'friend'}
            attributes={attributes || undefined}
            content={body}
            onlyEmoji={onlyEmoji}
            messagesLength={messagesLength}
            isTyping={status && status === 'typing'}
            openModalContractPropose={attributes ? () => this.handleOpenModalContractPropose(attributes) : () => false}
            openModalApprovePropose={attributes ? () => this.handleOpenModalApprovePropose(attributes) : () => false}
            openModalSendNFPropose={attributes ? () => this.handleOpenModalSendNFPropose(attributes) : () => false}
            openModalBriefingMatch={attributes ? () => this.handleOpenModalBriefingMatch(attributes) : () => false}
            listProposesRef={listProposesRef}
            chatMessagesRef={chatMessagesRef}
          />

        </_MessageWrapper>
      </_Container> 
    );

    /*return (
      <Fragment>
        {payload.map((item, index) => {
          const { attributes, content } = item;
          const regex = emojiRegex();
          const hasEmoji = regex.exec(content);
          const onlyEmoji = content && !content.replace(/\s/g, '').match(/(\w){1,}|(\d){1,}/g) && hasEmoji && hasEmoji[0];

          return (
            <_Container
              key={index}
              isPropose={attributes && attributes.type === 'propose'}
              isBriefing={attributes && attributes.type === 'briefing'}>
              {index === (payload.length - 1) && (!attributes || attributes.type !== 'briefing') &&
                <Fragment>
                  <_UserImage
                    category={category}
                    source={(profileImage === null || profileImage === '') ? DefaultProfileImage : {
                      uri: `https://storage.googleapis.com/crowd-hm.appspot.com/users/${friendUserId}/profile/${profileImage}`
                    }}
                  />
                  {!onlyEmoji && <_FriendBubble
                    width={15}
                    height={15}
                  />}
                </Fragment>}
              <_MessageWrapper
                from={'friend'}
                type={attributes && (attributes.type === 'propose' || attributes.type === 'briefing') ? attributes.type : 'answer'}
                onlyEmoji={onlyEmoji}>
                <ChatTypeMessages
                  from={'friend'}
                  attributes={attributes || undefined}
                  content={content}
                  onlyEmoji={onlyEmoji}
                  isTyping={status && status === 'typing'}
                  openModalContractPropose={attributes ? () => this.handleOpenModalContractPropose(attributes) : () => false}
                  openModalApprovePropose={attributes ? () => this.handleOpenModalApprovePropose(attributes) : () => false}
                  openModalSendNFPropose={attributes ? () => this.handleOpenModalSendNFPropose(attributes) : () => false}
                  openModalBriefingMatch={attributes ? () => this.handleOpenModalBriefingMatch(attributes) : () => false}
                  listProposesRef={listProposesRef}
                  chatMessagesRef={chatMessagesRef}
                />
              </_MessageWrapper>
            </_Container>
          )
        })}
      </Fragment>
    );*/
  }
}

export default FriendMessage;
