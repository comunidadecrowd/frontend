import styled from 'styled-components/native';
import { Dimensions } from 'react-native';
import { Colors } from 'theme/Colors';
import { FriendBubble } from 'components/Icon/ChatBubbles';
import { categoryColor } from 'utils/helpers';
const { width, height } = Dimensions.get('window');

export const _Container = styled.View`
  align-content: space-between;
  align-self: flex-start;
  min-width: ${({ isPropose, isBriefing }) => isPropose ? '85%' : isBriefing ? '100%' : 'auto'};
  max-width: ${({ isPropose, isBriefing }) => isPropose ? 'auto' : isBriefing ? '100%' : '85%'};
  /*margin-top: ${({ type }) => type !== 'briefing' ? 10 : 0}px;*/
  margin-bottom: ${({ from, onlyEmoji, type }) => onlyEmoji ? 3 : type === 'system' || type === 'propose' || type === 'requestApproval' || type === 'approvedPropose' ? 10 : 0}px;
`;

export const _MessageWrapper = styled.View`
  width: ${({ type }) => type === 'briefing' ? width : 'auto'};
  align-items: flex-start;
  min-height: 16px;
  background: ${({ type, from, onlyEmoji }) => from === 'friend' && !onlyEmoji && type !== 'briefing' ? '#F2F2F2' : 'transparent'};
  border: 1px solid #F2F2F2;
  border-color: ${({ type, from, onlyEmoji }) => from === 'friend' && !onlyEmoji && type !== 'briefing' ? '#F2F2F2' : !onlyEmoji && type !== 'briefing' ? Colors.BLACK : 'transparent'};
  padding: ${({ onlyEmoji }) => !onlyEmoji ? '7px' : '7px 0 0'};
  /*margin-top: ${({ type }) => type !== 'briefing' ? 10 : 0}px;*/
  /*margin-bottom: ${({ from, onlyEmoji, type }) => onlyEmoji ? -5 : from === 'user' || from === 'friend' && (type === 'propose' || type === 'requestApproval' || type === 'approvedPropose') ? 10 : 0}px;*/
  margin-left: ${({ type, from }) => from === 'friend' || type === 'briefing' ? 0 : 1}px;
`;

export const _UserImage = styled.Image`
  width: 36px;
  height: 36px;
  border-width: 1px;
  border-color: ${({ category }) => category ? categoryColor(category) : Colors.WHITE};
  border-radius: 18px;
  position: absolute;
  bottom: -5px;
  left: -55px;
`;

export const _FriendBubble = styled(FriendBubble)`
  position: absolute;
  bottom: 7px;
  left: -15px;
`;
