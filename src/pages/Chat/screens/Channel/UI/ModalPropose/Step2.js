import React, { Component } from 'react';
import withLocalization from 'utils/withLocalization';

import { Colors } from 'theme/Colors';
import Input from 'components/Input';
import { TextInputMask } from 'react-native-masked-text';
import ButtonSubmit from 'components/ButtonSubmit';
import BUTTON from 'GLOBALS/BUTTON';

const styles = {
  inputWrapper: {
    marginTop: 15,
  },
  activeInput: {
    paddingBottom: 15,
  },
  input: {
    paddingBottom: 8,
  },
  submit: {
    marginHorizontal: 'auto',
    marginTop: 30,
  },
};

import {
  _Step2Container,
  _WrapperFields,
  _WrapperButtonSubmit,
} from './styles';

class Step2 extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      activeInputIndex: null,
    };
  }

  textInputRef = [];

  onFocusInput = index => {
    this.setState({ activeInputIndex: index });
  };

  onBlur = () => {
    if (!this.state.activeInputIndex) {
      this.setState({ activeInputIndex: null });
    }
  };

  render() {
    const { t, active, onUpdate, onSubmit, disabled } = this.props;
    const { activeInputIndex } = this.state;

    return (
      <_Step2Container active={active}>
        <_WrapperFields>
          <Input
            setRef={ref => this.textInputRef[0] = ref}
            blurOnSubmit={false}
            styleWrapper={styles.inputWrapper}
            styleInput={activeInputIndex === 0 ? styles.activeInput : styles.input}
            label={t('chat:modalPropose:stepTwo:fieldTitleLabel')}
            onChange={value => onUpdate('title', value)}
            returnKeyType="next"
            id="inputTitle"
            enablesReturnKeyAutomatically
            autoFocus
            onFocus={() => this.onFocusInput(0)}
            onSubmitEditing={() => this.textInputRef[1]._component.focus()}
          />
          <Input
            setRef={ref => this.textInputRef[1] = ref}
            styleWrapper={styles.inputWrapper}
            styleInput={activeInputIndex === 1 ? styles.activeInput : styles.input}
            // label={t('chat:modalPropose:stepTwo:fieldMoneyLabel')} // TODO:  Bruno analisar para o placeholder (cor cinza quando sem valor) funcionar corretamente
            onChange={value => onUpdate('value', value)}
            returnKeyType="next"
            id="inputValue"
            enablesReturnKeyAutomatically
            onFocus={() => this.onFocusInput(1)}
            onSubmitEditing={() => this.textInputRef[2]._component.focus()}
            blurOnSubmit={false}
            type={'money'}
            component={TextInputMask}
          />
          <Input
            setRef={ref => this.textInputRef[2] = ref}
            styleWrapper={styles.inputWrapper}
            styleInput={activeInputIndex === 2 ? styles.activeInput : styles.input}
            label={t('chat:modalPropose:stepTwo:fieldDaysTermLabel')}
            onChange={value => onUpdate('term', value)}
            returnKeyType="send"
            id="inputTerm"
            enablesReturnKeyAutomatically
            onFocus={() => this.onFocusInput(2)}
            onSubmitEditing={onSubmit}
            type={'only-numbers'}
            component={TextInputMask}
          />
        </_WrapperFields>
        <_WrapperButtonSubmit style={{bottom: 13}}>
          <BUTTON
            onPressFn={onSubmit}
            color={Colors.SUCCESS}
            content={t('chat:modalPropose:stepTwo:buttonSubmit')}
            style={{top: -3}}
            disabled={disabled}
          />
          {/*
            // REFACT:  remover <ButtonSubmit /> após tratar disabled no GLOBALS/BUTTON;
            <ButtonSubmit
            style={styles.submit}
            onPress={onSubmit}
            disabled={disabled}
            type={'submit'}
            activeColor="lightblue"
          >
            enviar proposta
          </ButtonSubmit> */}
        </_WrapperButtonSubmit>
      </_Step2Container>
    );
  }

}

export default withLocalization(Step2);
