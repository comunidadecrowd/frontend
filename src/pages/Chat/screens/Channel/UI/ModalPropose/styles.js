import React from 'react';
import styled from 'styled-components/native';
import { Platform, Dimensions } from 'react-native';
import Interactable from 'react-native-interactable';
import Svg, { Polygon } from 'react-native-svg';

import { TEXT } from 'GLOBALS/TEXT';
import { Colors } from 'theme/Colors';
import { IconBackButton } from 'components/Icon/ChatHeader';
import IconPlus from 'components/Icon/IconPlus';
import IconBacktick from 'components/Icon/IconBacktick';
import { Trash, Restore } from 'components/Icon/ProposeIcons';

const
  { width, height } = Dimensions.get('window'),
  is_iOS = Platform === 'ios';

export const _ContainerInteractable = styled(Interactable.View)`
  position: absolute;
  width: ${width};
  height: 350px;
  bottom: ${is_iOS ? 10 : 0}px;
  z-index: 2;
`;

export const _HandlerIconClose = styled.View`
  width: 30%;
  height: 8px;
  background: ${Colors.GRAY_DARK};
  border-radius: 50px;
  margin: 29px auto 0;
`;

export const _WrapperContent = styled.View`
  flex: 1;
  margin-top: 2px;
  background: ${Colors.BLACK};
  /* border-radius: 15px; */
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  /* border-bottom-right-radius: 0;
  border-bottom-left-radius: 0; */
`;

export const _ModalHeader = styled.View`
  width: 100%;
  height: 70px;
  padding: 0 25px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  /* background: green; */
`;

export const _LeftIcon = styled.TouchableOpacity`
  width: 18px;
  /* justify-content: center; */
`;

export const _IconBack = styled(IconBackButton)`
  /* align-self: center; */
`;

export const _WrapperTitle = styled.View`
  flex-grow: 2;
  align-items: center;
  color: ${Colors.WHITE};
`;

export const _HeaderTitle = styled(TEXT)`
  color: ${Colors.WHITE};
`;

export const _RightIcon = styled.TouchableOpacity`
  width: 18px;
`;

export const _IconPlus = styled(IconPlus)`
  align-self: center;
`;

export const _StepsManager = styled.View`
  align-content: space-between;
  padding: 0 0 60px;
`;

export const _Step1Container = styled.View`
  height: 200px;
`;

export const _WrapperCarousel = styled.View`
  align-content: space-between;
`;

export const _WrapperButtonSubmit = styled.View`
  height: ${({ height: wrapperHeight }) => !wrapperHeight ? 60 : wrapperHeight}px;
  align-content: flex-end;
`;

export const _TextWrapper = styled.View`
  padding: 15px 25px 0;
  height: 150px;
  color: ${Colors.WHITE};
`;

export const _Step2Container = styled.View`
  height: 240px;
  padding: 0 25px 0;
  align-content: space-between;
`;

export const _WrapperFields = styled.View`
  height: 190px;
`;

export const _ListProposesContainer = styled.View`
  padding: 0 0 105px;
`;

export const _ListProposesScroll = styled.ScrollView``;

export const _ListProposesItem = styled.View`
  width: 100%;
  flex-direction: row;
  background: ${({isOdd}) => isOdd ? 'rgba(255, 255, 255, .075)' : 'rgba(255, 255, 255, 0.025)'};
  align-items: center;
`;

export const _ProposeInfos = styled.View`
  width: ${({ noActionButton }) => !noActionButton ? '85%' : '100%'};
  padding: 15px 20px;
`;

export const _ProposeHeader = styled.View`
  flex-direction: row;
  margin: 0 0 15px;
`;

export const _ProposeTags = styled.View`
  background: rgba(255, 255, 255, .15);
  flex-direction: row;
  margin: 0 5px 0 0;
  height: 22px;
  padding: 0 10px 0 0;
`;

export const _ProposeTagsBorder = styled.View`
  width: 5px;
  height: 100%;
  background: ${({bgColor}) => bgColor ? bgColor : '#000'};
`;

export const _ProposeTagsText = styled(TEXT)`
  font-weight: bold;
  line-height: 22px; 
  margin: 0 0 0 10px;
  color: ${Colors.WHITE};
`;

export const _ProposeDaysTerm = styled.View`
  background: rgba(255, 255, 255, .15);
  height: 22px;
  flex-direction: row;
  align-items: center;
  padding: 0 10px;
`;

export const _ProposeDaysTermNumber = styled(TEXT)``;
export const _ProposeDaysTermWord = styled(TEXT)``;

export const _ProposeTitle = styled(TEXT)`
  width: 100%;
  margin-bottom: 10px;
`;

export const _ProposePrice = styled(TEXT)``;

export const _ProposeActions = styled.View`
  width: 15%;
  padding: 0 20px 0 0;
`;

export const _ProposeActionsButton = styled.TouchableOpacity`
  width: 100%;
  align-items: flex-end;
  justify-content: space-between;
`;

export const _ProposeActionIcon = styled.View`
  width: 30px;
  height: 30px;
  align-items: center;
  justify-content: center;
  border-radius: 25px;
  background: ${({isApprovedDelivery}) => isApprovedDelivery ? 'rgba(0,0,0,0)' : Colors.BLUE};
`;

export const _ProposeActionLabel = styled(TEXT)`
  color: ${Colors.WHITE};
  margin-top: 10px;
`;

export const _IconBacktick = styled(props => (
  <IconBacktick color={props.isApprovedDelivery ? Colors.GREEN : Colors.BLACK} {...props} />
))``;

export const _IconRestore = styled(props => (
  <Restore color={Colors.BLACK} {...props} />
))``;

// TODO:  Mover esse item para icons;
export const _IconSendPropose = (props) => (
  <Svg {...props}>
    <Polygon points="24,24 12,0 0,24" fill="#000" stroke="none" />
  </Svg>
);
