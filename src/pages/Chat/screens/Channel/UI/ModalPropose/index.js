import React, { Component } from 'react';
import { Dimensions, Keyboard } from 'react-native';
import withLocalization from 'utils/withLocalization';
import APIS from 'apis';
const { width, height } = Dimensions.get('window');

import StepsManager from './StepsManager';

import {
  _ContainerInteractable,
  _HandlerIconClose,
  _WrapperContent,
  _ModalHeader,
  _LeftIcon,
  _IconBack,
  _WrapperTitle,
  _HeaderTitle,
  _RightIcon,
  _IconPlus,
  _Step1Container,
} from './styles';

class ModalPropose extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSnapIndex: 0,
      proposes: [],
      submitingPropose: false,
    };
  }

  componentDidMount() {
    this.getListProposes();
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.state.proposes !== nextState.proposes
      || this.state.currentSnapIndex !== nextState.currentSnapIndex
      || this.state.submitingPropose !== nextState.submitingPropose;
  };

  getListProposes = () => {
    const { channel } = this.props;

    APIS.Propose.listProposes(channel, ({ listProposeByChannelSid }) => {
      this.setState({
        proposes: listProposeByChannelSid,
      });
    });
  };

  getLastStatusProposeById = (proposeId) => {
    const { proposes } = this.state;

    APIS.Propose.lastStatusProposeById(proposeId, ({ lastProposeByProposeId }) => {
      proposes.find(propose => parseInt(propose.proposeId) === parseInt(lastProposeByProposeId.proposeId)).status = lastProposeByProposeId.status;
      this.setState({
        proposes: [...proposes]
      });
    });
  };

  onSnap = ({ nativeEvent: { index } }) => {
    this.setState({ currentSnapIndex: index });
  };

  handleSendPropose = async (title, value, term) => {
    const { channel: cSID, userId: userID } = this.props;

    this.setState({ submitingPropose: true });
    await APIS.Propose.createPropose({
      cSID, userID,
      title, value, term
    }, ({ insertPropose }) => {
      this.setState({ submitingPropose: false });
      this.getListProposes();
    });
    Keyboard.dismiss();
    this.modalPropose.snapTo({ index: 0 });
  };

  handleReSendPropose = async (proposeId) => {
    const { channel: channelSid, userId: userID } = this.props;
    const { proposes } = this.state;
    const proposeToResend = proposes.find(propose => parseInt(propose.proposeId) === parseInt(proposeId));

    if (!proposeToResend) return;
    const { body: { title, price, daysTerm } } = proposeToResend;

    this.setState({ submitingPropose: true });
    await APIS.Propose.reSendPropose({
      channelSid, userID, proposeId,
      title, price, daysTerm
    }, ({ insertPropose: resendPropose }) => {
      this.setState({ submitingPropose: false });
      this.modalPropose.snapTo({ index: 0 });
    });
  };

  handleProposeButtonAction = (proposeId, authorPropose, typeModal=null) => {
    const { userId, contractProposeModalRef, requestApproveProposeModalRef, approveProposeModalRef } = this.props;
    if (!proposeId || !authorPropose) return false;

    this.modalPropose.snapTo({ index: 0 });
    if (typeModal === 'contractPropose' && parseInt(authorPropose) !== parseInt(userId) && contractProposeModalRef && contractProposeModalRef.props) {
      return contractProposeModalRef.props.openModalContractPropose(proposeId, userId);

    } else if (typeModal === 'contractPropose' && parseInt(authorPropose) === parseInt(userId) && contractProposeModalRef && contractProposeModalRef.props) {
      return this.handleReSendPropose(proposeId);

    } else if (typeModal === 'requestApprovePropose' && requestApproveProposeModalRef && requestApproveProposeModalRef.props) {
      return requestApproveProposeModalRef.props.openModalRequestApprovePropose(proposeId, authorPropose);

    } else if (typeModal === 'approvePropose' && approveProposeModalRef && approveProposeModalRef.props) {
      return approveProposeModalRef.props.openModalApprovePropose(proposeId, userId);
    }
  };

  onDrag = ({ nativeEvent: { state, y } }) => {
    if (state === 'end' && y > 50) this.modalPropose.snapTo({ index: 0 });
  }

  render() {
    
    const 
      { t, getRef, userId, channel, getListProposesRef } = this.props,
      { currentSnapIndex, proposes, submitingPropose } = this.state;

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height + 50 }}
        ref={ref => {
          this.modalPropose = ref;
          getRef(ref);
        }}
        snapPoints={[{ y: height + 50 }, { y: 0 }]}
        onSnap={this.onSnap}
        currentSnapIndex={currentSnapIndex}
        boundaries={{ top: 0 }}
        onDrag={this.onDrag}
        getListProposes={this.getListProposes}
        getLastStatusProposeById={this.getLastStatusProposeById}
      >
        <_WrapperContent>
          <_HandlerIconClose />
          <StepsManager
            ref={ref => this.stepsManagerRef = ref}
            getListProposesRef={getListProposesRef}
            titleModal={t('chat:modalPropose:title')}
            hasProposes={proposes.length > 0}
            proposes={proposes}
            userId={userId}
            channel={channel}
            handleSendPropose={this.handleSendPropose}
            handleProposeButtonAction={this.handleProposeButtonAction}
            submitingPropose={submitingPropose}
          />
        </_WrapperContent>
      </_ContainerInteractable>
    )
  }
}

export default withLocalization(ModalPropose)