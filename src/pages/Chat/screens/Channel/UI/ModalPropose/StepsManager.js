import React, { Component } from 'react';
import { Trash, Restore } from 'components/Icon/ProposeIcons';

import Step1 from './Step1';
import Step2 from './Step2';
import ListProposes from './ListProposes';

import {
  _StepsManager,
  _ModalHeader,
  _LeftIcon,
  _IconBack,
  _WrapperTitle,
  _HeaderTitle,
  _RightIcon,
  _IconPlus,
} from './styles';

class StepsManager extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      step: 0,
      title: '',
      value: '',
      term: '',
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.proposes !== nextProps.proposes
      || this.props.submitingPropose !== nextProps.submitingPropose
      || this.state.step !== nextState.step
      || this.state.title !== nextState.title
      || this.state.value !== nextState.value
      || this.state.term !== nextState.term;
  };

  onClickBack = () => {
    const { proposes } = this.props;
    const { step } = this.state;

    if (step > 0) this.setState({ step: step - 1 });
  };

  onClickPlus = () => {
    const { step } = this.state;

    this.setState({ step: 1 });
  };

  onClickNextStep = () => {
    const { step } = this.state;

    this.setState({ step: step + 1 });
  };

  submitEnabled = () => {
    const { title, value, term } = this.state;

    return title !== '' && (value !== '' && value !== 'R$0,00') && term !== '';
  };

  onUpdateValue = (name, value) => {
    this.setState({ [name]: value });
  };

  handleSendPropose = async () => {
    const { handleSendPropose } = this.props;
    const { title, value, term } = this.state;

    if (!this.submitEnabled()) return;

    await handleSendPropose(title, value, term);
    return this.setState({ step: 0, title: '', value: '', term: '' });
  };

  render() {
    
    const {
      userId,
      channel,
      titleModal,
      proposes,
      handleProposeButtonAction,
      submitingPropose,
      getListProposesRef
    } = this.props,
    { step } = this.state;

    return (
      <_StepsManager>
        <_ModalHeader>
          <_LeftIcon onPress={step > 0 ? this.onClickBack : () => false}>
            {step > 0 && <_IconBack width={18} height={18} color="#fff" />}
            {/*: !step && proposes.length ? <Trash width={24} height={24} /> : undefined*/}
          </_LeftIcon>

          <_WrapperTitle>
            <_HeaderTitle
              euclid
              content={titleModal}
              fontSize={24}
            />
          </_WrapperTitle>

          <_RightIcon onPress={!step || !step && proposes.length ? this.onClickPlus : () => false}>
            {!step || !step && proposes.length ? <_IconPlus width={18} height={18} color="#fff" /> : undefined}
          </_RightIcon>
        </_ModalHeader>

        {!step && proposes.length > 0 && (
          <ListProposes
            ref={ref => getListProposesRef(ref)}
            proposes={proposes}
            userId={userId}
            channel={channel}
            handleProposeButtonAction={handleProposeButtonAction}
          />
        )}

        {!step && !proposes.length && <Step1 goToNextStep={this.onClickNextStep} />}

        {step === 1 &&
          <Step2
            onSubmit={this.handleSendPropose}
            disabled={!this.submitEnabled() || submitingPropose}
            onUpdate={this.onUpdateValue}
          />
        }
      </_StepsManager>
    );
  }

}

export default StepsManager;
