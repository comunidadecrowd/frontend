import React, { Component, Fragment } from 'react';
import { Text, View, Dimensions } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import withLocalization from 'utils/withLocalization';

// import ButtonSubmit from 'components/ButtonSubmit';
import BUTTON from 'GLOBALS/BUTTON';
import { DeliveryMan, Buy, Interview } from 'components/Icon/CarouselPropose';

const sliderFirstItem = 0;

const styles = {
  textWrapper: {
    paddingTop: 15,
    paddingHorizontal: 25,
    height: 160,
    color: 'white',
  },
  text: {
    color: 'white',
    textAlign: 'center',
    paddingVertical: 10,
  },
  indicator: {
    color: 'white',
  },
  buttonText: {
    color: 'lightblue',
    alignSelf: 'center',
  },
  paginatorWrapper: {
    marginTop: -30,
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  paginationDot: {
    marginHorizontal: 2,
  },
};

import { _Step1Container, _WrapperCarousel, _WrapperButtonSubmit, _TextWrapper } from './styles';

class Step1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tutorialStep: 1,
      entries: [
        'Durante a conversa você garante o acordo verbal de comprometimento, prazo e valor com o contratante.',
        'O pagamento ocorrerá mediante a aprovação do trabalho. Você receberá o valor 15 dias após o fechamento. Não esqueça de emitir sua nota fiscal.',
        'O projeto pode ser iniciado quando a proposta for aceita. Ao finalizar, você deve enviá-lo para aprovação. Certifique-se de que o trabalho seja entregue como combinado.',
      ],
      sliderActiveItem: sliderFirstItem,
    };
  }

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.textWrapper} key={index}>
        {index === 0 && (
          <Interview height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 1 && <Buy height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />}
        {index === 2 && (
          <DeliveryMan height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        <Text style={styles.text}>{item}</Text>
      </View>
    );
  };

  render() {
    const { t, goToNextStep } = this.props;
    const { entries, sliderActiveItem } = this.state;
    const { width: itemWidth } = Dimensions.get('window');

    return (
      <_Step1Container>
        <_WrapperCarousel>
          <Carousel
            ref={ref => (this._carousel = ref)}
            data={entries}
            renderItem={this.renderItem}
            sliderWidth={itemWidth}
            itemWidth={itemWidth}
            swipeThreshold={10}
            horizontal
            firstItem={sliderFirstItem}
            onSnapToItem={index => this.setState({ sliderActiveItem: index })}
          />
        </_WrapperCarousel>
        <Pagination
          dotStyle={styles.paginationDot}
          inactiveDotColor={'#FFFFFF'}
          activeDotIndex={sliderActiveItem}
          dotColor={'lightblue'}
          dotsLength={entries.length}
          containerStyle={styles.paginatorWrapper}
          carouselRef={this._carousel}
          inactiveDotScale={0.9}
        />
        <_WrapperButtonSubmit>
          <BUTTON
            onPressFn={goToNextStep}
            content={t('chat:modalPropose:stepOne:buttonSubmit')}
            verticalAlign
          />
        </_WrapperButtonSubmit>
      </_Step1Container>
    );
  }
}

export default withLocalization(Step1);
