import React, { Component } from 'react';
import withLocalization from 'utils/withLocalization';
import APIS from 'apis';

import {
  _ListProposesContainer,
  _ListProposesScroll,
  _ListProposesItem,
  _ProposeInfos,
  _ProposeTitle,
  _ProposeDaysTerm,
  _ProposeDaysTermNumber,
  _ProposeDaysTermWord,
  _ProposePrice,
  _ProposeActions,
  _ProposeActionsButton,
  _ProposeActionIcon,
  _ProposeActionLabel,
  _IconBacktick,
  _IconRestore,
  _IconSendPropose,
  _ProposeTags,
  _ProposeTagsBorder,
  _ProposeTagsText,
  _ProposeHeader
} from './styles';

class ListProposes extends Component {

  constructor(props) {
    super(props);
    this.state = {
      proposesFiltred: [],
    };
  }

  render() {
    const { t, proposes, userId, handleProposeButtonAction } = this.props;
    const { proposesFiltred } = this.state;

    return (
      <_ListProposesContainer>
        <_ListProposesScroll>
          {proposes
            .filter(propose => propose.status >= 200 && propose.status <= 240)
            .map((propose, key) => {

              const {
                body: { title: proposeTitle, daysTerm: proposeDaysTerm, price: proposePrice },
                userId: proposeFromUser,
                status: proposeStatus,
                proposeId
              } = propose;

              let tagContext = (() => {
                switch (proposeStatus) {
                  case 200:
                    return ['sending', '#666']
                    break;
                  case 210:
                    return ['onBudget', '#fff']
                    break;
                  case 220:
                    return ['onExecution', '#FFC701']
                    break;
                  case 230:
                    return ['onApproval', '#84C9F2']
                    break;
                  default:
                    return ['approvedDelivery', '#B7EB81']
                    break;
                }
              })();

              let typeModal = (() => {
                switch (proposeStatus) {
                  case 210:
                    return 'contractPropose';
                    break;
                  case 220:
                    if (parseInt(proposeFromUser) === parseInt(userId))
                      return 'requestApprovePropose';
                    return 'approvePropose';
                    break;
                  case 230:
                    if (parseInt(proposeFromUser) === parseInt(userId))
                      return 'requestApprovePropose';
                    return 'approvePropose';
                    break;
                  default:
                    return null;
                    break;
                }
              });

              const hasDecimal = proposePrice.toString().split('.');

              return (
                <_ListProposesItem key={key} isOdd={key % 2 !== 0}>
                  <_ProposeInfos>
                    <_ProposeHeader>
                      <_ProposeTags>
                        <_ProposeTagsBorder bgColor={tagContext[1]} />
                        <_ProposeTagsText
                          fontSize={10}
                          content={t(`chat:modalPropose:tags:${tagContext[0]}`).toUpperCase()}
                        />
                      </_ProposeTags>
                      <_ProposeDaysTerm>
                        <_ProposeDaysTermNumber bold content={proposeDaysTerm} fontSize={10} />
                        <_ProposeDaysTermWord content={' dias'} fontSize={10} />
                      </_ProposeDaysTerm>
                    </_ProposeHeader>
                    <_ProposeTitle content={proposeTitle} fontSize={18} />
                    <_ProposePrice content={hasDecimal.length === 1 ? `R$ ${proposePrice.toLocaleString('pt-BR')},00` : hasDecimal.length > 1 && hasDecimal[1].length === 1 ? `R$ ${proposePrice.toLocaleString('pt-BR')}0` : `${proposePrice.toLocaleString('pt-BR')}`} fontSize={12} />
                  </_ProposeInfos>
                  <_ProposeActions>
                    <_ProposeActionsButton activeOpacity={proposeStatus === 240 ? 1 : 0.2} onPress={proposeStatus !== 240 ? () => handleProposeButtonAction(proposeId, proposeFromUser, typeModal()) : null}>
                      <_ProposeActionIcon isApprovedDelivery={proposeStatus === 240}>
                        {parseInt(proposeFromUser) === parseInt(userId) && proposeStatus === 210
                          ? <_IconSendPropose width={24} height={14} />
                          : <_IconBacktick width={16} height={18} isApprovedDelivery={proposeStatus === 240} />
                        }
                      </_ProposeActionIcon>
                    </_ProposeActionsButton>
                  </_ProposeActions>
                </_ListProposesItem>
              )
            })
          }
        </_ListProposesScroll>
      </_ListProposesContainer>
    );

  }

}

export default withLocalization(ListProposes);
