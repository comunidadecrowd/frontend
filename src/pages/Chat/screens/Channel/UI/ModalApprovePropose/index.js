import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import withLocalization from 'utils/withLocalization';
import { IconCloseButton } from 'components/Icon/SearchView';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { FirstIcon, SecondIcon, ThirdIcon } from 'components/Icon/CarouselApprovePropose';
import BUTTON from 'GLOBALS/BUTTON';
import { Colors } from 'theme/Colors';
import APIS from 'apis';
const { width, height } = Dimensions.get('window');

import {
  _ContainerInteractable,
  _ButtonClose,
  _CarouselContainer,
  _CarouselTextWrapper,
  _CarouselText,
  _WrapperButtonSubmit,
} from './styles';

const styles = {
  paginatorWrapper: {
    bottom: -10,
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  paginationDot: {
    width: 13,
    height: 13,
    borderRadius: 20,
    marginHorizontal: 10,
  },
};

class ModalApprovePropose extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSnapIndex: 0,
      sliderActiveItem: 0,
      entries: [
        'Você recebeu seu projeto? Certifique-se que o trabalho entregue está como o acordado. Seu feedback é muito importante para o profissional!',
        'Confira se o trabalho está como você esperava! É muito importante checar as informações e garantir que seu projeto seja entregue com qualidade.',
        'Expectativas alcançadas? Agora você precisa aprovar o trabalho que foi entregue pelo profissional. Apenas após essa etapa ocorrerá o pagamento.',
      ],
      submiting: false,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.state.currentSnapIndex !== nextState.currentSnapIndex ||
      this.state.sliderActiveItem !== nextState.sliderActiveItem ||
      this.props.proposeId !== nextProps.proposeId ||
      this.props.contractorUserId !== nextProps.contractorUserId ||
      this.props.modalProposeRef !== nextProps.modalProposeRef ||
      this.props.modalApproveProposeRef !== nextProps.modalApproveProposeRef ||
      this.props.openModalApprovePropose !== nextProps.openModalApprovePropose ||
      this.state.submiting !== nextState.submiting
    );
  }

  saveRef = ref => {
    const { getRef } = this.props;
    getRef(ref);
    this.modal = ref;
  };

  closeModal = () => {
    this.modal.snapTo({ index: 0 });
  };

  onSubmitApprovePropose = () => {
    const { proposeId, contractorUserId, modalProposeRef } = this.props;

    this.setState({ submiting: true });
    APIS.Propose.approvePropose(
      { proposeId: parseInt(proposeId), contractorUserId: parseInt(contractorUserId) },
      ({ approvePropose }) => {
        const {
          props: { getListProposes },
        } = modalProposeRef;
        this.setState({ submiting: false });
        getListProposes();
        this.closeModal();
      }
    );
  };

  renderItem = ({ item, index }) => {
    return (
      <_CarouselTextWrapper key={index}>
        {index === 0 && (
          <FirstIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 1 && (
          <SecondIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 2 && (
          <ThirdIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        <_CarouselText>{item}</_CarouselText>
      </_CarouselTextWrapper>
    );
  };

  render() {
    const { t, openModalApprovePropose } = this.props,
      { currentSnapIndex, sliderActiveItem, entries, submiting } = this.state;

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        ref={ref => this.saveRef(ref)}
        snapPoints={[{ y: height }, { y: 5 }]}
        currentSnapIndex={currentSnapIndex}
        openModalApprovePropose={openModalApprovePropose}
      >
        <_ButtonClose onPress={!submiting ? this.closeModal : () => false} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>

        <_CarouselContainer>
          <Carousel
            ref={ref => (this.carousel = ref)}
            data={entries}
            renderItem={this.renderItem}
            sliderWidth={width}
            itemWidth={width}
            swipeThreshold={10}
            horizontal
            firstItem={0}
            onSnapToItem={index => this.setState({ sliderActiveItem: index })}
          />

          <Pagination
            dotStyle={styles.paginationDot}
            containerStyle={styles.paginatorWrapper}
            inactiveDotColor={'#FFFFFF'}
            activeDotIndex={sliderActiveItem}
            dotColor={'lightblue'}
            dotsLength={entries.length}
            carouselRef={this.carousel}
            inactiveDotScale={0.9}
          />

          <_WrapperButtonSubmit>
            <BUTTON
              onPressFn={this.onSubmitApprovePropose}
              color={Colors.SUCCESS}
              content={t('chat:typeMessages:propose:approveProposeSubmit')}
              disabled={submiting}
            />
          </_WrapperButtonSubmit>
        </_CarouselContainer>
      </_ContainerInteractable>
    );
  }
}

export default withLocalization(ModalApprovePropose);
