import React, { Component } from 'react';
import withLocalization from 'utils/withLocalization';
import { Colors } from 'theme/Colors';
import BUTTON from 'GLOBALS/BUTTON';

import {
  _BriefingMatch,
  _BriefingContent,
  _BriefingLabel,
  _BriefingTitle,
  _BriefingObjective,
  _BriefingDescription,
  _BriefingButtonAnswer,
} from './styles';

class BriefingMatch extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      t,
      from,
      title,
      description,
      excerpt,
      status,
      messagesLength,
      children,
      openModalBriefingMatch,
    } = this.props;

    return (
      <_BriefingMatch>
        <_BriefingContent>
          <_BriefingLabel content={'Título: '} color={Colors.BLACK} fontSize={12} bold />
          <_BriefingTitle
            content={title}
            color={Colors.BLACK}
            fontSize={14}
          />

          <_BriefingLabel content={'Objetivo: '} color={Colors.BLACK} fontSize={12} bold />
          <_BriefingObjective
            content={excerpt}
            color={Colors.BLACK}
            fontSize={12}
          />

          <_BriefingLabel content={'Descrição: '} color={Colors.BLACK} fontSize={12} bold />
          <_BriefingDescription
            content={description}
            color={Colors.BLACK}
            fontSize={12}
          />
        </_BriefingContent>

        {from === 'friend' && status <= 199 && (
          <_BriefingButtonAnswer messagesLength={messagesLength}>
            <BUTTON
              onPressFn={openModalBriefingMatch}
              content={t('chat:typeMessages:briefingMatch:answerBriefing')}
              color={Colors.BLUE}
              fontSize={16}
            />
          </_BriefingButtonAnswer>
        )}
      </_BriefingMatch>
    );
  }
}

export default withLocalization(BriefingMatch);
