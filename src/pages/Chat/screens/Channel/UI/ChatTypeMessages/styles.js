import styled from 'styled-components/native';
import { Dimensions, Platform } from 'react-native';
import { Colors } from 'theme/Colors';
import { P } from 'theme/ChatTheme';
import { TEXT } from 'GLOBALS/TEXT';

const { width, height } = Dimensions.get('window');
const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  width: 100%;
`;

export const _TextFormat = styled.Text`
  font-size: ${({ onlyEmoji }) => onlyEmoji ? 42 : 14}px;
  color: ${Colors.BLACK};
`;

export const _ProposeMessage = styled.View`
  width: 100%;
`;

export const _ProposeLabel = styled.Text`
  padding: 5px 0;
  color: #CCCCCC;
`;

export const _ProposeAttributes = styled.View`
  width: 100%;
  border-bottom-color: lightblue;
  border-bottom-width: 1px;
  padding-bottom: 5px;
  margin-bottom: 15px;
`;

export const _ProposeTitle = styled.Text`
  color: ${Colors.BLACK};
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  font-size: 14px;
`;

export const _ProposeValue = styled.Text`
  color: ${Colors.BLACK};
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  font-size: 14px;
`;

export const _ProposeTerm = styled.Text`
  color: ${Colors.BLACK};
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  font-size: 14px;
`;

export const _ProposeRequestApproval = styled.View`
  width: 100%;
`;

export const _ProposeApproved = styled.View`
  width: 100%;
`;

// TypingFormat
export const _TypingFormatWrapper = styled.View`
  min-height: 16px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const _BriefingMatch = styled.View`
  width: ${width};
  min-height: ${(height / 3) + 10}px;
  /*height: 100%;*/
  padding: 0 15px;
  margin-left: -10px;
  justify-content: space-between;
`;

export const _BriefingContent = styled.View`
  /*flex-grow: 2;*/
`;

export const _BriefingLabel = styled(TEXT)``;
export const _BriefingTitle = styled(TEXT)`
  padding-left: 5px;
  margin-bottom: 10px;
`;
export const _BriefingObjective = styled(_BriefingTitle)``;
export const _BriefingDescription = styled(_BriefingTitle)``;
export const _BriefingButtonAnswer = styled.View`
  width: ${width};
  /*margin-top: 40px;*/
  margin-top: 20px;
  margin-left: -13px;
  margin-bottom: ${({ messagesLength }) => messagesLength === 1 ? -25 : 0}px;
  /*flex-grow: 1;*/
  background: ${Colors.BLACK};
`;

export const _Emoji = styled(TEXT)``;
