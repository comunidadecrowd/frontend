import React, { Component } from 'react';
import withLocalization from 'utils/withLocalization';
import { Colors } from 'theme/Colors';
import BUTTON from 'GLOBALS/BUTTON';

import {
  _ProposeMessage,
  _ProposeLabel,
  _ProposeAttributes,
  _ProposeTitle,
  _ProposeValue,
  _ProposeTerm,
} from './styles';

class ProposeMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    
    const {
      t,
      from,
      title,
      term,
      value,
      approved,
      openModalContractPropose,
    } = this.props;
    const hasDecimal = value.toString().split('.');

    return (
      <_ProposeMessage from={from}>
        <_ProposeLabel>Titulo</_ProposeLabel>
        <_ProposeAttributes>
          <_ProposeTitle>{title}</_ProposeTitle>
        </_ProposeAttributes>

        <_ProposeLabel>Valor</_ProposeLabel>
        <_ProposeAttributes>
          <_ProposeValue>R$ {hasDecimal.length === 1 ? `${value.toLocaleString('pt-BR')},00` : hasDecimal[1].length === 1 ? `${value.toLocaleString('pt-BR')}0` : value.toLocaleString('pt-BR')}</_ProposeValue>
        </_ProposeAttributes>

        <_ProposeLabel>Prazo</_ProposeLabel>
        <_ProposeAttributes>
          <_ProposeTerm>{term}</_ProposeTerm>
        </_ProposeAttributes>

        {from === 'friend' && (
          <BUTTON
            onPressFn={!approved ? openModalContractPropose : () => false}
            content={!approved
              ? t('chat:typeMessages:propose:contractProposeSubmit')
              : t('chat:typeMessages:propose:proposeApproved')
            }
            color={Colors.BLACK}
            fontSize={14}
            disabled={approved}
          />
        )}
      </_ProposeMessage>
    );
  }
}

export default withLocalization(ProposeMessage);
