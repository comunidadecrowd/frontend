import React, { Component } from 'react';
import withLocalization from 'utils/withLocalization';
import { Colors } from 'theme/Colors';
import BUTTON from 'GLOBALS/BUTTON';

import {
  _Emoji,
  _TextFormat,
} from './styles';

class Emoji extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      t,
      from,
      children,
    } = this.props;

    return (
      <_Emoji
        content={children}
        fontSize={48}
      />
    );
  }
}

export default withLocalization(Emoji);