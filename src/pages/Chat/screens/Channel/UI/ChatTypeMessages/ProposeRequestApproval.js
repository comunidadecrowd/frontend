import React, { Component } from 'react';
import withLocalization from 'utils/withLocalization';
import { Colors } from 'theme/Colors';
import BUTTON from 'GLOBALS/BUTTON';

import {
  _ProposeRequestApproval,
  _TextFormat,
} from './styles';

class ProposeRequestApproval extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      t,
      from,
      approved,
      children,
      openModalApprovePropose,
    } = this.props;

    return (
      <_ProposeRequestApproval from={from}>
        <_TextFormat>{children}</_TextFormat>

        {from === 'friend' && (
          <BUTTON
            onPressFn={!approved ? openModalApprovePropose : () => false}
            content={!approved
              ? t('chat:typeMessages:propose:approveProposeSubmit')
              : t('chat:typeMessages:propose:proposeApproved')
            }
            color={Colors.BLACK}
            fontSize={14}
            disabled={approved}
          />
        )}
      </_ProposeRequestApproval>
    );
  }
}

export default withLocalization(ProposeRequestApproval);
