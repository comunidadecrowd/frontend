import React, { Component } from 'react';
import ProposeMessage from './ProposeMessage';
import ProposeRequestApproval from './ProposeRequestApproval';
import ProposeApproved from './ProposeApproved';
import BriefingMatch from './BriefingMatch';
import Emoji from './Emoji';
import { TypingFormat } from './TypingFormat';

import {
  _Container,
  _TextFormat,
} from './styles';

class ChatTypeMessages extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  shouldComponentUpdate(nextProps, nextState) {
    return this.props.from !== nextProps.from
      || this.props.isTyping !== nextProps.isTyping
      || this.props.attributes !== nextProps.attributes
      || this.props.content !== nextProps.content
      || this.props.openModalContractPropose !== nextProps.openModalContractPropose
      || this.props.openModalApprovePropose !== nextProps.openModalApprovePropose
      || this.props.openModalSendNFPropose !== nextProps.openModalSendNFPropose
      || this.props.openModalBriefingMatch !== nextProps.openModalBriefingMatch
      || this.props.listProposesRef !== nextProps.listProposesRef
      || this.props.chatMessagesRef !== nextProps.chatMessagesRef;
  };

  render() {
    const {
      from,
      attributes,
      content,
      onlyEmoji,
      messagesLength,
      openModalContractPropose,
      openModalApprovePropose,
      openModalSendNFPropose,
      openModalBriefingMatch,
      listProposesRef,
      isTyping
    } = this.props;

    if(isTyping) {
      return <TypingFormat />
    }

    if (attributes && attributes.type === 'propose') {
      const { proposeId, body, status } = attributes;
      const { title, term, value } = attributes.body;
      const approved = from === 'friend' ? listProposesRef && listProposesRef.find(propose => parseInt(propose.proposeId) === parseInt(proposeId) && propose.status === 240) || status === 240 : false;

      return (
        <ProposeMessage
          from={from}
          proposeId={proposeId}
          title={title}
          value={value}
          term={term}
          approved={approved}
          openModalContractPropose={openModalContractPropose}
        />
      );
    } else if (attributes && attributes.type === 'requestApproval') {
      const { proposeId, body, status } = attributes;
      const approved = from === 'friend' ? listProposesRef && listProposesRef.find(propose => parseInt(propose.proposeId) === parseInt(proposeId) && propose.status === 240) || status === 240 : false;

      return (
        <ProposeRequestApproval
          from={from}
          proposeId={proposeId}
          approved={approved}
          openModalApprovePropose={openModalApprovePropose}
        >{content}</ProposeRequestApproval>
      );
    } else if (attributes && attributes.type === 'approvedPropose') {

      return (
        <ProposeApproved
          from={from}
          openModalSendNFPropose={openModalSendNFPropose}
        >{content}</ProposeApproved>
      );
    } else if (attributes && attributes.type === 'briefing') {
      const { briefingId, title, description, excerpt, status } = attributes;

      return (
        <BriefingMatch
          from={from}
          title={title}
          briefingId={briefingId}
          description={description}
          excerpt={excerpt}
          status={status}
          messagesLength={messagesLength}
          openModalBriefingMatch={openModalBriefingMatch}
        >{content}</BriefingMatch>
      );
    } else if (onlyEmoji) {
      return <Emoji from={from}>{content}</Emoji>;
    }

    return <_TextFormat>{content}</_TextFormat>;
  
  }

}

export default ChatTypeMessages;
