import React, { Component } from 'react';
import withLocalization from 'utils/withLocalization';
import { Colors } from 'theme/Colors';
import BUTTON from 'GLOBALS/BUTTON';

import {
  _ProposeApproved,
  _TextFormat,
} from './styles';

class ProposeApproved extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      t,
      from,
      children,
      openModalSendNFPropose,
    } = this.props;

    return (
      <_ProposeApproved from={from}>
        <_TextFormat>{children}</_TextFormat>

        {from === 'friend' && (
          <BUTTON
            onPressFn={openModalSendNFPropose}
            content={t('chat:typeMessages:propose:approvedProposeSubmit')}
            color={Colors.BLACK}
            fontSize={14}
          />
        )}
      </_ProposeApproved>
    );
  }
}

export default withLocalization(ProposeApproved);
