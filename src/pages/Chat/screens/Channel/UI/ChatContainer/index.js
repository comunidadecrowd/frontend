import React, { Component } from 'react';
import { Platform, Dimensions } from 'react-native';

import { Colors } from 'theme';

import Arrow from 'GLOBALS/CROWD_ARROW';
import ChatProfile from './../ChatProfile';
import ChatMessages from './../ChatMessages';
import ModalContractPropose from './../ModalContractPropose';
import ModalRequestApprovePropose from './../ModalRequestApprovePropose';
import ModalApprovePropose from './../ModalApprovePropose';
import ModalSendNFPropose from './../ModalSendNFPropose';
import ModalBriefingMatch from './../ModalBriefingMatch';

import {
  _Container,
  _Scroll,
  _TouchToggleCloseModal,
  _ItemMessage,
  _ScrollToEnd,
  _CROWDIcon
} from './styles';

const
  { width: screenW, height: screenH } = Dimensions.get('window'),
  is_IOS = Platform === 'ios';

class ChatContainer extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      briefingId: null,
      hireProposeId: null,
      contractorUserId: null,
      scrollToEnd: true,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('# shouldComponentUpdate ChatContainer');
    return this.props.userId !== nextProps.userId
      || this.props.briefingMatch !== nextProps.briefingMatch
      || this.props.chatMessagesRef !== nextProps.chatMessagesRef
      || this.state.briefingId !== nextState.briefingId
      || this.state.hireProposeId !== nextState.hireProposeId
      || this.state.contractorUserId !== nextState.contractorUserId
      || this.state.scrollToEnd !== nextState.scrollToEnd
      || this.props.chatFooterHeight !== nextProps.chatFooterHeight;
  };

  openModalContractPropose = (proposeId, contractorUserId) => {
    this.contractProposeModal.snapTo({ index: 1 });
    this.setState({ hireProposeId: proposeId, contractorUserId });
  };

  openModalRequestApprovePropose = (proposeId, contractorUserId) => {
    this.requestApproveProposeModal.snapTo({ index: 1 });
    this.setState({ hireProposeId: proposeId, contractorUserId });
  };

  openModalApprovePropose = (proposeId, contractorUserId) => {
    this.approveProposeModal.snapTo({ index: 1 });
    this.setState({ hireProposeId: proposeId, contractorUserId });
  };

  openModalSendNFPropose = () => {
    this.sendNfProposeModal.snapTo({ index: 1 });
  };

  openModalBriefingMatch = (briefingId) => {
    this.setState({ briefingId }, () => this.briefingMatchModal.snapTo({ index: 1 }));
  };

  scrollOnContentSizeChange = (contentWidth, contentHeight) => {
    const { briefingMatch } = this.props;

    this.setState({ contentHeight: contentHeight }, () => this.handleScrollToEnd());
  };

  scrollOnScrollEndDrag = ({ nativeEvent }) => {
    /*console.log('# scrollOnScrollEndDrag',
      nativeEvent,
      '\n showing =>', (this.state.contentHeight - nativeEvent.contentOffset.y),
      '\n screen =>', (screenH - 80 - 60 - 0.5)
    );*/
      
    this.setState({ scrollToEnd: (this.state.contentHeight - nativeEvent.contentOffset.y) < (screenH - 80 - 60 - 0.5)});
  };

  handleScrollToEnd = () => {
    this.setState({ scrollToEnd: true }, this.scrollMessage.scrollToEnd({ animated: false }));
  };

  render() {
    const {
      userName,
      profileImage,
      title,
      category,
      userId,
      friendUserId,
      briefingMatch,
      channelSid,
      changeBriefingMatch,
      onToggleModalPropose,
      modalProposeRef,
      modalRequestApproveProposeRef,
      modalApproveProposeRef,
      getContractProposeModalRef,
      getRequestApproveProposeModalRef,
      getApproveProposeModalRef,
      getSendNFModalRef,
      getBriefingMatchModalRef,
      listProposesRef,
      chatContainerRef,
      chatMessagesRef,
      chatFooterHeight,

      // REFACT:  INICIO do refact para isolar view de mensagens - by Bruno Vinícius;
      getChatMessagesRef,
      // REFACT:  FIM do refact para isolar view de mensagens - by Bruno Vinícius;
    } = this.props;
    const { briefingId, hireProposeId, contractorUserId, scrollToEnd } = this.state;

    return (
      <_Container briefingMatch={briefingMatch} chatFooterHeight={chatFooterHeight}>
        <ModalContractPropose
          getRef={ref => {
            this.contractProposeModal = ref;
            getContractProposeModalRef(ref);
          }}
          openModalContractPropose={this.openModalContractPropose}
          modalProposeRef={modalProposeRef}
          proposeId={hireProposeId}
          contractorUserId={contractorUserId}
        />
        <ModalRequestApprovePropose
          getRef={ref => {
            this.requestApproveProposeModal = ref;
            getRequestApproveProposeModalRef(ref);
          }}
          openModalRequestApprovePropose={this.openModalRequestApprovePropose}
          modalProposeRef={modalProposeRef}
          modalRequestApproveProposeRef={modalRequestApproveProposeRef}
          proposeId={hireProposeId}
          contractorUserId={contractorUserId}
        />
        <ModalApprovePropose
          getRef={ref => {
            this.approveProposeModal = ref;
            getApproveProposeModalRef(ref);
          }}
          openModalApprovePropose={this.openModalApprovePropose}
          modalProposeRef={modalProposeRef}
          modalApproveProposeRef={modalApproveProposeRef}
          proposeId={hireProposeId}
          contractorUserId={contractorUserId}
        />
        <ModalSendNFPropose
          getRef={ref => {
            this.sendNfProposeModal = ref;
            getSendNFModalRef(ref);
          }}
          openModalSendNFPropose={this.openModalSendNFPropose}
        />
        <ModalBriefingMatch
          getRef={ref => {
            this.briefingMatchModal = ref;
            getBriefingMatchModalRef(ref);
          }}
          userId={userId}
          briefingId={briefingId}
          channelSid={channelSid}
          changeBriefingMatch={changeBriefingMatch}
          openModalBriefingMatch={this.openModalBriefingMatch}
          chatMessagesRef={chatMessagesRef}
        />

        <_Scroll
          ref={ref => this.scrollMessage = ref}
          onContentSizeChange={this.scrollOnContentSizeChange}
          onScrollEndDrag={this.scrollOnScrollEndDrag}
        >
          <_TouchToggleCloseModal
            activeOpacity={1}
            onPress={() => onToggleModalPropose(0)}
          >
            <ChatProfile 
              profileImage={profileImage}
              userName={userName}
              userId={friendUserId}
              title={title}
              category={category}
            />
            <ChatMessages
              ref={ref => getChatMessagesRef(ref)}
              userId={userId}
							channelSid={channelSid}
              profileImage={profileImage}
              friendUserId={friendUserId}
              category={category}
              briefingMatch={briefingMatch}
              listProposesRef={listProposesRef}
              chatContainerRef={chatContainerRef}
              chatMessagesRef={chatMessagesRef}
              openModalContractPropose={this.openModalContractPropose}
              openModalApprovePropose={this.openModalApprovePropose}
              openModalSendNFPropose={this.openModalSendNFPropose}
              openModalBriefingMatch={this.openModalBriefingMatch}
            />
          </_TouchToggleCloseModal>
        </_Scroll>
        <_ScrollToEnd show={!scrollToEnd} activeOpacity={0.6} onPress={this.handleScrollToEnd}>
          {is_IOS ? <Arrow orientation={'down'} size={6} style={{color: '#000'}} /> : <_CROWDIcon width={21} height={16} color={Colors.BLACK} />}
        </_ScrollToEnd>
      </_Container>
    );
  }
}

export default ChatContainer;
