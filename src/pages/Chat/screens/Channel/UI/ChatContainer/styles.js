import React from 'react';
import Svg, { Polygon } from 'react-native-svg';
import { Platform, Dimensions } from 'react-native';
import styled from 'styled-components/native';
import { Colors } from 'theme';

const
  { width, height } = Dimensions.get('window')
  is_IOS = Platform.OS === 'ios';

export const _Container = styled.View`
  padding-top: ${({ briefingMatch }) => !briefingMatch ? 80 : 60}px;
  /*height: ${({ briefingMatch, chatFooterHeight }) => {
    if(briefingMatch) {
      return height
    } else {
      if(chatFooterHeight <= 50) {
        return `${is_IOS ? (height - 60) : (height - 83)}px`
      } else if(chatFooterHeight > 50 && chatFooterHeight <= 84) {
        return `${height - (chatFooterHeight + 17)}px`
      } else {
        return `${height - (84 + 17)}px`
      }
    }
  }};*/
  height: ${({ briefingMatch, chatFooterHeight }) => {
    if(briefingMatch) {
      return height
    } else {
      if(chatFooterHeight <= 50) {
        return `91%`;
      } else if(chatFooterHeight > 50 && chatFooterHeight <= 84) {
        return `90%`;
      } else {
        return `88%`;
      }
    }
  }};
  z-index: 2;
`;

export const _Scroll = styled.ScrollView``;

export const _TouchToggleCloseModal = styled.TouchableOpacity``;

export const _ScrollToEnd = styled.TouchableOpacity`
  display: ${({ show }) => show ? 'flex' : 'none'};
  background: ${Colors.GRAY_LIGHT};
  width: 30px;
  height: 30px;
  align-items: center;
  justify-content: center;
  padding-top: ${is_IOS ? 0 : 0}px;
  padding-right: ${is_IOS ? 3 : 3}px;
  padding-bottom: ${is_IOS ? 3 : 3}px;
  position: absolute;
  bottom: 30px;
  right: 10px;
  border: 1px solid transparent;
  border-radius: 50px;
  transform: rotate(180deg);
`;

// TODO:  Mover esse item para icons;
export const _CROWDIcon = styled((props) => (
  <Svg {...props}>
    <Polygon points="26,26 13,0 0,26" fill={props.color ? props.color : '#666'} stroke="none" />
  </Svg>
))`
  transform: scale(0.8);
`;
