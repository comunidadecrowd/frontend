import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import withLocalization from 'utils/withLocalization';
import { IconCloseButton } from 'components/Icon/SearchView';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { FirstIcon, SecondIcon, ThirdIcon } from 'components/Icon/CarouselSendNFPropose';
const { width, height } = Dimensions.get('window');

import {
  _ContainerInteractable,
  _ButtonClose,
  _CarouselContainer,
  _CarouselTextWrapper,
  _CarouselText,
  _WrapperButtonSubmit,
} from './styles';

const styles = {
  paginatorWrapper: {
    backgroundColor: 'transparent',
    alignItems: 'center',
  },
  paginationDot: {
    width: 13,
    height: 13,
    borderRadius: 20,
    marginHorizontal: 10,
  },
};

class ModalSendNFPropose extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentSnapIndex: 0,
      sliderActiveItem: 0,
      entries: [
        'O cliente aprovou sua entrega, agora garanta que será feita a emissão de nota fiscal. Na Crowd essa etapa é obrigatória.',
        'O cliente conferiu que está tudo certo! DICA: comunicação contínua cria oportunidades de se trabalhar mais vezes.',
        'Com aprovação da entrega e a emissão da sua nota fiscal, o pagamento acontecerá 15 dias após o fechamento. Parabéns!',
      ],
      submiting: false,
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.state.currentSnapIndex !== nextState.currentSnapIndex ||
      this.state.sliderActiveItem !== nextState.sliderActiveItem ||
      this.props.proposeId !== nextProps.proposeId ||
      this.props.contractorUserId !== nextProps.contractorUserId ||
      this.props.modalProposeRef !== nextProps.modalProposeRef ||
      this.props.openModalSendNFPropose !== nextProps.openModalSendNFPropose
    );
  }

  saveRef = ref => {
    const { getRef } = this.props;
    getRef(ref);
    this.modal = ref;
  };

  closeModal = () => {
    this.modal.snapTo({ index: 0 });
  };

  renderItem = ({ item, index }) => {
    return (
      <_CarouselTextWrapper key={index}>
        {index === 0 && (
          <FirstIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 1 && (
          <SecondIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 2 && (
          <ThirdIcon height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        <_CarouselText>{item}</_CarouselText>
      </_CarouselTextWrapper>
    );
  };

  render() {
    const { t, openModalSendNFPropose } = this.props,
      { currentSnapIndex, sliderActiveItem, entries, submiting } = this.state;

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        ref={ref => this.saveRef(ref)}
        snapPoints={[{ y: height }, { y: 5 }]}
        currentSnapIndex={currentSnapIndex}
        openModalSendNFPropose={openModalSendNFPropose}
      >
        <_ButtonClose onPress={!submiting ? this.closeModal : () => false} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>

        <_CarouselContainer>
          <Carousel
            ref={ref => (this.carousel = ref)}
            data={entries}
            renderItem={this.renderItem}
            sliderWidth={width}
            itemWidth={width}
            swipeThreshold={10}
            horizontal
            firstItem={0}
            onSnapToItem={index => this.setState({ sliderActiveItem: index })}
          />

          <Pagination
            dotStyle={styles.paginationDot}
            containerStyle={styles.paginatorWrapper}
            inactiveDotColor={'#FFFFFF'}
            activeDotIndex={sliderActiveItem}
            dotColor={'lightblue'}
            dotsLength={entries.length}
            carouselRef={this.carousel}
            inactiveDotScale={0.9}
          />
        </_CarouselContainer>
      </_ContainerInteractable>
    );
  }
}

export default withLocalization(ModalSendNFPropose);
