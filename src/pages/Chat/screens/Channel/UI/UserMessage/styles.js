import styled from 'styled-components/native';
import { Dimensions } from 'react-native';
import { Colors } from 'theme/Colors';
import IconBacktick from 'components/Icon/IconBacktick';
import { IconCloseButton } from 'components/Icon/SearchView';
import { UserBubble } from 'components/Icon/ChatBubbles';
const { width, height } = Dimensions.get('window');

export const _Container = styled.View`
  align-content: space-between;
  align-self: flex-end;
  color: #000;
  min-width: ${({ isPropose, isBriefing }) => isPropose ? '85%' : isBriefing ? '100%' : 'auto'};
  max-width: ${({ isPropose, isBriefing }) => isPropose ? 'auto' : isBriefing ? '100%' : '85%'};
`;

export const _MessageWrapper = styled.View`
  width: ${({ type }) => type === 'briefing' ? width : 'auto'};
  align-items: ${({ type, from }) => from === 'friend' && type == 'propose' ? 'flex-end' : 'flex-start'};
  min-height: 16px;
  background: ${({ type, from, onlyEmoji }) => from === 'friend' && type === 'propose' && !onlyEmoji ? '#F2F2F2' : 'transparent'};
  border: 1px solid #F2F2F2;
  border-color: ${({ type, status, from, onlyEmoji }) => from === 'friend' && type === 'propose' && !onlyEmoji ? '#F2F2F2' : !onlyEmoji && type !== 'briefing' && status !== 'error' ? Colors.BLACK : status === 'error' && from === 'user' ? Colors.DESIGN : 'transparent'};
  padding: ${({ type }) => type === 'briefing' ? '7px' : '7px 30px 7px 15px'};
  margin-left: ${({ type, from }) => from === 'friend' && type == 'propose' || type === 'briefing' ? 0 : 23}px;
  /* margin-bottom: ${({ lastMessage }) => lastMessage ? 10 : 0}px; */
  /*margin-bottom: 10px;*/
`;

export const _TextFormat = styled.Text``;

export const _IconBacktick = styled(IconBacktick)`
  position: absolute;
  bottom: 9px;
  right: 10px;
`;

export const _IconSendError = styled(IconCloseButton)`
  position: absolute;
  bottom: 2px;
  right: 4px;
  transform: scale(0.5);
`;

export const _ClearBorder = styled.View`
  width: 2px;
  height: 12px;
  background: ${Colors.WHITE};
  position: absolute;
  top: 7.35px;
  right: 0;
  z-index: 1;
`;
export const _UserBubble = styled(UserBubble)`
  position: absolute;
  top: 6px;
  right: -14px;
`;
