import React, { Component, Fragment } from 'react';
import ChatTypeMessages from './../ChatTypeMessages';

import {
  _Container,
  _MessageWrapper,
  _TextFormat,
  _IconBacktick,
  _ClearBorder,
  _UserBubble,
} from './styles';

class UserMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      message: {
        attributes,
        body: content,
        status
      },
      index,
      messagesCount,
    } = this.props;
    
    return (
      <_Container isPropose={attributes && attributes.type === 'propose'}>
        {index === messagesCount - 1 && (
        <Fragment>
          <_ClearBorder />
          <_UserBubble
            width={15}
            height={15}
          />
        </Fragment>)}
        {/* <View
          style={[
            userMessageWrapper,
            attributes && attributes.type === 'propose' ? proposeWrapper : messageAnswerWrapper,
            index !== payload.length - 1 && { marginBottom: 10 },
            attributes && attributes.type === 'propose' ? userProposeWrapper : props.style,
          ]}
        > */}
        <_MessageWrapper
          from={'user'}
          type={attributes && attributes.type === 'propose' ? attributes.type : 'answer'}
          lastMessage={index === messagesCount - 1}>
          <ChatTypeMessages
            from={'user'}
            attributes={attributes}
            content={content}
          />
          <_IconBacktick
            width={10}
            height={8}
            color={
              status === 'send' ? 'rgba(0,0,0,.3)' : status === 'sent' ? 'rgba(0,0,0,1)' : '#84C9F2'
            }
          />
        </_MessageWrapper>
        {/* </View> */}
      </_Container>
    );
  }
}

export default UserMessage;