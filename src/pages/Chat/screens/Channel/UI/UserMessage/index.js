import React, { Component, Fragment } from 'react';
import ChatTypeMessages from './../ChatTypeMessages';
import { Colors } from 'theme/Colors';

import {
  _Container,
  _MessageWrapper,
  _TextFormat,
  _IconBacktick,
  _IconSendError,
  _ClearBorder,
  _UserBubble,
} from './styles';

const emojiRegex = require('emoji-regex');

class UserMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      index,
      messagesLength,
      message, message: { state: { attributes, body, status } },
			messages,
			friendUserId,
    } = this.props,
    regex = emojiRegex(),
    hasEmoji = regex.exec(body),
    onlyEmoji = !body.replace(/\s/g, '').match(/(\w){1,}|(\d){1,}/g) && hasEmoji && hasEmoji[0];

    return (
      <_Container
        from={'user'}
        isPropose={attributes && attributes.type === 'propose'}
        isBriefing={attributes && attributes.type === 'briefing'}
      >
        <_MessageWrapper
          from={'user'}
          status={status}
          type={attributes && (attributes.type === 'propose' || attributes.type === 'briefing') ? attributes.type : 'answer'}
          onlyEmoji={onlyEmoji}
        >
          <ChatTypeMessages
            from={'user'}
            attributes={attributes || undefined}
            content={body}
            onlyEmoji={onlyEmoji}
            messagesLength={messagesLength}
          />
          {status === 'error'
            ? <_IconSendError
                width={20}
                height={20}
                color={Colors.DESIGN}
              />
            : <_IconBacktick
                width={10}
                height={8}
                color={
                  status === 'sending' ? 'rgba(0,0,0,.3)' : status === 'sent' ? '#000' : '#1796E9'
                }
              />
          }
        </_MessageWrapper>
        {!onlyEmoji && (!attributes || attributes.type !== 'briefing') && (index === (messagesLength - 1) || index < (messagesLength - 1) && parseInt(messages[index + 1].state.author) === parseInt(friendUserId)) &&
          <Fragment>
            <_ClearBorder />
            <_UserBubble
              width={15}
              height={15}
              color={status === 'error' ? Colors.DESIGN : '#000000'}
            />
          </Fragment>
        }
      </_Container>
    );
  }
}

export default UserMessage;
