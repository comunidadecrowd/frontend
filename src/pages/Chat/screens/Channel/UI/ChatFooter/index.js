import React, { Component, Fragment } from 'react';
import { Text } from 'react-native';

import IconAudio from 'components/Icon/IconAudio';
import IconMoney from 'components/Icon/IconMoney';
import IconPlus from 'components/Icon/IconPlus';

import {
  _Container,
  _IconAttachment,
  _WrapperInput,
  _MessageInput,
  _IconProposes,
  _IconSendAudio,
  _IconSendMessage,
  _IconSend,
} from './styles';

class ChatFooter extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      newMessage: '',
      inputHeight: 50
    };
  }

  typingMessage = (text) => {
    const { isTyping } = this.props;

    if (isTyping) isTyping();
    this.setState({ newMessage: text });
  };

  setInputHeight = (height) => {

    const { chatFooterChangeHeight } = this.props;

    this.setState({ inputHeight: height });
    chatFooterChangeHeight(height)
  
  };

  render() {
    
    const 
      { sendNewMessage, onToggleModalPropose } = this.props,
      { newMessage, inputHeight } = this.state;

    return (
      <_Container inputHeight={inputHeight}>
        
        {/* // TODO:  Habilitar quando houver a funcionalidade
          <_IconAttachment>
            <IconPlus width={19} height={19} />
          </_IconAttachment> 
        */}

        <_WrapperInput activeOpacity={1} onPress={() => this.refs._MessageInputRef.focus()}> 
          <_MessageInput
            ref={'_MessageInputRef'}
            placeholder="Escreva uma mensagem"
            placeholderTextColor="#fff"
            value={newMessage}
            onChange={event => this.typingMessage(event.nativeEvent.text)}
            keyboardType="default"
            enablesReturnKeyAutomatically
            onContentSizeChange={event => this.setInputHeight(event.nativeEvent.contentSize.height)}
            multiline
            numberOfLines={5}
          />
        </_WrapperInput> 
        
        {!newMessage.length ? (
          <Fragment>
            
            <_IconProposes onPress={() => onToggleModalPropose(1)}>
              <IconMoney width={10.01} height={18.95} />
            </_IconProposes>

            {/* // TODO:  Habilitar quando houver a funcionalidade
              <_IconSendAudio>
                <IconAudio width={15} height={19} />
              </_IconSendAudio> 
            */}
          </Fragment>
        ) : (
            <_IconSendMessage onPress={newMessage.trim() !== '' ? () => {
                this.setState({newMessage: '' }, () => sendNewMessage(newMessage))
              } : null}>
              <_IconSend width={24} height={20} />
            </_IconSendMessage>
          )}
      </_Container>
    );
  }
}

export default ChatFooter;
