import React from 'react';
import styled from 'styled-components/native';
import { Dimensions, TextInput } from 'react-native';
import Svg, { Polygon } from 'react-native-svg';
import { Colors } from 'theme/Colors';

const { width, height } = Dimensions.get('window');

export const _Container = styled.View`
  width: 100%;
  /*height: ${({inputHeight}) => {
    if(inputHeight <= 50) {
      return '60px'
    } else {
      if( inputHeight > 50 && inputHeight <= 84 ) {
        return `${(inputHeight + 17)}px`
      } else {
        return `${84 + 17}px`
      }
    }
  }};*/
  height: ${({inputHeight}) => {
    if(inputHeight <= 50) {
      return '9%';
    } else {
      if( inputHeight > 50 && inputHeight <= 84 ) {
        return `10%`;
      } else {
        return `12%`;
      }
    }
  }};
  flex-direction: row;
  align-items: center;
  padding: 0 15px;
`;

export const _Buttons = styled.TouchableOpacity`
  height: 100%;
  width: 40px;
  align-items: center;
  justify-content: center;
`;

export const _IconAttachment = styled(_Buttons)`
  margin: 0 0 0 18px;
`;

export const _WrapperInput = styled.TouchableOpacity`
  height: 100%;
  justify-content: center;
`;

export const _MessageInput = styled(TextInput)`
  width: ${({value}) => value === '' ? (width - ((15 * 2) + (40))) : (width - 110)};
  color: ${Colors.WHITE};
  font-size: 14px;
  font-family: 'Inter Ui';
  padding: 0 0 2px;
`;

export const _IconProposes = styled(_Buttons)`
  width: 40px;
`;

export const _IconSendAudio = styled(_Buttons)`
  margin: 0 18px 0 0;
`;

export const _IconSendMessage = styled(_Buttons)`
  width: 80px;
`;

// TODO:  Mover esse item para icons;
export const _IconSend = (props) => (
  <Svg {...props}>
    <Polygon points="24,24 12,0 0,24" fill="#84C9F2" stroke="none" />
  </Svg>
);
