import React, { Component } from 'react';
import { Text } from 'react-native';

import UserMessage from './../UserMessage';
import FriendMessage from './../FriendMessage';

// import { _ItemMessage } from './../ChatContainer/styles';
import { _Container, _ItemMessage } from './styles';

class ChatMessages extends Component {

  constructor(props) {
    super(props);
    this.state = {
      messages: [],
    };
    
    this.timeoutList = [];
  }

  /*
  // TODO:  BRUNO | Verificar se shouldComponentUpdate ainda é necessário após várias mensagens
    shouldComponentUpdate(nextProps, nextState) {
      console.log('# shouldComponentUpdate ChatMessages', this.state.messages, nextState.messages);
      return this.state.messages !== nextState.messages
        || this.props.chatMessagesRef !== nextProps.chatMessagesRef;
    };*/

  addNewMessage = (obj) => {
    const
      { userId } = this.props,
      { messages } = this.state,
      { newMessage, crowdID, userBlocked } = obj;

    let messageToPush = {
      state: {
        author: userId,
        body: newMessage,
        attributes: {
          crowdID,
          type: 'default'
        },
        status: !userBlocked ? 'sending' : 'error'
      }
    };

    this.setState({
      messages: [
        ...messages,
        messageToPush
      ]
    }, () => {
      if (!userBlocked) this.timeoutList[crowdID] = setTimeout(() => {
        let { messages } = this.state;
				console.log('# timeout', messages.find(msg => msg.state.attributes.crowdID === crowdID && msg.state.status !== 'sent'));
        messages.find(msg => msg.state.attributes.crowdID === crowdID && msg.state.status !== 'sent').state.status = 'error';
        this.setState({ messages });
      }, 20000);
    });
  };

  updateMessagesFromUser = (message) => {
    let { messages } = this.state, { index, attributes, author, body, dateUpdated } = message.state;

    console.log('# ChatMessages | updateMessagesFromUser/message', message);

    let messageToPush = {
      state: {
        author,
        dateUpdated,
        body,
        attributes,
				index,
        status: 'sent'
      }
    };

    this.setState({
      messages: [
        ...messages,
        messageToPush 
      ]
    });
  };

  updateMessagesFromFriend = (message) => {
    let { messages } = this.state, { index, attributes, author, body, dateUpdated } = message.state;

    console.log('# ChatMessages | updateMessagesFromFriend/message', message);

    let messageToPush = {
      state: {
        author,
        dateUpdated,
        body,
        attributes,
				index,
        status: 'sent'
      }
    };

    this.setState({
      messages: [
        ...messages,
        messageToPush 
      ]
    });
  };

  typingStatus = (type) => {
    const { friendUserId } = this.props;

    if (type === 'end') {
      this.setState({
        messages: [
          ...this.state.messages.filter(msg => !msg.state.status || msg.state.status !== 'typing')
        ]
      });
    } else {
      let getTyping = this.state.messages.find(msg => msg.state.status && msg.state.status === 'typing');

      if (!getTyping) {
        let stateTyping = {
          state: {
            author: friendUserId,
            attributes: {
              type: 'default'
            },
            status: 'typing'
          }
        }

        this.setState({
          messages: [
            ...this.state.messages,
            stateTyping
          ]
        });
      }
    }
  };

  setMessageStatus = async (indexFromTwilio, crowdIDReceived, status, updateComesFromMe = false, dateUpdated = null) => {
    const
      { userId, channelSid } = this.props,
      { messages } = this.state;
    var messageFiltered;
    
    if(crowdIDReceived !== null) {

      if (this.timeoutList[crowdIDReceived]) {
        clearTimeout(this.timeoutList[crowdIDReceived]);
        delete this.timeoutList[crowdIDReceived];
      }

			messageFiltered = messages.filter(({ state: { author, status } }) => parseInt(author) === parseInt(userId) && status && status !== 'error')
				 .find(msg => msg.state.attributes.crowdID === crowdIDReceived);
				 
			if (messageFiltered) {
        if (dateUpdated) messageFiltered.state.dateUpdated = dateUpdated;
				messageFiltered.state.status = status;
				messageFiltered.state.index = indexFromTwilio;
      }

    } else {

      if(updateComesFromMe) return;

      messageFiltered = messages.filter(({ state: { index: indexFromTwilioToCompare } }) => status === 'sent' && indexFromTwilioToCompare <= indexFromTwilio)
        .map(msg => msg.state.status = status);
				 
      // if(messageFiltered) messageFiltered.state.status = status;
			// delete messageFiltered[0][0].crowdID

			console.log('# chatMessages | setMessageStatus/messageFiltered', messageFiltered, indexFromTwilio);

    }
		
		// atualiza mensagem no state do channel;
    if(messageFiltered) {
      this.setState({ messages });
      if (status !== 'sent') APP_STORAGE.updateItem(`CHAT_${channelSid}`, messages);
    }

  }

  render() {
    const {
      userId,
      friendUserId,
      profileImage,
      category,
      briefingMatch,
      openModalContractPropose,
      openModalApprovePropose,
      openModalSendNFPropose,
      openModalBriefingMatch,
      listProposesRef,
      chatMessagesRef,
    } = this.props;
    const { messages, briefingId } = this.state;

    return (
      <_Container isBriefingMatch={briefingMatch}> 
        {messages.filter(({ state: { attributes, status } }) => attributes && (!attributes.type || attributes.type && attributes.type !== 'matchApproved') || status && status === 'typing').map((message, index) => {
          const { author, body, attributes } = message.state;
          const isLastOnView = index === (messages.length - 1);

          return parseInt(userId) === parseInt(author) ? (
            <_ItemMessage
              key={index}
              isBriefingMatch={attributes.type === 'briefing'}
              isLastOnView={isLastOnView}
              from={'user'}
            >
              <UserMessage
                key={index}
                index={index}
                message={message}
                messages={messages.filter(({ state: { attributes, status } }) => attributes && (!attributes.type || attributes.type && attributes.type !== 'matchApproved') || status && status === 'typing')}
                messagesCount={0}
                messagesLength={messages.filter(({ state: { attributes, status } }) => attributes && (!attributes.type || attributes.type && attributes.type !== 'matchApproved') || status && status === 'typing').length}
								friendUserId={friendUserId}
              />
            </_ItemMessage>
          ) : (
            <_ItemMessage
              key={index}
              isBriefingMatch={attributes.type === 'briefing'}
              isLastOnView={isLastOnView}
              from={'friend'}
            >
              <FriendMessage
                key={index}
                index={index}
                message={message}
                messages={messages.filter(({ state: { attributes, status } }) => attributes && (!attributes.type || attributes.type && attributes.type !== 'matchApproved') || status && status === 'typing')}
                messagesCount={0}
                messagesLength={messages.filter(({ state: { attributes, status } }) => attributes && (!attributes.type || attributes.type && attributes.type !== 'matchApproved') || status && status === 'typing').length}
                userId={userId}
                friendUserId={friendUserId}
                profileImage={profileImage}
                category={category}
                openModalContractPropose={openModalContractPropose}
                openModalApprovePropose={openModalApprovePropose}
                openModalSendNFPropose={openModalSendNFPropose}
                openModalBriefingMatch={openModalBriefingMatch}
                listProposesRef={listProposesRef}
                chatMessagesRef={chatMessagesRef}
              />
            </_ItemMessage>
          );
        })}
      </_Container>
    );
  }

}

export default ChatMessages;
