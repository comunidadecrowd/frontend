import styled from 'styled-components/native';
import { Dimensions } from 'react-native';
import { Colors } from 'theme/Colors';

const { height } = Dimensions.get('window');

export const _Container = styled.View`
  min-height: ${({ isBriefingMatch }) => !isBriefingMatch ? (height - (height / 2)) - 140 : (height - (height / 2)) - 60}px;
  background: ${Colors.WHITE};
  /*padding: 30px 15px 0;*/
  padding: 30px 15px 0;
`;

export const _ItemMessage = styled.View`
  min-height: 36px;
  padding-left: ${({ isBriefingMatch }) => !isBriefingMatch ? 48 : 0}px;
  margin-left: ${({ isBriefingMatch }) => !isBriefingMatch ? 0 : -15}px;
  margin-bottom: ${({ isLastOnView, isBriefingMatch, from }) => isLastOnView && from === 'friend' ? 17 : 10}px;
`;
