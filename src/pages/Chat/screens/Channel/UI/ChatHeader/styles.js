import styled from 'styled-components/native';
import { Colors } from 'theme/Colors';
import { TEXT } from 'GLOBALS/TEXT';

export const _Container = styled.View`
  flex-direction: row;
  /*height: 80px;*/
  height: 12%;
  position: absolute;
  z-index: 1;
  background: ${Colors.BLACK};
  width: 100%;
`;

export const _Buttons = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
`;

export const _ButtonBack = styled(_Buttons)`
  width: 18%;
  height: 100%;
  padding: 0 0 0 9px;
`;

export const _ChatInfos = styled.View`
  justify-content: center;
  align-items: center;
  width: 60%;
  height: 100%;
`;

export const _UserName = styled(TEXT)``;

export const _WrapperStatus = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const _UserStatus = styled(TEXT)`
  align-items: center;
`;

export const _WrapperButtons = styled.View`
  flex-direction: row;
  /*justify-content: space-between;*/
  justify-content: flex-end;
  width: 22%;
  height: 100%;
  padding: 0 2.5% 0 0;
`;


export const _ButtonCallUser = styled(_Buttons)`
  width: 50%;
`;

export const _ButtonOpenMenu = styled(_Buttons)`
  width: 50%;
`;

export const _HeaderMenuList = styled.View`
  width: 100%;
  height: auto;
  background: ${Colors.BLACK};
  position: absolute;
  top: 72px;
  right: 0;
  z-index: 5;
`;

export const _HeaderMenuItem = styled.TouchableOpacity`
  opacity: ${({ disabled }) => disabled ? 0.3 : 1};
  padding: 10px 7px;
  border: 0px solid ${Colors.WHITE};
  border-top-width: ${({ isFirstItem }) => isFirstItem ? 0 : 1}px;
`;

export const _HeaderMenuLabel = styled(TEXT)``;
