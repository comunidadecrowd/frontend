import React, { Component } from 'react';
import { withTwilio } from 'services/Twilio';
import { Colors } from 'theme';

import { IconBackButton, IconCallUser, IconOpenMenu, IconStar } from 'components/Icon/ChatHeader';
import IconStatus from 'components/Icon/IconStatus';

import {
  _Container,
  _ButtonBack,
  _ChatInfos,
  _UserName,
  _WrapperStatus,
  _UserStatus,
  _ButtonCallUser,
  _ButtonOpenMenu,
  _WrapperButtons,
  _HeaderMenuList,
  _HeaderMenuItem,
  _HeaderMenuLabel
} from './styles';

class ChatHeader extends Component {
  
  constructor(props) {
    
    super(props);
    
    this.state = {
      userBlocked: this.props.userBlocked || false,
      friendState: 'Offline',
      isRequesting: false,
    };
  
  }
  
  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    constructor(props) {
      
      super(props);
      
      this.Chat = APP_SWORKER.Chat;
      
      this.state = {
        friendState: 'Offline'
      };
    
    }
  */

  async componentDidMount() {

    const
      { friendUserId, channelBlocked, Twilio } = this.props,
      { userBlocked } = this.state;
    this.mounted = true;

    global.emitters.on('blockedList', (blockedList) => {
      if (blockedList.find(user => parseInt(user) === parseInt(friendUserId))) {
        this.setState({
          userBlocked: true,
          friendState: 'Bloqueado',
        });
      } else {
        Twilio.checkFriendOnlineStatus(friendUserId, (friendUserOnlineStatus) => {
          if(friendUserOnlineStatus) {
            this.setState({
              userBlocked: false,
              friendState: !channelBlocked ? 'Online' : 'Bloqueado',
            });
          } else {
            this.setState({
              userBlocked: false,
              friendState: !channelBlocked ? 'Offline' : 'Bloqueado',
            });
          }
        });
      }
    });

    if (userBlocked || channelBlocked) this.setState({ friendState: 'Bloqueado' });

    Twilio.checkFriendOnlineStatus(friendUserId, (friendUserOnlineStatus) => {if(friendUserOnlineStatus && !userBlocked && !channelBlocked) this.setState({friendState: 'Online'})})
    
    Twilio.emitter.on(`${friendUserId}_onlineStatus`, (onlineStatus) => {

      if(!this.mounted || userBlocked || channelBlocked) return;
      this.setState({ friendState: onlineStatus ? 'Online' : 'Offline' });

    });

  }
  
  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    async componentDidMount() {

      const { friendUserId } = this.props;
      this.mounted = true;
      
      this.Chat.getUser(friendUserId).then(user => {

        const { online: currentOnline } = user;

        if(currentOnline) {
          this.setState({
            friendState: 'Online'
          })
        }
        
        user.on('updated', (event) => {
        
          const { updateReasons, user: { online } } = event;
    
          if (updateReasons.includes('online')) {
            
            if(!this.mounted) return;
            
            this.setState({
              friendState: online === true ? 'Online' : 'Offline'
            })
          
          }
    
        })
      
      })

    }
  */

  componentWillUnmount() {
    const { friendUserId, Twilio } = this.props;
    Twilio.emitter.removeAllListeners([`${friendUserId}_onlineStatus`]);
    global.emitters.removeAllListeners(['blockedList']);
    this.mounted = false;
  }

  menuListToggle = () => {
    this.setState({ menuListOpened: !this.state.menuListOpened }, () => {
      setTimeout(() => {
				if (this.state.menuListOpened) this.menuListToggle();
			}, 5000);
    });
  };

  handlerBlockUser = () => {
    const { channelSid, handlerBlockUser, friendUserId, Twilio } = this.props;

    this.setState({ isRequesting: true });
    return handlerBlockUser(channelSid, friendUserId, () => {
      this.setState({ isRequesting: false, }, () => this.menuListToggle());
    });
  };

  handlerReportUser = () => {
    const { channelSid, handlerReportUser, friendUserId, handleBack, Twilio } = this.props;

    this.setState({ isRequesting: true });
    return handlerReportUser(channelSid, friendUserId, () => {
      this.setState({ isRequesting: false, }, () => {
				this.menuListToggle();
				return handleBack();
			});
    });
  };

  render() {
    const
      { channelBlocked, friendUserId, userName, handleBack } = this.props,
      { loading, friendState, menuListOpened, userBlocked, isRequesting } = this.state;

    return (
      <_Container>

        <_ButtonBack onPress={handleBack}>
          <IconBackButton width={18} height={20} color="#fff" />
        </_ButtonBack>

        <_ChatInfos>
          <_UserName bold content={userName ? userName : 'UserName'} fontSize={16} />
          <_WrapperStatus>
            <IconStatus width={6} height={6} userStatus={!channelBlocked ? friendState : 'Bloqueado'} />
            <_UserStatus content={!channelBlocked ? friendState : 'Bloqueado'} fontSize={12} />
          </_WrapperStatus>
        </_ChatInfos>

        <_WrapperButtons>
        {/* // TODO:  Habilitar quando houver a funcionalidade
          <_ButtonCallUser onPress={() => false}>
            <IconCallUser width={18} height={20} color="#fff" />
          </_ButtonCallUser>
        */}

          <_ButtonOpenMenu onPress={() => this.menuListToggle()}>
            <IconOpenMenu width={26} height={21} color="#fff" />
          </_ButtonOpenMenu>
        </_WrapperButtons> 

        {
          // REFACT: Montar com flatList quando houver mais opções no menu;
          menuListOpened && <_HeaderMenuList>
          <_HeaderMenuItem
            isFirstItem
            onPress={!isRequesting ? this.handlerBlockUser : () => false}
            disabled={isRequesting}
            activeOpacity={1}>
              <_HeaderMenuLabel content={!userBlocked ? 'Bloquear usuário' : 'Desbloquear usuário'} fontSize={14} align={'center'} />
            </_HeaderMenuItem>
            <_HeaderMenuItem
              onPress={!isRequesting ? this.handlerReportUser : () => false}
              disabled={isRequesting}
              activeOpacity={1}>
                <_HeaderMenuLabel content={'Denunciar usuário'} fontSize={14} align={'center'} color={Colors.ERROR} />
            </_HeaderMenuItem>
          </_HeaderMenuList>
        }

      </_Container>
    );
  }
}

export default withTwilio(ChatHeader);
