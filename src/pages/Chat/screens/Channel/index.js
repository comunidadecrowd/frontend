import React, { Component } from 'react';
import { KeyboardAvoidingView, Keyboard, AppState } from 'react-native';
import { Navigation } from 'react-native-navigation';

import ChatHeader from './UI/ChatHeader';
import ChatContainer from './UI/ChatContainer';
import ChatFooter from './UI/ChatFooter';

import ModalPropose from './UI/ModalPropose';

import { withTwilio } from 'services/Twilio';

import { 
  _Container,
  _KeyboardAvoidingView,
  _BriefingButtonAnswer
} from './styles';

class Channel extends Component {

  constructor(props) {

    super(props);

    this.userBlocked = this.props.userBlocked;
    this.ChatChannel; // FIX:  Alex -> Repensar nessa lógica que está sendo usada apenas para não ter que pegar o getChannel da Class Twilio toda vez 

    this.state = {
      loadingFromTwilio: false,
      briefingMatch: this.props.briefingMatch,
      chatFooterHeight: '',
      channelBlocked: this.props.channelBlocked,
    };

    modalPropose = {};
    listProposes = {};
    contractProposeModal = {};
    requestApproveProposeModal = {};
    approveProposeModal = {};
    briefingMatchModal = {};

    // console.log('$Twilio Channel | constructor/this', this);

  };

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    constructor(props) {

      super(props);

      this.ChatChannel = null;

      this.state = {
        loadingFromTwilio: false,
        briefingMatch: this.props.briefingMatch,
        chatFooterHeight: ''
      };

      modalPropose = {};
      listProposes = {};
      contractProposeModal = {};
      requestApproveProposeModal = {};
      approveProposeModal = {};
      briefingMatchModal = {};

    };
  */

  async componentDidMount() {
    const {
      userId,
      friendUserId,
      channelSID,
      Twilio 
    } = this.props;
		let channelStorage = await APP_STORAGE.getItem(`CHAT_${channelSID}`);	

		if (channelStorage && channelStorage.length && this.chatMessagesRef) {
			this.chatMessagesRef.setState({
				messages: channelStorage
			}, () => {
				if (this.chatContainerRef) {
					this.chatContainerRef.handleScrollToEnd();
				}
			});
		}

    Twilio.getChannel(channelSID, true, async (Channel) => this.ChatChannel = await Channel) // TODO:  Alex, veriicar a necessidade desse async/await
    
    Twilio.emitter.on(`${channelSID}_messageAdded`, (message) => this.receiveTwilioMessage(message));
    Twilio.emitter.on(`${channelSID}_channelTypingStarted`, () => this.getTypingStatus('start'));
    Twilio.emitter.on(`${channelSID}_channelTypingEnded`, () => this.getTypingStatus('end'));
    Twilio.emitter.on(`${channelSID}_memberUpdated`, (obj) => this.memberUpdated(obj));
    Twilio.emitter.on('channelUpdated', ({ channel: { sid, state } }) => this.channelUpdated(sid, state));
    
    if (channelStorage && channelStorage.length && parseInt(channelStorage.slice(-1)[0].state.author) !== parseInt(userId)) {
      Twilio.setAllMessagesConsumed(channelSID);
    }

    global.emitters.on('blockedList', (blockedList) => {
      if (blockedList.find(user => parseInt(user) === parseInt(friendUserId))) {
        this.userBlocked = true;
      } else {
        this.userBlocked = false;
      }
    });
  };

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    async componentDidMount() {
    const { channelSID, userId } = this.props;
		let channelStorage = await APP_STORAGE.getItem(`CHAT_${channelSID}`);	
    
		this.ChatChannel = await APP_SWORKER.Chat.getChannelBySid(channelSID);

		console.log('# channel | componentDidMount/channelStorage =', channelStorage);

		if (channelStorage && channelStorage.length && this.chatMessagesRef) {
			this.chatMessagesRef.setState({
				messages: channelStorage
			}, () => {
				if (this.chatContainerRef) {
					this.chatContainerRef.handleScrollToEnd();
				}
			});
		}

    // LISTENERS
    this.ChatChannel.on('messageAdded', (message) => this.receiveTwilioMessage(message));
    this.ChatChannel.on('typingStarted', () => this.getTypingStatus('start'));
    this.ChatChannel.on('typingEnded', () => this.getTypingStatus('end'));
    this.ChatChannel.on('memberUpdated', async ({ member }) => {

			console.log('# channel | memberUpdated/member', member, parseInt(member.identity) !== parseInt(userId));

			if (this.chatMessagesRef) {
				this.chatMessagesRef.setMessageStatus(member.lastConsumedMessageIndex, null, 'read', parseInt(member.identity) === parseInt(userId));

				if (parseInt(member.identity) !== parseInt(userId)) {
					this.chatMessagesRef.setState({
						messages: this.chatMessagesRef.state.messages.map(({ state: { index, author, dateUpdated, attributes, body, status } }) => {
							if (parseInt(author) === parseInt(userId) && status === 'sent') {
								return {
									state: {
										author,
										dateUpdated,
										attributes,
										body,
										index,
										status: 'read'
									}
								}
							} else {
								return {
									state: {
										author,
										dateUpdated,
										attributes,
										body,
										index,
										status: 'read'
									}
								}
							}
						})
					});
        }
      }

    });

    if (this.chatMessagesRef.state.messages.filter(msg => (!msg.state.status || msg.state.status === 'sent') && parseInt(msg.state.author) !== parseInt(userId))) this.ChatChannel.setAllMessagesConsumed();
  }*/

  memberUpdated = (obj) => {
    const
      {  userId } = this.props,
      { member: { identity, lastConsumedMessageIndex } } = obj;

    if(this.chatMessagesRef) {
      let
        { messages } = this.chatMessagesRef.state,
        messagesUnread;

      this.chatMessagesRef.setMessageStatus(lastConsumedMessageIndex, null, 'read', parseInt(identity) === parseInt(userId));

      if (messages.length) {
        messagesUnread = messages.filter(({ state: { index, author, status } }) => status === 'sent' && parseInt(author) !== parseInt(identity) && index <= lastConsumedMessageIndex)
          .map(msg => msg.state.status = 'read');
        this.chatMessagesRef.setState({ messages });
      }
    }
  };

	channelUpdated = (channelSid, { attributes: { blocked } }) => {
    const
      { channelSID } = this.props,
      { channelBlocked } = this.state;

		if (channelSid === channelSID && blocked !== channelBlocked) this.setState({ channelBlocked: blocked });
	};

  componentWillUnmount() {
    this.props.Twilio.emitter.removeAllListeners()
    global.emitters.removeAllListeners(['blockedList']);
  }

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    appStateFN = (appState) => {
      if (appState === 'inactive') this.updateMessagesOnStorage()
    }
  */
  
  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    updateMessagesOnStorage = (callback=null) => {

      const
        { channelSID } = this.props,
        { messages } = this.chatMessagesRef.state;

      APP_STORAGE.updateItem(`CHAT_${channelSID}`, messages.filter(msg => msg.state.status !== 'typing'), () => {
        if (callback) callback();
        console.log('## CHANNEL updateMessagesOnStorage -> messages', messages);
      });

    }
  */

  handleBack = () => {

    Navigation.pop(this.props.componentId);

  };

  getChatMessagesRef = (ref) => {
    this.chatMessagesRef = ref;
  };

  receiveTwilioMessage = async (message) => {
    const
      { userId, 
        channelSID, 
        Twilio 
      } = this.props,
      { 
        author, 
        dateUpdated, 
        attributes, 
        index: indexFromTwilio 
      } = message.state;
    
    if (!attributes.type) attributes.type = 'default';

    const { type } = attributes;

    if (this.chatMessagesRef) {
      
      if (parseInt(author) !== parseInt(userId)) {

        if (type === 'propose' || type === 'system' || type === 'requestApproval' || type === 'approvedPropose') {
          if (this.modalPropose) {
            
            if (type === 'propose') this.modalPropose.props.getListProposes();
            
            if (type === 'system' || type === 'requestApproval' || type === 'approvedPropose') this.modalPropose.props.getLastStatusProposeById(attributes.proposeId);

          }
        }

        this.chatMessagesRef.updateMessagesFromFriend(message);
        Twilio.updateLastConsumedMessageIndex(channelSID, indexFromTwilio)
      
      } else {
        
        if (type === 'propose' || type === 'requestApproval' || type === 'approvedPropose' || type === 'system') {

          if (this.chatMessagesRef && this.chatMessagesRef.state && this.chatMessagesRef.state.messages.length) {
            const { messages } = this.chatMessagesRef.state;

            if (type === 'system') messages.filter(message => parseInt(message.state.author) !== parseInt(userId))
                .map(msg => {
                  if (msg.state.attributes.type === 'propose' && parseInt(msg.state.attributes.proposeId) === parseInt(attributes.proposeId)) msg.state.attributes.status = 220;
                });

            if (type === 'approvedPropose') messages.filter(message => parseInt(message.state.author) !== parseInt(userId))
                .map(msg => {
                  if ((msg.state.attributes.type === 'propose' || msg.state.attributes.type === 'requestApproval') && parseInt(msg.state.attributes.proposeId) === parseInt(attributes.proposeId)) msg.state.attributes.status = 240;
                });
          }

          return this.chatMessagesRef.updateMessagesFromUser(message);
        } else {
          this.chatMessagesRef.setMessageStatus(indexFromTwilio, attributes.crowdID, 'sent', false, dateUpdated)
        }

      }

    }
  };

  handleAddNewMessage = (newMessage, callback = null) => {
    let crowdID = [...Array(9)].map(i=>(~~(Math.random()*36)).toString(36)).join(''),
      { channelBlocked } = this.state;

    if (this.chatMessagesRef) this.chatMessagesRef.addNewMessage({newMessage, crowdID, userBlocked: this.userBlocked || channelBlocked});

    if (this.ChatChannel && !this.userBlocked && !channelBlocked) {
      this.ChatChannel
        .sendMessage(newMessage, { crowdID }) // FIX:  Alex -> Repensar nessa lógica que está sendo usada apenas para não ter que pegar o getChannel da Class Twilio toda vez 
        .catch(e => console.log(e));
    }
          
    if (callback) callback();
  };

  getTypingStatus = (type) => {
    if (this.chatMessagesRef) this.chatMessagesRef.typingStatus(type);
  };

  isTyping = () => {
    if (this.ChatChannel) this.ChatChannel.typing();
  };

  onToggleModalPropose = (index) => {
    Keyboard.dismiss();
    if (this.modalPropose) this.modalPropose.snapTo({ index });
  };

  changeBriefingMatch = (rejectMatch = null) => {
    const { changeBriefingMatchChannelItem } = this.props;

    if (rejectMatch) return changeBriefingMatchChannelItem(rejectMatch);

    this.setState({ briefingMatch: false }, changeBriefingMatchChannelItem());
  };

  chatFooterChangeHeightFN = (height) => {
    this.setState({
      chatFooterHeight: height
    })
  }

  render() {
    const { channelSID, userId, friendUserId, userName, profileImage, title, category, handlerBlockUser, handlerReportUser } = this.props;
    const { channelBlocked, briefingMatch, chatFooterHeight } = this.state;

    return (
      <_Container>
        <ChatHeader
          channelSid={channelSID}
          userName={userName}
          handleBack={this.handleBack}
          friendUserId={friendUserId}
          userBlocked={this.userBlocked}
          channelBlocked={channelBlocked}
          handlerBlockUser={handlerBlockUser}
          handlerReportUser={handlerReportUser}
        />
        <_KeyboardAvoidingView behavior="position">
          <ChatContainer
            ref={ref => this.chatContainerRef = ref}
            onToggleModalPropose={this.onToggleModalPropose}
            getContractProposeModalRef={ref => this.contractProposeModal = ref}
            getRequestApproveProposeModalRef={ref => this.requestApproveProposeModal = ref}
            getApproveProposeModalRef={ref => this.approveProposeModal = ref}
            getSendNFModalRef={ref => this.sendNfProposeModal = ref}
            getBriefingMatchModalRef={ref => this.briefingMatchModal = ref}
            modalProposeRef={this.modalPropose}
            listProposesRef={this.listProposes ? this.listProposes.props.proposes : []}
            profileImage={profileImage}
            userName={userName}
            friendUserId={friendUserId}
            userId={userId}
            briefingMatch={briefingMatch}
            channelSid={channelSID}
            changeBriefingMatch={this.changeBriefingMatch}
            chatContainerRef={this.chatContainerRef}
            getChatMessagesRef={this.getChatMessagesRef}
            chatMessagesRef={this.chatMessagesRef}
            title={title}
            category={category}
            chatFooterHeight={chatFooterHeight}
          />
          {!briefingMatch && <ChatFooter
            sendNewMessage={this.handleAddNewMessage}
            onToggleModalPropose={this.onToggleModalPropose}
            isTyping={this.isTyping}
            chatFooterChangeHeight={(height) => this.chatFooterChangeHeightFN(height)}
          />}
          <ModalPropose
            getRef={ref => this.modalPropose = ref}
            getListProposesRef={ref => this.listProposes = ref}
            contractProposeModalRef={this.contractProposeModal}
            requestApproveProposeModalRef={this.requestApproveProposeModal}
            approveProposeModalRef={this.approveProposeModal}
            userId={userId}
            channel={channelSID}
          />
        </_KeyboardAvoidingView>
      </_Container>
    )
  }

}

export default withTwilio(Channel)
