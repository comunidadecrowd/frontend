import React from 'react';
import styled from 'styled-components/native';
import { Dimensions, Image, View } from 'react-native';
import Interactable from 'react-native-interactable';

import { TEXT } from 'GLOBALS/TEXT';
import { Colors } from 'theme/Colors';
import { IconBackButton } from 'components/Icon/ChatHeader';
import IconPlus from 'components/Icon/IconPlus';
import IconBacktick from 'components/Icon/IconBacktick';
import { Trash, Restore } from 'components/Icon/ProposeIcons';

import PlayIcon from './img/play.png';
import PauseIcon from './img/pause.png';
import CloseIcon from './img/close.png';

const { width, height } = Dimensions.get('window');

export const _ContainerInteractable = styled(Interactable.View)`
  position: absolute;
  width: ${width};
  height: ${height};
  z-index: 2;
  background: ${Colors.BLACK};
`;

export const _HandlerIconClose = styled.View`
  z-index: 3;
  width: 30%;
  height: 8px;
  border-radius: 50px;
  margin: 23px auto 0;
  background: ${Colors.GRAY_DARK};
`;

export const _ModalHeader = styled.View`
  width: 100%;
  height: 70px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background: ${Colors.BLACK};
`;

export const _LeftIcon = styled.TouchableOpacity`
  width: 69px;
  height: 100%;
`;

export const _IconBack = styled(IconBackButton)``;

export const _WrapperTitle = styled.View``;

export const _HeaderTitle = styled(TEXT)``;

export const _RightIcon = styled.TouchableOpacity`
  width: 69px;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const _ReloadIcon = styled.Image`
  width: 20px;
  height: 20px;
`;

export const _ConnectionsSync = styled.View`
  width: 100%;
  background: ${Colors.BLACK};
`;

export const _Row = styled.View`
  width: 100%;
  padding: 0 29px;
  height: 35px;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

export const _Syncitle = styled(TEXT)``;

export const _ConnectionsSyncButtonsWrapper = styled.View`
  flex-direction: row;
  height: 100%;
`;

export const _ConnectionsSyncButtons = styled.TouchableOpacity`
  width: 25px;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

export const _ConnectionsSyncPlayPause = (props) => (
  <_ConnectionsSyncButtons {...props}>
    <Image source={props.syncStatus === 'onPause' ? PlayIcon : PauseIcon} style={{ width: 13, height: 15 }} />
  </_ConnectionsSyncButtons>
);

export const _ConnectionsSyncStop = (props) => (
  <_ConnectionsSyncButtons {...props}>
    <Image source={CloseIcon} style={{ width: 15, height: 15 }} />
  </_ConnectionsSyncButtons>
);

export const _ConnectionsSyncLoading = (props) => {

  const
    { stepsLeft, stepsTotal, current } = props.syncSteps,
    statusColor = props.syncStatus === 'onPause' ? '255, 200, 11' : '134, 199, 237',
    syncStatusBG = `rgba(${statusColor}, .2)`,
    syncPercentageBG = `rgb(${statusColor})`,
    syncPercentage = stepsLeft === 0 && (current !== stepsTotal) ? '0%' : stepsLeft === 0 && (current === stepsTotal) ? '100%' : `${100 - (parseInt((stepsLeft / stepsTotal) * 100))}%`;

  return (
    <View style={{ position: 'absolute', top: 0, left: 0, zIndex: -1, height: '100%', width: '100%', backgroundColor: syncStatusBG }}>
      <View style={{ position: 'absolute', top: 0, left: 0, height: '100%', width: syncPercentage, backgroundColor: syncPercentageBG }} />
    </View>
  )
};

export const _WrapperContent = styled.View`
  height: ${({reloadSync}) => !reloadSync ? (height - 189) : (height - 164)}px;
  background: ${Colors.BLACK};
`;

export const _WrapperContentScroll = styled.ScrollView``;

export const _IconPlus = styled(IconPlus)`
  align-self: center;
`;

export const _TotalConnections = styled.View`
  height: 78px;
  align-items: center;
  justify-content: center;
`;

export const _TotalConnectionsText = styled(TEXT)`
  font-size: 18px;
  color: #4A4A4A;
`;

