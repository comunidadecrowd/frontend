import React, { Component } from 'react';
import { categoryColor, STORAGE_URL } from 'utils/Helpers';

import Icon, { Icons } from 'components/Icon';
import { AVATAR } from 'GLOBALS/AVATAR';

import {
  _ConnectiontItem,
  _UserInfos,
  _Name,
  _MemberTitle,
  _AvatarDefault,
  _CROWDIcon,
} from './styles';

class ConnectiontItem extends Component {
  
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    
    const {
      userId,
      friendUserId,
      profileImage,
      name,
      title,
      disabled,
      handleCreateChat,
      category,
    } = this.props;

    return (
      <_ConnectiontItem onPress={() => handleCreateChat({ 
        userId, 
        friendUserId,
        profileImage,
        title,
        category
      })} disabled={disabled}>
        {profileImage
          ? <AVATAR 
            uri={{ uri: `${STORAGE_URL}.appspot.com/users/${friendUserId}/profile/${profileImage}` }} 
            size={'big'}
            color={categoryColor(category)} 
          />
          : <_AvatarDefault 
              color={categoryColor(category)} 
              size={'big'}
            >
              <Icon icon={Icons['PERFIL']} style={{fontSize: 62, color: '#666'}} />
            </_AvatarDefault>
        }
        <_UserInfos>
          <_Name bold fontSize={14} content={name} />
          {
            title !== null && 
            <_MemberTitle 
              color={'#666'}
              fontSize={14} 
              content={title} 
            />
          }
        </_UserInfos>
      </_ConnectiontItem>
    );
  }

}

export default ConnectiontItem;
