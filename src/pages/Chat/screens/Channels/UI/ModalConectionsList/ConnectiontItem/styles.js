import React from 'react';
import styled from 'styled-components/native';
import Svg, { Polygon } from 'react-native-svg';
import { TEXT } from 'GLOBALS/TEXT';

export const _ConnectiontItem = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-content: flex-start;
  justify-content: space-between;
  opacity: ${({ disabled }) => disabled ? 0.5 : 1};
  align-items: center;
  padding: 15px 29px 15px 25px;
  width: 100%;
`;

export const _UserInfos = styled.View`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
  padding: 0 0 0 8px;
  justify-content: center;
`;

export const _Name = styled(TEXT)``;

export const _MemberTitle = styled(TEXT)``;

export const _AvatarDefault = styled.View`
  width: ${({size}) => size === 'big' ? 67 : 52};
  height: ${({size}) => size === 'big' ? 67 : 52};
  align-items: center;
  justify-content: center;
  border: 1px solid ${({ color }) => color ? color : '#666'};
  border-radius: ${({size}) => size === 'big' ? 67 : 52};
  overflow: hidden;
`;

// TODO:  Mover esse item para icons;
export const _CROWDIcon = (props) => (
  <Svg {...props}>
    <Polygon points="26,26 13,0 0,26" fill="#666" stroke="none" />
  </Svg>
);