import React, { PureComponent } from 'react';
import { Alert, Platform } from 'react-native';
import withLocalization from 'utils/withLocalization';

import AttentionIcon from './img/attention.png';
import Contacts from "react-native-unified-contacts";
import LoadingModal from 'components/LoadingModal';

import APIS from 'apis';

import {
  _Main,
  _AttentionIcon,
  _AlertMessage,
  _Instruction,
  _Wrapper,
  _Buttons
} from './styles';

const isiOS = Platform.OS === 'ios';

class ImportContacts extends PureComponent {

  constructor(props) {

    super(props);

    this.state = {
      isLoading: false,
      importMessage: ''
    };

  }

  getContacts = () => {

    const { importContactsCompleted, userID } = this.props;

    Contacts.getContacts((error, contacts) => {
      if (error) {
        console.error('** importContacts error', error);
      }
      else {

        this.setState({
          isLoading: true
        })

        let
          getContactsWithCellPhone = contacts.filter(item => item.hasOwnProperty('phoneNumbers')),
          freshContacts = getContactsWithCellPhone.map(contact => {

            const { identifier, fullName, emailAddresses, phoneNumbers } = contact;

            let freshPhoneNumbers = phoneNumbers.map((tel) => {
              const { digits } = tel;
              return { digits };
            });

            return {
              identifier,
              fullName,
              emailAddresses,
              phoneNumbers: freshPhoneNumbers
            };

          });

        console.log('++ freshContacts', freshContacts)

        syncConnectionsSteps = {
          stepsTotal: Math.ceil(freshContacts.length / 30),
          stepsLeft: Math.ceil(freshContacts.length / 30),
          current: 0
        };

        APIS.Contact.listFriendsByUserId(userID, (data) => {
          
          const { listFriendsByUserId } = data;
          console.log('++ listFriendsByUserId', listFriendsByUserId)

          let clearEmojiOnListFriendsByUserId = listFriendsByUserId.map(item => {

            const {identifier, name, profileImage, title, friendUserId, category} = item;

            return {
              identifier, 
              name: name.replace(/[�]+/g, "").trim(), 
              profileImage, 
              title, 
              friendUserId,
              category
            }

          })

          let filtered = freshContacts.filter(contact => !listFriendsByUserId.map(item => item.identifier).includes(contact.identifier))
          console.log('++ filtered', filtered)
          
          APP_STORAGE.setItem('SyncConnectionsSteps', syncConnectionsSteps,
          APP_STORAGE.setItem('ContactsList', filtered,
          APP_STORAGE.setItem('ConnectionsList', clearEmojiOnListFriendsByUserId, this.setState({
              isLoading: false
            }, () => importContactsCompleted()))
          ))

        })

      }
    });

  }

  importContacts = () => {

    const { t } = this.props;

    Contacts.userCanAccessContacts((canAccess) => {

      if (!canAccess) {
        Contacts.requestAccessToContacts((userResponse) => {
          if (userResponse) {
            this.getContacts();
          } else {
            Alert.alert(
              t('chat:importContacts:accessToContactsMessages:alert:title'),
              t('chat:importContacts:accessToContactsMessages:alert:message'),
              [{ text: t('chat:importContacts:accessToContactsMessages:alert:button'), onPress: () => Contacts.openPrivacySettings() }]
            )
          }
        })
      } else {
        this.getContacts();
      }

    });

  }

  render() {

    const { t } = this.props;
    const { isLoading, importMessage } = this.state;

    return (
      <_Main>
        {
          (isLoading || isLoading === 'hasImportMessage') &&
          <LoadingModal
            message={isLoading === 'hasImportMessage' ? importMessage : t('chat:importContacts:onLoadingMessage')}
            opacity={'rgba(0,0,0,.85)'}
            messageColor={"#84C9F2"}
            isGreen={isLoading === 'hasImportMessage'}
          />
        }
        <_AttentionIcon
          source={AttentionIcon}
        />
        <_AlertMessage
          content={t('chat:importContacts:alertMessage')}
          euclid
          fontSize={42}
          lineHeight={isiOS ? 50 : 55}
        />
        <_Instruction
          content={t('chat:importContacts:instruction')}
          euclid
          fontSize={22}
          lineHeight={30}
        />
        <_Wrapper>
          <_Buttons
            onPressFn={this.importContacts}
            content={t('chat:importContacts:buttons:importConnections')}
            color={'#BCDEA1'}
            isAndroid={!isiOS}
          />
          <_Buttons
            disabled
            normalText
            content={t('chat:importContacts:buttons:linkedIn')}
            isAndroid={!isiOS}
          />
        </_Wrapper>
      </_Main>
    )

  }

}

export default withLocalization(ImportContacts);