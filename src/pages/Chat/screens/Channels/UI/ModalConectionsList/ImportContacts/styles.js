import styled from 'styled-components/native';

import { TEXT } from 'GLOBALS/TEXT';
import BUTTON from 'GLOBALS/BUTTON';

export const _Main = styled.View`
  flex-grow: 1;
  padding: 30px 29px;
`;

export const _AttentionIcon = styled.Image`
  width: 54px;
  height: 23px;
  margin: 0 0 30px;
`;

export const _AlertMessage = styled(TEXT)`
  margin: 0 0 30px;
`;

export const _Instruction = styled(TEXT)`
  margin: 0 0 30px;
`;

export const _Buttons = styled(BUTTON)``;