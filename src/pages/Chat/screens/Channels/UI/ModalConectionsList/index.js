import React, { Component, Fragment } from 'react';
import { Dimensions } from 'react-native';
import withLocalization from 'utils/withLocalization';

import Contacts from "react-native-unified-contacts";
import APIS from 'apis';

import ConnectiontItem from './ConnectiontItem';
import ReloadIcon from './img/reload.png';

import ImportContacts from './ImportContacts';
import LoadingModal from 'components/LoadingModal';

import {
  _ContainerInteractable,
  _HandlerIconClose,
  _WrapperContent,
  _WrapperContentScroll,
  _ModalHeader,
  _LeftIcon,
  _WrapperTitle,
  _HeaderTitle,
  _RightIcon,
  _ReloadIcon,
  _ConnectionsSync,
  _Row,
  _Syncitle,
  _ConnectionsSyncButtonsWrapper,
  _ConnectionsSyncPlayPause,
  _ConnectionsSyncStop,
  _ConnectionsSyncLoading,
  _TotalConnections,
  _TotalConnectionsText
} from './styles';

const { width, height } = Dimensions.get('window');

class ModalConectionsList extends Component {

  constructor(props) {

    super(props);

    this.isFromNavigation = this.props.fromNavigation;

    this.reportedList = [];
    this.User = !this.isFromNavigation ? global.CrowdStorage.User : null;
    this.ContactsList = !this.isFromNavigation ? global.CrowdStorage.ContactsList : null;

    this.state = {
      currentSnapIndex: 0,
      connections: (this.isFromNavigation || !global.CrowdStorage.hasOwnProperty('ConnectionsList')) ? [] : global.CrowdStorage.ConnectionsList,
      loading: true,
      syncStatus: 'onPause',
      reloadSync: false,
      syncSteps: (this.isFromNavigation || !global.CrowdStorage.hasOwnProperty('SyncConnectionsSteps')) ? { stepsTotal: 0, stepsLeft: 0 } : global.CrowdStorage.SyncConnectionsSteps,
      alreadyImportedContacts: this.isFromNavigation ? false : global.CrowdStorage.User.hasOwnProperty('alreadyImportedContacts') ? true : false,
      reSyncModal: false
    };

  }

  async componentDidMount() {
		const { connections } = this.state;

    this.reportedList = await APP_STORAGE.getItem('ReportedList');
	
    let connectionsFiltered = this.reportedList && this.reportedList.length ? connections.filter(user => !this.reportedList.find(reported => parseInt(reported) === parseInt(user.friendUserId))) : connections;
    this.setState({ connections: connectionsFiltered });

    if (this.isFromNavigation) {
      this.User = await APP_STORAGE.getItem('User');
    } else {

      const { syncSteps: { stepsLeft } } = this.state;
      if (stepsLeft !== 0) return;
      this.setState({ reloadSync: true })
    }

		global.emitters.on('reportedList', async (reportedList) => {
			this.reportedList = reportedList;	
      let connectionsOnStorage = await APP_STORAGE.getItem('ConnectionsList');
			if (this.reportedList) {
				let connectionsFiltered = connectionsOnStorage.filter(user => !this.reportedList.find(reported => parseInt(reported) === parseInt(user.friendUserId)));
				this.setState({ connections: connectionsFiltered });
			}
		});

  }

  /* // REFACT:  19/06 - Novo fluxo sem Import de Contacts obrigatório
    async componentDidMount() {

      const {fromNavigation} = this.props;

      let
        getUserOnStorage = await APP_STORAGE.getItem('User'),
        getContactsListOnStorage = await APP_STORAGE.getItem('ContactsList'),
        getSyncConnectionsStepsOnStorage = await APP_STORAGE.getItem('SyncConnectionsSteps'),
        getConnectionsListOnStorage = await APP_STORAGE.getItem('ConnectionsList');
      
      this.User = getUserOnStorage;
      this.ContactsList = getContactsListOnStorage;
      
      this.setState({
        syncSteps: getSyncConnectionsStepsOnStorage,
        connections: getConnectionsListOnStorage,
        loading: false
      }, () => {
        const { stepsLeft } = getSyncConnectionsStepsOnStorage;
        if( stepsLeft === 0 ) {
          this.setState({
            reloadSync: true
          })
        } else {
          if(fromNavigation) setTimeout(() => this.syncConnections('init'), 500)
        }
      })

    }
  */

  handleCreateChat = (obj) => {

    const { goToChannel } = this.props;
    const { userId, friendUserId, profileImage, title, category } = obj;

    this.setState({ loading: true });

    APIS.Channels.createChat({
      userId: parseInt(userId),
      friendUserId: parseInt(friendUserId)
    }, ({ createChat }) => {
      this.setState({ loading: false });
      goToChannel(
        createChat.channelSid,
        createChat.members.find(member => parseInt(member.userId) === parseInt(friendUserId)).name,
        friendUserId,
        createChat.attributes.type === 'match',
        profileImage,
        title,
        category
      )
      if (this.modalContactList)
        this.modalContactList.snapTo({ index: 0 });
    });

  }

  onSnap = ({ nativeEvent: { index } }) => {
    this.setState({ currentSnapIndex: index });
  };

  onDrag = ({ nativeEvent: { state, y } }) => {
    if (state === 'end' && y > 50) this.modalContactList.snapTo({ index: 0 });
  }

  reloadSyncConnections = () => {

    const { userID } = this.props;

    Contacts.getContacts((error, contacts) => {
      if (error) {
        console.error('$$ importContacts error', error);
      }
      else {

        this.setState({ reSyncModal: true })

        let
          getContactsWithCellPhone = contacts.filter(item => item.hasOwnProperty('phoneNumbers')),
          freshContacts = getContactsWithCellPhone.map(contact => {

            const { identifier, fullName, emailAddresses, phoneNumbers } = contact;

            let freshPhoneNumbers = phoneNumbers.map((tel) => {
              const { digits } = tel;
              return { digits };
            });

            return {
              identifier,
              fullName,
              emailAddresses,
              phoneNumbers: freshPhoneNumbers
            };

          }),
          syncConnectionsSteps = {
            stepsTotal: Math.ceil(freshContacts.length / 30),
            stepsLeft: Math.ceil(freshContacts.length / 30),
            current: 0
          };

        APIS.Contact.listFriendsByUserId(userID, (data) => {

          const { listFriendsByUserId } = data;

          let
            clearEmojiOnListFriendsByUserId = listFriendsByUserId.map(item => {

              const { identifier, name, profileImage, title, friendUserId, category } = item;

              return {
                identifier,
                name: name.replace(/[�]+/g, "").trim(),
                profileImage,
                title,
                friendUserId,
                category
              }

            }),
            filtered = freshContacts.filter(contact => !listFriendsByUserId.map(item => item.identifier).includes(contact.identifier));            
          
          this.importContactsCompletedFN({
            syncConnectionsSteps,
            contactsList: filtered,
            connectionsList: clearEmojiOnListFriendsByUserId
          }, true)

        })

      }
    });

  }

  callAPIConnections = (withPause = false) => {

    const
      { userID } = this.props,
      apiLengthFactor = 30;

    let
      { connections, syncStatus, syncSteps, syncSteps: { current, stepsLeft } } = this.state,
      contactsGroup = current === 0 ? this.ContactsList.slice(0, apiLengthFactor) : this.ContactsList.slice((current * apiLengthFactor), ((current + 1) * apiLengthFactor));

    console.log('$$ contactsGroup', contactsGroup)

    if (withPause && stepsLeft === 0) {
      setTimeout(() => {
        this.setState({
          reloadSync: true
        })
      }, 1000);
    }

    if (syncStatus === 'onPause') return;

    syncSteps = {
      ...syncSteps,
      stepsLeft: stepsLeft - 1,
      current: current + 1,
    }

    APIS.Contact.sendUserContacts({ userID, contactsGroup }, (data) => {

      const { importContacts } = data;

      if (importContacts.length === 0) {

        APP_STORAGE.updateItem('SyncConnectionsSteps', syncSteps, () => {
          this.setState({ syncSteps }, () => {

            if (stepsLeft === 1) {
              this.setState({
                syncStatus: 'onPause'
              }, () => this.callAPIConnections(true))
            } else {
              this.callAPIConnections()
            }

          })
        });

      } else {

        importContacts.map(item => {

          const { identifier, name, profileImage, title, friendUserId, category } = item;

          if (friendUserId === userID) return;

          let clearEmoji = {
            identifier,
            name: name.replace(/[�]+/g, "").trim(),
            profileImage,
            title,
            friendUserId,
            category
          }

          return connections.push(clearEmoji)

        })

        APP_STORAGE.updateItem('SyncConnectionsSteps', syncSteps, () => {
          APP_STORAGE.updateItem('ConnectionsList', connections, () => {
            let connectionsFiltered = this.reportedList && this.reportedList.length ? connections.filter(user => !this.reportedList.find(reported => parseInt(reported) === parseInt(user.friendUserId))) : connections;
            this.setState({ syncSteps, connections: connectionsFiltered }, () => {

              if (stepsLeft === 1) {
                this.setState({
                  syncStatus: 'onPause'
                }, () => this.callAPIConnections(true))
              } else {
                this.callAPIConnections()
              }

            });
          });
        });

      }

    })

  }

  syncConnections = (context = false) => {

    let { syncStatus } = this.state;

    if (context === 'init') {

      this.setState({
        syncStatus: 'onPlay'
      }, () => this.callAPIConnections())

    } else {

      this.setState({
        syncStatus: syncStatus === 'onPlay' ? 'onPause' : 'onPlay'
      }, () => this.callAPIConnections())

    }

  }

  stopSyncConnections = () => {
    console.log('** closeSyncConnections', true)
  }

  importContactsCompletedFN = async (initData, isOnReSyncModal = false) => {

    const { syncConnectionsSteps, contactsList, connectionsList } = initData;

    this.ContactsList = contactsList;

    APP_STORAGE.setItem('SyncConnectionsSteps', syncConnectionsSteps,
      APP_STORAGE.setItem('ContactsList', contactsList,
        APP_STORAGE.setItem('ConnectionsList', connectionsList, () => {
          if (isOnReSyncModal) {

            this.setState({
              syncSteps: syncConnectionsSteps,
              connections: connectionsList,
              reSyncModal: false,
              reloadSync: false
            }, () => setTimeout(() => this.syncConnections('init'), 500))

          } else {
            APP_STORAGE.updateItem('User', {
              ...this.User,
              alreadyImportedContacts: true
            }, (() => {
              setTimeout(() => {
                this.setState({
                  syncSteps: syncConnectionsSteps,
                  connections: connectionsList,
                  alreadyImportedContacts: true
                }, () => setTimeout(() => this.syncConnections('init'), 500))
              }, 300)
            })())
          }
        })
      )
    )

  }

  removeDuplicates = (myArr, prop) => {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  render() {

    const
      { t, getRef, userID } = this.props,
      { currentSnapIndex, connections, loading, syncStatus, reloadSync, syncSteps, alreadyImportedContacts, reSyncModal } = this.state;

    return (

      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        ref={ref => {
          this.modalContactList = ref;
          getRef(ref);
        }}
        snapPoints={[{ y: height }, { y: 0 }]}
        onSnap={this.onSnap}
        currentSnapIndex={currentSnapIndex}
        boundaries={{ top: 0 }}
        onDrag={this.onDrag}
      >
        {
          reSyncModal &&
          <LoadingModal
            message={t('chat:importContacts:onReSyncMessage')}
            opacity={'rgba(0,0,0,.85)'}
            messageColor={"#84C9F2"}
          />
        }
        <_HandlerIconClose />
        {
          !alreadyImportedContacts ? (
            <ImportContacts
              userID={userID}
              importContactsCompleted={this.importContactsCompletedFN.bind(this)}
            />
          ) : (
              <Fragment>
                <_ModalHeader>
                  <_LeftIcon onPress={() => false} />
                  <_WrapperTitle>
                    <_HeaderTitle euclid content={t('chat:connectionsList:title')} />
                  </_WrapperTitle>
                  <_RightIcon onPress={reloadSync ? this.reloadSyncConnections : () => false} activeOpacity={reloadSync ? 0.5 : 1}>
                    {
                      reloadSync &&
                      <_ReloadIcon source={ReloadIcon} />
                    }
                  </_RightIcon>
                </_ModalHeader>
                {
                  !reloadSync &&
                  <_ConnectionsSync>
                    <_Row>
                      <_Syncitle
                        withLoading
                        loadingHeight={14}
                        loadingAlign={'left'}
                        loadingStyles={{ paddingRight: 5 }}
                        loadingStatus={syncStatus}
                        euclid
                        content={syncStatus === 'onPlay' ? t('chat:connectionsList:onSyncPlay') : t('chat:connectionsList:onSyncPause')}
                        fontSize={14}
                      />
                      <_ConnectionsSyncButtonsWrapper>
                        <_ConnectionsSyncPlayPause
                          syncStatus={syncStatus}
                          onPress={this.syncConnections}
                        />
                        <_ConnectionsSyncStop
                          onPress={this.stopSyncConnections}
                        />
                      </_ConnectionsSyncButtonsWrapper>
                    </_Row>
                    <_ConnectionsSyncLoading syncStatus={syncStatus} syncSteps={syncSteps} />
                  </_ConnectionsSync>
                }
                <_WrapperContent reloadSync={reloadSync} fromNavigation={this.isFromNavigation}>
                  <_WrapperContentScroll>
                    {
                      this.removeDuplicates(connections, 'friendUserId').sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : ((b.name.toLowerCase() > a.name.toLowerCase()) ? -1 : 0)).map((connection, index) => {

                        const { friendUserId, name, profileImage, title, category } = connection;

                        return (
                          <ConnectiontItem
                            key={index}
                            userId={userID}
                            friendUserId={friendUserId}
                            profileImage={profileImage}
                            name={name}
                            title={title}
                            category={category}
                            // disabled={loading} // TODO:  Alex -> Verificar se essa prop ainda será necessária
                            handleCreateChat={this.handleCreateChat}
                          />
                        )

                      })
                    }
                    <_TotalConnections>
                      <_TotalConnectionsText
                        content={connections.length === 1 ? `1 ${t('chat:connectionsList:totalOf:one')}` : `${connections.length} ${t('chat:connectionsList:totalOf:moreThanOne')}`}
                      />
                    </_TotalConnections>
                  </_WrapperContentScroll>
                </_WrapperContent>
              </Fragment>
            )
        }
      </_ContainerInteractable>

    )

  }

  /* // REFACT:  19/06 - Novo fluxo sem Import de Contacts obrigatório
    render() {

      const { t, getRef, fromNavigation } = this.props;
      const { currentSnapIndex, connections, loading, syncStatus, reloadSync, syncSteps } = this.state;

      return (
        <_ContainerInteractable
          verticalOnly
          initialPosition={{ x: 0, y: fromNavigation ? 0 : height }}
          ref={ref => {
            this.modalContactList = ref;
            getRef(ref);
          }}
          snapPoints={[{ y: height }, { y: 0 }]}
          onSnap={this.onSnap}
          currentSnapIndex={currentSnapIndex}
          boundaries={{ top: 0 }}
          onDrag={this.onDrag}
        >
          <_HandlerIconClose />

          <_ModalHeader>
            <_LeftIcon onPress={() => false} />
            <_WrapperTitle>
              <_HeaderTitle withLoading={loading} euclid content={t('chat:connectionsList:title')} />
            </_WrapperTitle>
            <_RightIcon onPress={reloadSync ? this.reloadSyncConnections : () => false} activeOpacity={reloadSync ? 0.5 : 1}>
              {/* { // TODO:  ALEX -> Habilitar quando fizer a função de Resync
                reloadSync &&
                <_ReloadIcon source={ReloadIcon} />
              } /*}
            </_RightIcon>
          </_ModalHeader>

          {
            !reloadSync &&
            <_ConnectionsSync>
              <_Row>
                <_Syncitle
                  withLoading
                  loadingHeight={14}
                  loadingAlign={'left'}
                  loadingStyles={{ paddingRight: 5 }}
                  loadingStatus={syncStatus}
                  euclid
                  content={syncStatus === 'onPlay' ? t('chat:connectionsList:onSyncPlay') : t('chat:connectionsList:onSyncPause')}
                  fontSize={14}
                />
                <_ConnectionsSyncButtonsWrapper>
                  <_ConnectionsSyncPlayPause
                    syncStatus={syncStatus}
                    onPress={this.syncConnections}
                  />
                  <_ConnectionsSyncStop
                    onPress={this.stopSyncConnections}
                  />
                </_ConnectionsSyncButtonsWrapper>
              </_Row>
              <_ConnectionsSyncLoading syncStatus={syncStatus} syncSteps={syncSteps} />
            </_ConnectionsSync>
          }

          <_WrapperContent reloadSync={reloadSync} fromNavigation={fromNavigation}>
            <_WrapperContentScroll>
              {
                connections.sort((a, b) => (a.name.toLowerCase() > b.name.toLowerCase()) ? 1 : ((b.name.toLowerCase() > a.name.toLowerCase()) ? -1 : 0)).map((connection, index) => {

                  const { friendUserId, name, profileImage, title, category } = connection;

                  return (
                    <ConnectiontItem
                      key={index}
                      userId={this.User.userID}
                      friendUserId={friendUserId}
                      profileImage={profileImage}
                      name={name}
                      title={title}
                      category={category}
                      disabled={loading}
                      handleCreateChat={this.handleCreateChat}
                    />
                  )

                })
              }
              <_TotalConnections>
                <_TotalConnectionsText
                  content={connections.length === 1 ? `1 ${t('chat:connectionsList:totalOf:one')}` : `${connections.length} ${t('chat:connectionsList:totalOf:moreThanOne')}`}
                />
              </_TotalConnections>
            </_WrapperContentScroll>
          </_WrapperContent>

        </_ContainerInteractable>
      )
    }
  */

}

export default withLocalization(ModalConectionsList)
