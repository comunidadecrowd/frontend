import React from 'react';
import styled from 'styled-components/native';
import Svg, { Polygon } from 'react-native-svg';
import { TEXT } from 'GLOBALS/TEXT';

export const _ChannelItem = styled.TouchableOpacity`
  display: ${({ channelType, channelFromFriend, onlyChannelsEmpty, empty }) => (!onlyChannelsEmpty && !empty) && (channelType !== 'match' || channelFromFriend) ? 'flex' : 'none'};
  flex-direction: row;
  align-content: flex-start;
  justify-content: space-between;
  opacity: ${({ disabled }) => disabled ? 0.5 : 1};
  align-items: center;
  padding: 15px 29px 15px 25px;
`;

export const _ChannelUserInfos = styled.View`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
  padding: 0 0 0 12px;
`;

export const _ChannelName = styled(TEXT)``;

export const _ChannelMemberTitle = styled(TEXT)`
`;

export const _ChannelLastMessage = styled(TEXT)`
  text-align: left;
`;

export const _ChannelInfos = styled.View`
  display: flex;
  justify-content: flex-start;
  align-content: center;
`;

export const _ChannelDateLastMessage = styled(TEXT)`
  display: flex;
  margin-bottom: 8px;
  text-transform: uppercase;
`;

export const _ChannelCountMessagesUnread = styled.View`
  display: flex;
  width: 25px;
  height: 25px;
  border-radius: 30px;
  background-color: #fff;
  color: #000;
  align-items: center;
  justify-content: center;
  align-self: center;
`;

export const _AvatarDefault = styled.View`
  width: ${({size}) => size === 'big' ? 67 : 52};
  height: ${({size}) => size === 'big' ? 67 : 52};
  align-items: center;
  justify-content: center;
  border: 1px solid ${({ color }) => color ? color : '#ED4B68'};
  border-radius: ${({size}) => size === 'big' ? 67 : 52};
  overflow: hidden;
`;


// TODO:  Mover esse item para icons;
export const _CROWDIcon = (props) => (
  <Svg {...props}>
    <Polygon points="26,26 13,0 0,26" fill="#666" stroke="none" />
  </Svg>
);
