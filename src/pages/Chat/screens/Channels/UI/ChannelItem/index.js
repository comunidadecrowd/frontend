import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import withLocalization from 'utils/withLocalization';
import { withTwilio } from 'services/Twilio';
import APP_STORAGE from 'APP_STORAGE';
import Routes from 'Routes';

import Icon, { Icons } from 'components/Icon';
import { Text } from 'react-native';
import { categoryColor, STORAGE_URL } from 'utils/Helpers';
import { AVATAR } from 'GLOBALS/AVATAR';

import {
  _ChannelItem,
  _ChannelUserInfos,
  _ChannelName,
  _ChannelMemberTitle,
  _ChannelLastMessage,
  _ChannelInfos,
  _ChannelDateLastMessage,
  _ChannelCountMessagesUnread,
  _AvatarDefault,
  _CROWDIcon,
} from './styles';

class ChannelItem extends Component {

  constructor(props) {

    super(props);

    this.Channels = [];
    this.ChannelStorage;

    this.state = {
      unreadMessagesCount: 0,
      isTyping: false,
      disabled: false,
      isChatOnStack: false,
      //isChatOnStack: this.props.isCreatedFromTwilio === true ? false : true,
    };

    this.navigationEventListener = Navigation.events().bindComponent(this);

    // console.log('$Twilio channelItem | constructor/this.ChannelItem', this);

  }

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    constructor(props) {
      super(props);

      this.Chat = APP_SWORKER.Chat;
      this.ChatChannel = null;
      this.Channels = [];
      this.ChannelStorage = null;

      this.state = {
        unreadMessagesCount: this.props.unreadMessagesCount || null,
        isTyping: false,
        disabled: false,
        isChatOnStack: this.props.isCreatedFromTwilio === true ? false : true,
      };
    }
  */

  async componentDidMount() {

    const {
      userId,
      channelSid,
      updateChannelToState,
      Twilio
    } = this.props;
    let { channelsMessages } = this.props;
    
    //this.ChannelStorage = await APP_STORAGE.getItem(`CHAT_${channelSid}`);
    this.ChannelStorage = channelsMessages.find(channel => channel.channelSid === channelSid) ? channelsMessages.find(channel => channel.channelSid === channelSid).messages : [];

    Twilio.getChannel(channelSid, true);
    Twilio.emitter.on(`${channelSid}_messageAdded`, (obj) => this.updateLastMessage(obj));
    Twilio.emitter.on(`${channelSid}_channelTypingStarted`, () => this.getTypingStatus('start'));
    Twilio.emitter.on(`${channelSid}_channelTypingEnded`, () => this.getTypingStatus('end'));
    Twilio.emitter.on(`${channelSid}_memberUpdated`, (obj) => this.memberUpdated(obj));

    let
      members = await Twilio.getChannelMembers(channelSid, (members) => members),
      lastConsumedMessageIndexFromFriend,
      lastConsumedMessageIndexFromUser;

    if (members.length) {
      lastConsumedMessageIndexFromFriend = members.find(({ state: { identity } }) => parseInt(identity) !== parseInt(userId)).lastConsumedMessageIndex;
      lastConsumedMessageIndexFromUser = members.find(({ state: { identity } }) => parseInt(identity) === parseInt(userId)).lastConsumedMessageIndex;
    
      updateChannelToState(channelSid, await Twilio.getChannelMessages(channelSid, (messages) => messages), lastConsumedMessageIndexFromFriend, lastConsumedMessageIndexFromUser);
    }
  
  }

  /* // REFACT:  15/06 - Refactor TWILIO desvinculado de UI
    async componentDidMount() {
      const {
        channelSid,
        channelType,
        updateChannelToState,
        changeChannelType,
        checkChannelEmpty,
        userId,
        friendUserId
      } = this.props;
      
      this.ChatChannel = await APP_SWORKER.Chat.getChannelBySid(channelSid);
      this.ChannelStorage = await APP_STORAGE.getItem(`CHAT_${channelSid}`);

    this.setState({ unreadMessagesCount: this.ChannelStorage.filter(msg => msg.state.status === 'sent' && parseInt(msg.state.author) !== parseInt(userId)).length });

    this.ChatChannel.on('messageAdded', (obj) => this.updateLastMessage(obj));
    this.ChatChannel.on('typingStarted', () => this.getTypingStatus('start'));
    this.ChatChannel.on('typingEnded', () => this.getTypingStatus('end'));
    this.ChatChannel.on('memberUpdated', async ({ member: { identity, lastConsumedMessageIndex } }) => {
      const
        { channelSid } = this.props,
        { isChatOnStack } = this.state;
      let channelOnStorage = await APP_STORAGE.getItem(`CHAT_${channelSid}`);

      if (!isChatOnStack) {
        // se o channel não estiver aberto e o amigo abrir a conversa, atualiza status
        // das minhas mensagens no meu storage;
				if (parseInt(identity) !== parseInt(userId)) {
          console.log('# channelItem | memberUpdated/friend lastConsumedMessageIndex', lastConsumedMessageIndex);
          if (channelOnStorage.length) {
            let messagesUnread = channelOnStorage
              .filter(({ state: { author, status, index } }) => parseInt(author) === parseInt(userId) && status === 'sent' && index <= lastConsumedMessageIndex)
              .map(msg => msg.state.status = 'read');

            APP_STORAGE.updateItem(`CHAT_${channelSid}`, channelOnStorage);
          }
        }
      } else {
        // se eu abrir a conversa, atualiza status das mensagens do meu amigo
        // no meu storage;
        if (parseInt(identity) === parseInt(userId)) {
          console.log('# channelItem | memberUpdated/me lastConsumedMessageIndex', lastConsumedMessageIndex);
          if (channelOnStorage.length) {
            let messagesUnread = channelOnStorage
              .filter(({ state: { author, status, index } }) => parseInt(author) !== parseInt(userId) && status === 'sent' && index <= lastConsumedMessageIndex)
              .map(msg => msg.state.status = 'read');

            APP_STORAGE.updateItem(`CHAT_${channelSid}`, channelOnStorage);
          }
        }
      }

      this.setState({ unreadMessagesCount: channelOnStorage.filter(msg => msg.state.status === 'sent' && parseInt(msg.state.author) !== parseInt(userId)).length });
    });

    this.navigationEventListener = Navigation.events().bindComponent(this);

    let
      messages = await this.ChatChannel.getMessages(100), // FIX:   Alex && Bruno alterar para um valor dinâmico
      members = await this.ChatChannel.getMembers(),
      lastConsumedMessageIndexFromFriend,
      lastConsumedMessageIndexFromUser;
    console.log('# channelItem | didMount/messages', messages);

    if (members) {
      lastConsumedMessageIndexFromFriend = members.find(({ state: { identity } }) => parseInt(identity) !== parseInt(userId)).lastConsumedMessageIndex;
      lastConsumedMessageIndexFromUser = members.find(({ state: { identity } }) => parseInt(identity) === parseInt(userId)).lastConsumedMessageIndex;

      updateChannelToState(channelSid, messages, lastConsumedMessageIndexFromFriend, lastConsumedMessageIndexFromUser);
    }
  };*/

  async componentDidUpdate(prevProps) {
    if (prevProps.unreadMessagesCount !== this.state.unreadMessagesCount) {
      console.log('# channelItem | componentDidUpdate/prevProps', prevProps.unreadMessagesCount, this.state.unreadMessagesCount);
      this.setState({ unreadMessagesCount: prevProps.unreadMessagesCount });
    }
  };

  memberUpdated = async ({ member: { identity, lastConsumedMessageIndex } }) => {
    const
      { channelSid, userId, channelsMessages, updateChannelToState, Twilio } = this.props,
      { isChatOnStack } = this.state;

    let
      members = await Twilio.getChannelMembers(channelSid, (members) => members),
      lastConsumedMessageIndexFromFriend,
      lastConsumedMessageIndexFromUser;

    if (members.length) {
      lastConsumedMessageIndexFromFriend = members.find(({ state: { identity } }) => parseInt(identity) !== parseInt(userId)).lastConsumedMessageIndex;
      lastConsumedMessageIndexFromUser = members.find(({ state: { identity } }) => parseInt(identity) === parseInt(userId)).lastConsumedMessageIndex;
    
      updateChannelToState(channelSid, await Twilio.getChannelMessages(channelSid, (messages) => messages), lastConsumedMessageIndexFromFriend, lastConsumedMessageIndexFromUser);
    }
  
    if (!isChatOnStack) {
      // se o channel não estiver aberto e o amigo abrir a conversa, atualiza status
      // das minhas mensagens no meu storage;
      if (parseInt(identity) !== parseInt(userId)) {
        if (channelsMessages.find(channel => channel.channelSid === channelSid).messages.length) {
          let messagesUnread = channelsMessages.find(channel => channel.channelSid === channelSid).messages
            .filter(({ state: { author, status, index } }) => parseInt(author) === parseInt(userId) && status === 'sent' && index <= lastConsumedMessageIndex)
            .map(msg => msg.state.status = 'read');

          APP_STORAGE.updateItem(`CHAT_${channelSid}`, channelsMessages.find(channel => channel.channelSid === channelSid).messages);
        }
      }
    } else {
      // se eu abrir a conversa, atualiza status das mensagens do meu amigo
      // no meu storage;
      if (parseInt(identity) === parseInt(userId)) {
        if (channelsMessages.find(channel => channel.channelSid === channelSid).messages.length) {
          let messagesUnread = channelsMessages.find(channel => channel.channelSid === channelSid).messages
            .filter(({ state: { author, status, index } }) => parseInt(author) !== parseInt(userId) && status === 'sent' && index <= lastConsumedMessageIndex)
            .map(msg => msg.state.status = 'read');

          APP_STORAGE.updateItem(`CHAT_${channelSid}`, channelsMessages.find(channel => channel.channelSid === channelSid).messages);
        }

        updateChannelToState(channelSid, { items: channelsMessages.find(channel => channel.channelSid === channelSid).messages }, null, lastConsumedMessageIndex);
      }
    }
  };

  formatLastMessage = (message) => {
    if (message.length > 20) return message.replace(/\r?\n|\r/g, " ").slice(0, 20) + '...';
    return message.replace(/\r?\n|\r/g, " ");
  };

  getDateLastMessage = () => {
    const
      { dateUpdated } = this.props,
      today = new Date(),
      monthNames = ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"];
    let lastMessageDateUpdated = new Date(dateUpdated);

    if (lastMessageDateUpdated.getMonth() < today.getMonth()) {
      return `${monthNames[lastMessageDateUpdated.getMonth()].toUpperCase()}`;
    } else {
      if (lastMessageDateUpdated.getDate() === today.getDate()) {
        return `${lastMessageDateUpdated.getHours().toString().length === 1 ? '0'+lastMessageDateUpdated.getHours() : lastMessageDateUpdated.getHours()}:${lastMessageDateUpdated.getMinutes().toString().length === 1 ? '0'+lastMessageDateUpdated.getMinutes() : lastMessageDateUpdated.getMinutes()}`;
      } else if (lastMessageDateUpdated.getDate() === today.getDate() - 1) {
        return `ONTEM`;
      }
    }
  };

  getTypingStatus = (status) => {
    return this.setState({ isTyping: status === 'start' });
  };

  onPressGoToChannel = () => {
    const {
      goToChannel,
      channelSid,
      channelType,
      members,
      friendUserId,
      userId,
      profileImage,
      title,
      category,
      channelsOrderUpdate,
      userBlocked
    } = this.props;

    this.setState({ disabled: true, isChatOnStack: true });
    goToChannel(
      channelSid,
      members.length > 1 ? members.find(member => parseInt(member.userId) !== parseInt(userId)).name : members[0].name,
      friendUserId,
      channelType === 'match',
      profileImage,
      title,
      category,
      this.changeChatOnStack,
      this.changeBriefingMatchChannelItem,
      channelsOrderUpdate,
      userBlocked
    );
    setTimeout(() => {
      this.setState({ disabled: false, unreadMessagesCount: null });
    }, 1000);
  };

  updateLastMessage = (obj) => {
    const
      { channelSid, userId, channelsMessages, saveMessageStorage, channelType, changeChannelType, checkChannelEmpty } = this.props,
      { state, state: { author, attributes } } = obj,
      messageToPush = { state };

    this.ChannelStorage = channelsMessages.find(channel => channel.channelSid === channelSid).messages;

    if (!attributes.type) attributes.type = 'default';

    if (!messageToPush.state.status) messageToPush.state.status = 'sent';

    if (messageToPush.state.attributes.type === 'matchApproved') messageToPush.state.status = 'read';

    if (channelType === 'match' && attributes.type === 'default') changeChannelType(channelSid);

    if (parseInt(author) !== parseInt(userId)) {
      let messagesReadFromUser = this.ChannelStorage && this.ChannelStorage.length ? this.ChannelStorage.filter(msg => parseInt(msg.state.author) === parseInt(userId) && msg.state.status === 'read') : []; 
      saveMessageStorage(messageToPush, channelSid, state.index, messagesReadFromUser.length ? messagesReadFromUser.slice(-1)[0].state.index : null);
    } else {
      let messagesReadFromFriend = this.ChannelStorage && this.ChannelStorage.length ? this.ChannelStorage.filter(msg => parseInt(msg.state.author) !== parseInt(userId) && msg.state.status === 'read') : []; 
      saveMessageStorage(messageToPush, channelSid, messagesReadFromFriend.length ? messagesReadFromFriend.slice(-1)[0].state.index : null, state.index);
    }
  };

  changeChatOnStack = () => {
    const { channelSid, checkChannelEmpty } = this.props;

    this.setState({
      isChatOnStack: !this.state.isChatOnStack,
    });
  };

  changeBriefingMatchChannelItem = (rejectMatch = null) => {
    const { channelSid, deleteChannel, changeChannelType } = this.props;

    if (rejectMatch) return deleteChannel(channelSid, () => Navigation.pop(Routes.CHAT.CHANNEL));

    return changeChannelType(channelSid);
  };

  componentDidAppear() {
    const { channelSid, isCreatedFromTwilio, checkChannelEmpty } = this.props;

    this.setState({
      isChatOnStack: !this.state.isChatOnStack,
    });
  };

  componentWillUnmount() {
    if (this.navigationEventListener) {
      this.navigationEventListener.remove();
    }
    this.props.Twilio.emitter.removeAllListeners()
  }

  render() {
    const {
      t,
      onlyChannelsEmpty,
      empty,
      lastMessage,
      channelType,
      channelBlocked,
      createdBy,
      userId,
      friendUserId,
      profileImage,
      name,
      title,
      category,
      isOdd
    } = this.props;

    const { unreadMessagesCount, isTyping, disabled } = this.state;

    return (
      <_ChannelItem isOdd={isOdd} empty={empty} onlyChannelsEmpty={onlyChannelsEmpty} channelType={channelType} channelFromFriend={parseInt(createdBy) !== parseInt(userId)} onPress={this.onPressGoToChannel} disabled={disabled}>
        {
          profileImage
            ? (
              <AVATAR
                uri={{ uri: `${STORAGE_URL}.appspot.com/users/${friendUserId}/profile/${profileImage}` }}
                color={categoryColor(category)}
                size={'big'}
              />
            ) : (
              <_AvatarDefault color={categoryColor(category)} size={'big'}>
                <Icon icon={Icons['PERFIL']} style={{fontSize: 62, color: '#666'}} />
              </_AvatarDefault>
            )
        }
        <_ChannelUserInfos>
          <_ChannelName color={categoryColor(category)} bold fontSize={14} content={name} />
          {
            title &&
            <_ChannelMemberTitle color={'#666'} fontSize={14} content={title} />
          }
          {channelBlocked || !isTyping ? <_ChannelLastMessage color={'#FFF'} fontSize={14} content={this.formatLastMessage(lastMessage)} />
            : <_ChannelLastMessage color={'#FFF'} fontSize={14} content={t('chat:isTyping')} />}
        </_ChannelUserInfos>
        <_ChannelInfos>
          <_ChannelDateLastMessage fontSize={12} content={this.getDateLastMessage()} />
          {(unreadMessagesCount !== null && unreadMessagesCount > 0) &&
            <_ChannelCountMessagesUnread>
              <Text>{unreadMessagesCount}</Text>
            </_ChannelCountMessagesUnread>
          }
        </_ChannelInfos>
      </_ChannelItem>
    );
  }

}

export default withLocalization(withTwilio(ChannelItem));
