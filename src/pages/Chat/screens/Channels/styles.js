import styled from 'styled-components/native';
import { TEXT } from 'GLOBALS/TEXT';

export const _Container = styled.View`
  height: 100%;
  align-items: center;
`;

export const _Header = styled.View`
  flex-direction: row;
  align-items: center;
  width: 100%;
  margin: 20px 0 0;
  padding: 0 29px;
  height: 70px;
`;

export const _ButtonLeft = styled.View`
  width: 40px;
  height: 40px;
`;

export const _WrapperTitle = styled.View`
  flex-grow: 2;
`;

export const _TitleHeader = styled(TEXT)``;

export const _TitleEmptyList = styled(TEXT)`
  margin: 10px 0 0;
  padding: 0 29px;
`;

export const _ButtonRight = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  align-items: center;
  justify-content: center;
`;

export const _ScrollContainer = styled.ScrollView`
  width: 100%;
`;

export const _ButtonRightIcon = styled.Image`
  width: 20px;
  height: 20px;
`;