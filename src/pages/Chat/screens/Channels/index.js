import React, { Component } from 'react';
import { Platform } from 'react-native';
import Routes from 'Routes';
import APP_STORAGE from 'APP_STORAGE';
import { withTwilio } from 'services/Twilio';
import withLocalization from 'utils/withLocalization';
import { navigateFromTo } from 'utils/navigateFromTo';
import APIS from 'apis';

import NewChatMessage from 'assets/icons/newChatMessage.png';

import ChannelItem from './UI/ChannelItem';
import ModalConectionsList from './UI/ModalConectionsList';

import {
  _Container,
  _Header,
  _ButtonLeft,
  _WrapperTitle,
  _TitleHeader,
  _TitleEmptyList,
  _ButtonRight,
  _ScrollContainer,
  _ButtonRightIcon,
} from './styles';

class Channels extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      channels: [],
      onlyChannelsEmpty: true,
      goToChannelRequesting: false,
      reOrdering: false
    };

    this.blockedList = [];
    this.reportedList = [];
    this.channelsMessages = [];
  }

  async componentDidMount() {
    const { userId, Twilio } = this.props;
    this.blockedList = await APP_STORAGE.getItem('BlockedList');
    this.reportedList = await APP_STORAGE.getItem('ReportedList');

    APIS.User.listUsersBlocked(userId, ({ listBlocked }) => {
      if (!this.blockedList) {
        this.blockedList = listBlocked || [];
        global.emitters.emit('BlockedList', this.blockedList); 
        APP_STORAGE.setItem('BlockedList', this.blockedList);
      } else {
        this.blockedList = listBlocked || [];
        global.emitters.emit('BlockedList', this.blockedList); 
        APP_STORAGE.updateItem('BlockedList', this.blockedList);
      }
    });

    APIS.User.listUsersReported(userId, ({ listReported }) => {
      if (!this.reportedList) {
        this.reportedList = listReported || [];
        global.emitters.emit('ReportedList', this.reportedList); 
        APP_STORAGE.setItem('ReportedList', this.reportedList);
      } else {
        this.reportedList = listReported || [];
        global.emitters.emit('ReportedList', this.reportedList); 
        APP_STORAGE.updateItem('ReportedList', this.reportedList);
      }
    });

    Twilio.emitter.on('channelAdded', (channelTwilio) => this.getChannelDetails(channelTwilio.sid, channelTwilio.state));
    Twilio.emitter.on('channelUpdated', ({ channel: { sid, state } }) => this.channelUpdated(sid, state));
  }

  getChannelDetails = (channelSid) => {
    const { userId } = this.props;

    return APIS.Channels.channelDetails({ channelSid, userId }, ({ channelDetails: channelDetailsResult }) => this.addChannelToState(channelDetailsResult));
  };

  addChannelToState = async (channelDetails) => {
    const { userId } = this.props;
    let
      { onlyChannelsEmpty } = this.state,
      { channelSid, createdBy, attributes, lastConsumedMessageIndex } = channelDetails;

    if (channelDetails && !this.state.channels.find(channel => channel.channelSid === channelSid)) {

      try {
        let
          channelOnStorage = await APP_STORAGE.getItem(`CHAT_${channelSid}`),
          channelInfoOnStorage = await APP_STORAGE.getItem(`CHANNEL_${channelSid}`),
          channelToState;
        
        if (!attributes.blocked) {
          attributes.blocked = false;
        }

        if (!channelInfoOnStorage) {
          if (channelOnStorage && channelOnStorage.length) {
            channelDetails.dateUpdated = channelOnStorage[channelOnStorage.length - 1].state.dateUpdated;
          } else {
            attributes.type = parseInt(createdBy) === parseInt(userId) || attributes.type === 'match' ? 'match' : 'default';
          }

          channelToState = {
            ...channelDetails,
            lastConsumedMessageIndex,
            createdFromTwilio: attributes.type === 'match',
            unreadMessagesCount: 0,
            emptyMessages: true,
            lastMessage: '...'
          };

          if (!channelOnStorage) {
            // inicializa channel no storage..
            APP_STORAGE.setItem(`CHAT_${channelSid}`, [], () => {
              this.channelsMessages.push({
                channelSid,
                channelInfo: channelToState,
                messages: []
              });
            });
          }

          APP_STORAGE.setItem(`CHANNEL_${channelSid}`, channelToState);
        } else {
          channelToState = channelInfoOnStorage;
          
          if (channelOnStorage) {
            this.channelsMessages.push({
              channelSid,
              channelInfo: channelInfoOnStorage,
              messages: channelOnStorage
            });
          }
        }

        this.setState({
          onlyChannelsEmpty: channelToState.emptyMessages && onlyChannelsEmpty,
          channels: [
            ...this.state.channels,
            channelToState
          ]
        });
      } catch (e) {
        console.log('# channels | addChannelToState/catch', e);
      }

    }
  };

  updateChannelToState = async (channelSid, messages, lastConsumedMessageIndexFromFriend=null, lastConsumedMessageIndexFromUser=null) => {
    const channelOnStorage = await APP_STORAGE.getItem(`CHAT_${channelSid}`);

    this.channelsMessages.find(channel => channel.channelSid === channelSid).messages = channelOnStorage;

    if (this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.length) {
      if (messages.items.length && !this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.find(msg => msg.state.index === messages.items.slice(-1)[0].state.index)) {
        return messages.items.filter(msg => msg.state.index > this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.slice(-1)[0].state.index).map(message => this.saveMessageStorage(message, channelSid, lastConsumedMessageIndexFromFriend, lastConsumedMessageIndexFromUser));
      } else {
        this.checkChannelEmpty(channelSid, lastConsumedMessageIndexFromFriend, lastConsumedMessageIndexFromUser);
      }
    } else {
      this.channelsMessages.find(channel => channel.channelSid === channelSid).messages = [];
      return messages.items.map(message => this.saveMessageStorage(message, channelSid, lastConsumedMessageIndexFromFriend, lastConsumedMessageIndexFromUser));
    }
  };

  channelUpdated = (channelSid, state) => {
    const { channels } = this.state;
    let channel = channels.find(channel => channel.channelSid === channelSid);

    console.log('# channels | channelUpdated/channelSid', channelSid, state);
    if (channel && state.attributes && state.attributes.blocked !== channel.attributes.blocked) {
      channels.find(channel => channel.channelSid === channelSid).attributes.blocked = state.attributes.blocked;
      this.setState({ channels }, () => {
        APP_STORAGE.updateItem(`CHANNEL_${channelSid}`, channels.find(channel => channel.channelSid === channelSid));
      });
    }
  };

  handleGoToChannel = (channelSID, name, friendUserId, briefingMatch, profileImage, title, category, changeChatOnStack = null, changeBriefingMatchChannelItem = null, channelsOrderUpdate) => {

    const { userId, setActiveChannel } = this.props;
    let
      { channels } = this.state,
      channel;
    
    this.setState({ goToChannelRequesting: true }, () => {
			channel = channels.find(channel => channel.channelSid === channelSID);
      console.log('# channels | handleGoToChannel/channel', channel);
      briefingMatch = channel ? channel.attributes.type === 'match' && parseInt(channel.createdBy) !== parseInt(userId) : false;

      navigateFromTo(Routes.CHAT._, Routes.CHAT.CHANNEL, {
        bottomTabs: {
          visible: false,
          ...Platform.select({ android: { drawBehind: true } })
        },
        topBar: {
          visible: false,
        },
        animations: {
          push: {
            waitForRender: true
          }
        }
      }, {
          channelSID,
          userId,
          userName: name ? name : 'User Name',
          friendUserId,
          briefingMatch,
          profileImage,
          title,
          category,
          changeChatOnStack,
          changeBriefingMatchChannelItem,
          channelsOrderUpdate,
          handlerBlockUser: this.handlerBlockUser,
          handlerReportUser: this.handlerReportUser,
          userBlocked: this.blockedList && this.blockedList.find(user => parseInt(user) === parseInt(friendUserId)),
          channelBlocked: channel ? channels.find(channel => channel.channelSid === channelSID).attributes.blocked : false,
        }
      )

      setActiveChannel(channelSID)

      setTimeout(() => {
        this.setState({ goToChannelRequesting: false });
      }, 2000);
		});

  }

  /**
   * // REFACT:  Esse metodo foi substuído por "getChannelDetails"
   * remover após verificar se não está sendo chamando em nenhum outro lugar;
  **/
  channelDetails = async (channelSid, userId) => {
    let channelOnStorage = await APP_STORAGE.getItem(`CHAT_${channelSid}`);

    return APIS.Channels.channelDetails({ channelSid, userId }, ({ channelDetails }) => {
      console.log('# channels | channelDetails/channelDetails', channelDetails);
      if (channelOnStorage && channelOnStorage.length) {
        channelDetails.dateUpdated = channelOnStorage[channelOnStorage.length - 1].state.dateUpdated;
      } else {
        channelDetails.attributes.type = parseInt(channelDetails.createdBy) === parseInt(userId) || channelDetails.attributes.type === 'match' ? 'match' : 'default';
      }

      console.log('# channels | channelDetails/channelDetails;', channelDetails);
      return channelDetails;
    });
  };

  checkChannelEmpty = async (channelSid, lastConsumedMessageIndexFromFriend=null, lastConsumedMessageIndexFromUser=null) => {
    const { userId } = this.props;
    let { channels } = this.state;

    // verifica se o channel tem mensagens;
    if (this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.length && channels.find(channel => channel.channelSid === channelSid)) {
      let lastMessageOnStorage = this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.length ? this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.slice(-1)[0] : null;

      if ((lastConsumedMessageIndexFromFriend !== null || lastConsumedMessageIndexFromUser !== null) && (lastMessageOnStorage.state.index <= lastConsumedMessageIndexFromFriend || lastMessageOnStorage.state.index >= lastConsumedMessageIndexFromUser)) {

        channels.find(channel => channel.channelSid === channelSid).unreadMessagesCount = this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.filter(({ state: { author, status } }) => parseInt(author) !== parseInt(userId) && status === 'sent').length;

        this.channelsMessages.find(channel => channel.channelSid === channelSid).messages
          .filter(({ state: { status } }) => status === 'sent')
          .map(msg => msg.state.status = (msg.state.index <= lastConsumedMessageIndexFromUser && parseInt(msg.state.author) !== parseInt(userId)) || (msg.state.index <= lastConsumedMessageIndexFromFriend && parseInt(msg.state.author) === parseInt(userId)) ? 'read' : msg.state.status);

        APP_STORAGE.updateItem(`CHAT_${channelSid}`, this.channelsMessages.find(channel => channel.channelSid === channelSid).messages);
      }

      channels.find(channel => channel.channelSid === channelSid).emptyMessages = false;
      channels.find(channel => channel.channelSid === channelSid).dateUpdated = lastMessageOnStorage.state.dateUpdated;
      channels.find(channel => channel.channelSid === channelSid).lastMessage = lastMessageOnStorage.state.body;
      channels.find(channel => channel.channelSid === channelSid).attributes.type = this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.filter(msg => !msg.state.attributes.type || msg.state.attributes.type === 'default').length || (!lastMessageOnStorage.state.attributes.type || lastMessageOnStorage.state.attributes.type === 'default' || lastMessageOnStorage.state.attributes.type === 'propose' || lastMessageOnStorage.state.attributes.type === 'requestApproval' || lastMessageOnStorage.state.attributes.type === 'approvedPropose' || lastMessageOnStorage.state.attributes.type === 'system') || lastMessageOnStorage.state.attributes.type === 'matchApproved' && parseInt(lastMessageOnStorage.state.author) === parseInt(userId) ? 'default' : 'match';

      this.setState({
        channels,
        onlyChannelsEmpty: false
      }, () => {
        this.channelsOrderUpdate();

        if (channels.find(channel => channel.channelSid === channelSid).emptyMessages !== this.channelsMessages.find(channel => channel.channelSid === channelSid).channelInfo.emptyMessages
          || channels.find(channel => channel.channelSid === channelSid).dateUpdated !== this.channelsMessages.find(channel => channel.channelSid === channelSid).channelInfo.dateUpdated
          || channels.find(channel => channel.channelSid === channelSid).lastMessage !== this.channelsMessages.find(channel => channel.channelSid === channelSid).channelInfo.lastMessage
          || channels.find(channel => channel.channelSid === channelSid).attributes.type !== this.channelsMessages.find(channel => channel.channelSid === channelSid).channelInfo.attributes.type
          || channels.find(channel => channel.channelSid === channelSid).unreadMessagesCount !== this.channelsMessages.find(channel => channel.channelSid === channelSid).channelInfo.unreadMessagesCount) APP_STORAGE.updateItem(`CHANNEL_${channelSid}`, channels.find(channel => channel.channelSid === channelSid));
      });
    }
  };

  changeChannelType = (channelSid) => {
    let
      { channels } = this.state,
      channel = channels.find(channel => channel.channelSid === channelSid),
      channelOnScope = this.channelsMessages.find(channel => channel.channelSid === channelSid);

    channel.attributes.type = 'default';
    this.setState({ channels }, () => {
      let { channelInfo } = channelOnScope;
      
      if (channel.attributes.type !== channelInfo.attributes.type) APP_STORAGE.updateItem(`CHANNEL_${channelSid}`, channel);
    });
  };

  deleteChannel = (channelSid, callback = null) => {
    const { channels } = this.state;

    APP_STORAGE.deleteItem(`CHAT_${channelSid}`);
    APP_STORAGE.deleteItem(`CHANNEL_${channelSid}`);
    this.setState({
      channels: channels.filter(channel => channel.channelSid !== channelSid)
    }, () => callback ? callback() : false);
  };

  handleNewChatMessage = () => {
    if (this.modalContectionsList) this.modalContectionsList.snapTo({ index: 1 });
  };

  saveMessageStorage = async (message, channelSid, lastConsumedMessageIndexFromFriend=null, lastConsumedMessageIndexFromUser=null) => {
    const
			{ userId } = this.props,
      { state, state: { author, body, attributes, index: indexFromTwilioToCompare } } = message,
      messageToPush = { state };
    let channelOnStorage;
    
    if (!this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.find(({ state: { index } }) => index === indexFromTwilioToCompare) && !messageToPush.state.status) messageToPush.state.status = (indexFromTwilioToCompare <= lastConsumedMessageIndexFromUser && parseInt(messageToPush.state.author) !== parseInt(userId)) || (indexFromTwilioToCompare <= lastConsumedMessageIndexFromFriend && parseInt(messageToPush.state.author) === parseInt(userId)) ? 'read' : 'sent';

    if (messageToPush.state.attributes.type === 'matchApproved') this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.find(msg => parseInt(msg.state.attributes.briefingId) === parseInt(messageToPush.state.attributes.briefingId)).state.attributes.status = 200;

    if (messageToPush.state.attributes.type === 'system') this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.filter(msg => parseInt(msg.state.author) !== parseInt(userId))
        .map(msg => {
          if (msg.state.attributes.type === 'propose' && parseInt(msg.state.attributes.proposeId) === parseInt(messageToPush.state.attributes.proposeId)) msg.state.attributes.status = 220;
        });

    if (messageToPush.state.attributes.type === 'approvedPropose') this.channelsMessages.find(channel => channel.channelSid === channelSid).messages.filter(msg => parseInt(msg.state.author) !== parseInt(userId))
        .map(msg => {
          if ((msg.state.attributes.type === 'propose' || msg.state.attributes.type === 'requestApproval') && parseInt(msg.state.attributes.proposeId) === parseInt(messageToPush.state.attributes.proposeId)) msg.state.attributes.status = 240;
        });

    // atualizando storage do channel;
    channelOnStorage = [
      ...this.channelsMessages.find(channel => channel.channelSid === channelSid).messages,
      messageToPush
    ];
    
		// atualizando mensagens no escopo do component;
    this.channelsMessages.find(channel => channel.channelSid === channelSid).messages = channelOnStorage;

    return APP_STORAGE.updateItem(`CHAT_${channelSid}`, channelOnStorage, () => {
      this.checkChannelEmpty(channelSid, lastConsumedMessageIndexFromFriend, lastConsumedMessageIndexFromUser);
    });
  };

  handlerBlockUser = (channelSid, blockedUserId, callback=null) => {
    const { userId } = this.props;
    let { channels } = this.state;

    if (this.blockedList && this.blockedList.find(user => parseInt(user) === parseInt(blockedUserId))) {
      return APIS.User.blockUser({
        channelSid,
        blockedUserId,
        isBlocked: false,
        userId
      }, () => {
        this.blockedList = this.blockedList.filter(user => parseInt(user) !== parseInt(blockedUserId));
        channels.find(channel => channel.channelSid === channelSid).attributes.blocked = false;
        
        global.emitters.emit('blockedList', this.blockedList); 
        
        this.setState({ channels }, () => callback ? callback() : false);
        APP_STORAGE.updateItem('BlockedList', this.blockedList);
      });
    } else {
      return APIS.User.blockUser({
        channelSid,
        blockedUserId,
        isBlocked: true,
        userId
      }, () => {
        if (this.blockedList && !this.blockedList.find(user => parseInt(user) === parseInt(blockedUserId))) this.blockedList.push(blockedUserId);
        channels.find(channel => channel.channelSid === channelSid).attributes.blocked = true; 
        global.emitters.emit('blockedList', this.blockedList); 
        this.setState({ channels }, () => callback ? callback() : false);
        APP_STORAGE.updateItem('BlockedList', this.blockedList);
      });
    }
  };

  handlerReportUser = (channelSid, reportedUserId, callback=null) => {
    const { userId } = this.props;
    let { channels } = this.state;

    if (this.reportedList && this.reportedList.find(user => parseInt(user) === parseInt(reportedUserId))) {
      return APIS.User.reportUser({
        channelSid,
        reportedUserId,
        isReported: false,
        userId
      }, () => {
        this.reportedList = this.reportedList.filter(user => parseInt(user) !== parseInt(reportedUserId));
        channels.find(channel => channel.channelSid === channelSid).attributes.blocked = false;
        
        global.emitters.emit('reportedList', this.reportedList); 
        
        this.setState({ channels }, () => callback ? callback() : false);
        APP_STORAGE.updateItem('ReportedList', this.reportedList);
      });
    } else {
      return APIS.User.reportUser({
        channelSid,
        reportedUserId,
        isReported: true,
        userId
      }, () => {
        if (this.reportedList && !this.reportedList.find(user => parseInt(user) === parseInt(reportedUserId))) this.reportedList.push(reportedUserId);
        channels.find(channel => channel.channelSid === channelSid).attributes.blocked = true; 
        global.emitters.emit('reportedList', this.reportedList); 
        this.setState({ channels }, () => callback ? callback() : false);
        APP_STORAGE.updateItem('ReportedList', this.reportedList);
      });
    }
  };

  orderByMessageUpdated = (channels) => {
    let sorted = channels.sort((a, b) => new Date(a.dateUpdated).getTime() < new Date(b.dateUpdated).getTime() ? 1 : new Date(b.dateUpdated).getTime() < new Date(a.dateUpdated).getTime() ? -1 : 0)
    return sorted;
  };

  channelsOrderUpdate = () => {
    let { channels } = this.state;
    this.setState({ channels: this.orderByMessageUpdated(channels) });
  };

  render() {
    const
      { t, userId, fromNavigation } = this.props,
      { channels, onlyChannelsEmpty, goToChannelRequesting } = this.state,
      channelsFiltred = channels.filter(channel => channel.members && channel.members.length && this.reportedList && !this.reportedList.find(user => parseInt(user) === parseInt(channel.members.find(member => parseInt(member.userId) !== parseInt(userId)).userId)));

    return (
      <_Container hasChannels={channelsFiltred.length}>
        <_Header>
          <_ButtonLeft />
          <_WrapperTitle>
            <_TitleHeader
              euclid
              align={'center'}
              content={t('chat:messages')}
            />
          </_WrapperTitle>
          <_ButtonRight onPress={this.handleNewChatMessage}>
            <_ButtonRightIcon source={NewChatMessage} />
          </_ButtonRight>
        </_Header>
        <_ScrollContainer>
        {channelsFiltred.length ? channelsFiltred.map((channel, index) => {

            const {
              channelSid,
              lastConsumedMessageIndex,
              unreadMessagesCount,
              friendlyName,
              members,
              createdBy,
              createdFromTwilio,
              attributes,
              emptyMessages,
              lastMessage,
              dateUpdated
            } = channel,
            { blocked, type } = attributes,
            friendUserId = members.length > 1 ? members.find(member => parseInt(member.userId) !== parseInt(userId)).userId : members[0].userId;

            return <ChannelItem
              key={channel.channelSid}
              channelsMessages={this.channelsMessages}
              handlerBlockUser={this.handlerBlockUser}
              handlerReportUser={this.handlerReportUser}
              updateChannelToState={this.updateChannelToState}
              changeChannelType={this.changeChannelType}
              deleteChannel={this.deleteChannel}
              saveMessageStorage={this.saveMessageStorage}
              onlyChannelsEmpty={onlyChannelsEmpty}
              checkChannelEmpty={this.checkChannelEmpty}
              channelsOrderUpdate={this.channelsOrderUpdate}
              goToChannel={!goToChannelRequesting ? this.handleGoToChannel : () => false}
              channelSid={channelSid}
              lastConsumedMessageIndex={lastConsumedMessageIndex}
              dateUpdated={dateUpdated}
              channelType={type !== null ? type : 'default'}
              channelBlocked={blocked}
              createdBy={createdBy}
              empty={emptyMessages}
              members={members}
              friendlyName={friendlyName}
              friendUserId={friendUserId}
              userId={userId}
              profileImage={members.length > 1 ? members.find(member => parseInt(member.userId) !== parseInt(userId)).profileImage : members[0].profileImage}
              name={members.length > 1 ? members.find(member => parseInt(member.userId) !== parseInt(userId)).name : members[0].name}
              title={members.length > 1 ? members.find(member => parseInt(member.userId) !== parseInt(userId)).title : members[0].title}
              category={members.length > 1 ? members.find(member => parseInt(member.userId) !== parseInt(userId)).category : members[0].category}
              unreadMessagesCount={unreadMessagesCount}
              lastMessage={lastMessage}
              componentId={'/chat'}
              isCreatedFromTwilio={createdFromTwilio}
            />
          }) : undefined}

          {(!channelsFiltred.length || !channelsFiltred.find(channel => channel.attributes && channel.attributes.type !== 'match') && channelsFiltred.find(channel => channel.attributes && channel.attributes.type === 'match' && parseInt(channel.createdBy) === parseInt(userId)) && !channelsFiltred.find(channel => channel.attributes && channel.attributes.type === 'match' && parseInt(channel.createdBy) !== parseInt(userId)) || onlyChannelsEmpty) &&
            <_TitleEmptyList
              euclid
              content={t('chat:messagesEmptyList')}
              fontSize={42}
              lineHeight={52}
            />
          }
        </_ScrollContainer>
          <ModalConectionsList
            userID={userId}  
            getRef={ref => this.modalContectionsList = ref}
            goToChannel={this.handleGoToChannel}
            listChannels={this.listChannels}
            fromNavigation={fromNavigation}
          /> 
       
        {/* // REFACT:  19/06 - Novo fluxo sem Import de Contacts obrigatório
          <ModalConectionsList
            getRef={ref => this.modalContectionsList = ref}
            goToChannel={this.handleGoToChannel}
            listChannels={this.listChannels}
            fromNavigation={this.props.fromNavigation}
          /> 
        */}
      </_Container>
    );
  }

}

export default withLocalization(withTwilio(Channels));
