import React, { Component } from 'react';
import { Platform } from 'react-native';
import withLocalization from 'utils/withLocalization';
import Routes from 'Routes';

import BUTTON from 'GLOBALS/BUTTON';
import { navigateFromTo } from 'utils/navigateFromTo';

import { 
  _Container, 
  _HEADER,
  _H1,
  _ButtonActions 
} from './styles';

class Welcome extends Component {

  constructor(props) {
    super(props);
  }
  
  render() {

    const {t} = this.props;

    return (
      <_Container>
        <_HEADER logoShort paddingTop={35} />
        <_H1
          euclid
          content={t('welcome:open')}
          fontSize={36}
          lineHeight={46}
        />
        <_ButtonActions>
          <BUTTON
            onPressFn={() => navigateFromTo(Routes.WELCOME, Routes.BRIEFING._)}
            content={t('welcome:buttons:budgetAProject')}
            fontSize={24}
            isAndroid={Platform.OS === 'android'}
          />
          <BUTTON
            onPressFn={() => navigateFromTo(Routes.WELCOME, Routes.LOGIN._)}
            content={t('welcome:buttons:login')}
            fontSize={24}
            isAndroid={Platform.OS === 'android'}
          />
        </_ButtonActions>
      </_Container>
    );

  }

}

export default withLocalization(Welcome);
