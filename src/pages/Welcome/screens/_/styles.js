import styled from 'styled-components/native';
import { HEADER } from 'GLOBALS/HEADER';
import { TEXT } from 'GLOBALS/TEXT';

export const _Container = styled.View`
  flex: 1;
  padding: 0 29px 29px;
`;

export const _HEADER = styled(HEADER)``;

export const _H1 = styled(TEXT)`
  flex-grow: 1;
  margin: 29px 0;
`;

export const _ButtonActions = styled.View``;
