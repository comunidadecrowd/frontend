import React, { PureComponent } from 'react';
import { KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Animated, Easing, Platform } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { HEADER } from 'GLOBALS/HEADER';
import CROWD_ARROW from 'GLOBALS/CROWD_ARROW';

import APIS from 'apis';
import Routes from 'Routes';
import { newSetRoot } from 'utils/newSetRoot';
import { navigateFromTo } from 'utils/navigateFromTo';

import firebase from "react-native-firebase";
import { isIphoneX } from 'utils/Helpers';

import {
  _Container,
  _Main,
  _ContainerWrapper,
  _Title,
  _OptionalText,
  _FormWrapper,
  _InputsWrapper,
  _FormInputs,
  _CategoryItem,
  _CategoryRadio,
  _CategoryText,
  _WrapperButton,
  _MsgError,
  _SendButton
} from './styles';

const CROWD_ARROWAnimated = Animated.createAnimatedComponent(CROWD_ARROW);
const _InputsWrapperAnimated = Animated.createAnimatedComponent(_InputsWrapper);
const isiOS = Platform.OS === 'ios';

class Register extends PureComponent {

  constructor(props) {

    super(props);

    this.animations = {
      inputs: Array.from(Array(2), () => new Animated.Value(0)),
      arrow: Array.from(Array(2), () => new Animated.Value(0)),
    }

    this.user = null;

    this.state = {
      userID: this.props.hasOwnProperty('userID') ? this.props.userID : undefined,
      credentials: {
        name: __DEV__ ? 'Cleito' : '',
        title: __DEV__ ? 'Gato' : '',
        category: __DEV__ ? 'media' : ''
        // name: '',
        // title: '',
        // category: ''
      },
      requesting: false,
      msgError: null
    }

  }

  async componentDidMount() {

    const { userID } = this.props;
    let getUserOnStorage = await APP_STORAGE.getItem('User');

    this.user = getUserOnStorage;

    if (!userID) {

      this.setState({
        userID: getUserOnStorage.userID
      })

    }

  }

  handlerInput = (context, item, value = false) => {

    const
      { inputs, arrow } = this.animations,
      { credentials } = this.state,
      easing = Easing.in(),
      duration = 300;

    if (item === 'name') {

      if (context === 'change') {
        this.setState({
          credentials: {
            ...credentials,
            name: value.trim()
          }
        })
      }

      if (context === 'blur') {

        Animated.timing(inputs[0], {
          toValue: credentials.name.trim().length < 3 ? 2 : 3,
          easing,
          duration
        }).start();

        Animated.timing(arrow[0], {
          toValue: 0,
          easing,
          duration
        }).start();

      }

    }

    if (item === 'professionalTitle') {

      if (context === 'change') {
        this.setState({
          credentials: {
            ...credentials,
            title: value.trim()
          }
        })
      }

      if (context === 'blur') {

        Animated.timing(inputs[1], {
          toValue: credentials.title.trim().length < 3 ? 2 : 3,
          easing,
          duration
        }).start();

        Animated.timing(arrow[1], {
          toValue: 0,
          easing,
          duration
        }).start();

      }

    }

    if (context === 'focus') {

      Animated.timing(inputs[item === 'name' ? 0 : 1], {
        toValue: 1,
        easing,
        duration
      }).start();

      Animated.timing(arrow[item === 'name' ? 0 : 1], {
        toValue: 1,
        easing,
        duration
      }).start();

    }

  }

  handlerRadioBox = (choice) => {

    const { credentials } = this.state;

    this.setState({
      credentials: {
        ...credentials,
        category: choice
      }
    })

  }

  sendCredentials = () => {

    const
      objToSend = this.state,
      { userID, credentials: { name, title, category } } = objToSend,
      sendFunction = (firebaseUserToken) => APIS.User.sendCredentials({
        ...objToSend,
        firebaseUserToken
      }, (data) => {
        const { inputCredentials: { profileImage, email, phone, firebaseUserToken } } = data;

        let newDataToUser = {
          ...this.user,
          hasRegister: true,
          // alreadyImportedContacts: false, // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
          name,
          title,
          category,
          profileImage,
          email,
          phone,
          _FIREBASEUSERTOKEN: firebaseUserToken
        }

        const callback = async () => {
          let hasBriefingOnStorage = await APP_STORAGE.getItem('BriefingDraft');

          this.setState({ requesting: false });

          if (hasBriefingOnStorage)
            return navigateFromTo(Routes.LOGIN.REGISTER, Routes.BRIEFING.PUBLISH, null, {
              userID: parseInt(userID),
              title: hasBriefingOnStorage.title,
              description: hasBriefingOnStorage.description,
              excerpt: hasBriefingOnStorage.excerpt
            });

          return newSetRoot({
            // alreadyImportedContacts: false,  // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
            userID,
            name,
            title,
            category,
            profileImage,
            _FIREBASEUSERTOKEN: firebaseUserToken
          });
        };

        APP_STORAGE.updateItem('User', newDataToUser, callback);

      });

    firebase.messaging().requestPermission()
      .then(() => {
        console.log('# Register | sendCredentials/then requestPermission');
        firebase.messaging().getToken().then((firebaseUserToken) => {
          console.log('# Register | sendCredentials/then firebaseUserToken', firebaseUserToken);
          if (firebaseUserToken) {
            this.setState({ requesting: true, msgError: null }, () => sendFunction(firebaseUserToken));
          }
        }).catch(e => {
          console.log('# Register | sendCredentials/catch firebaseUserToken', e);
          this.setState({ msgError: 'Tente novamente!' }, () => setTimeout(() => {
            this.setState({ msgError: null });
          }, 2000));
        });
      })
      .catch(e => sendFunction(null))

  }

  render() {

    const
      { t } = this.props,
      { inputs, arrow } = this.animations,
      { credentials, credentials: { name, title }, requesting, msgError } = this.state,
      inputsWrapperInterpolate = inputs.map(item => {

        return {
          borderBottomColor: item.interpolate({
            inputRange: [0, 1, 2, 3],
            outputRange: ['#444', '#84C9F2', '#FFC80B', '#BCDEA1']
          }),
          left: item.interpolate({
            inputRange: [0, 1, 2, 3],
            outputRange: isiOS ? [-15, 0, -15, -15] : [0, 0, 0, 0] // TODO:  Alex || Bruno -> Refatorar para aparecer a CROWD_ARROW no Android
          })
        }

      }),
      arrowInterpolate = arrow.map(item => item.interpolate({
        inputRange: [0, 1],
        outputRange: [-10, 0]
      })),
      categories = ['design', 'technology', 'content', 'media'],
      formIsDisabled = (name.trim().length < 3 || title.trim().length < 3);

    return (
      <KeyboardAvoidingView behavior="padding">
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <_Container>
            <HEADER logoShort paddingTop />
            <_Main isIphoneX={isIphoneX}>
              <_Title
                content={t(`login:register:title`)}
                fontSize={20}
                lineHeight={30}
                euclid
              />
              <_FormWrapper>
                {
                  ['name', 'professionalTitle'].map((item, index) => {
                    return (
                      <_InputsWrapperAnimated
                        key={index}
                        style={inputsWrapperInterpolate[index]}
                      >
                        {
                          isiOS &&
                          <CROWD_ARROWAnimated
                            orientation={"right"}
                            size={5}
                            style={{ top: 0, color: "#84C9F2", left: arrowInterpolate[index] }}
                          />
                        }
                        <_FormInputs
                          placeholder={index === 0 ? t(`login:register:formInputs:name`) : t(`login:register:formInputs:professionalTitle`)}
                          placeholderTextColor={'#9B9B9B'}
                          keyboardType={'default'}
                          onFocus={() => this.handlerInput('focus', item)}
                          onBlur={() => this.handlerInput('blur', item)}
                          onChangeText={(value) => this.handlerInput('change', item, value)}
                          value={__DEV__ ? (index === 0 ? name : title) : undefined}
                        />
                      </_InputsWrapperAnimated>
                    )
                  })
                }
              </_FormWrapper>
              <_OptionalText
                content={t(`login:register:optionalText`)}
                fontSize={14}
                euclid
              />
              <_ContainerWrapper>
                {
                  categories.map(item =>
                    <_CategoryItem
                      key={item}
                      isLast={item === (categories.length - 1)}
                      onPress={() => this.handlerRadioBox(item)}
                    >
                      <_CategoryRadio
                        categoryActive={credentials.category === item ? credentials.category : null}
                      />
                      <_CategoryText
                        categoryActive={credentials.category === item ? credentials.category : null}
                        content={t(`login:register:formInputs:categories:${item}`)}
                      />
                    </_CategoryItem>
                  )
                }
              </_ContainerWrapper>
              <_WrapperButton>
                {msgError && <_MsgError content={msgError} fontSize={12} align={'center'} />}
                <_SendButton
                  disabled={formIsDisabled || requesting}
                  onPressFn={this.sendCredentials}
                  content={t('login:register:sendButton')}
                  color={!formIsDisabled ? "#BCDEA1" : undefined}
                />
              </_WrapperButton>
            </_Main>
          </_Container>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    )

  }

}

export default withLocalization(Register)
