import React, { Component } from 'react';
import { KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Animated, Easing } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { HEADER } from 'GLOBALS/HEADER';
import BUTTON from 'GLOBALS/BUTTON';
import { AVATAR } from 'GLOBALS/AVATAR';

import APP_SWORKER from 'APP_SWORKER';
import { newSetRoot } from 'utils/newSetRoot';

import {
  _Container,
  _Main,
  _Title,
  _Form,
  _EmailInput,
  _PasswordInput,
  _LostYourPassword,
  _LostYourPasswordText,
  // TODO:  Após a implementação do fluxo de Login, habilitar os itens abaixo
  _ButtonWrapper,
  _Name
} from './styles';

const _EmailInputAnimated = Animated.createAnimatedComponent(_EmailInput);
const _PasswordInputAnimated = Animated.createAnimatedComponent(_PasswordInput);

// TODO:  Depois de implementar o fluxo correto de Login, remover o item USERS abaixo (remover também a pasta img dentro dessa pasta). Estão sendo usados apenas na fase de Dev.
const USERS = {
  male: {
    name: 'Rony Weasley',
    ID: 25523386059076410,
    avatar: require('./img/male.png')
  },
  female: {
    name: 'Hermione Granger',
    ID: 39593488156767110,
    avatar: require('./img/female.png')
  }
};

class Legacy extends Component {

  constructor(props) {
    super(props);

    this.animations = {
      email: new Animated.Value(0),
      password: new Animated.Value(0)
    }

    this.state = {
      formInputs: {
        placeholderTextColor: '#9B9B9B',
        email: {
          isActive: false
        },
        password: {
          isActive: false
        }
      },
      buttonIsDisabled: false
    }
  }

  inputAnimation = (inputElement, context) => {

    Animated.timing(this.animations[inputElement], {
      toValue: context === 'focus' ? 1 : 0,
      easing: Easing.in(),
      duration: 500,
    }).start()

  }

  handleLogin = (user) => {

    const { buttonIsDisabled } = this.state;

    this.setState({
      buttonIsDisabled: !buttonIsDisabled
    });

    const updateUserOnStorage = {
      name: USERS[user].name,
      userID: USERS[user].ID,
      avatar: USERS[user].name.includes('Hermione') ? 'femaleUser' : 'maleUser',
      // alreadyImportedContacts: false, // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
      hasLogin: true
    }

    APP_SWORKER.initChat(USERS[user].ID).then(({ fpaToken }) => {
      if (fpaToken)
        APP_STORAGE.updateItem('User', {
          ...updateUserOnStorage,
          _TOKEN: fpaToken,
        }, newSetRoot({
          // alreadyImportedContacts: false, // REFACT:  19/06 - Novo fulxo sem Import de Contacts obrigatório
          userID: USERS[user].ID
        }))
    })

  }

  render() {

    const { t } = this.props;
    const { formInputs: { placeholderTextColor, email, password }, buttonIsDisabled } = this.state;
    const { email: emailAnimated, password: passwordAnimated } = this.animations;

    const inputInterpolateEmail = emailAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: ['#444', '#84C9F2']
    })

    const inputInterpolatePassword = passwordAnimated.interpolate({
      inputRange: [0, 1],
      outputRange: ['#444', '#84C9F2']
    })

    return (
      <KeyboardAvoidingView behavior="position">
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <_Container>
            <HEADER />
            <_Main>
              <_Title
                euclid
                bold
                content={t('login:legacy:connectWithUs')}
                fontSize={42}
                lineHeight={52}
              />
              {
                // TODO:  Após a implementação do fluxo de Login, remover os itens abaixo
                ['male', 'female'].map((item, index) => {
                  return (
                    <_ButtonWrapper key={index} isLast={index !== 0} disabled={buttonIsDisabled} onPress={buttonIsDisabled ? null : () => this.handleLogin(item)}>
                      <AVATAR color={index !== 0 ? "#BCDEA1" : false} uri={USERS[item].avatar} />
                      <_Name bold fontSize={14} content={USERS[item].name} />
                    </_ButtonWrapper>
                  )
                })
              }
              {
                /* // TODO:  Após a implementação do fluxo de Login, habilitar os itens abaixo
                  <_Form>
                    <_EmailInputAnimated
                      placeholder={t('login:legacy:yourEmail')}
                      placeholderTextColor={placeholderTextColor}
                      borderColor={inputInterpolateEmail}
                      onFocus={() => this.inputAnimation('email', 'focus')}
                      onBlur={() => this.inputAnimation('email', 'blur')}
                    />
                    <_PasswordInputAnimated
                      placeholder={t('login:legacy:yourPassword')}
                      placeholderTextColor={placeholderTextColor}
                      borderColor={inputInterpolatePassword}
                      onFocus={() => this.inputAnimation('password', 'focus')}
                      onBlur={() => this.inputAnimation('password', 'blur')}
                    />
                  </_Form>
                  <View>
                    <BUTTON
                      onPressFn={this.handleLogin}
                      content={t('login:legacy:loginButton')}
                    />
                    <_LostYourPassword>
                      <_LostYourPasswordText
                        content={t('login:legacy:forgotPassword')}
                        color={'#9B9B9B'}
                        fontSize={14}
                        align={'center'}
                      />
                    </_LostYourPassword>
                  </View>
                */
              }
            </_Main>
          </_Container>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    )
  }

}


export default withLocalization(Legacy)