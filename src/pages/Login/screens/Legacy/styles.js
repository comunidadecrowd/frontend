import styled from 'styled-components/native';
import {Dimensions, Platform} from 'react-native';
import { TEXT } from 'GLOBALS/TEXT';

const {height} = Dimensions.get('screen');
const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  padding: 0 29px;
  height: 100%;
  /* background: red; */
`;

export const _Main = styled.View`
  /* justify-content: space-around;  */
  height: ${(height - (72+29))+'px'};
  /* background: blue; */
`;

export const _Title = styled(TEXT)`
  margin: 30px 0;
`;

export const _Form = styled.View``;

export const _TextInput = styled.TextInput`
  border-bottom-width: 1px;
  border-color: ${({borderColor}) => borderColor ? borderColor : '#fff'};
  padding: 20px 0;
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  font-size: 14px;
  color: #fff;
`;

export const _EmailInput = styled(_TextInput)``;
export const _PasswordInput = styled(_TextInput)``;
export const _LostYourPassword = styled.TouchableOpacity``;
export const _LostYourPasswordText = styled(TEXT)``;

// TODO:  Após a implementação do fluxo de Login, remover os itens abaixo
export const _ButtonWrapper = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  margin-bottom: ${({isLast}) => isLast ? 0 : 20};
`;

export const _Name = styled(TEXT)`
  margin: 0 0 0 8px;
`;