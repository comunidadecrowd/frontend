import React, { useState, useEffect } from 'react';

import {
  _Timer
} from './styles';

const ResendCodeTimer = ({ timerIsOn, changeTimerState }) => {

  [minutes, setMinutes] = useState(5);
  [seconds, setSeconds] = useState(0);
  [prevTimerIsOn, setPrevTimerIsOn] = useState(timerIsOn);

  useEffect(() => {

    const timerInterval = setInterval(() => setSeconds(prevSeconds => prevSeconds === 0 ? 59 : (prevSeconds - 1)), 1000)

    if (minutes === 0 && seconds === 0) {
      clearInterval(timerInterval)
      changeTimerState()
      return;
    }

    if (seconds === 0) setTimeout(() => setMinutes(prevMinutes => prevMinutes - 1), 1000)
    
    return () => clearInterval(timerInterval)

  }, [seconds])

  if (timerIsOn !== prevTimerIsOn) {

    setPrevTimerIsOn(timerIsOn)
    setMinutes(5)

    if (timerIsOn) {
      setMinutes(4)
      setSeconds(59)
    }

  }

  return (
    <_Timer
      content={`${minutes}:${seconds < 10 ? `0${seconds}` : seconds}`}
      align={'center'}
      fontSize={14}
    />
  )

}

export default ResendCodeTimer