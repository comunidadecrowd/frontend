import React, { useState } from 'react';

import examples from 'libphonenumber-js/examples.mobile.json'
import { getExampleNumber, AsYouType, } from 'libphonenumber-js';

// console.log('examples', examples)

import {
  _PhoneInputContainer,
  _CountryChoiceButton,
  _CountryChoiceText,
  _Input
} from './styles';

const PhoneInput = ({ snapModal, countriesList, selectedCountry }) => {

  [country, setCountry] = useState({ flag, acronym, code } = countriesList.filter(country => country.acronym === selectedCountry)[0]);
  [phoneNumberMask, setPhoneNumberMask] = useState(() => getExampleNumber(selectedCountry, examples).formatNational());
  [phoneNumber, setPhoneNumber] = useState('');

  if (country.acronym !== selectedCountry) {
    setCountry({ flag, acronym, code } = countriesList.filter(country => country.acronym === selectedCountry)[0])
    setPhoneNumberMask(() => getExampleNumber(selectedCountry, examples).formatNational())
  }

  handlePhoneInput = (value) => {

    const { nativeEvent: { key: inputValue } } = value;

    // console.log('handlePhoneInput -> phoneNumber', phoneNumber)

    // setPhoneNumber(new AsYouType(selectedCountry).input(inputValue))
    // setPhoneNumber(inputValue)


  }

  return (
    <_PhoneInputContainer>
      <_CountryChoiceButton onPress={() => snapModal()}>
        <_CountryChoiceText
          content={`${flag} ${acronym} ${code}`}
          fontSize={14}
          color={'#9B9B9B'}
        />
      </_CountryChoiceButton>
      <_Input
        placeholder={phoneNumberMask.replace(new RegExp("[0-9]", "g"), "X")}
        placeholderTextColor={"#fff"}
        keyboardType="number-pad"
        // onFocus={() => this.inputAnimation('focus')}
        // onBlur={() => this.inputAnimation('blur')}
        onKeyPress={handlePhoneInput}
      // value={phoneNumber !== '' ? phoneNumber : undefined}
      />
    </_PhoneInputContainer>
  )
}

export default PhoneInput