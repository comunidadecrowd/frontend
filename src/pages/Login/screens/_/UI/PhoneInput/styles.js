import styled from 'styled-components/native';
import { TEXT } from 'GLOBALS/TEXT';

const
  _View = styled.View``,
  _TouchableOpacity = styled.TouchableOpacity``,
  _TextInput = styled.TextInput``;

export const _PhoneInputContainer = styled(_View)`
  flex-direction: row;
  border-bottom-width: 1px;
  border-color: ${({ borderColor }) => borderColor ? borderColor : '#444'};
  margin: 0 0 20px;
`;

export const _CountryChoiceButton = styled(_TouchableOpacity)`
  width: 35%;
  height: 60px;
  background: #000;
  align-items: center;
  justify-content: center;
`;

export const _CountryChoiceText = styled(TEXT)``;

export const _Input = styled(_TextInput)`
  padding: 0 2px;
  font-size: 17px;
  width: 65%;
  color: #fff;
`;

