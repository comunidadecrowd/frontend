import React, { Component } from 'react';
import { Keyboard, Platform } from 'react-native';
import examples from 'libphonenumber-js/examples.mobile.json'
import { getExampleNumber, AsYouType, } from 'libphonenumber-js';

import {
  _PhoneInputContainer,
  _CountryChoiceButton,
  _CountryChoiceText,
  _Input
} from './styles';

export default class PhoneInput extends Component {

  constructor(props) {
    super(props)
    this.state = {
      country: this.props.countriesList.filter(country => country.acronym === this.props.selectedCountry)[0],
      phoneNumberMask: (() => getExampleNumber(this.props.selectedCountry, examples).formatNational())(),
      phoneNumber: ''
    }
  }

  componentDidUpdate(prevProps) {

    const { selectedCountry, clearUserPhone } = this.props;

    if (prevProps.selectedCountry !== selectedCountry) {
      this.setState({
        country: this.props.countriesList.filter(country => country.acronym === selectedCountry)[0],
        phoneNumberMask: (() => getExampleNumber(this.props.selectedCountry, examples).formatNational())(),
        phoneNumber: ''
      }, () => {
        this.inputRef.setNativeProps({ text: '' })
        clearUserPhone()
      })
    }

  }

  handlePhoneInput = (value) => {

    const
      { selectedCountry, userPhone, clearUserPhone } = this.props,
      { phoneNumber, phoneNumberMask } = this.state,
      { nativeEvent: { key: inputValue } } = value;

    let phoneInputValue = inputValue === 'Backspace' ? phoneNumber.slice(0, -1) : phoneNumber.length !== phoneNumberMask.length ? new AsYouType(selectedCountry).input(phoneNumber + inputValue) : phoneNumber;

    this.setState({ phoneNumber: phoneInputValue }, () => {

      if (inputValue === 'Backspace') {
        clearUserPhone(true)
      }

      if ((phoneNumber.length + 1) === phoneNumberMask.length) {
        Keyboard.dismiss();
        userPhone(phoneInputValue)
      }

    })

  }

  render() {

    const
      { snapModal } = this.props,
      { country: { flag, acronym, code }, phoneNumberMask, phoneNumber } = this.state;

    return (
      <_PhoneInputContainer>
        <_CountryChoiceButton onPress={() => snapModal()}>
          <_CountryChoiceText
            content={`${flag} ${acronym} ${code}`}
            fontSize={14}
            color={'#9B9B9B'}
          />
        </_CountryChoiceButton>
        <_Input
          ref={ref => this.inputRef = ref}
          placeholder={phoneNumberMask.replace(new RegExp("[0-9]", "g"), "X")}
          placeholderTextColor={"#9B9B9B"}
          keyboardType={Platform.OS === 'ios' ? "number-pad" : "default"}
          onKeyPress={this.handlePhoneInput}
          value={phoneNumber !== '' ? phoneNumber : undefined}
        />
      </_PhoneInputContainer>
    )
  }

}