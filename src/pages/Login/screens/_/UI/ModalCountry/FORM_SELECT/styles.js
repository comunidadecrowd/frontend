import styled from 'styled-components/native';
import { Colors } from 'theme';
import { TEXT } from 'GLOBALS/TEXT';

export const _Container = styled.View``;

export const _SelectList = styled.FlatList`
  width: 100%;
	height: 100%;
`;

export const _SelectItem = styled.View`
  width: 100%;
  height: 30px;
	justify-content: center;
	background: ${({ isSelected }) => isSelected ? Colors.BLUE : 'transparent'};
`;

export const _Button = styled.TouchableOpacity`
  padding: 5px 10px;
`;

export const _Label = styled(TEXT)``;
