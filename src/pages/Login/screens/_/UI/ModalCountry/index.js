import React, { Component } from 'react';
import { Dimensions } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { IconCloseButton } from 'components/Icon/SearchView';

import FORM_SELECT from './FORM_SELECT';

import {
  _ContainerInteractable,
  _ButtonClose,
  _WrapperSelect
} from './styles';

const { height } = Dimensions.get('window');

class ModalCountry extends Component {

  constructor(props) {
    super(props);
    this.state = {
      country: ''
    };
  }

  closeModal = () => {
    
    const { setModalCountryPosition } = this.props;

    this.modal.snapTo({ index: 0 });
    setModalCountryPosition()

  };

  handleOnChange = (country) => {

    const { setCountry } = this.props;

    this.setState({ country }, () => setTimeout(() => this.closeModal(), 300));
    setCountry(country)
  
  };

  componentDidUpdate(prevProps) {

    const { modalCountryPosition } = this.props;

    if (prevProps.modalCountryPosition !== modalCountryPosition && modalCountryPosition === 1) {
      this.modal.snapTo({ index: 1 });
    }

  }

  render() {

    const 
      { countriesList } = this.props,
      { country } = this.state;

    return (
      <_ContainerInteractable
        verticalOnly
        initialPosition={{ x: 0, y: height }}
        snapPoints={[{ y: height }, { y: 5 }]}
        ref={ref => this.modal = ref}
      >
        <_ButtonClose onPress={this.closeModal} activeOpacity={1}>
          <IconCloseButton width={20} height={20} color="#FFF" />
        </_ButtonClose>
        <_WrapperSelect>
          <FORM_SELECT
            data={countriesList}
            valueProp={'acronym'}
            labelProp={'name.ptBR'}
            labelPropNested
            inputValue={country}
            handleSelect={this.handleOnChange}
          />
        </_WrapperSelect>
      </_ContainerInteractable>
    );
  }

}

export default withLocalization(ModalCountry);
