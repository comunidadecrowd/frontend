import styled from 'styled-components/native';
import { Dimensions, Platform } from 'react-native';
import { TEXT } from 'GLOBALS/TEXT';

const { width } = Dimensions.get('window');
const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  height: 100%;
  width: ${width - (29 * 2)}px;
  align-self: center;
  overflow: hidden;
  /* background: red; */
`;

export const _Title = styled(TEXT)`
  margin: 40px 0;
`;

export const _Description = styled(TEXT)`
  margin: 0 0 30px;
`;

export const _ColoredPhoneWrapper = styled.View`
  flex-wrap: wrap;
  flex-direction: row;
  margin: 0 0 30px;
`;

export const _ColoredPhone = styled(TEXT)`
`;

export const _PhoneInputContainer = styled.View`
  display: flex;
  flex-direction: row;
  border-bottom-width: 1px;
  border-color: ${({ borderColor }) => borderColor ? borderColor : '#fff'};
  padding: 20px 0;
  margin: 0 0 20px;
  overflow: hidden;
  align-items: center;
  width: ${width + 15}px;
  /* background: blue; */
`;

export const _Row = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 20px;
  margin: 0 11px 0 0;
`;

export const _Flag = styled.Image`
  width: 17px;
  height: 10px;
  margin: 0 11px 0 0;
`;

export const _PhoneInput = styled.TextInput`
  font-size: 17px;
  color: #fff;
  padding: 0 2px;
  margin: 0 0 0 5px;
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  width: 100%;
`;

export const _Footer = styled.View`
  display: flex;
  flex-direction: row;
  align-self: center;
  position: absolute;
  bottom: 29px;
`;

export const _EnterButton = styled.TouchableOpacity`
  margin: 0 0 0 5px;
`;

export const _CodeInputWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin: 20px 0;
`;

export const _CodeInput = styled.TextInput`
  text-align: center;
  border-bottom-width: 1px;
  border-color: ${({ borderColor }) => borderColor ? borderColor : '#fff'};
  font-size: 20px;
  height: 50px;
  color: #fff;
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  width: 15%;
`;

export const _CodeInput_A = styled(_CodeInput)`
  width: 100%;
`;

export const _SendCodeAgainWrapper = styled.View`
  align-self: center;
  position: absolute;
  bottom: 29px;
`;

export const _SendCodeAgainText = styled(TEXT)`
  margin: 22px 0 0;
`;

export const _SendCodeAgainButton = styled.TouchableOpacity`
  margin: 8px 0 0;
`;