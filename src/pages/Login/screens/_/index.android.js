import React, { Component } from 'react';
import { KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Animated, Easing } from 'react-native';
import withLocalization from 'utils/withLocalization';

import { HEADER } from 'GLOBALS/HEADER';
import { TEXT } from 'GLOBALS/TEXT';
import BUTTON from 'GLOBALS/BUTTON';
import CROWD_ARROW from 'GLOBALS/CROWD_ARROW';

import brazilFlag from './img/flag.png';

import Routes from 'Routes';
import { navigateFromTo } from 'utils/navigateFromTo';
import LoadingModal from 'components/LoadingModal';

import APIS from 'apis';
import { Colors } from 'theme';

import ResendCodeTimer from './UI/ResendCodeTimer';
import PhoneInput from './UI/PhoneInput';

import ModalCountry from './UI/ModalCountry';

import countriesList from './UI/ModalCountry/countries.json';


import {
  _Container,
  _Title,
  _Description,
  _ColoredPhoneWrapper,
  _ColoredPhone,
  _PhoneInputContainer,
  _Row,
  _Flag,
  _PhoneInput,
  _Footer,
  _EnterButton,
  _CodeInputWrapper,
  _CodeInput_A,
  _SendCodeAgainWrapper,
  _SendCodeAgainText,
  _SendCodeAgainButton,
} from './styles';

const _PhoneInputContainerAnimated = Animated.createAnimatedComponent(_PhoneInputContainer);
const CROWD_ARROWAnimated = Animated.createAnimatedComponent(CROWD_ARROW);
const _CodeInputAnimated = Animated.createAnimatedComponent(_CodeInput_A);

class Login extends Component {

  constructor(props) {

    super(props);

    this.animations = {
      input: new Animated.Value(0),
      arrow: new Animated.Value(0),
      inputPosition: new Animated.Value(0),
      codeInput: new Animated.Value(0)
    }

    this.state = {
      phoneInputContainer: {
        inputColor: '#9B9B9B',
        // buttonIsDisabled: __DEV__ ? false : true,
        buttonIsDisabled: true,
      },
      // phoneInputMask: __DEV__ ? '(11) 96575-7319' : '',
      phoneInputMask: '',
      step: 'one',
      codeValue: '',
      timerIsOn: true,
      forwardedCode: false,
      isLoading: false,
      codeIsInvalid: false,
      modalCountryPosition: 0,
      selectedCountry: "BR"
    }

  }

  inputAnimation = (context) => {

    const { phoneInputMask } = this.state;
    const { input, arrow, inputPosition } = this.animations;

    Object.entries([input, arrow, inputPosition]).map((item, index) => {
      return (
        Animated.timing(item[1], {
          toValue: context === 'focus' ? 1 : 0,
          easing: Easing.in(),
          duration: 300,
        }).start()
      )
    })

    if (context === 'blur' && phoneInputMask.length === 15) {
      Animated.timing(input, {
        toValue: 2,
        easing: Easing.in(),
        duration: 300,
      }).start();
    }

  }

  inputMask = (numberLength, key, phoneInputMask) => {

    let maskedValue;
    if (isNaN(key)) return;

    if (numberLength === 0) {
      maskedValue = `(${key}`
    } else if (numberLength === 3) {
      maskedValue = `(${phoneInputMask.charAt(1)}${phoneInputMask.charAt(2)}) ${key}`
    } else if (numberLength === 10) {
      maskedValue = `${phoneInputMask}-${key}`
    } else {
      maskedValue = `${phoneInputMask}${key}`
    }

    return maskedValue;

  }

  handlePhoneInput = (value) => {

    const
      { phoneInputMask, phoneInputContainer } = this.state,
      { input: inputAnimated } = this.animations,
      { nativeEvent: { key } } = value,
      numberLength = phoneInputMask.length;

    if (isNaN(key) && key !== 'Backspace') return;

    let phoneInputValue = key === 'Backspace' ? phoneInputMask.slice(0, -1) : numberLength !== 15 ? this.inputMask(numberLength, key, phoneInputMask) : phoneInputMask;

    this.setState({ phoneInputMask: phoneInputValue })

    if (phoneInputValue.length === 15) {
      this.setState({
        phoneInputContainer: {
          ...phoneInputContainer,
          buttonIsDisabled: false
        }
      }, () => {
        Keyboard.dismiss()
        setTimeout(() => {
          Animated.timing(inputAnimated, {
            toValue: 2,
            easing: Easing.in(),
            duration: 300,
          }).start();
        }, 500)
      })
    } else {
      this.setState({
        phoneInputContainer: {
          ...phoneInputContainer,
          buttonIsDisabled: true
        }
      })
    }

  }

  codeInputAnimation = (context, value = null) => {

    const
      { codeValue, phoneInputContainer } = this.state,
      { codeInput } = this.animations,
      { _lastNativeSelection, _lastNativeText } = this.refs.codeInputAnimatedREF._component,
      codeValueLength = codeValue.length;

    let finalCode;

    if (/(blur|focus)/.test(context)) {

      Animated.timing(codeInput, {
        toValue: context === 'focus' ? 1 : 2,
        easing: Easing.in(),
        duration: 300,
      }).start();

    } else {

      if ((codeValueLength === 6 || isNaN(value)) && value !== 'Backspace') return;

      finalCode = value === 'Backspace' ? codeValue.slice(0, -1) : codeValueLength === 0 ? value : (codeValue + value);

      this.setState({
        codeValue: finalCode,
        phoneInputContainer: {
          ...phoneInputContainer,
          buttonIsDisabled: false
        }
      }, () => {
        if (codeValueLength === 5) {
          this.refs.codeInputAnimatedREF._component.blur();
          this.setState({
            phoneInputContainer: {
              ...phoneInputContainer,
              buttonIsDisabled: false
            }
          })
        }
      })

    }

  }

  clearCodeValueInput = () => {

    const { codeInput } = this.animations;

    Animated.timing(codeInput, {
      toValue: 0,
      duration: 100,
    }).start();

    this.setState({
      codeValue: ''
    })

    this.refs.codeInputAnimatedREF._component.setNativeProps({
      text: ''
    })

  }

  handleCallApi = (context, isResendCode = false) => {

    const { phoneInputMask, step, codeValue, phoneInputContainer, selectedCountry } = this.state;

    if (step === 'one') {
      setTimeout(() => {
        this.setState({
          phoneInputContainer: {
            ...phoneInputContainer,
            buttonIsDisabled: true
          },
          step: 'two'
        })
      }, 1500)
    } else if (step === 'two' && !isResendCode) {
      this.setState({
        isLoading: true
      })
    }

    if (isResendCode) {
      setTimeout(() => {
        this.setState({
          forwardedCode: true,
        }, () => {
          setTimeout(() => {
            this.setState({
              timerIsOn: true
            })
          }, 1000)
        })
      }, 500)
    }

    APIS.User[context](step === 'one' || isResendCode ? { code: countriesList.filter(country => country.acronym === selectedCountry)[0].code, phoneInputMask } : { phoneInputMask, codeValue }, (data) => {

      if (step === 'one' || isResendCode) return;

      const { phoneLogin: { jwt, success, userId } } = data;

      if (!success) {
        this.clearCodeValueInput();
        this.setState({
          isLoading: false,
          codeIsInvalid: true,
          phoneInputContainer: {
            ...phoneInputContainer,
            buttonIsDisabled: true
          }
        })
      } else {

        const updateUserOnStorage = {
          userID: userId,
          hasLogin: true,
          hasRegister: false,
          alreadyImportedContacts: false,
          _JWT: jwt
        }

        APP_STORAGE.updateItem(
          'User',
          updateUserOnStorage,
          () => {
            this.setState({ isLoading: false });
            navigateFromTo(Routes.LOGIN._, Routes.LOGIN.REGISTER, null,
              {
                userID: userId
              }
            );
          }
        );
      }

    });

  }

  changeTimerStateFN = () => {
    this.setState({
      timerIsOn: false,
      forwardedCode: false,
      phoneInputContainer: {
        ...this.state.phoneInputContainer,
        buttonIsDisabled: true
      }
    })
  }

  snapModalFN = () => {
    this.setState({
      modalCountryPosition: 1
    })
  }

  setModalCountryPositionFN = () => {
    this.setState({
      modalCountryPosition: 0
    })
  }

  setCountryFN = (selectedCountry) => {
    this.setState({ selectedCountry })
  }

  setUserPhone = (phoneInputMask) => {

    const { phoneInputContainer } = this.state;

    this.setState({
      phoneInputContainer: {
        ...phoneInputContainer,
        buttonIsDisabled: false
      },
      phoneInputMask
    })

  }

  clearUserPhoneFN = (onlyDisableButton = false) => {

    const { phoneInputContainer } = this.state;

    if (onlyDisableButton && phoneInputContainer.buttonIsDisabled === false) {
      this.setState({
        phoneInputContainer: {
          ...phoneInputContainer,
          buttonIsDisabled: true
        }
      })
    } else {
      this.setState({
        phoneInputContainer: {
          ...phoneInputContainer,
          buttonIsDisabled: true
        },
        phoneInputMask: ''
      })
    }

  }

  render() {

    const { t } = this.props;
    const { phoneInputContainer: { inputColor, buttonIsDisabled }, phoneInputMask, step, forwardedCode, isLoading, codeIsInvalid, codeValue, timerIsOn, modalCountryPosition, selectedCountry } = this.state;
    const { input, arrow, inputPosition, codeInput } = this.animations;

    const _PhoneInputContainerAnimatedInterpolate = input.interpolate({
      inputRange: [0, 1, 2],
      outputRange: ['#444', '#84C9F2', '#BCDEA1']
    })

    // FIX:  Alex voltar a setinha quando refatorar as UIs para Android
    // const arrowInterpolate = {
    //   // opacity: arrow.interpolate({
    //   //   inputRange: [0, 1],
    //   //   outputRange: [0, 1]
    //   // }),
    //   left: arrow.interpolate({
    //     inputRange: [0, 1],
    //     outputRange: [-10, 0]
    //   })
    // }
    // FIX: 

    const inputPositionInterpolate = inputPosition.interpolate({
      inputRange: [0, 1],
      // outputRange: [-15, 0] // FIX:  Alex voltar essa config para quando refatorar as UIs para Android
      outputRange: [0, 5]
    })

    const codeInputInterpolate = codeInput.interpolate({
      inputRange: [0, 1, 2],
      outputRange: ['#444', '#84C9F2', '#BCDEA1']
    })

    return (
      <>
        {
          isLoading &&
          <LoadingModal
            message={t('login:stepMessages:onLoadingMessage')}
            opacity={'rgba(0,0,0,.70)'}
            isGreen
          />
        }
        <KeyboardAvoidingView behavior="position">
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <_Container>
              <HEADER />
              <_Title
                euclid
                content={t(`login:stepMessages:${codeIsInvalid ? 'codeIsInvalid' : step}:title`)}
                fontSize={42}
                lineHeight={52}
                color={codeIsInvalid ? "#FFC701" : false}
              />
              {/* <_Description
                euclid
                content={t(`login:stepMessages:${codeIsInvalid ? 'codeIsInvalid' : step}:description`)}
                fontSize={24}
                lineHeight={30}
                color={codeIsInvalid ? "#FFC701" : false}
              /> */}
              {
                step === 'one' || codeIsInvalid ? (
                  <_Description
                    euclid
                    content={t(`login:stepMessages:${codeIsInvalid ? 'codeIsInvalid' : 'one'}:description`)}
                    fontSize={24}
                    lineHeight={35}
                    color={codeIsInvalid ? "#FFC701" : null}
                  />
                ) : (

                    (() => {

                      const splitText = t(`login:stepMessages:two:description`).split(' ');

                      return (
                        <_ColoredPhoneWrapper>
                          {
                            splitText.map((word, index) => {

                              const isPhone = /({phone})/.test(word);

                              return (
                                <_ColoredPhone
                                  key={index}
                                  euclid
                                  content={isPhone ? `${phoneInputMask} ` : `${word} `}
                                  fontSize={24}
                                  lineHeight={35}
                                  color={isPhone ? Colors.SUCCESS : null}
                                />
                              )
                            })
                          }
                        </_ColoredPhoneWrapper>
                      )

                    })()

                  )
              }
              {
                step === 'one'
                  ? (
                    <PhoneInput
                      snapModal={this.snapModalFN}
                      countriesList={countriesList}
                      selectedCountry={selectedCountry}
                      userPhone={(value) => this.setUserPhone(value)}
                      clearUserPhone={this.clearUserPhoneFN}
                    />
                    // <_PhoneInputContainerAnimated
                    //   borderColor={_PhoneInputContainerAnimatedInterpolate}
                    //   style={{
                    //     left: inputPositionInterpolate
                    //   }}>
                    //   {/* // FIX:  Alex voltar a setinha quando refatorar as UIs para Android
                    //     <CROWD_ARROWAnimated
                    //       orientation={"right"}
                    //       size={5}
                    //       style={[{ top: 0, color: "#84C9F2" }, arrowInterpolate]}
                    //     /> 
                    //   */}
                    //   <_Flag
                    //     source={brazilFlag}
                    //   />
                    //   <TEXT
                    //     content={"BR + 55"}
                    //     fontSize={14}
                    //     color={inputColor}
                    //   />
                    //   <_PhoneInput
                    //     placeholder={"(XX) 0000-0000"}
                    //     placeholderTextColor={inputColor}
                    //     keyboardType="default"
                    //     onFocus={() => this.inputAnimation('focus')}
                    //     onBlur={() => this.inputAnimation('blur')}
                    //     onKeyPress={this.handlePhoneInput}
                    //     value={phoneInputMask !== '' ? phoneInputMask : undefined}
                    //   />
                    // </_PhoneInputContainerAnimated>
                  ) : (
                    <_CodeInputAnimated
                      borderColor={codeInputInterpolate}
                      // key={item.toString()}
                      ref={'codeInputAnimatedREF'}
                      placeholder={"0 0 0 0 0 0"}
                      placeholderTextColor={inputColor}
                      keyboardType="default"
                      maxLength={6}
                      onFocus={() => this.codeInputAnimation('focus')}
                      onBlur={() => this.codeInputAnimation('blur')}
                      onKeyPress={(el) => this.codeInputAnimation('change', el.nativeEvent.key)}
                      value={codeValue}
                    />
                  )
              }
              <BUTTON
                onPressFn={() => this.handleCallApi(step === 'one' ? 'sendSMStoUser' : 'validateUserCode')}
                color={!buttonIsDisabled ? '#BCDEA1' : null}
                disabled={buttonIsDisabled}
                content={t(`login:stepMessages:${step}:button`)}
                isAndroid
              />
              {
                step === 'two' &&
                <ResendCodeTimer
                  timerIsOn={timerIsOn}
                  changeTimerState={() => this.changeTimerStateFN()}
                />
              }
              {
                step === 'one'
                  ? (
                    <_Footer>
                      {/* <TEXT // TODO:  Alex -> Habilitar quando tiver a função 
                        content={t('login:hasAnAccount')}
                        fontSize={14}
                        color={"#9B9B9B"}
                      />
                      <_EnterButton onPress={() => navigateFromTo(Routes.LOGIN._, Routes.LOGIN.LEGACY)}>
                        <TEXT
                          content={t('login:hasAnAccountEnter')}
                          fontSize={14}
                          color={"#fff"}
                        />
                      </_EnterButton> */}
                    </_Footer>
                  ) : (
                    <>
                      {
                        !timerIsOn &&
                        <_SendCodeAgainWrapper>
                          <_SendCodeAgainText
                            align={'center'}
                            color={forwardedCode ? "#BCDEA1" : "#9B9B9B"}
                            fontSize={14}
                            content={forwardedCode ? t('login:stepMessages:sendCodeAgain:forwardedCode') : t('login:stepMessages:sendCodeAgain:message')}
                          />
                          <_SendCodeAgainButton disabled={forwardedCode} onPress={() => this.handleCallApi('sendSMStoUser', true)}>
                            <TEXT
                              align={'center'}
                              content={t('login:stepMessages:sendCodeAgain:button')}
                              fontSize={14}
                              color={forwardedCode ? "#9B9B9B" : "#fff"}
                            />
                          </_SendCodeAgainButton>
                        </_SendCodeAgainWrapper>
                      }
                    </>
                  )
              }
            </_Container>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
        <ModalCountry
          modalCountryPosition={modalCountryPosition}
          setModalCountryPosition={this.setModalCountryPositionFN}
          countriesList={countriesList}
          setCountry={(country) => this.setCountryFN(country)}
        />
      </>
    )

  }

}

export default withLocalization(Login);