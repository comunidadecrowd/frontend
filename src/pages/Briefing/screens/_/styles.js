/* eslint-disable */
import {Platform} from 'react-native';
import styled from 'styled-components/native';
import Column from 'components/Grid/Column';
import { Colors } from 'theme/Colors';

const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  background: ${Colors.BLACK};
  flex-grow: 1;
  padding: 0 29px 29px;
  justify-content: space-between;
`;

export const _Column = styled(Column)`
  align-self: center;
`;

export const _HeadLine = styled.Text`
  color: ${Colors.WHITE};
  text-align: left;
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  font-size: 44px;
  line-height: 44px;
  letter-spacing: 0;
`;