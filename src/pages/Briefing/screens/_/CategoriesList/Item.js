import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';
import withLocalization from 'utils/withLocalization';

import designAnimation from 'assets/animations/icons/design.json';
import technologyAnimation from 'assets/animations/icons/technology.json';
import contentAnimation from 'assets/animations/icons/content.json';
import midiaAnimation from 'assets/animations/icons/midia.json';

import { Icons } from 'components/Icon';
// import { BriefingRoutes } from 'constants/Routes';
import Routes from 'Routes';

import { _Item, _ItemContent, _LottieAnimation, _Icon, _Label } from './styles';

class CategoriesItem extends Component {
  static propTypes = {
    category: PropTypes.shape({
      slug: PropTypes.string.isRequired,
    }).isRequired,

    // functions
    t: PropTypes.func.isRequired,
  };

  _animation = null;

  _handlePress = category => {
    Navigation.push(Routes.BRIEFING._, {
      component: {
        name: Routes.BRIEFING.CATEGORY,
        id: Routes.BRIEFING.CATEGORY,
        passProps: {
          slug: category,
          handleBack: this.goBackToCategories,
        },
        options: {
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
          topBar: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
        },
      },
    });
  };

  goBackToCategories = () => {
    Navigation.popTo(Routes.BRIEFING._);
  };

  componentDidMount() {
    this._animation.play && this._animation.play();
  }

  render() {
    const {
      category: { slug },
      t,
    } = this.props;
    let iconAnimation = null;

    /**
     * React Native doesn't handle require with dynamic strings.
     */
    switch (slug) {
      case 'design':
        iconAnimation = designAnimation;
        break;
      case 'technology':
        iconAnimation = technologyAnimation;
        break;
      case 'content':
        iconAnimation = contentAnimation;
        break;
      case 'midia':
        iconAnimation = midiaAnimation;
        break;
    }

    return (
      <_Item onPress={() => this._handlePress(slug)}>
        <_ItemContent>
          {iconAnimation ? (
            <_LottieAnimation
              ref={animation => {
                this._animation = animation;
              }}
              source={iconAnimation}
              autoplay={true}
              loop={false}
            />
          ) : (
            <_Icon icon={Icons[slug.toUpperCase().replace(/-/g, '_')]} />
          )}
          <_Label>{t(`categories:${slug}`).toLowerCase()}</_Label>
        </_ItemContent>
      </_Item>
    );
  }
}

export default withLocalization(CategoriesItem);
