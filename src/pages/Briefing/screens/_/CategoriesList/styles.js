/* eslint-disable */
import styled from 'styled-components/native';
import Row from 'components/Grid/Row';
import Column from 'components/Grid/Column';
import Lottie from 'components/Lottie';
import { Colors } from 'theme/Colors';
import Icon, { Icons } from 'components/Icon';

export const _Container = styled(Row)`
  justify-content: space-around;
  flex-wrap: wrap;
`;

export const _Column = styled(Column)`
  margin-top: 32px;
  margin-right: 0;
`;

export const _Item = styled.TouchableOpacity`
  align-items: center;
`;

export const _ItemContent = styled.View``;

export const _LottieAnimation = styled(Lottie)`
  width: 68px;
  height: 68px;
`;

export const _Icon = styled(Icon)`
  color: ${Colors.WHITE}l
  font-size: 80px;
  text-align: center;
  height: 90px;
`;

export const _Label = styled.Text`
  color: ${Colors.WHITE};
  text-align: center;
  font-family: Inter UI;
  font-weight: normal;
  font-style: normal;
  font-size: 16px;
  line-height: 26px;
  letter-spacing: 0px;
`;