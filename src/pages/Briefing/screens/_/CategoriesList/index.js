import React, { Component } from 'react';
import PropTypes from 'prop-types';
import withDimensions from 'utils/withDimensions';

import Category from './Item';
import { _Container, _Column } from './styles';

class CategoriesList extends Component {
  static propTypes = {
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
      })
    ).isRequired,
  };

  render() {
    const { categories } = this.props;

    return (
      <_Container>
        {categories.map((item, index) => (
          <_Column key={index} size={6} sizeMedium={3} withMargin={false}>
            <Category category={item} />
          </_Column>
        ))}
      </_Container>
    );
  }
}

export default withDimensions(CategoriesList);
