import React, { Component } from 'react';
import { Navigation } from 'react-native-navigation';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';
import { withProps } from 'recompose';
import withLocalization from 'utils/withLocalization';

import {HEADER} from 'GLOBALS/HEADER';
import BUTTON from 'GLOBALS/BUTTON';
import Routes from 'Routes';

import CategoriesList from './CategoriesList';
import CATEGORIES from './../_constants';

import { _Container, _Column, _HeadLine } from './styles';

class Brefing extends Component {
  static propTypes = {
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
      })
    ).isRequired,

    // functions
    t: PropTypes.func.isRequired,
  };

  _handlePress = category => {
    Navigation.push(Routes.BRIEFING._, {
      component: {
        name: Routes.BRIEFING.SUBCATEGORY,
        id: Routes.BRIEFING.SUBCATEGORY,
        passProps: {
          category: 'others',
          slug: 'others',
        },
        options: {
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
          topBar: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
        },
      },
    });
  };

  render() {
    const { t, categories } = this.props;

    return (
      <_Container>
        <HEADER logoShort paddingTop />
        <_Column size={12} sizeMedium={6} sizeLarge={4}>
          <_HeadLine>{t('briefing:CATEGORY_TITLE')}</_HeadLine>
        </_Column>
        <CategoriesList categories={categories} />
        <BUTTON onPressFn={this._handlePress} content={t('briefing:buttonOthers')} />
      </_Container>
    );
  }
}

export default withProps({ categories: CATEGORIES })(withLocalization(Brefing));
