import {Platform} from 'react-native';
import styled from 'styled-components/native';
import Icon from 'components/Icon';
import { Colors } from 'theme/Colors';

const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  background: ${Colors.BLACK};
  justify-content: center;
`;

export const _Column = styled.View`
  padding: 20px 20px;
`;

export const _CloseWrapper = styled.TouchableOpacity`
  align-items: flex-end;
`;

export const _TitleWrapper = styled.View`
  margin-bottom: 10px;
`;

export const _Label = styled.Text`
  color: ${Colors.WHITE};
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  font-size: 42px;
  line-height: 64px;
  letter-spacing: 0;
`;

export const _Icon = styled(Icon)`
  color: ${Colors.WHITE};
  font-size: ${({size}) => size ? size : '100'}px;
  margin-left: -16px;
`;

export const _ListWrapper = styled.View`
  justify-content: center;
`;

export const _ListItem = styled.TouchableOpacity`
  flex-direction: row;
  align-self: flex-start;
  align-items: center;
  justify-content: space-between;
  margin: 3px 0 0 0;
`;

export const _LabelItem = styled.Text`
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  font-size: 36px;
  line-height: 38px;
  letter-spacing: 0;
`;