import React, { Component } from 'react';
import { Animated, Platform } from 'react-native';
import { compose } from 'recompose';
import { withRouter } from 'react-router-native';
import { Navigation } from 'react-native-navigation';
import withAnimation from 'utils/withAnimation';
import withLocalization from 'utils/withLocalization';
import Routes from 'Routes';

import { Colors } from 'theme';
import CROWD_ARROW from 'GLOBALS/CROWD_ARROW';

import { _ListItem, _LabelItem } from './../styles';

class SubCategoriesItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animations: {
        itemLabel: new Animated.Value(0),
      },
    };
  }

  onPressCallback = () => {
    const { animations: { itemLabel } } = this.state;

    Animated.timing(itemLabel, {
      toValue: 1,
      duration: 200,
    }).start(() => {
      this.onPressFn();
      setTimeout(() => {
        Animated.timing(itemLabel, {
          toValue: 0,
          duration: 200,
        }).start();
      }, 1000);
    });
  };

  onPressFn = () => {
    const {
      category,
      subCategory: { slug },
    } = this.props;

    Navigation.push(Routes.BRIEFING.CATEGORY, {
      component: {
        name: Routes.BRIEFING.SUBCATEGORY,
        id: Routes.BRIEFING.SUBCATEGORY,
        passProps: {
          category,
          slug,
        },
        options: {
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
          topBar: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
        },
      },
    });
  };

  render() {
    const {
      subCategory: { slug },
      t,
    } = this.props;

    const { animations: { itemLabel } } = this.state;

    const arrowOpaciyInterpolate = itemLabel.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 1],
    });

    const showArrowInterpolate = itemLabel.interpolate({
      inputRange: [0, 1],
      outputRange: [-12, 5],
    });

    const colorInterpolate = itemLabel.interpolate({
      inputRange: [0, 1],
      outputRange: [Colors.GRAY_DARK, Colors.BLUE],
    });``

    const marginTextInterpolate = itemLabel.interpolate({
      inputRange: [0, 1],
      outputRange: [0, 0],
    });

    const opacityAnimated = {
      opacity: arrowOpaciyInterpolate,
      marginLeft: showArrowInterpolate,
      transform: [{
        translateX: showArrowInterpolate
      }],
    };

    const colorAnimated = {
      color: colorInterpolate,
    };

    const marginTextAnimated = {
      marginLeft: marginTextInterpolate,
    };

    const AnimatedCROWD_ARROW = Animated.createAnimatedComponent(CROWD_ARROW);

    return (
      <_ListItem onPress={this.onPressCallback}>
        <AnimatedCROWD_ARROW orientation={'right'} size={6} style={[opacityAnimated, colorAnimated]} />
        <Animated.Text style={[marginTextAnimated, colorAnimated]}>
          <_LabelItem>{t(`categories:${slug}`).toLowerCase()}</_LabelItem>
        </Animated.Text>
      </_ListItem>
    );
  }
}

export default compose(
  withRouter,
  withLocalization,
  withAnimation('hovering')
)(SubCategoriesItem);
