import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SubCategoryItem from './Item';

class SubCategoriesList extends Component {
  static propTypes = {
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
      })
    ).isRequired,
  };

  render() {
    const { categories, category } = this.props;

    return categories.map((item, index) => <SubCategoryItem category={category} subCategory={item} key={index} />);
  }
}

export default SubCategoriesList;
