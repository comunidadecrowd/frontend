import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { compose, withProps } from 'recompose';
import { withRouter } from 'react-router-native';
import withLocalization from 'utils/withLocalization';

import { Icons } from 'components/Icon';

import SubCategoriesList from './SubCategoriesList';
import CATEGORIES from './../_constants';

import { _Container, _Column, _TitleWrapper, _Label, _Icon, _CloseWrapper, _ListWrapper } from './styles';

class SubCategory extends Component {
  static propTypes = {
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
      })
    ).isRequired,
    slug: PropTypes.string.isRequired,

    // functions
    t: PropTypes.func.isRequired,
  };

  render() {
    const { handleBack, slug: category, categories, t } = this.props;

    return (
      <_Container>
        <_Column>
          <_CloseWrapper onPress={handleBack}>
            <_Icon icon={Icons['CLOSE']} size={30} />
          </_CloseWrapper>
          <_TitleWrapper>
            <_Icon icon={Icons[category.toUpperCase().replace(/-/g, '_')]} />
            <_Label>{t(`categories:${category}`).toLowerCase()}</_Label>
          </_TitleWrapper>
          <_ListWrapper>
            <SubCategoriesList category={category} categories={categories} />
          </_ListWrapper>
        </_Column>
      </_Container>
    );
  }
}

export default compose(
  withLocalization,
  withRouter,
  withProps(({ slug }) => {
    const category = CATEGORIES.find(item => item.slug === slug);

    return {
      categories: category ? category.subcategories : null,
    };
  })
)(SubCategory);
