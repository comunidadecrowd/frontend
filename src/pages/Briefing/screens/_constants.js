export default [
  {
    slug: 'design',
    subcategories: [
      {
        slug: 'logotipo',
      },
      {
        slug: 'layout app/site',
      },
      {
        slug: 'apresentação',
      },
      {
        slug: 'peças digitais',
      },
      {
        slug: 'Material Impresso',
      },
      {
        slug: 'others',
      },
      // {
      //   slug: 'material gráfico',
      // },
      // {
      //   slug: 'ux/ui',
      // },
      // {
      //   slug: 'webdesign',
      // },
      // {
      //   slug: 'ilustração',
      // },
      // {
      //   slug: 'others',
      // },
    ],
  },
  {
    slug: 'technology',
    subcategories: [
      {
        slug: 'app/plataforma',
      },
      {
        slug: 'site',
      },
      {
        slug: 'ecommerce',
      },
      {
        slug: 'landing page',
      },
      {
        slug: 'seo',
      },
      {
        slug: 'others',
      },
      // {
      //   slug: 'app',
      // },
      // {
      //   slug: 'ecommerce',
      // },
      // {
      //   slug: 'web',
      // },
      // {
      //   slug: 'landing page',
      // },
      // {
      //   slug: 'sistema',
      // },
      // {
      //   slug: 'others',
      // },
    ],
  },
  {
    slug: 'content',
    subcategories: [
      {
        slug: 'artigos c/ seo',
      },
      {
        slug: 'redes sociais',
      },
      {
        slug: 'vídeo',
      },
      {
        slug: 'Redação Publicitária',
      },
      {
        slug: 'marketing pessoal',
      },
      {
        slug: 'others',
      },
      // {
      //   slug: 'tradução',
      // },
      // {
      //   slug: 'rede social',
      // },
      // {
      //   slug: 'artigo',
      // },
      // {
      //   slug: 'foto',
      // },
      // {
      //   slug: 'vídeo',
      // },
      // {
      //   slug: 'others',
      // },
    ],
  },
  {
    slug: 'midia',
    subcategories: [
      {
        slug: 'google ads',
      },
      {
        slug: 'social ads',
      },
      {
        slug: 'mídia offline',
      },
      {
        slug: 'email mkt',
      },
      {
        slug: 'influenciadores',
      },
      {
        slug: 'others',
      },
      // {
      //   slug: 'seo',
      // },
      // {
      //   slug: 'facebook ads',
      // },
      // {
      //   slug: 'google ads',
      // },
      // {
      //   slug: 'influenciadores',
      // },
      // {
      //   slug: 'performance',
      // },
      // {
      //   slug: 'others',
      // },
    ],
  },
];
