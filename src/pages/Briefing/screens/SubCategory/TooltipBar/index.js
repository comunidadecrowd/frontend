import React from 'react';
import PropTypes from 'prop-types';
import { ViewPropTypes } from 'react-native';
import { withRouter } from 'react-router-native';
import { compose } from 'recompose';
// import { BlurView } from 'react-native-blur'; // NOTE:  O react-native-blur foi removido do projeto em 04/06 devido a problemas no build da Release do Android

import { Component } from 'components/BaseComponent';
import Button from 'components/Button';
import { withGlobalsContext } from 'components/GlobalsProvider';
import { Icons } from 'components/Icon';
import TooltipControlItem from 'components/TooltipControlItem';

import CustomPropTypes from 'constants/CustomPropTypes';
import { BriefingRoutes } from 'constants/Routes';

import withLocalization from 'utils/withLocalization';
import withDimensions from 'utils/withDimensions';

import {
  _ContainerScroll,
  _Container,
  _IconsHeader,
  _SideBar,
  _WrapperCloseIcon,
  _CloseIcon,
  _TooltipControl,
  _DetailsWrapper,
  _IconDetails,
  _TitleDetails,
  _ContentDetails,
  _WrapperButtonAction,
  _ButtonInsertText,
} from './styles';

class TooltipBar extends Component {
  static propTypes = {
    currentIndex: PropTypes.number,
    history: PropTypes.any.isRequired,
    openIndex: PropTypes.number,
    shouldStartOpen: PropTypes.bool,
    style: ViewPropTypes.style,
    tips: PropTypes.arrayOf(
      PropTypes.shape({
        icon: CustomPropTypes.icon,
        title: PropTypes.string,
        description: PropTypes.string,
      })
    ),
    windowHeight: PropTypes.number.isRequired,

    // functions
    t: PropTypes.func.isRequired,
    toggleFooter: PropTypes.func.isRequired,
  };

  static defaultProps = {
    currentIndex: 0,
    openIndex: null,
    shouldStartOpen: false,
    style: null,
    tips: [],
  };

  state = { openTipIndex: this.props.shouldStartOpen ? 0 : null };

  _toggleFooter = () => {
    const isOpen = this.state.openTipIndex !== null;

    this.props.toggleFooter(!isOpen);
  };

  _handleOpenTip = index => {
    const { handleTooltipOpen } = this.props;

    this.setState({ openTipIndex: this.state.openTipIndex === index ? null : index });
    handleTooltipOpen(index);
  };

  _handleClose = () => {
    const { history, handleTooltipClose } = this.props;
    const { openTipIndex } = this.state;

    if (openTipIndex === null) {
      history.push(BriefingRoutes.CATEGORY);
    } else {
      this.setState({ openTipIndex: null });
      handleTooltipClose();
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.openIndex !== prevProps.openIndex) {
      this.setState({ openTipIndex: this.props.openIndex });
    }

    if (this.state.openTipIndex !== prevState.openTipIndex) {
      this._toggleFooter();
    }
  }

  componentDidMount() {
    this._toggleFooter();
  }

  template(css) {
    const { tips, style, currentIndex, t, windowHeight } = this.props;
    const { openTipIndex } = this.state;

    return (
      <_Container hasClose={openTipIndex !== null}>
        <_IconsHeader hasClose={openTipIndex !== null}>
          <_WrapperCloseIcon
            onPress={this._handleClose}
            showing={openTipIndex !== null}
          >
            <_CloseIcon icon={Icons.CLOSE} />
          </_WrapperCloseIcon>
          <_TooltipControl showing={openTipIndex === null}>
            {tips.map((item, index) => (
              <TooltipControlItem
                key={index}
                style={css([index !== currentIndex && 'hidden'], 'visible')}
                isActive={openTipIndex === index}
                onPress={() => this._handleOpenTip(index)}
              />
            ))}
          </_TooltipControl>
        </_IconsHeader>
        <_ContainerScroll
          ref={ref => this.containerScroll = ref}
          onContentSizeChange={() => this.containerScroll.scrollTo({x: 0, y: 0, animated: true})}
          scrollEnabled={true}
          showing={openTipIndex !== null}>
          {/* <BlurView // NOTE:  O react-native-blur foi removido do projeto em 04/06 devido a problemas no build da Release do Android
            style={css('absolute')}
            // viewRef={this.state.viewRef}
            blurType="dark"
            blurAmount={10}
          /> */}
          {/* // REFACT:  _SideBar será usado somente na versão web - IS_WEB or TooltipBar.web.js */}
          {/* <_SideBar showing={openTipIndex !== null}>
            <_TooltipControl showing={openTipIndex !== null}>
              {tips.map((item, index) => (
                <TooltipControlItem
                  key={index}
                  style={css(['tooltipIcon', index !== currentIndex && 'hidden'], 'visible')}
                  isActive={openTipIndex === index}
                  onPress={() => this._handleOpenTip(index)}
                />
              ))}
            </_TooltipControl>
          </_SideBar> */}
          {/* // REFACT:  _SideBar será usado somente na versão web - IS_WEB or TooltipBar.web.js */}

          {openTipIndex !== null && (
            <_DetailsWrapper>
              <_IconDetails icon={tips[openTipIndex].icon} />
              <_TitleDetails>{tips[openTipIndex].title}</_TitleDetails>
              <_ContentDetails>{tips[openTipIndex].description}</_ContentDetails>
              <_WrapperButtonAction>
                <_ButtonInsertText
                  onPressFn={this._handleClose}
                  content={t('briefing:TOOLTIP_INSERT_TEXT')}
                />
              </_WrapperButtonAction>
              {/* <Button style={css('button', 'hidden')} onPress={this._handleClose}>
                {t('briefing:TOOLTIP_INSERT_TEXT')}
              </Button> */}
            </_DetailsWrapper>
          )}
        </_ContainerScroll>
      </_Container>
    );
  }
  styles() {
    return {
      rootMedium: { left: 'auto' },
      absolute: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
      },

      control: { marginTop: 0 },

      controlOpen: { marginTop: 32 },

      controlMedium: { marginTop: 200 },

      tooltipIcon: {
        marginBottom: 88,
      },

      button: { marginTop: 32 },

      visible: {
        display: 'flex',
        opacity: 1,
      },

      hidden: { display: 'none' },
    };
  }
}

export default compose(
  withLocalization,
  withDimensions,
  withRouter,
  withGlobalsContext
)(TooltipBar);
