import styled from 'styled-components/native';
import { Colors } from 'theme/Colors';
import { Dimensions, Platform } from 'react-native';
import { MARGIN_HORIZONTAL } from 'constants/Layout';
import BUTTON from 'GLOBALS/BUTTON';
import Icon from 'components/Icon';

const { width, height } = Dimensions.get('window');
const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  width: ${({ hasClose }) => hasClose ? width : '42px'};
  height: ${({ hasClose }) => hasClose ? height + 10 : '84px'};
  position: absolute;
  top: ${({ hasClose }) => hasClose ? -52 : 0}px;
  right: 0;
  background: ${({ hasClose }) => hasClose ? Colors.BLACK : Colors.WHITE};
  z-index: 2;
`;

export const _ContainerScroll = styled.ScrollView`
  flex: 1;
  display: ${({ showing }) => showing ? 'flex' : 'none'};
  padding: 10px 0 0;
  background: ${Colors.BLACK};
  z-index: 2;
`;

export const _IconsHeader = styled.View`
  width: 32px;
  position: ${({ hasClose }) => hasClose ? 'absolute' : 'relative'};
  top: ${({ hasClose }) => hasClose ? 32 : 13}px;
  right: ${({ hasClose }) => hasClose ? 10 : 0}px;
  flex-direction: ${({ hasClose }) => hasClose ? 'column' : 'row'};
  align-content: space-between;
  z-index: 3;
`;

export const _WrapperCloseIcon = styled.TouchableHighlight`
  display: ${({ showing }) => showing ? 'flex' : 'none'};
  opacity: ${({ showing }) => showing ? 1 : 0};
`;

export const _CloseIcon = styled(Icon)`
  color: ${Colors.WHITE};
  font-size: 30px;
`;

export const _TooltipControl = styled.View`
  margin-top: ${({ showing }) => showing ? 42 : 42}px;
`;

export const _SideBar = styled.View`
  display: none;
  width: 92px;
  top: ${({ showing }) => showing ? 32 : 32}px;
  right: 0;
  bottom: 200px;
  flex-direction: column;
  position: absolute;
  align-items: center;
  z-index: 1;
`;

export const _DetailsWrapper = styled.View`
  justify-content: center;
  padding: 30px ${MARGIN_HORIZONTAL + 10}px 30px ${MARGIN_HORIZONTAL}px;
  background: ${Colors.BLACK};
`;

export const _IconDetails = styled(Icon)`
  color: ${Colors.WHITE};
  font-size: 58px;
  margin-left: -16px;
  margin-bottom: 4px;
`;

export const _TitleDetails = styled.Text`
  color: ${Colors.WHITE};
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  font-size: 44px;
  lineHeight: 44px;
  letterSpacing: 0;
  margin-bottom: 32px;
`;

export const _ContentDetails = styled.Text`
  color: ${Colors.WHITE};
  text-align: left;
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  font-size: 16px;
  line-height: 26px;
  letter-spacing: 0;
`;

export const _WrapperButtonAction = styled.View`
  margin-top: 32px;
`;

export const _ButtonInsertText = styled(BUTTON)``;
