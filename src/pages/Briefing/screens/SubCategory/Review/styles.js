import { Dimensions, Platform } from 'react-native';
import styled from 'styled-components/native';
import { Colors } from 'theme/Colors';
import BUTTON from 'GLOBALS/BUTTON';
import { TEXT } from 'GLOBALS/TEXT';
import Icon from 'components/Icon';
import { isSmallIOS } from 'utils/Helpers';

const { width, height } = Dimensions.get('window');
const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  display: ${({ active }) => active ? 'flex' : 'none'};
  align-content: space-between;
  height: ${height - 130};
`;

export const _ScrollContainer = styled.ScrollView`
  flex: 1;
  padding: ${isSmallIOS ? `0 10px 0 0` : `0 7px 0 0`};
`;

export const _SectionTitle = styled.Text`
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  font-size: 36px;
  line-height: 38px;
  letter-spacing: 0;
  margin-bottom: 24px;
`;

export const _WrapperFields = styled.View`
  min-height: ${(height / 2) + 10};
`;

export const _Field = styled.View`
  flex-direction: row;
`;

export const _Icon = styled(Icon)`
  font-size: 30px;
  color: ${({ goBackStep }) => goBackStep ? Colors.BLUE : Colors.GRAY_DARKER};
  margin-right: 8px;
  z-index: 1;
`;

export const _TimeLineItem = styled.TouchableOpacity`
  flex-direction: row;
`;

export const _TextTitle = styled.Text`
  width: 90%;
  flex-wrap: wrap;
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  font-size: 36px;
  line-height: 36px;
  letter-spacing: 0;
  color: ${({ goBackStep }) => goBackStep ? Colors.BLUE : Colors.GRAY_DARKER};
  margin-bottom: 32px;
  padding-right: 20px;
`;

export const _TextObjective = styled.Text`
  width: 90%;
  flex-wrap: wrap;
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  font-size: 18px;
  line-height: 23px;
  letter-spacing: 0;
  color: ${({ goBackStep }) => goBackStep ? Colors.BLUE : Colors.GRAY_DARKER};
  margin: 4px 0 32px 0;
  padding-right: 20px;
`;

export const _TextBriefing = styled.Text`
  width: 90%;
  flex-wrap: wrap;
  font-family: ${(isiOS ? 'Inter UI' : 'inter-ui-regular')};
  color: ${({ goBackStep }) => goBackStep ? Colors.BLUE : Colors.GRAY_DARKER};
  margin-top: 4px;
  padding-right: 20px;
`;

export const _WrapperButton = styled.View`
  margin: 20px 0;
`;

export const _ButtonSubmit = styled(BUTTON).attrs({ color: Colors.GREEN })``;

export const _MicrocopyText = styled(TEXT)``;
