import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { PureComponent } from 'components/BaseComponent';
import withLocalization from 'utils/withLocalization';

import { Icons } from 'components/Icon';
import { Colors } from 'theme';

import {
  _Container,
  _ScrollContainer,
  _SectionTitle,
  _WrapperFields,
  _Field,
  _Icon,
  _TimeLineItem,
  _TextTitle,
  _TextObjective,
  _TextBriefing,
  _WrapperButton,
  _ButtonSubmit,
  _MicrocopyText,
} from './styles';

class Review extends PureComponent {
  static propTypes = {
    briefing: PropTypes.string.isRequired,
    objective: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,

    // functions
    onPressStep: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      titleGoBackChange: false,
      objectiveGoBackChange: false,
      briefingGoBackChange: false,
    }
  }

  _handleOnPress = (field, step) => {
    const { onPressStep } = this.props;

    this.setState({ [`${field}GoBackChange`]: true });
    setTimeout(() => {
      onPressStep(step);
      this.setState({ [`${field}GoBackChange`]: false });
    }, 250);
  }

  template(css) {
    const { t, active, title, objective, briefing, onSubmit } = this.props;
    const { titleGoBackChange, objectiveGoBackChange, briefingGoBackChange } = this.state;

    return (
      <_Container active={active}>
        <_SectionTitle>Vamos revisar?</_SectionTitle>
        <_ScrollContainer scrollEnabled={true}>
          <_WrapperFields>
            <_Field>
              <View style={css(['timeline', 'timelineTitle'])} />
              <_TimeLineItem onPress={() => this._handleOnPress('title', 0)} activeOpacity={1}>
                <_Icon goBackStep={titleGoBackChange} icon={Icons.TOOLTIP} />
                <_TextTitle goBackStep={titleGoBackChange}>{title}</_TextTitle>
              </_TimeLineItem>
            </_Field>
            <_Field>
              <View style={css(['timeline', 'timelineObjective'])} />
              <_TimeLineItem onPress={() => this._handleOnPress('objective', 1)} activeOpacity={1}>
                <_Icon goBackStep={objectiveGoBackChange} icon={Icons.TOOLTIP} />
                <_TextObjective goBackStep={objectiveGoBackChange}>{objective}</_TextObjective>
              </_TimeLineItem>
            </_Field>
            <_Field>
              <_TimeLineItem onPress={() => this._handleOnPress('briefing', 2)} activeOpacity={1}>
                <_Icon goBackStep={briefingGoBackChange} icon={Icons.TOOLTIP} />
                <_TextBriefing goBackStep={briefingGoBackChange}>{briefing}</_TextBriefing>
              </_TimeLineItem>
            </_Field>
          </_WrapperFields>
          <_WrapperButton>
            <_ButtonSubmit
              normalText
              content={t('briefing:BRIEFING_SUBMIT')}
              onPressFn={onSubmit}
            />
            <_MicrocopyText
              fontSize={10}
              align={'center'}
              color={Colors.GRAY}
              content={t('briefing:BRIEFING_SUBMIT_MICROCOPY')}
            />
          </_WrapperButton>
        </_ScrollContainer>
      </_Container>
    );
  }

  styles() {
    return {
      icon: {
        fontSize: 30,
        color: Colors.GRAY_DARKER,
        marginRight: 8,
        zIndex: 1,
      },

      iconTitle: { marginTop: 8 },

      timeline: {
        position: 'absolute',
        left: 15,
        width: 1,
        borderLeftWidth: 1,
        borderLeftColor: Colors.GRAY_DARKER,
      },

      timelineTitle: {
        bottom: -8,
        top: 32,
      },

      timelineObjective: {
        bottom: -8,
        top: 24,
      },

      submitWrapper: {
        marginTop: 24,
        marginHorizontal: 'auto',
      },

      hidden: { display: 'none' },
    };
  }
}

export default withLocalization(Review);
