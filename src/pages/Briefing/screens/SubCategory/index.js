import { compose, lifecycle } from 'recompose';

import React from 'react';
import PropTypes from 'prop-types';
import { Platform, Keyboard } from 'react-native';
import { withRouter } from 'react-router-native';
import { Navigation } from 'react-native-navigation';

// Components
import { Component } from 'components/BaseComponent';
import ButtonForward from 'components/ButtonForward';
import { Icons } from 'components/Icon';
import Input from 'components/Input';
import { withTheme } from 'components/ThemeProvider';
import RichInput from 'components/RichInput';
import RichInputControls from 'components/RichInputControls';
// import TooltipBar from 'components/TooltipBar'; // REFACT:  remover após concluir novo tooltipbar para mobile (./TooltipBar)
import TooltipBar from './TooltipBar';

// Constants
import CustomPropTypes from 'constants/CustomPropTypes';
import { MARGIN_HORIZONTAL } from 'constants/Layout';
import Routes from 'Routes';
import { THEME_WHITE } from 'constants/Themes';

// Theme
import { Colors } from 'theme/Colors';
import { TextStyles } from 'theme/TextStyles';
import { SMALL } from 'theme/Breakpoints';
import LogoShort from 'assets/svgs/LogoShort';

// Utils
import withDimensions from 'utils/withDimensions';
import withLocalization from 'utils/withLocalization';
import { navigateFromTo } from 'utils/navigateFromTo';

import Review from './Review';

import {
  _Container,
  _KeyboardAvoidingView,
  _NavigationHeader,
  _NavigationPrev,
  _IconPrev,
  _NavigationNext,
  _IconNext,
  _Row,
  _Column,
  _CategoryIcon,
  _Section,
  _TitleLabel,
  _ControlsBar,
  _ControlsBarWrapperRich,
  _ControlsBarWrapperButton,
} from './styles';

const IS_WEB = Platform.OS === 'web';
const INPUT_TITLE = 'title';
const INPUT_OBJECTIVE = 'objective';
const INPUT_BRIEFING = 'briefing';
const numberOfSteps = 3;

class Briefing extends Component {
  static propTypes = {
    slug: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
    breakpoint: CustomPropTypes.breakpoint.isRequired,
    history: PropTypes.any.isRequired,
    location: PropTypes.any.isRequired,
  };

  state = {
    tooltipOpenIndex: 0,
    title: '',
    objective: '',
    briefing: '',
    keyboardOpened: false,
    keyboardAvoidingViewKey: 'keyboardAvoidingViewKey',
  };

  // member variables
  _briefing = null;

  componentDidMount() {
    const { history, location } = this.props;

    // Reset location state
    !!location.state && history.push(location.pathname);

    // using keyboardWillHide is better but it does not work for android
    this.keyboardHideListener = Keyboard.addListener(Platform.OS === 'android' ? 'keyboardDidHide' : 'keyboardWillHide', this.keyboardHideListener.bind(this));
  }

  /*shouldComponentUpdate(nextProps, nextState) {
    console.log('# shouldComponentUpdate', nextProps, nextState);

    console.log('# keyboardOpened', nextState.keyboardOpened !== this.state.keyboardOpened);

    if (nextState.keyboardOpened !== this.state.keyboardOpened) return;
    return true;
  }*/

  componentWillUnmount() {
    this.keyboardHideListener.remove();
  }

  keyboardHideListener() {
    this.setState({
      keyboardAvoidingViewKey: 'keyboardAvoidingViewKey' + new Date().getTime()
    });
  }

  _gotoStep = step => {
    const { history, location } = this.props;

    history.push(location.pathname, {
      step,
      isEditing: true,
    });
  };

  _handleBack = () => {
    const { history, location, category, slug: subCategory } = this.props;

    const currentStep = location.state ? location.state.step || 0 : 0;
    Keyboard.dismiss();
    this.setState({ keyboardOpened: false });
    if (currentStep) {
      history.push(location.pathname, {
        step: location.state.step - 1,
        isEditing: true,
      });
    } else {
      Navigation.pop(this.props.componentId);
    }
  };

  _handleTooltipOpen = (index) => {
    const { tooltipOpenIndex } = this.state;

    this.setState({ tooltipOpenIndex: tooltipOpenIndex !== null ? null : index });
  };

  _handleTooltipClose = () => {
    this.setState({ tooltipOpenIndex: null });
  };

  _handleBlur = () => {
    const { breakpoint } = this.props;

    if (breakpoint !== SMALL) {
      this.setState({ tooltipOpenIndex: null });
    }
  };

  _handleChange = (field, value) => {
    this.setState({ [field]: value });
  };

  _handleChangeBriefing = value => this._handleChange('briefing', value);
  _handleChangeObjective = value => this._handleChange('objective', value);
  _handleChangeTitle = value => this._handleChange('title', value);

  _handleContinue = () => {
    const { history, location } = this.props;

    let nextStep = location.state ? location.state.step + 1 : 1;

    history.push(location.pathname, { step: nextStep });
    Keyboard.dismiss();
    this.setState({ keyboardOpened: false });

    if (nextStep < numberOfSteps) {
      this.setState({ tooltipOpenIndex: nextStep });
    }
  };

  _handleFocus = index => {
    const { breakpoint } = this.props;

    this.setState({ keyboardOpened: true });

    if (breakpoint !== SMALL) {
      this.setState({ tooltipOpenIndex: index });
    }
  };

  _handleSubmit = async () => {
    const { title, briefing: description, objective: excerpt } = this.state;
    let getUserOnStorage = await APP_STORAGE.getItem('User');

    if (getUserOnStorage.hasLogin) return navigateFromTo(Routes.BRIEFING.SUBCATEGORY, Routes.BRIEFING.PUBLISH, null, {
      userID: parseInt(getUserOnStorage.userID),
      title,
      description,
      excerpt
    });

    APP_STORAGE.setItem('BriefingDraft', {
      title,
      description,
      excerpt
    });

    return navigateFromTo(Routes.BRIEFING.SUBCATEGORY, Routes.LOGIN._);
  };

  template(css) {
    const { category, slug: subCategory, t, breakpoint, location } = this.props;
    const { tooltipOpenIndex, title, objective, briefing, keyboardAvoidingViewKey, keyboardOpened } = this.state;

    const currentStep = location.state ? location.state.step || 0 : 0;

    const stepValues = [title, objective, briefing];
    const isButtonEnabled = !!stepValues[currentStep] && stepValues[currentStep].length > 0;

    const isSmall = breakpoint === SMALL;
    const isLast = currentStep === numberOfSteps - 1;
    const isReviewing = currentStep === numberOfSteps;
    const tips = [
      {
        icon: Icons.TEXTO,
        title: t('briefing:TOOLTIP_TITLE_TITLE'),
        description: t('briefing:TOOLTIP_TITLE_DESCRIPTION'),
      },
      {
        icon: Icons.TARGET,
        title: t('briefing:TOOLTIP_OBJECTIVE_TITLE'),
        description: t('briefing:TOOLTIP_OBJECTIVE_DESCRIPTION'),
      },
      {
        icon: Icons.BRIEFING,
        title: t('briefing:TOOLTIP_BRIEFING_TITLE'),
        description: t('briefing:TOOLTIP_BRIEFING_DESCRIPTION'),
      },
    ];

    return (
      <_Container hideFooter={isReviewing || tooltipOpenIndex !== null}>
        <_NavigationHeader keyboardOpened={keyboardOpened} currentStep={currentStep}>
          {!isReviewing && (
            <_NavigationPrev onPress={this._handleBack}>
              <_IconPrev icon={Icons.CHEVRON_LEFT} />
            </_NavigationPrev>
          )}
          {!isReviewing && (
            <_NavigationNext disabled={!isButtonEnabled} onPress={this._handleContinue}>
              <_IconNext icon={Icons.CHEVRON_RIGHT} disabled={!isButtonEnabled} />
            </_NavigationNext>
          )}
        </_NavigationHeader>
        <_KeyboardAvoidingView keyboardVerticalOffset={0} behavior={'position'} keyboardOpened={keyboardOpened} currentStep={currentStep}>
          {/* <Column style={css('hidden', 'formatMediumColumn')} size={1} withMargin>
            {IS_WEB && (
              <RichInputControls
                style={css('formatMedium')}
                iconStyle={css('formatIconMedium')}
                toggleBold={this._briefing && this._briefing.toggleBold}
                toggleItalic={this._briefing && this._briefing.toggleItalic}
                toggleUnderline={this._briefing && this._briefing.toggleUnderline}
              />
            )}
          </Column> */}
          <_Column keyboardOpened={keyboardOpened} currentStep={currentStep}>
            {category !== 'others'
              ? <_CategoryIcon icon={Icons[category.toUpperCase().replace(/-/g, '_')]} />
              : <LogoShort color={Colors.BLACK} style={{ marginBottom: 30 }} />}
            <TooltipBar
              tips={tips}
              currentIndex={currentStep}
              openIndex={tooltipOpenIndex}
              shouldStartOpen={breakpoint === SMALL}
              handleTooltipOpen={this._handleTooltipOpen}
              handleTooltipClose={this._handleTooltipClose}
            />
            <_Section active={!currentStep}>
              <_TitleLabel>Qual é o título?</_TitleLabel>
              <Input
                label={t(isSmall ? 'briefing:BRIEFING_TITLE_SMALL' : 'briefing:BRIEFING_TITLE')}
                styleWrapper={css('inputWrapper')}
                styleLabel={css(null, 'titleMedium')}
                styleInput={css('input', 'titleMedium')}
                styleLabelHelper={css(null, 'titleHelperMedium')}
                id={INPUT_TITLE}
                onBlur={this._handleBlur}
                onFocus={() => this._handleFocus(0)}
                onChange={this._handleChangeTitle}
                enablesReturnKeyAutomatically
                returnKeyType="next"
                onSubmitEditing={this._handleContinue}
                autoCorrect={true}
              />
            </_Section>
            <_Section active={currentStep === 1}>
              <_TitleLabel>Qual é o objetivo?</_TitleLabel>
              <Input
                label={t(
                  isSmall ? 'briefing:BRIEFING_OBJECTIVE_SMALL' : 'briefing:BRIEFING_OBJECTIVE'
                )}
                styleWrapper={css('inputWrapper')}
                styleLabel={css(null, 'objectiveMedium')}
                styleInput={css('input', 'objectiveMedium')}
                styleLabelHelper={css(null, 'objectiveHelperMedium')}
                id={INPUT_OBJECTIVE}
                onBlur={this._handleBlur}
                onFocus={() => this._handleFocus(1)}
                onChange={this._handleChangeObjective}
                enablesReturnKeyAutomatically
                returnKeyType="next"
                onSubmitEditing={this._handleContinue}
                autoCorrect={true}
              />
            </_Section>
            <_Section active={currentStep === 2}>
              <_TitleLabel>{t('briefing:BRIEFING_BRIEFING')}</_TitleLabel>
              <Input
                component={RichInput}
                label={t('briefing:BRIEFING_BRIEFING')}
                styleWrapper={css(['inputWrapper', 'description'])}
                styleInput={css('inputTextarea')}
                id={INPUT_BRIEFING}
                setRef={ref => (this._briefing = ref)}
                shouldKeepFocusWhenHasValue
                multiline
                numberOfLines={6}
                onBlur={this._handleBlur}
                onFocus={() => this._handleFocus(2)}
                onChange={this._handleChangeBriefing}
                autoCorrect={true}
              />
            </_Section>
            {/* <ButtonSubmit
              style={css('hidden', 'submitWrapper')}
              labelStyle={css('submit')}
              arrowLeftStyle={css('submitArrowLeft')}
              arrowRightStyle={css('submitArrowRight')}
              onPress={this._handleSubmit}
            >
              {t('briefing:BRIEFING_SUBMIT')}
            </ButtonSubmit> */}
            <Review
              active={currentStep === 3}
              title={title}
              objective={objective}
              briefing={briefing}
              onPressStep={step => this._gotoStep(step)}
              onSubmit={this._handleSubmit}
            />
          </_Column>
          <_ControlsBar active={!isReviewing && tooltipOpenIndex === null}>
            <_ControlsBarWrapperRich>
              {/*isLast && (
                <RichInputControls
                  style={css('format')}
                  iconStyle={css('formatIcon')}
                  toggleBold={this._briefing && this._briefing.toggleBold}
                  toggleItalic={this._briefing && this._briefing.toggleItalic}
                  toggleUnderline={this._briefing && this._briefing.toggleUnderline}
                />
             )*/}
            </_ControlsBarWrapperRich>
            <_ControlsBarWrapperButton>
              <ButtonForward
                isLastStep={isLast}
                onPress={this._handleContinue}
                disabled={!isButtonEnabled}
              >
                {t(isLast ? 'briefing:BRIEFING_SUBMIT' : 'briefing:BRIEFING_CONTINUE')}
              </ButtonForward>
            </_ControlsBarWrapperButton>
          </_ControlsBar>
        </_KeyboardAvoidingView>
      </_Container>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
        paddingLeft: MARGIN_HORIZONTAL,
      },

      icon: {
        color: Colors.BLACK,
        fontSize: 100,
        marginLeft: -16,
        marginTop: 0,
      },

      description: {
        marginTop: 24,
        minHeight: 96,
        maxHeight: 196,
      },

      inputWrapper: {
        backgroundColor: Colors.WHITE,
      },

      input: { color: Colors.BLACK },

      inputTextarea: {
        height: 96,
        color: Colors.BLACK,
      },

      titleMedium: TextStyles.h1,

      titleHelperMedium: {
        borderLeftWidth: 30,
        borderTopWidth: 15,
        borderBottomWidth: 15,
        top: 56,
      },

      objectiveMedium: {
        ...TextStyles.h2,
        lineHeight: 66,
      },

      objectiveHelperMedium: {
        borderLeftWidth: 22,
        borderTopWidth: 11,
        borderBottomWidth: 11,
        top: 52,
      },

      submitWrapper: {
        alignSelf: 'flex-start',
        display: 'flex',
        marginTop: 24,
      },

      submit: { color: Colors.GRAY },

      submitArrowLeft: { borderLeftColor: Colors.GRAY },

      submitArrowRight: { borderRightColor: Colors.GRAY },

      format: {
        flexDirection: 'row',
      },

      formatIcon: {
        color: Colors.WHITE,
        fontSize: 30,
      },

      formatMediumColumn: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: 370,
      },

      formatMedium: {
        flexDirection: 'column',
      },

      formatIconMedium: {
        color: Colors.BLACK,
        fontSize: 30,
      },

      hidden: { display: 'none' },
    };
  }
}

export default compose(
  withDimensions,
  withLocalization,
  withRouter,
  withTheme,
  lifecycle({
    componentDidMount() {
      const { changeTheme } = this.props;

      changeTheme(THEME_WHITE);
    },
  })
)(Briefing);
