import styled from 'styled-components/native';
import { Platform, Dimensions } from 'react-native';

import Icon from 'components/Icon';
import ControlsBar from 'components/ControlsBar';
import { MARGIN_HORIZONTAL } from 'constants/Layout';
import { Colors } from 'theme/Colors';
import { isSmallIOS } from 'utils/Helpers';

const { width, height } = Dimensions.get('window');
const isiOS = Platform.OS === 'ios';

export const _Container = styled.View`
  flex-direction: column;
  height: ${({ hideFooter }) => hideFooter ? height + height : height};
  align-content: space-between;
  background: ${Colors.WHITE};
`;

export const _KeyboardAvoidingView = styled.KeyboardAvoidingView`
  height: ${height - 48}px;
`;

export const _NavigationHeader = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  height: 48px;
  margin-top: ${({ keyboardOpened, currentStep }) => keyboardOpened ? 0 : 4}px;
  z-index: ${({ keyboardOpened, currentStep }) => keyboardOpened ? 1 : 0};
  background: ${Colors.WHITE};
`;

export const _NavigationPrev = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  justify-content: center;
  align-items: center;
`;

export const _IconPrev = styled(Icon)`
  color: ${({ disabled }) => disabled ? Colors.GRAY : Colors.BLACK};
  font-size: 24px;
  margin: 0 4px;
`;

export const _NavigationNext = styled.TouchableOpacity`
  width: 40px;
  height: 40px;
  justify-content: center;
  align-items: center;
`;

export const _IconNext = styled(Icon)`
  color: ${({ disabled }) => disabled ? Colors.GRAY : Colors.BLACK};
  font-size: 24px;
  margin: 0 4px;
`;

export const _Row = styled.View`
`;

export const _Column = styled.View`
  height: ${({ keyboardOpened, currentStep }) => !keyboardOpened ? height - 94 : height - 92}px;
  padding: 0 ${(isSmallIOS ? 15 : MARGIN_HORIZONTAL)}px;
  padding-top: ${({ keyboardOpened, currentStep }) => !keyboardOpened ? 0 : currentStep !== 2 ? (height - 92) / 3 : 50}px;
`;

export const _CategoryIcon = styled(Icon)`
  color: ${Colors.BLACK};
  font-size: 100px;
  margin-top: -32px;
  margin-left: -16px;
`;

export const _Section = styled.View`
  display: ${({ active }) => active ? 'flex' : 'none'};
`;

export const _TitleLabel = styled.Text`
  font-family: ${(isiOS ? 'Euclid Flex' : 'euclid-flex-medium')};
  font-size: 36px;
  line-height: 36px;
  letter-spacing: 0;
  margin-bottom: 24px;
  padding-right: 16px;
`;

export const _ControlsBar = styled(ControlsBar)`
  display: ${({ active }) => active ? 'flex' : 'none'};
  justify-content: space-between;
  width: 100%;
  height: 48px;
  z-index: 0;
`;

export const _ControlsBarWrapperRich = styled.View`
  margin-left: 5px;
`;

export const _ControlsBarWrapperButton = styled.View`

`;
