import React from 'react';
import { View, Text } from 'react-native';
import ReactDOM from 'react-dom';

const App = () => {
  return (
    <View>
      <Text>Olá Crowd - Native</Text>
    </View>
  )
}

ReactDOM.render(<App />, document.getElementById('root'));