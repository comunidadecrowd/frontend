import { Client as TwilioChatClient } from 'twilio-chat';
import { AccessManager } from 'twilio-common';
import APIS from 'apis';

export default APP_SWORKER = {

  Chat: null,

  initChat: async (userID, hasToken = null) => {

    const getUserOnStorage = await APP_STORAGE.getItem('User');
    let token, accessManager, Chat;
    
    let lifeCycleToken = async (tokenParam) => { 

      accessManager = new AccessManager(tokenParam);
      Chat = await TwilioChatClient.create(tokenParam);

      if(getUserOnStorage._FIREBASEUSERTOKEN !== null) {
        // TODO:  Alex: Depois refaforar setPushRegistrationId para Android e Web
        // console.log('### getUserOnStorage._PLATFORMNOTIFICATIONTOKEN', getUserOnStorage._PLATFORMNOTIFICATIONTOKEN)
        Chat.setPushRegistrationId('fcm', getUserOnStorage._FIREBASEUSERTOKEN);
      }

      accessManager.on('tokenWillExpire', obj => {
        console.log('## ACCESSMANAGER tokenWillExpire obj', obj)
        APIS.Chat.getToken(userID, getUserOnStorage._FIREBASEUSERTOKEN, (response) => {
          token = response.requestAccessToken.token;
          accessManager.updateToken(token);
        })
      });

      accessManager.on('tokenExpired', obj => {
        console.log('## ACCESSMANAGER tokenWillExpire obj', obj)
        APIS.Chat.getToken(userID, getUserOnStorage._FIREBASEUSERTOKEN, (response) => {
          token = response.requestAccessToken.token;
          accessManager.updateToken(token)
        })
      });

      accessManager.on('tokenUpdated', async (obj) => {
        console.log('## ACCESSMANAGER tokenUpdated obj', obj)
        
        let updateUserOnStorage = {
          ...getUserOnStorage,
          _TOKEN: token
        }
        
        APP_SWORKER.Chat = await TwilioChatClient.create(token);
        APP_STORAGE.updateItem('User', updateUserOnStorage);
      
      });

      /* // FIX:  ALEX
        APP_SWORKER.Chat não está atualizando após os listners de renovação 
        do Token do accessManager
      */

      APP_SWORKER.Chat = await Chat;
      return await Chat ? Chat : null;

    } 

    if (hasToken) {

      token = getUserOnStorage._TOKEN;
      return lifeCycleToken(token);

    } else {

      return APIS.Chat.getToken(userID, getUserOnStorage._FIREBASEUSERTOKEN, async (response) => {

        token = response.requestAccessToken.token;
        return lifeCycleToken(token);

      })

    }

  }

}