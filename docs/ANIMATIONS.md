[react-native-animated]: https://facebook.github.io/react-native/docs/animated.html
[react-native-interactable]: https://github.com/wix/react-native-interactable

# Fonts

> Quick guide on how we are handling animations

## Motivation

The first idea for this project was to use [Interactable][react-native-interactable], but since it's not supported by [React Native for Web][react-native-web] and it is starting to lack of maintenance, this project will rely on [React Native][react-native]'s [Animated][react-native-animated] library.

It is already supported on web, has a good performance on mobile and it's really handy when we need to control animations based on states or interactions.

## Easy to use

To prevent bad behaviors, performance and mistakes, this project has a custom HOC called `withAnimations` that helps the implementation of [Animated][react-native-animated]. That HOC take care of animation states and instead pass some props down to wrapped component.

Usage:

```js
import React from 'react';
import { Animated } from 'react-native';
import withAnimation from '/src/utils/withAnimations';

class Component extends React.Component {
  componentDidMount() {
    this.props.startFadingTiming({
      duration: 1000,
      toValue: 1,
    });
  }

  render() {
    const { fading } = this.props;

    return <Animated.View style={{ opacity: fading }} />;
  }
}

export default withAnimation('fading', 0)(AnimatedTriangle);
```

It will make the view fade in as soon as component gets mounted.

### HOC documentation

The HOC receive the following params:

- animationName (string - default: 'animating') - The animation name that will be used to generate all prop names. Convention says that it should be a verb in present particile, that way props as `startAnimatingTiming` would make more sense than `startAnimateTiming`
- initialValue (number - default: 0) - The initial value. Since this library is often used with `interpolate`, you may use it to animate a number from 0 to 1 and use the intermediate value in order to make the necessary transitions in the component.

`withAnimation` HOC will generate the following props, considering `fading` as `animationName` param.

- fading - Animation instance
- fadingTiming - Timing animation instance
- startFadingTiming - Starts the animation using time, with easing functions
- fadingSpring - Spring animation instance
- startFadingSpring - Starts the animation using a simple spring physics model
- stopFading - Stop animations
- resetFading - Reset animations
