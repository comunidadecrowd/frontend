# Responsive

> Quick guide on how we're approaching responsive style

## Usage

```js
import React from 'react';
import { Text, View } from 'react-native';

import { Colors } from '../theme';
import { PureComponent } from '../components/BaseComponent';

class Component extends PureComponent {
  template(css) {
    return (
      <View style={css('container', ['containerMedium', { color: Colors.GRAY }], 'containerLarge')}>
        <Text style={css('text', 'textMedium')}>CROWD</Text>
      </View>
    );
  }

  styles() {
    return {
      container: {
        flex: 1,
        backgroundColor: Colors.BLACK,

        alignItems: 'center',
        justifyContent: 'center',
      },

      containerMedium: { backgroundColor: Colors.WHITE },

      containerLarge: { backgroundColor: Colors.GRAY },

      text: { color: Colors.WHITE },

      textMedium: { color: Colors.BLACK },
    };
  }
}

export default Component;
```

## Explanation

As you can see in the example above, this project uses an abstraction of React's Component and PureComponent, instead of importing from `react`, we should import from `/src/components/BaseComponent`.

Instead of `render`, `BaseComponent` uses a `template` static method to receive a `css` parser that connects with `styles()` static method. That way the components are not responsible for creating stylesheets anymore and can rely this role to `BaseComponent`.

In order to add responsive styles to elements, `css` method receives 3 arguments, one for each breakpoint (small, medium, large) and these arguments can be either a single _string_ with the name of the style set on `styles()` or an _array_ of _strings_ or _style objects_.

Take the example above as reference, the result would be having `<View>` changing its bg color from `BLACK` (_small_), to `WHITE` (_medium_) and finally, `GRAY` (_large_), the color would change on _medium_ and large since it has an object as argument child as well.

The `<Text>` element, has only two arguments set on `style`, which make it having the same style for both _medium_ and _large_.
