# Fonts

> Quick guide on how to install new fonts

## Web

For installing a new custom font for web:

- Add font's `woff` and `woff2` files to `/public/assets/fonts`
- Declare it on `/src/styles/fonts.css`
- Create a new constants with its base styles on `/src/theme/Fonts.web.js`
- Import the new style on `/src/theme/index.js` (if not made before)
- Use it on your styles

## Native apps (Android and iOS)

For installing a new custom font for native apps:

- Add font's `ttf` file to `/assets`
- Import it on `/App.js` using `Expo.Font.loadAsync`
- Create a new constants with its base styles on `/src/theme/Fonts.js`
- Import the new style on `/src/theme/index.js` (if not made before)
- Use it on your styles

## Multi-platform usage

For using a custom font, you need to import it and then spread it into your style:

```js
import { StyleSheet } from 'react-native';
import { openSans } from '/src/theme';

const styles = StyleSheet.create({
  view: { ...openSans };
});
```
