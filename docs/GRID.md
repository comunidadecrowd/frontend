# Grid

> Quick guide on how we are handling grids on this project

## Grid Design

The design project of this product defined 12 grids for all breakpoints, with proportional gutters and fluid columns.

Since this project make use of react-native's Dimensions in order to define window/screen size, the `<Column />` component sets the width value in _pixels_, which makes it easier to position all column-based views, independent on their parent's size.

As a helper component, is recommended to add a `<Row />` as parent of `<Column />`s, that way they can be displayed side-by-side.

Finally, `<Container />` is responsible for adding the global horizontal margins and should be used on containers (first component of a route), to make all content fitting the margins of the project.

## Responsiveness

The `<Column />` component can be responsive in the strategy describe on [Responsive doc](./RESPONSIVE.md), to add different sizes for each breakpoint use `sizeMedium` and `sizeLarge`, always with mobile-first concept in mind.

## Usage

We have an `Icon` component to help using it.

```js
import React from 'react';
import Container from '/src/components/Grid/Container';
import Row from '/src/components/Grid/Row';
import Column from '/src/components/Grid/Column';

const Component = () => (
  <Container>
    <Row>
      <Column size={4} sizeMedium={3} sizeLarge={5}>
        <Text>4 columns on small / 3 columns on medium / 5 columns on large</Text>
      </Column>
      <Column size={3} sizeMedium={2}>
        <Text>3 / 2 / 2</Text>
      </Column>
      <Column size={5}>
        <Text>5</Text>
      </Column>
    </Row>
  </Container>
);

const styles = StyleSheet.create({
  icon: { fontSize: 30 },
});

export default Component;
```

## Debugging

To help validating if all elements are correct aligned with the grid, it's possible to toggle a grid guide (only for non-production web environment).

To show/hide it, with the window focused, use the hotkey: `ALT + .` (alt/option + point).

![grid overlay](https://bitbucket-assetroot.s3.amazonaws.com/repository/ekjrEMq/3422899909-Jun-02-2018%2013-49-29.gif?Signature=aazaVr1Ezd8IrmBz7yqCapGS6Rc%3D&Expires=1527962573&AWSAccessKeyId=AKIAIQWXW6WLXMB5QZAQ)
