[iconfont-cli]: https://github.com/morlay/iconfont-cli

# Icons

> Quick guide on how to use and install new static icons

## Add new icons

After adding the new icon SVG (we recommend using another icon as reference since it has some guides to help it out) to `/icons` folder, run `yarn icons` in order to generate the whole set of icons again.

It will trigger a script that uses [iconfont-cli][iconfont-cli] to convert the SVG icons into a font.

After generating the font, it will copy:

- The `ttf` file to `/assets` for native apps
- Both `woff` and `woff2` to `/public/assets/fonts` for web
- The glyphs icon map to `/src/constants`

## Usage

We have an `Icon` component to help using it.

```js
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Icons } from '/src/components/constants/Icons';
import Icon from '/src/components/Icons';

const Component = () => (
  <View>
    <Icon style={styles.icon} icon={Icons.PERFIL} />
  </View>
);

const styles = StyleSheet.create({
  icon: { fontSize: 30 },
});

export default Component;
```
