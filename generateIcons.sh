./node_modules/.bin/iconfont ./src/assets/icons/*.svg -o ./_icons_temp -t .iconsCharMap
mv ./_icons_temp/iconfont.ttf ./src/assets/iconfont.ttf
mv ./_icons_temp/iconfont.woff ./public/assets/fonts/iconfont.woff
mv ./_icons_temp/iconfont.woff2 ./public/assets/fonts/iconfont.woff2
mv ./_icons_temp/.iconsCharMap ./src/constants/Icons.js
rm -R ./_icons_temp
exit
