import React from 'react';
import { StyleSheet, View } from 'react-native';

import Container from 'components/Grid/Container';
import Overlay from 'components/Grid/Overlay';

import Header from './Header';
import AnimatedTriangle from './AnimatedTriangle';

const AnimationsStory = () => (
  <React.Fragment>
    <Overlay />
    <Container>
      <Header title="Animations">
        Please refer to the{' '}
        <a
          href="https://bitbucket.org/comunidadecrowd/frontend/src/develop/docs/ANIMATIONS.md"
          target="_blank"
          rel="noopener noreferrer"
        >
          documentation
        </a>{' '}
        for more information.
      </Header>
      <View style={styles.animationWrapper}>
        <AnimatedTriangle />
      </View>
    </Container>
  </React.Fragment>
);

const styles = StyleSheet.create({
  animationWrapper: {
    alignItems: 'center',
  },
});

export default AnimationsStory;
