import React from 'react';
import { View, Text } from 'react-native';

import { PureComponent } from 'components/BaseComponent';
import Container from 'components/Grid/Container';
import Overlay from 'components/Grid/Overlay';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import Header from './Header';

class TextStylesStory extends PureComponent {
  template(css) {
    return (
      <React.Fragment>
        <Overlay />
        <Container>
          <Header title="TextStyles">
            The following styles are based on styleguide and can be used using spread operator
            directly on styles.
          </Header>
          {Object.keys(TextStyles).map((textStyle, index) => (
            <View key={index} style={css('item')}>
              <Text style={css(['text', { ...TextStyles[textStyle] }])}>
                <Text style={css('textStyle')}>{textStyle}</Text> - A Crowd oferece serviços e
                profissionais de comunicação e tecnologia sob demanda.
              </Text>
            </View>
          ))}
        </Container>
      </React.Fragment>
    );
  }

  styles() {
    return {
      item: {
        paddingBottom: 32,
        marginBottom: 32,
        borderBottomColor: Colors.BLACK,
        borderBottomStyle: 'dashed',
        borderBottomWidth: 1,
      },
      textStyle: {
        backgroundColor: Colors.GRAY_LIGHT,
        paddingHorizontal: 16,
      },
    };
  }
}

export default TextStylesStory;
