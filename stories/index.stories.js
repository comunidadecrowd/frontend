import React from 'react';

import fonts from 'styles/fonts.css'; // eslint-disable-line

import { storiesOf } from '@storybook/react';
import Animations from './Animations';
import Grid from './Grid';
import Icons from './Icons';
import TextStyles from './TextStyles';
import Responsive from './Responsive';
import Lottie from './Lottie';

storiesOf('Style guide', module)
  .add('Animations', () => <Animations />)
  .add('Grid', () => <Grid />)
  .add('Icons', () => <Icons />)
  .add('Lottie', () => <Lottie />)
  .add('Responsive', () => <Responsive />)
  .add('Text Styles', () => <TextStyles />);
