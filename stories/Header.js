import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import PropTypes from 'prop-types';

import { TextStyles } from 'theme/TextStyles';

const Header = ({ title, children }) => (
  <View style={styles.root}>
    <Text style={styles.title}>{title}</Text>
    {!!children && <Text style={styles.children}>{children}</Text>}
  </View>
);

const styles = StyleSheet.create({
  root: {
    marginTop: 32,
    marginBottom: 64,
  },
  title: {
    ...TextStyles.h2,
    marginBottom: 16,
  },
  children: TextStyles.body,
});

Header.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
};

Header.defaultProps = {
  children: null,
  title: '',
};

export default Header;
