import React from 'react';
import { View, Text, Dimensions } from 'react-native';

import { PureComponent } from 'components/BaseComponent';
import Container from 'components/Grid/Container';
import Overlay from 'components/Grid/Overlay';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import Header from './Header';

class ResponsiveStory extends PureComponent {
  template(css) {
    const { width: windowWidth } = Dimensions.get('window');

    return (
      <View>
        <Overlay />
        <Container>
          <Header title="Responsive">
            The example bellow will show different background colors and different texts based on
            window width.<br />
            Please refer to the{' '}
            <a
              href="https://bitbucket.org/comunidadecrowd/frontend/src/develop/docs/RESPONSIVE.md"
              target="_blank"
              rel="noopener noreferrer"
            >
              documentation
            </a>{' '}
            for more information.
          </Header>
          <View style={css('view', 'viewMedium', 'viewLarge')}>
            <Text style={css(['text', 'textSmall'], 'textHidden')}>
              You are seeing small breakpoint content.
            </Text>
            <Text style={css(['textHidden', 'textMedium'], 'text', 'textHidden')}>
              You are seeing medium breakpoint content.
            </Text>
            <Text style={css(['textHidden', 'textLarge'], null, 'text')}>
              You are seeing large breakpoint content.
            </Text>
            <Text style={css('windowWidth', null, 'windowWidthLarge')}>
              (window width: {windowWidth})
            </Text>
          </View>
        </Container>
      </View>
    );
  }

  styles() {
    return {
      view: {
        backgroundColor: Colors.BLACK,
        padding: 32,
        borderColor: Colors.GRAY_LIGHT,
        borderStyle: 'dashed',
        borderWidth: 1,
      },
      viewMedium: { backgroundColor: Colors.GRAY },
      viewLarge: { backgroundColor: Colors.WHITE },

      text: {
        ...TextStyles.h2,
        display: 'block',
        textAlign: 'center',
      },
      textHidden: { display: 'none' },

      textSmall: { color: Colors.WHITE },
      textMedium: { color: Colors.WHITE },
      textLarge: { color: Colors.BLACK },

      windowWidth: {
        color: Colors.WHITE,
        marginTop: 32,
        marginHorizontal: 'auto',
      },
      windowWidthLarge: { color: Colors.BLACK },
    };
  }
}

export default ResponsiveStory;
