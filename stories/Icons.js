import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Container from 'components/Grid/Container';
import Overlay from 'components/Grid/Overlay';
import Icon, { Icons } from 'components/Icon';

import { TextStyles } from 'theme/TextStyles';

import Header from './Header';

const IconsStory = () => (
  <React.Fragment>
    <Overlay />
    <Container>
      <Header title="Icons">
        Please refer to the{' '}
        <a
          href="https://bitbucket.org/comunidadecrowd/frontend/src/develop/docs/ICONS.md"
          target="_blank"
          rel="noopener noreferrer"
        >
          documentation
        </a>{' '}
        for more information.
      </Header>
      <View style={styles.row}>
        {Object.keys(Icons).map((iconName, index) => (
          <View key={index} style={styles.column}>
            <Icon icon={Icons[iconName]} style={styles.icon} />
            <Text style={styles.iconName}>{iconName}</Text>
          </View>
        ))}
      </View>
    </Container>
  </React.Fragment>
);

const styles = StyleSheet.create({
  row: {
    display: 'flex',
    flexFlow: 'row wrap',
  },
  column: {
    alignItems: 'center',
    marginBottom: 32,
    width: 200,
  },
  icon: { fontSize: 30 },
  iconName: { ...TextStyles.body },
});

export default IconsStory;
