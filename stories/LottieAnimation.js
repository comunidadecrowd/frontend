import React from 'react';
import { StyleSheet } from 'react-native';

import Lottie from 'components/cross-platform/Lottie';

import Data from './LottieAnimationData.json';

export default class LottieAnimation extends React.Component {
  _animation = null;

  componentDidMount() {
    if (!this._animation) return;

    this._animation.play();
  }

  render() {
    return (
      <Lottie
        ref={animation => {
          this._animation = animation;
        }}
        style={styles.animation}
        source={Data}
        autoPlay={true}
      />
    );
  }
}

const styles = StyleSheet.create({
  animation: {
    width: 500,
    height: 500,
  },
});
