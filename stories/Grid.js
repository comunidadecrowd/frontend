import React from 'react';
import { Text, View } from 'react-native';

import { PureComponent } from 'components/BaseComponent';
import { Colors } from 'theme/Colors';
import Column from 'components/Grid/Column';
import Container from 'components/Grid/Container';
import Overlay from 'components/Grid/Overlay';
import Row from 'components/Grid/Row';

import Header from './Header';

class GridStory extends PureComponent {
  template(css) {
    return (
      <View>
        <Overlay />
        <Container>
          <Header title="Grids">
            To toggle the overlay, use the hotkey ALT + . (alt/option + point).<br />
            Please refer to the{' '}
            <a
              href="https://bitbucket.org/comunidadecrowd/frontend/src/develop/docs/GRID.md"
              target="_blank"
              rel="noopener noreferrer"
            >
              documentation
            </a>{' '}
            for more information.
          </Header>
          <Row>
            <Column style={css('column')} size={4}>
              <Text>4 columns</Text>
            </Column>
            <Column style={css('column')} size={2}>
              <Text>2 columns</Text>
            </Column>
            <Column style={css('column')} size={5}>
              <Text>5 columns</Text>
            </Column>
          </Row>
        </Container>
      </View>
    );
  }

  styles() {
    return {
      column: { backgroundColor: Colors.GRAY },
    };
  }
}

export default GridStory;
