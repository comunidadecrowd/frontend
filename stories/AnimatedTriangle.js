import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Animated, Text } from 'react-native';
import { compose } from 'recompose';
import { noop } from 'lodash';

import withAnimation from 'utils/withAnimation';
import CustomPropTypes from 'constants/CustomPropTypes';

class AnimatedTriangle extends React.Component {
  static propTypes = {
    fading: CustomPropTypes.animation,
    rotating: CustomPropTypes.animation,

    // functions
    rotatingTiming: PropTypes.func,
    startFadingTiming: PropTypes.func,
  };

  static defaultProps = {
    fading: null,
    rotating: null,

    // functions
    rotatingTiming: noop,
    startFadingTiming: noop,
  };

  state = { rotateAnimation: new Animated.Value(0) };

  _rotate = () => {
    const { rotatingTiming } = this.props;

    Animated.sequence([
      rotatingTiming({
        toValue: 1,
        duration: 1000,
      }),
      rotatingTiming({
        toValue: 0,
        duration: 1000,
      }),
    ]).start(() => {
      this._rotate();
    });
  };

  componentDidMount() {
    this.props.startFadingTiming({
      toValue: 1,
      duration: 2000,
    });
    this._rotate();
  }

  render() {
    const { rotating, fading } = this.props;

    return (
      <Animated.View
        style={{
          opacity: fading,
          transform: [
            {
              rotate: rotating.interpolate({
                inputRange: [0, 1],
                outputRange: ['0deg', '360deg'],
              }),
            },
          ],
        }}
      >
        <Text style={styles.text}>◀</Text>
      </Animated.View>
    );
  }
}

const styles = StyleSheet.create({
  text: { fontSize: 100 },
});

export default compose(
  withAnimation('fading', 0),
  withAnimation('rotating', 0)
)(AnimatedTriangle);
