import React from 'react';
import { View } from 'react-native';

import Container from 'components/Grid/Container';
import Overlay from 'components/Grid/Overlay';

import Header from './Header';
import LottieAnimation from './LottieAnimation';

const LottieStory = () => (
  <React.Fragment>
    <Overlay />
    <Container>
      <Header title="Lottie">We are using Lottie for complex animations.</Header>
      <View>
        <LottieAnimation />
      </View>
    </Container>
  </React.Fragment>
);

export default LottieStory;
