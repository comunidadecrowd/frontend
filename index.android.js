import { Navigation } from "react-native-navigation";
import { Platform, YellowBox } from 'react-native';
import { Colors } from './src/theme';
import { RegisterPages } from './src/RegisterPages';
import Routes from './src/Routes';
import { createCrowdStorage } from 'APP_STORAGE';

import EventEmitter from 'events';

YellowBox.ignoreWarnings(['Setting a timer', 'Remote debugger'])

const commandListener = Navigation.events().registerCommandListener(async (name, params) => {
  if ( name === 'popTo' && params.componentId === '/chat' ) {
    let getUserOnStorage = await APP_STORAGE.getItem('User');
    if (getUserOnStorage.userID && getUserOnStorage.name && getUserOnStorage.title && getUserOnStorage.category)
      return navigateFromTo(Routes.BRIEFING.PUBLISH, Routes.CHAT._, null, {
        alreadyImportedContacts: getUserOnStorage.alreadyImportedContacts,
        userID: getUserOnStorage.userID
      });
  }
});

const appLaunchedListener = Navigation.events().registerAppLaunchedListener(async () => {

  await createCrowdStorage();
  global.emitters = new EventEmitter();
  RegisterPages();

  Navigation.setDefaultOptions({
    layout: {
      backgroundColor: '#000'
    },
    bottomTabs: {
      backgroundColor: '#000',
      ...Platform.select({ android: { drawBehind: false, titleDisplayMode: 'alwaysHide' } })
    },
    bottomTab: {
      selectedIconColor: Colors.BLUE
    },
    topBar: {
      visible: false,
      ...Platform.select({ android: { drawBehind: true } })
    },
    animations: {
      setRoot: {
        waitForRender: true
      },
      pop: {
        enabled: true,
        waitForRender: true
      },
      push: {
        enabled: true,
        waitForRender: true
      }
    }
  })

  Navigation.setRoot({
    root: {
      stack: {
        children: [
          {
            component: {
              name: Routes.ANDROIDSPLASHSCREEN, 
              id: Routes.ANDROIDSPLASHSCREEN,
              options: {
                bottomTabs: {
                  visible: false
                }
              }
            }
          }
        ]
      }
    }
  })

});