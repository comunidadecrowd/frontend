[react-native]: https://facebook.github.io/react-native/
[react-native-web]: https://github.com/necolas/react-native-web
[graphql]: https://graphql.org/
[apollo]: https://www.apollographql.com/
[flow]: https://flow.org/
[jest]: https://facebook.github.io/jest/
[google-cloud]: https://cloud.google.com/
[cloud-speed-to-text]: https://cloud.google.com/speech-to-text/
[firebase]: https://firebase.google.com/
[firebase-auth]: https://firebase.google.com/docs/auth/
[gitflow]: https://nvie.com/posts/a-successful-git-branching-model/
[trello]: https://trello.com/b/JrRf2Bmk
[prettier]: https://prettier.io
[eslint]: https://eslint.org

[Twilio](https://stackoverflow.com/questions/tagged/twilio) [Verify](https://www.twilio.com/docs/verify/api) e [Authy](https://www.twilio.com/docs/authy/api) (autenticação SMS/Push)
[Algolia](https://www.algolia.com/doc/) (base de profissionais)
Twilio [Programmable Chat](https://www.twilio.com/docs/chat) (mensageria)

# ▶ CROWD ◀

Crowd's next generation app and website.

## Run & Build & Deploy

This project is multiplatform and hybrid, it means that it can be run either as a native app (**Android** and **iOS**) or for **Web**.

For running:

```sh
$ yarn android
$ yarn ios
$ yarn web
```

For building (web):

```sh
$ yarn build:web
```

## Guides

- [React Native (original)](./docs/REACT_NATIVE.md)
- [Responsive](./docs/RESPONSIVE.md)
- [Fonts](./docs/FONTS.md)
- [Animations](./docs/ANIMATIONS.md)
- [Icons](./docs/ICONS.md)
- [Grid](./docs/GRID.md)

You can take a look on some of the project's style guide definitions and usage examples running:

```sh
$ yarn storybook
```

## Stack

### React Native

[React Native][react-native] is the framework behind this project. For both mobile apps and web, we use React Native.

To make the web magic happen, we use [React Native for Web][react-native-web].

### GraphQL

This project data is provided by a [GraphQL][graphql] API, which makes possible to query for data at runtime and in a non-feature-dependent way.

We use [Apollo][apollo] as GraphQL client on this project.

### Debugging on device

For debugging on device we use  [React-Native Debugger](https://github.com/jhen0409/react-native-debugger). Make sure to install it globally.

Make sure all debugger clients of React Native are closed, then open debugger by running:

```sh
$ yarn debug:device
```

After debugger is opened enable Debug JS Remotely of developer menu on your app.

>Note: debugger will listen to port 8081 by default. For custom ports consult the package documentation.

### Type checking

Since JS is a weakly typed language, we use [Flow][flow] as a static type checker, that way we can prevent cross-data and cross-state issues away from runtime.

[Flow][flow] is mainly used on utils functions and HOCs since they are used across the whole app. For containers and components, `propTypes` is being used instead. That way we can prevent runtime issues.

### Testing

We use [Jest][jest] for testing the project against unit and snapshot tests.

### Lint

The code consistency is guaranteed by [ESLint][eslint] (linter) and [Prettier][prettier] (formatter).

```sh
$ yarn lint # to run linter
$ yarn format # to run formatter
```

### Speech to text

Since the whole platform is under [Google Cloud][google-cloud]'s solutions, this project uses [Cloud Speech To Text][cloud-speech-to-text] API to handle by-microphone typing.

### Authentication

The [authentication][firebase-auth] of this project is provided by [Firebase][firebase].

## Workflow

We use [gitflow][gitflow] as branch interrelationship and a [Trello board][trello] for project management.

## License

Unlicensed.
