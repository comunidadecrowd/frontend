import { initApp } from './src/App';
import { createCrowdStorage } from 'APP_STORAGE';
import { YellowBox } from 'react-native';

(async () => {
  
  YellowBox.ignoreWarnings(['Remote debugger'])
  
  await createCrowdStorage();
    
  APP_STORAGE.init().then((res) => {
  
    return initApp(
      (!res || !res.hasLogin) 
        ? 'needLogin'
          : (res.hasLogin && !res.hasRegister)
          ? 'needRegister'
          : 'hasLoginAndRegister'
    )
  
  })

})()