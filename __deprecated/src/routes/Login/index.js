import React from 'react';
import { SafeAreaView, View } from 'react-native';
import { Switch, Route } from 'react-router-native';

import { PureComponent } from 'components/BaseComponent';
import KeyboardSpacer from 'components/KeyboardSpacer';

import { MARGIN_HORIZONTAL } from 'constants/Layout';
import Routes from 'constants/Routes';

import { Colors } from 'theme/Colors';
import withLocalization from 'utils/withLocalization';

import Background from './components/Background';
import Header from './components/Header';
import LoginLegacy from './components/LoginLegacy';
import LoginWithPhone from './components/LoginWithPhone';

class Login extends PureComponent {
  template(css) {
    return (
      <SafeAreaView style={css('root', 'rootMedium')}>
        <Background />
        <View style={css('wrapper')}>
          <Header />
          <Switch>
            <Route path={Routes.LOGIN_LEGACY} component={LoginLegacy} />
            <Route component={LoginWithPhone} />
          </Switch>
        </View>
        <KeyboardSpacer topSpacing={-70} />
      </SafeAreaView>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
      },

      rootMedium: {
        alignItems: 'center',
        justifyContent: 'center',
      },

      wrapper: {
        justifyContent: 'flex-end',
        flexGrow: 1,
        marginHorizontal: MARGIN_HORIZONTAL,
        paddingTop: 24,
      },

      form: {
        flex: 1,
        borderWidth: 1,
        borderColor: 'yellow',
      },

      headline: {
        marginTop: 24,
        alignSelf: 'flex-start',
      },

      headlineText: { color: Colors.WHITE },

      headlineTextMedium: { textAlign: 'center' },

      formWrapper: {
        flexGrow: 1,
        marginTop: 48,
      },

      formWrapperMedium: {
        flexGrow: 1,
        marginVertical: 56,
        justifyContent: 'center',
      },

      form: {
        justifyContent: 'space-between',
      },

      formMedium: {
        justifyContent: 'center',
      },
    };
  }
}

export default withLocalization(Login);
