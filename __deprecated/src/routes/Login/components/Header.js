import React from 'react';
import { View } from 'react-native';
import { Link } from 'react-router-native';

import Logo from 'assets/svgs/Logo';
import LogoShort from 'assets/svgs/LogoShort';

import { PureComponent } from 'components/BaseComponent';

import CustomPropTypes from 'constants/CustomPropTypes';
import Routes from 'constants/Routes';

import { SMALL } from 'theme';

import withDimensions from 'utils/withDimensions';

class Header extends PureComponent {
  static propTypes = {
    breakpoint: CustomPropTypes.breakpoint.isRequired,
  };

  template(css) {
    const { breakpoint } = this.props;

    return (
      <View style={css('root', 'rootMedium')}>
        <Link to={Routes.HOME}>
          {breakpoint === SMALL ? (
            <View>
              <LogoShort />
            </View>
          ) : (
            <View>
              <Logo />
            </View>
          )}
        </Link>
      </View>
    );
  }

  styles() {
    return {
      root: {
        alignSelf: 'flex-start',
        marginBottom: 32,
      },

      rootMedium: {
        alignSelf: 'center',
        marginTop: 100,
      },
    };
  }
}

export default withDimensions(Header);
