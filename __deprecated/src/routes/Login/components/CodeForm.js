import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes, Text, TouchableHighlight } from 'react-native';
import { noop } from 'lodash';

import { PureComponent } from 'components/BaseComponent';
import ButtonSubmit from 'components/ButtonSubmit';
import InputCode from 'components/InputCode';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import withLocalization from 'utils/withLocalization';

class CodeForm extends PureComponent {
  static propTypes = {
    error: PropTypes.string,
    style: ViewPropTypes.style,
    t: PropTypes.func.isRequired,

    // functions
    onCancel: PropTypes.func,
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    error: '',
    style: null,

    // functions
    onCancel: noop,
  };

  codeLength = 6;
  state = { code: '' };

  _handleSubmit = () => {
    const { onSubmit } = this.props;
    const { code } = this.state;

    code.length === this.codeLength && onSubmit && onSubmit(code);
  };

  _handleChange = value => {
    this.setState({ code: value });
  };

  template(css) {
    const { t, style, onCancel, error } = this.props;
    const { code } = this.state;

    const isButtonEnabled = code.length === 6;

    return (
      <View style={style}>
        <InputCode
          length={this.codeLength}
          onChange={this._handleChange}
          onFinish={this._handleSubmit}
          style={css('input')}
          error={error}
        />
        <ButtonSubmit
          onPress={this._handleSubmit}
          style={css('loginButton')}
          disabled={!isButtonEnabled}
        >
          {t('login:CONFIRM')}
        </ButtonSubmit>
        <TouchableHighlight onPress={onCancel}>
          <Text style={css('didntReceiveTheCode')}>{t('login:DIDNT_RECEIVE_THE_CODE')}</Text>
        </TouchableHighlight>
      </View>
    );
  }

  styles() {
    return {
      input: {
        marginBottom: 24,
      },

      didntReceiveTheCode: {
        ...TextStyles.small,
        color: Colors.GRAY_DARKER,
        textAlign: 'center',
        textDecorationColor: Colors.TRANSPARENT,
      },
    };
  }
}

export default withLocalization(CodeForm);
