import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { View, Text, ScrollView } from 'react-native';
import { Link, withRouter } from 'react-router-native';
import firebase from 'react-native-firebase';
import { getCountryCallingCode, isValidNumber } from 'libphonenumber-js';
import { Navigation } from 'react-native-navigation';

import LoadingModal from 'components/LoadingModal';
import { PureComponent } from 'components/BaseComponent';

import Routes from 'constants/Routes';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import withLocalization from 'utils/withLocalization';

import CodeForm from './CodeForm';
import PhoneForm from './PhoneForm';

class LoginWithPhone extends PureComponent {
  static propTypes = {
    t: PropTypes.func.isRequired,
  };

  state = {
    phoneNumber: '',
    formatedPhoneNumber: '',
    confirmResult: null,
    errorMessage: '',
    isLoading: false,
  };

  _handleSubmitPhone = (phoneNumber, country) => {
    const { t } = this.props;
    const formatedPhoneNumber = `+${getCountryCallingCode(country)} ${phoneNumber}`;

    if (!isValidNumber(formatedPhoneNumber, country)) {
      this.setState({
        errorMessage: t('error:INVALID_PHONE_NUMBER'),
      });

      return;
    }

    const unFormatedNumber = formatedPhoneNumber.replace(/[ ()\-#]/g, '');

    this.setState(
      {
        phoneNumber,
        formatedPhoneNumber,
        isLoading: true,
        errorMessage: '',
      },
      () =>
        firebase
          .auth()
          .signInWithPhoneNumber(unFormatedNumber)
          .then(confirmResult => {
            this.setState({
              confirmResult,
              isLoading: false,
            });
          })
          .catch(error => {
            this.setState({
              errorMessage: t([`authError:${error.code}`, 'authError:default']),
              isLoading: false,
            });
          })
    );
  };

  _handleCancelCode = () => {
    this.setState({ confirmResult: null });
  };

  _handleSubmitCode = code => {
    const { t } = this.props;
    const { confirmResult } = this.state;

    if (confirmResult && code.length) {
      this.setState({ isLoading: true }, () =>
        confirmResult
          .confirm(code)
          .then(() => {
            this.setState({ isLoading: false });
            Navigation.popToRoot(Routes.SETTINGS);
          })
          .catch(error => {
            this.setState({
              errorMessage: t([`authError:${error.code}`, 'authError:auth/bad-code']),
              isLoading: false,
            });
          })
      );
    }
  };

  template(css) {
    const { t } = this.props;
    const { confirmResult, isLoading, phoneNumber, formatedPhoneNumber, errorMessage } = this.state;
    const phoneDesciption = t('login:PHONE_DESCRIPTION');
    const codeDescription = t('login:CODE_DESCRIPTION').replace(/{phone}/g, formatedPhoneNumber);

    return (
      <ScrollView
        scrollEnabled={false}
        style={css('root')}
        contentContainerStyle={css('rootContainer')}
      >
        <View style={css('headline')}>
          <Text style={css(['headlineText', TextStyles.h3], ['headlineTextMedium', TextStyles.h2])}>
            {t('login:TITLE')}
          </Text>
        </View>
        <View style={css('description')}>
          <Text style={css('descriptionText')}>
            {t(!confirmResult ? phoneDesciption : codeDescription)}
          </Text>
        </View>
        {!confirmResult ? (
          <PhoneForm
            style={css('form')}
            onSubmit={this._handleSubmitPhone}
            phoneNumber={phoneNumber}
            error={errorMessage}
          />
        ) : (
          <CodeForm
            style={css('form')}
            onSubmit={this._handleSubmitCode}
            onCancel={this._handleCancelCode}
            error={errorMessage}
          />
        )}
        {isLoading && <LoadingModal />}
        <View style={css('hasAnAccount')}>
          <Link to={Routes.LOGIN_LEGACY}>
            <Text style={css('hasAnAccountLabel')}>{t('login:HAS_AN_ACCOUNT')}</Text>
          </Link>
        </View>
      </ScrollView>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
      },

      rootContainer: {
        flex: 1,
        justifyContent: 'flex-end',
      },

      description: {
        marginBottom: 48,
      },

      descriptionText: {
        ...TextStyles.h5,
        color: Colors.WHITE,
      },

      form: {
        flexGrow: 1,
      },

      hasAnAccount: {
        alignSelf: 'center',
        marginBottom: 24,
      },

      hasAnAccountLabel: {
        ...TextStyles.small,
        color: Colors.GRAY_DARKER,
        textAlign: 'center',
        textDecorationColor: Colors.TRANSPARENT,
      },

      headline: {
        marginTop: 24,
        alignSelf: 'flex-start',
      },

      headlineText: { color: Colors.WHITE },

      headlineTextMedium: { textAlign: 'center' },
    };
  }
}

export default compose(
  withRouter,
  withLocalization
)(LoginWithPhone);
