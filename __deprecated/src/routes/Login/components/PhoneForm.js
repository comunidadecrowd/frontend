import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes } from 'react-native';
import { Rifm } from 'rifm';

import Input from 'components/Input';
import ButtonSubmit from 'components/ButtonSubmit';
import { PureComponent } from 'components/BaseComponent';
import SelectCountry from 'components/SelectCountry';

import { formatPhone } from 'utils/number';
import withLocalization from 'utils/withLocalization';

import { INPUT_PHONE } from '../_constants';

class PhoneForm extends PureComponent {
  static propTypes = {
    error: PropTypes.string,
    phoneNumber: PropTypes.string,
    style: ViewPropTypes.style,

    // functions
    onSubmit: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
  };

  static defaultProps = {
    error: '',
    phoneNumber: '',
    style: null,
  };

  state = {
    phoneNumber: this.props.phoneNumber,
    country: 'BR',
  };

  _handleSubmit = () => {
    const { onSubmit } = this.props;
    const { phoneNumber, country } = this.state;

    onSubmit && onSubmit(phoneNumber, country);
  };

  _handleChange = value => {
    this.setState({ phoneNumber: value });
  };

  _handleChangeCountry = value => {
    this.setState({ country: value });
  };

  template(css) {
    const { t, style, phoneNumber: initialValue, error } = this.props;
    const { phoneNumber, country } = this.state;
    const format = formatPhone(country);

    return (
      <View style={style}>
        <View style={css('inputs')}>
          <SelectCountry
            initialValue={country}
            style={css('selectCountry')}
            onChange={this._handleChangeCountry}
          />
          <Rifm value={phoneNumber} onChange={this._handleChange} format={format}>
            {({ value, onChange }) => (
              <Input
                label={t('login:PHONE_MASK').toLowerCase()}
                id={INPUT_PHONE}
                style={css('input')}
                keyboardType="phone-pad"
                initialValue={initialValue}
                value={value}
                onChange={value => onChange({ target: { value } })}
                error={error}
              />
            )}
          </Rifm>
        </View>
        <ButtonSubmit
          onPress={this._handleSubmit}
          style={css('button')}
          disabled={phoneNumber.length < 14}
        >
          {t('login:ACCESS')}
        </ButtonSubmit>
      </View>
    );
  }

  styles() {
    return {
      inputs: {
        marginBottom: 24,
        flexDirection: 'row',
        alignItems: 'flex-start',
      },

      selectCountry: {
        paddingTop: 8,
        marginRight: 8,
      },

      input: {
        flex: 1,
      },

      button: { marginBottom: 8 },
    };
  }
}

export default withLocalization(PhoneForm);
