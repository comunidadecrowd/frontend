import React from 'react';
import PropTypes from 'prop-types';
import { View, ViewPropTypes, Text } from 'react-native';
import { Link } from 'react-router-native';

import { PureComponent } from 'components/BaseComponent';
import ButtonSubmit from 'components/ButtonSubmit';
import Input from 'components/Input';

import Routes from 'constants/Routes';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import withLocalization from 'utils/withLocalization';

import { INPUT_EMAIL, INPUT_PASSWORD } from '../_constants';

class LoginForm extends PureComponent {
  static propTypes = {
    isValid: PropTypes.bool.isRequired,
    style: ViewPropTypes.style,
    errors: PropTypes.object,
    t: PropTypes.func.isRequired,
    touched: PropTypes.object,

    // functions
    onBlurEmail: PropTypes.func.isRequired,
    onBlurPassword: PropTypes.func.isRequired,
    onChangeEmail: PropTypes.func.isRequired,
    onChangePassword: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    errors: {},
    style: null,
    touched: {},
  };

  template(css) {
    const {
      t,
      style,
      onSubmit,
      errors,
      touched,
      onChangeEmail,
      onChangePassword,
      onBlurEmail,
      onBlurPassword,
      isValid,
    } = this.props;

    return (
      <View style={style}>
        <View style={css('inputs')}>
          <Input
            label={t('login:YOUR_EMAIL').toLowerCase()}
            id={INPUT_EMAIL}
            type="email"
            keyboardType="email-address"
            style={css('input')}
            error={!!touched[INPUT_EMAIL] && errors[INPUT_EMAIL]}
            onChange={text => onChangeEmail(text)}
            onBlur={onBlurEmail}
          />
          <Input
            label={t('login:YOUR_PASSWORD').toLowerCase()}
            id={INPUT_PASSWORD}
            type="password"
            style={css('input')}
            error={!!touched[INPUT_PASSWORD] && errors[INPUT_PASSWORD]}
            onChange={text => onChangePassword(text)}
            onBlur={onBlurPassword}
          />
        </View>
        <View>
          <ButtonSubmit onPress={onSubmit} style={css(['button'])} disabled={!isValid}>
            {t('login:LOGIN')}
          </ButtonSubmit>
          <Link to={Routes.FORGOT_PASSWORD} style={css('forgotPassword')}>
            <Text style={css('forgotPasswordLabel')}>{t('login:FORGOT_PASSWORD')}</Text>
          </Link>
        </View>
      </View>
    );
  }

  styles() {
    return {
      input: { marginBottom: 24 },

      button: { marginBottom: 8 },

      forgotPassword: {
        alignSelf: 'center',
      },

      forgotPasswordLabel: {
        ...TextStyles.small,
        color: Colors.GRAY_DARKER,
        textAlign: 'center',
        textDecorationColor: Colors.TRANSPARENT,
      },
    };
  }
}

export default withLocalization(LoginForm);
