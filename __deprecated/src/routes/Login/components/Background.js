import React from 'react';
import { View, Image } from 'react-native';

import { PureComponent } from 'components/BaseComponent';

import bgLarge from '../../../../public/assets/bg-large.png';

class Background extends PureComponent {
  template(css) {
    return (
      <View style={css('bg', 'bgMedium')} pointerEvents="none">
        <Image source={bgLarge} style={css('bgImage')} />
      </View>
    );
  }

  styles() {
    return {
      bg: {
        display: 'none',
        position: 'absolute',
        alignItems: 'stretch',
        top: -32,
        right: -30,
        bottom: -32,
        left: -30,
        flex: 1,
      },

      bgMedium: { display: 'flex' },

      bgImage: {
        resizeMode: 'cover',
        height: '100%',
        width: '100%',
      },
    };
  }
}

export default Background;
