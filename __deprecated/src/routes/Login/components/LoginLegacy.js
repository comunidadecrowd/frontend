import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ScrollView } from 'react-native';
import { compose } from 'recompose';
import { Link, withRouter } from 'react-router-native';
import { withFormik } from 'formik';
import * as Yup from 'yup';

import { login } from 'api/auth';

import { PureComponent } from 'components/BaseComponent';
import LoadingModal from 'components/LoadingModal';

import Routes from 'constants/Routes';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import { saveToken } from 'utils/auth';
import withLocalization from 'utils/withLocalization';

import { INPUT_EMAIL, INPUT_PASSWORD } from '../_constants';

import LoginForm from './LoginForm';

class LoginLegacy extends PureComponent {
  static propTypes = {
    errors: PropTypes.object,
    history: PropTypes.any.isRequired,
    isValid: PropTypes.bool.isRequired,
    touched: PropTypes.object,
    values: PropTypes.object,

    // functions
    handleSubmit: PropTypes.func.isRequired,
    setErrors: PropTypes.func.isRequired,
    setFieldTouched: PropTypes.func.isRequired,
    setFieldValue: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
  };

  static defaultProps = {
    errors: {},
    touched: {},
    values: {},
  };

  state = {
    isLoading: false,
  };

  _handleBlur = field => {
    const { setFieldTouched } = this.props;

    setFieldTouched && setFieldTouched(field);
  };

  _handleChange = (field, text) => {
    const { setFieldValue } = this.props;

    setFieldValue && setFieldValue(field, text);
  };

  _handleSubmit = event => {
    const { handleSubmit, isValid, values, setErrors, history } = this.props;

    handleSubmit && handleSubmit(event);

    if (isValid) {
      this.setState({ isLoading: true }, () =>
        login(values[INPUT_EMAIL], values[INPUT_PASSWORD])
          .then(response => {
            saveToken(response.data)
              .then(() => history.push(Routes.HOME))
              .catch(error => {
                setErrors({
                  [INPUT_EMAIL]: true,
                  [INPUT_PASSWORD]: error,
                });
                this.setState({ isLoading: false });
              });
          })
          .catch(error => {
            setErrors({
              [INPUT_EMAIL]: true,
              [INPUT_PASSWORD]: error,
            });
            this.setState({ isLoading: false });
          })
      );
    }
  };

  _handleBlurEmail = () => this._handleBlur(INPUT_EMAIL);
  _handleBlurPassword = () => this._handleBlur(INPUT_PASSWORD);
  _handleChangeEmail = () => this._handleChangeEmail(INPUT_EMAIL);
  _handleChangePassword = () => this._handleChangePassword(INPUT_PASSWORD);

  template(css) {
    const { t, errors, touched, isValid } = this.props;
    const { isLoading, phoneNumber } = this.state;

    return (
      <ScrollView
        scrollEnabled={false}
        style={css('root')}
        contentContainerStyle={css('rootContainer')}
      >
        <View style={css('headline')}>
          <Text style={css(['headlineText', TextStyles.h3], ['headlineTextMedium', TextStyles.h2])}>
            {t('login:TITLE')}
          </Text>
        </View>
        <View style={css('description')}>
          <Text style={css('descriptionText')}>{t('login:CONNECT_WITH_US')}</Text>
        </View>
        <LoginForm
          style={css('form')}
          onChangeEmail={this._handleChangeEmail}
          onChangePassword={this._handleChangePassword}
          onBlurEmail={this._handleBlurEmail}
          onBlurPassword={this._handleBlurEmail}
          onSubmit={this._handleSubmit}
          phoneNumber={phoneNumber}
          errors={errors}
          touched={touched}
          isValid={isValid}
        />
        {isLoading && <LoadingModal />}
        <View style={css('doesntHaveAnAccount')}>
          <Link to={Routes.LOGIN}>
            <Text style={css('doesntHaveAnAccountLabel')}>{t('login:DOESNT_HAVE_AN_ACCOUNT')}</Text>
          </Link>
        </View>
      </ScrollView>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
      },

      rootContainer: {
        flex: 1,
        justifyContent: 'flex-end',
      },

      description: {
        marginBottom: 24,
      },

      descriptionText: {
        ...TextStyles.h5,
        color: Colors.WHITE,
      },

      form: {
        flexGrow: 1,
      },

      doesntHaveAnAccount: {
        alignSelf: 'center',
        marginBottom: 24,
      },

      doesntHaveAnAccountLabel: {
        ...TextStyles.small,
        color: Colors.GRAY_DARKER,
        textAlign: 'center',
        textDecorationColor: Colors.TRANSPARENT,
      },

      headline: {
        marginTop: 24,
        alignSelf: 'flex-start',
      },

      headlineText: { color: Colors.WHITE },

      headlineTextMedium: { textAlign: 'center' },
    };
  }
}

export default compose(
  withRouter,
  withLocalization,
  withFormik({
    mapPropsToValues: () => ({
      [INPUT_EMAIL]: '',
      [INPUT_PASSWORD]: '',
    }),
    validationSchema: ({ t }) =>
      Yup.object().shape({
        [INPUT_EMAIL]: Yup.string()
          .email(t('error:INVALID_EMAIL'))
          .required(t('error:REQUIRED')),
        [INPUT_PASSWORD]: Yup.string().required(t('error:REQUIRED')),
      }),
  })
)(LoginLegacy);
