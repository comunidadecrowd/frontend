import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, Dimensions, View, Text, ImageBackground } from 'react-native';

import { PureComponent } from 'components/BaseComponent';
import Button from 'components/Button';
import ButtonForward from 'components/ButtonForward';
import Row from 'components/Grid/Row';
import Icon, { Icons } from 'components/Icon';

import { Colors } from 'theme/Colors';

class Profile extends PureComponent {
  static propTypes = {
    about: PropTypes.string,
    activities: PropTypes.string,
    city: PropTypes.string,
    education: PropTypes.string,
    name: PropTypes.string,
    picture: PropTypes.string,
    rating: PropTypes.string,
    role: PropTypes.string,
    uf: PropTypes.string,
  };

  static defaultProps = {
    about:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam suscipit mod sagittis.',
    activities:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam suscipit mod sagittis.',
    city: 'São Paulo',
    education:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam suscipit mod sagittis.',
    name: 'Tiago Cruz',
    picture: 'https://randomuser.me/api/portraits/men/77.jpg',
    rating: '4.98',
    role: 'Designer de Branding',
    uf: 'SP',
  };

  template(css) {
    return (
      <ScrollView contentContainerStyle={css('root')} contentOffset={{ y: 72 }}>
        <ImageBackground source={{ uri: this.props.picture }} style={css('avatarImage')}>
          <Row style={css('line')}>
            <Icon icon={Icons.CLOSE} style={css('icon')} />
            <Icon icon={Icons.CONTENT} style={css('icon')} />
          </Row>
          <View style={css('infoBox')}>
            <Text style={css('name')}>{this.props.name}</Text>
            <Text style={css('role')}>{this.props.role}</Text>
            <Text style={css('city')}>
              {this.props.city}, <Text style={css('city')}>{this.props.uf}</Text>
            </Text>
            <Row>
              <Icon icon={Icons.INFO} style={css('icon')} />
              <Text style={css('rating')}>{this.props.rating}</Text>
            </Row>
          </View>
        </ImageBackground>
        <View style={css('content')}>
          <ButtonForward>Completo</ButtonForward>
          <Row>
            <Button>importar in</Button>
            <Button>preferências</Button>
          </Row>
          <Text style={css('city')}>CONECTE-SE</Text>
          <Row style={css('line')}>
            <Text style={css('city')}>SOBRE</Text>
            <Icon icon={Icons.CONTENT} style={css('icon')} />
          </Row>
          <Text style={css('city')}>{this.props.about}</Text>
          <Row style={css('line')}>
            <Text style={css('city')}>ATIVIDADES E HABILIDADES</Text>
            <Icon icon={Icons.CONTENT} style={css('icon')} />
          </Row>
          <Text style={css('city')}>{this.props.activities}</Text>
          <Row style={css('line')}>
            <Text style={css('city')}>EDUCAÇÃO</Text>
            <Icon icon={Icons.CONTENT} style={css('icon')} />
          </Row>
          <Text style={css('city')}>{this.props.education}</Text>
        </View>
      </ScrollView>
    );
  }

  styles() {
    const { height, width } = Dimensions.get('window');

    return {
      root: {
        flexGrow: 1,
        justifyContent: 'flex-start',
        minHeight: height - 32,
      },
      content: {
        paddingHorizontal: 28,
      },
      avatarImage: {
        width: '100%',
        height: width,
        position: 'relative',
      },
      icon: {
        color: Colors.WHITE,
        fontSize: 40,
      },
      infoBox: {
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        alignItems: 'center',
      },
      name: {
        color: Colors.DESIGN,
        fontSize: 28,
        fontWeight: '500',
      },
      role: {
        color: Colors.GRAY_DARKER,
        fontSize: 14,
      },
      city: {
        color: Colors.WHITE,
        fontSize: 14,
        fontWeight: '500',
      },
      rating: {
        color: Colors.WHITE,
        fontSize: 18,
        fontWeight: '500',
      },
      line: { justifyContent: 'space-between' },
      divider: {
        color: '#444444',
        height: '1px',
      },
    };
  }
}

export default Profile;
