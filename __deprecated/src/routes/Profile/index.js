import React from 'react';

import { PureComponent } from 'components/BaseComponent';

import Profile from './components/Profile';

class Chat extends PureComponent {
  template() {
    return <Profile />;
  }
}

export default Chat;
