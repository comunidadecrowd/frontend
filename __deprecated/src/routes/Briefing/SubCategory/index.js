import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';
import { compose, withProps } from 'recompose';
import { withRouter } from 'react-router-native';

import { PureComponent } from 'components/BaseComponent';
import Column from 'components/Grid/Column';
import Icon, { Icons } from 'components/Icon';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import withLocalization from 'utils/withLocalization';

import SubCategoriesList from './components/SubCategoriesList';
import CATEGORIES from '../_constants';

class SubCategory extends PureComponent {
  static propTypes = {
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
      })
    ).isRequired,
    slug: PropTypes.string.isRequired,

    // functions
    t: PropTypes.func.isRequired,
  };

  template(css) {
    const { slug: category, categories, t } = this.props;

    return (
      <View style={css('root', 'rootMedium')}>
        <Column size={12} sizeMedium={10} style={css('wrapper')}>
          <View style={css('titleWrapper')}>
            <Text style={css('category', 'categoryMedium')}>
              {t(`categories:${category}`).toLowerCase()}
            </Text>
            <Icon
              icon={Icons[category.toUpperCase().replace(/-/g, '_')]}
              style={css('icon', 'iconMedium')}
            />
          </View>
          <View style={css('list', 'listMedium')}>
            <SubCategoriesList categories={categories} />
          </View>
        </Column>
      </View>
    );
  }

  styles() {
    return {
      root: {
        backgroundColor: Colors.BLACK,
        flexGrow: 1,
      },

      rootMedium: {
        justifyContent: 'center',
      },

      wrapper: {
        alignSelf: 'center',
        justifyContent: 'center',
        flex: 1,
      },

      titleWrapper: { marginBottom: 24 },

      category: {
        ...TextStyles.h1,
        color: Colors.WHITE,
        display: 'none',
      },

      categoryMedium: { display: 'flex' },

      icon: {
        color: Colors.WHITE,
        fontSize: 100,
        marginLeft: -16,
        marginTop: 16,
      },

      iconMedium: { display: 'none' },

      list: {
        flexGrow: 1,
        justifyContent: 'center',
        paddingBottom: 100,
      },

      listMedium: {
        flexGrow: 0,
        paddingBottom: 0,
      },
    };
  }
}

export default compose(
  withLocalization,
  withRouter,
  withProps(({ slug }) => {
    const category = CATEGORIES.find(item => item.slug === slug);

    return {
      categories: category ? category.subcategories : null,
    };
  })
)(SubCategory);
