import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SubCategoriesItem from './SubCategoriesItem';

class SubCategoriesList extends Component {
  static propTypes = {
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
      })
    ).isRequired,
  };

  render() {
    const { categories } = this.props;

    return categories.map((item, index) => <SubCategoriesItem category={item} key={index} />);
  }
}

export default SubCategoriesList;
