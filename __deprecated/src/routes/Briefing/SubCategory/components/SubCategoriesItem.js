import React from 'react';
import PropTypes from 'prop-types';
import { Animated, TouchableOpacity, Platform } from 'react-native';
import { compose } from 'recompose';
import { withRouter } from 'react-router-native';
import { Navigation } from 'react-native-navigation';

import { Component } from 'components/BaseComponent';

import { BriefingRoutes } from 'constants/Routes';
import CustomPropTypes from 'constants/CustomPropTypes';

import { Colors, TextStyles, Duration } from 'theme';

import withAnimation from 'utils/withAnimation';
import withLocalization from 'utils/withLocalization';

class SubCategoriesItem extends Component {
  static propTypes = {
    category: PropTypes.shape({
      slug: PropTypes.string.isRequired,
    }).isRequired,
    hovering: CustomPropTypes.animation.isRequired,

    // functions
    t: PropTypes.func.isRequired,
    startHoveringTiming: PropTypes.func.isRequired,
  };

  _handleHover = isHovering => {
    const { startHoveringTiming } = this.props;

    startHoveringTiming({
      toValue: Number(isHovering),
      duration: Duration.FAST,
    });
  };

  _handleMouseEnter = () => this._handleHover(true);
  _handleMouseLeave = () => this._handleHover(false);

  _handlePress = () => {
    const {
      category: { slug },
    } = this.props;

    Navigation.push(BriefingRoutes.SUBCATEGORY, {
      component: {
        name: BriefingRoutes.BRIEFING,
        id: BriefingRoutes.BRIEFING,
        passProps: {
          slug,
        },
        options: {
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
          topBar: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
        },
      },
    });
  };

  template(css) {
    const {
      category: { slug },
      t,
      hovering,
    } = this.props;

    return (
      <TouchableOpacity style={css('root', 'rootMedium')} onPress={this._handlePress}>
        <Animated.View
          style={css(
            {
              paddingLeft: hovering.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 24],
              }),
            },
            {
              paddingLeft: hovering.interpolate({
                inputRange: [0, 1],
                outputRange: [0, 32],
              }),
            }
          )}
        >
          <Animated.View
            style={css(
              [
                'arrow',
                {
                  opacity: hovering,
                  transform: [
                    {
                      translateX: hovering.interpolate({
                        inputRange: [0, 1],
                        outputRange: [-32, 0],
                      }),
                    },
                  ],
                },
              ],
              'arrowMedium'
            )}
          />
          <Animated.Text
            style={css(
              [
                'label',
                {
                  color: hovering.interpolate({
                    inputRange: [0, 1],
                    outputRange: [Colors.GRAY_DARK, Colors.BLUE],
                  }),
                },
                TextStyles.h4,
              ],
              TextStyles.h2
            )}
          >
            {t(`categories:${slug}`).toLowerCase()}
          </Animated.Text>
        </Animated.View>
      </TouchableOpacity>
    );
  }

  styles() {
    return {
      root: {
        marginBottom: 24,
        alignSelf: 'flex-start',
      },

      rootMedium: { marginBottom: 32 },

      arrow: {
        position: 'absolute',
        borderLeftColor: Colors.BLUE,
        borderTopColor: Colors.TRANSPARENT,
        borderBottomColor: Colors.TRANSPARENT,
        borderLeftWidth: 14,
        borderTopWidth: 7,
        borderBottomWidth: 7,
        left: 0,
        top: 20,
      },

      arrowMedium: {
        borderLeftWidth: 20,
        borderTopWidth: 10,
        borderBottomWidth: 10,
        top: 40,
      },
    };
  }
}

export default compose(
  withRouter,
  withLocalization,
  withAnimation('hovering')
)(SubCategoriesItem);
