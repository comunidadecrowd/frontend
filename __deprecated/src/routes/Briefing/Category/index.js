import { withProps } from 'recompose';

import React from 'react';
import PropTypes from 'prop-types';
import { View, Text } from 'react-native';

import Button from 'components/Button';
import { PureComponent } from 'components/BaseComponent';
import Column from 'components/Grid/Column';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import withLocalization from 'utils/withLocalization';

import CategoriesList from './components/CategoriesList';
import CATEGORIES from '../_constants';

class Category extends PureComponent {
  static propTypes = {
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
      })
    ).isRequired,

    // functions
    t: PropTypes.func.isRequired,
  };

  template(css) {
    const { t, categories } = this.props;

    return (
      <View style={css('root')}>
        <Column size={12} sizeMedium={6} sizeLarge={4} style={css('headlineWrapper')}>
          <Text style={css(['headlineText', TextStyles.h3], ['headlineTextMedium', TextStyles.h2])}>
            {t('briefing:CATEGORY_TITLE')}
          </Text>
        </Column>
        <CategoriesList categories={categories} />
        <Button
          style={css('others')}
          labelStyle={TextStyles.body}
          arrowsStyle={css('othersArrows')}
          onPress={() => {}}
        >
          outros
        </Button>
      </View>
    );
  }

  styles() {
    return {
      root: {
        backgroundColor: Colors.BLACK,
        flexGrow: 1,
        justifyContent: 'space-between',
        paddingTop: 24,
      },

      headlineWrapper: {
        alignSelf: 'center',
      },

      headlineText: { color: Colors.WHITE },

      headlineTextMedium: { textAlign: 'center' },

      others: {
        marginTop: 64,
        marginBottom: 16,
      },

      othersArrows: { top: 9 },
    };
  }
}

export default withProps({ categories: CATEGORIES })(withLocalization(Category));
