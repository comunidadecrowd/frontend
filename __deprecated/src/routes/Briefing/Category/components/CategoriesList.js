import React from 'react';
import PropTypes from 'prop-types';

import Row from 'components/Grid/Row';
import Column from 'components/Grid/Column';
import { PureComponent } from 'components/BaseComponent';

import withDimensions from 'utils/withDimensions';

import CategoriesItem from './CategoriesItem';

class CategoriesList extends PureComponent {
  static propTypes = {
    categories: PropTypes.arrayOf(
      PropTypes.shape({
        slug: PropTypes.string.isRequired,
      })
    ).isRequired,
  };

  template(css) {
    const { categories } = this.props;

    return (
      <Row style={css('root')}>
        {categories.map((item, index) => (
          <Column size={6} sizeMedium={3} style={css('column')} key={index} withMargin={false}>
            <CategoriesItem category={item} />
          </Column>
        ))}
      </Row>
    );
  }

  styles() {
    return {
      root: {
        justifyContent: 'space-around',
        flexWrap: 'wrap',
      },

      column: {
        marginRight: 0,
        marginTop: 32,
      },
    };
  }
}

export default withDimensions(CategoriesList);
