import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity, Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';

import designAnimation from 'assets/animations/icons/design.json';
import technologyAnimation from 'assets/animations/icons/technology.json';
import contentAnimation from 'assets/animations/icons/content.json';
import performanceAnimation from 'assets/animations/icons/performance.json';

import { Component } from 'components/BaseComponent';
import Icon, { Icons } from 'components/Icon';
import Lottie from 'components/Lottie';

import { BriefingRoutes } from 'constants/Routes';

import { TextStyles } from 'theme/TextStyles';
import { Colors } from 'theme/Colors';

import withLocalization from 'utils/withLocalization';

class CategoriesItem extends Component {
  static propTypes = {
    category: PropTypes.shape({
      slug: PropTypes.string.isRequired,
    }).isRequired,

    // functions
    t: PropTypes.func.isRequired,
  };

  _animation = null;

  _handlePress = category => {
    Navigation.push(BriefingRoutes.CATEGORY, {
      component: {
        name: BriefingRoutes.SUBCATEGORY,
        id: BriefingRoutes.SUBCATEGORY,
        passProps: {
          slug: category,
        },
        options: {
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
          topBar: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
        },
      },
    });
  };

  componentDidMount() {
    this._animation.play && this._animation.play();
  }

  template(css) {
    const {
      category: { slug },
      t,
    } = this.props;
    let iconAnimation = null;

    /**
     * React Native doesn't handle require with dynamic strings.
     */
    switch (slug) {
      case 'design':
        iconAnimation = designAnimation;
        break;
      case 'technology':
        iconAnimation = technologyAnimation;
        break;
      case 'content':
        iconAnimation = contentAnimation;
        break;
      case 'performance':
        iconAnimation = performanceAnimation;
        break;
    }

    return (
      <TouchableOpacity style={css('root')} onPress={() => this._handlePress(slug)}>
        <View>
          {iconAnimation ? (
            <Lottie
              ref={animation => {
                this._animation = animation;
              }}
              style={css(['animation', `animation_${slug}`])}
              source={iconAnimation}
              autoplay={true}
              loop={false}
            />
          ) : (
            <Icon icon={Icons[slug.toUpperCase().replace(/-/g, '_')]} style={css('icon')} />
          )}
          <Text style={css('item')}>{t(`categories:${slug}`).toLowerCase()}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  styles() {
    return {
      root: {
        alignItems: 'center',
      },

      icon: {
        color: Colors.WHITE,
        fontSize: 80,
        textAlign: 'center',
        height: 90,
      },

      animation: {
        height: 68,
      },

      animation_design: {
        width: 70,
      },

      item: {
        ...TextStyles.body,
        color: Colors.WHITE,
        textAlign: 'center',
      },
    };
  }
}

export default withLocalization(CategoriesItem);
