import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, TouchableOpacity } from 'react-native';

import Icon, { Icons } from 'components/Icon';
import ButtonSubmit from 'components/ButtonSubmit';
import { PureComponent } from 'components/BaseComponent';

import { TextStyles, Colors } from 'theme';

import withLocalization from 'utils/withLocalization';

class Review extends PureComponent {
  static propTypes = {
    briefing: PropTypes.string.isRequired,
    objective: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,

    // functions
    onPressStep: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
    t: PropTypes.func.isRequired,
  };

  template(css) {
    const { t, title, objective, briefing, onPressStep, onSubmit } = this.props;

    return (
      <View style={css(null, 'hidden')}>
        <Text style={css('sectionTitle')}>Vamos revisar?</Text>
        <View style={css('field')}>
          <View style={css(['timeline', 'timelineTitle'])} />
          <TouchableOpacity onPress={() => onPressStep(0)}>
            <Icon icon={Icons.TOOLTIP} style={css(['icon', 'iconTitle'])} />
          </TouchableOpacity>
          <Text style={css(['text', 'title'])}>{title}</Text>
        </View>
        <View style={css('field')}>
          <View style={css(['timeline', 'timelineObjective'])} />
          <TouchableOpacity onPress={() => onPressStep(1)}>
            <Icon icon={Icons.TOOLTIP} style={css('icon')} />
          </TouchableOpacity>
          <Text style={css(['text', 'objective'])}>{objective}</Text>
        </View>
        <View style={css('field')}>
          <TouchableOpacity onPress={() => onPressStep(2)}>
            <Icon icon={Icons.TOOLTIP} style={css('icon')} />
          </TouchableOpacity>
          <Text style={css(['text', 'briefing'])}>{briefing}</Text>
        </View>
        <ButtonSubmit style={css('submitWrapper')} onPress={onSubmit}>
          {t('briefing:BRIEFING_SUBMIT')}
        </ButtonSubmit>
      </View>
    );
  }

  styles() {
    return {
      sectionTitle: {
        ...TextStyles.h4,
        marginBottom: 24,
      },

      text: { flexGrow: 1 },

      title: {
        ...TextStyles.h4,
        marginBottom: 32,
        color: Colors.GRAY_DARKER,
      },

      objective: {
        ...TextStyles.h6,
        marginBottom: 32,
        color: Colors.GRAY_DARKER,
        marginTop: 4,
      },

      briefing: {
        ...TextStyles.body,
        color: Colors.GRAY_DARKER,
        marginTop: 4,
      },

      icon: {
        fontSize: 30,
        color: Colors.GRAY_DARKER,
        marginRight: 8,
        zIndex: 1,
      },

      iconTitle: { marginTop: 8 },

      field: {
        flexDirection: 'row',
      },

      timeline: {
        position: 'absolute',
        left: 15,
        width: 1,
        borderLeftWidth: 1,
        borderLeftColor: Colors.GRAY_DARKER,
      },

      timelineTitle: {
        bottom: -8,
        top: 32,
      },

      timelineObjective: {
        bottom: -8,
        top: 24,
      },

      submitWrapper: {
        marginVertical: 24,
        marginHorizontal: 'auto',
      },

      hidden: { display: 'none' },
    };
  }
}

export default withLocalization(Review);
