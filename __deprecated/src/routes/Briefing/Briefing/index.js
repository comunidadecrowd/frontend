import { compose, lifecycle } from 'recompose';

import React from 'react';
import PropTypes from 'prop-types';
import { Platform, View, Text, Keyboard, TouchableOpacity, ScrollView } from 'react-native';
import { withRouter } from 'react-router-native';
import { Navigation } from 'react-native-navigation';

// Components
import { Component } from 'components/BaseComponent';
import ButtonForward from 'components/ButtonForward';
import ButtonSubmit from 'components/ButtonSubmit';
import ControlsBar from 'components/ControlsBar';
import Icon, { Icons } from 'components/Icon';
import Input from 'components/Input';
import Column from 'components/Grid/Column';
import Row from 'components/Grid/Row';
import { withTheme } from 'components/ThemeProvider';
import RichInput from 'components/RichInput';
import RichInputControls from 'components/RichInputControls';
import TooltipBar from 'components/TooltipBar';

// Constants
import CustomPropTypes from 'constants/CustomPropTypes';
import { MARGIN_HORIZONTAL } from 'constants/Layout';
import Routes, { BriefingRoutes } from 'constants/Routes';
import { THEME_WHITE } from 'constants/Themes';

// Theme
import { Colors } from 'theme/Colors';
import { TextStyles } from 'theme/TextStyles';
import { SMALL } from 'theme/Breakpoints';

// Utils
import withDimensions from 'utils/withDimensions';
import withLocalization from 'utils/withLocalization';

import Review from './components/Review';
import { INPUT_TITLE, INPUT_OBJECTIVE, INPUT_BRIEFING } from './_constants';

const IS_WEB = Platform.OS === 'web';
const numberOfSteps = 3;

class Briefing extends Component {
  static propTypes = {
    slug: PropTypes.string.isRequired,
    t: PropTypes.func.isRequired,
    breakpoint: CustomPropTypes.breakpoint.isRequired,
    history: PropTypes.any.isRequired,
    location: PropTypes.any.isRequired,
  };

  state = {
    tooltipOpenIndex: null,
    title: '',
    objective: '',
    briefing: '',
  };

  // member variables
  _briefing = null;

  _gotoStep = step => {
    const { history, location } = this.props;

    history.push(location.pathname, {
      step,
      isEditing: true,
    });
  };

  _handleBack = () => {
    const { history, location } = this.props;

    history.push(location.pathname, {
      step: location.state.step - 1,
      isEditing: true,
    });
  };

  _handleBlur = () => {
    const { breakpoint } = this.props;

    if (breakpoint !== SMALL) {
      this.setState({ tooltipOpenIndex: null });
    }
  };

  _handleChange = (field, value) => {
    this.setState({ [field]: value });
  };

  _handleChangeBriefing = value => this._handleChange('briefing', value);
  _handleChangeObjective = value => this._handleChange('objective', value);
  _handleChangeTitle = value => this._handleChange('title', value);

  _handleContinue = () => {
    const { history, location } = this.props;

    let nextStep = location.state ? location.state.step + 1 : 1;

    history.push(location.pathname, { step: nextStep });
    Keyboard.dismiss();

    if (nextStep < numberOfSteps) {
      this.setState({ tooltipOpenIndex: nextStep });
    }
  };

  _handleFocus = index => {
    const { breakpoint } = this.props;

    if (breakpoint !== SMALL) {
      this.setState({ tooltipOpenIndex: index });
    }
  };

  _handleSubmit = () => {
    Navigation.push(Routes.BRIEFING, {
      component: {
        name: BriefingRoutes.PUBLISH,
        id: BriefingRoutes.PUBLISH,
        options: {
          bottomTabs: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
          topBar: {
            visible: false,
            ...Platform.select({ android: { drawBehind: true } }),
          },
        },
      },
    });
  };

  componentDidMount() {
    const { history, location } = this.props;

    // Reset location state
    !!location.state && history.push(location.pathname);
  }

  template(css) {
    const { slug: category, t, breakpoint, location } = this.props;
    const { tooltipOpenIndex, title, objective, briefing } = this.state;

    const currentStep = location.state ? location.state.step || 0 : 0;

    const stepValues = [title, objective, briefing];
    const isButtonEnabled = !!stepValues[currentStep] && stepValues[currentStep].length > 0;

    const isSmall = breakpoint === SMALL;
    const isLast = currentStep === numberOfSteps - 1;
    const isReviewing = currentStep === numberOfSteps;
    const tips = [
      {
        icon: Icons.TEXTO,
        title: t('briefing:TOOLTIP_TITLE_TITLE'),
        description: t('briefing:TOOLTIP_TITLE_DESCRIPTION'),
      },
      {
        icon: Icons.TARGET,
        title: t('briefing:TOOLTIP_OBJECTIVE_TITLE'),
        description: t('briefing:TOOLTIP_OBJECTIVE_DESCRIPTION'),
      },
      {
        icon: Icons.BRIEFING,
        title: t('briefing:TOOLTIP_BRIEFING_TITLE'),
        description: t('briefing:TOOLTIP_BRIEFING_DESCRIPTION'),
      },
    ];

    return (
      <ScrollView contentContainerStyle={css('root')} scrollEnabled={false}>
        <View style={css('navigation')}>
          {currentStep !== 0 &&
            !isReviewing && (
              <TouchableOpacity onPress={this._handleBack} style={css('navigationBack')}>
                <Icon icon={Icons.CHEVRON_LEFT} style={css('navigationIcon')} />
              </TouchableOpacity>
            )}
          {!isReviewing && (
            <TouchableOpacity onPress={this._handleContinue} style={css('navigationNext')}>
              <Icon icon={Icons.CHEVRON_RIGHT} style={css('navigationIcon')} />
            </TouchableOpacity>
          )}
        </View>
        <Row>
          <Column style={css('hidden', 'formatMediumColumn')} size={1} withMargin>
            {IS_WEB && (
              <RichInputControls
                style={css('formatMedium')}
                iconStyle={css('formatIconMedium')}
                toggleBold={this._briefing && this._briefing.toggleBold}
                toggleItalic={this._briefing && this._briefing.toggleItalic}
                toggleUnderline={this._briefing && this._briefing.toggleUnderline}
              />
            )}
          </Column>
          <Column sizeMedium={8}>
            <Icon
              icon={Icons[category.toUpperCase().replace(/-/g, '_')]}
              style={css('icon', 'iconMedium')}
            />
            <View style={css(['section', currentStep === 0 && 'sectionActive'], 'sectionMedium')}>
              <Text style={css('title', 'hidden')}>Qual é o título?</Text>
              <Input
                label={t(isSmall ? 'briefing:BRIEFING_TITLE_SMALL' : 'briefing:BRIEFING_TITLE')}
                styleWrapper={css('inputWrapper')}
                styleLabel={css(null, 'titleMedium')}
                styleInput={css('input', 'titleMedium')}
                styleLabelHelper={css(null, 'titleHelperMedium')}
                id={INPUT_TITLE}
                onBlur={this._handleBlur}
                onFocus={() => this._handleFocus(0)}
                onChange={this._handleChangeTitle}
                enablesReturnKeyAutomatically
                returnKeyType="next"
                onSubmitEditing={this._handleContinue}
                autoCorrect={false}
              />
            </View>
            <View style={css(['section', currentStep === 1 && 'sectionActive'], 'sectionMedium')}>
              <Text style={css('title', 'hidden')}>Qual é o objetivo?</Text>
              <Input
                label={t(
                  isSmall ? 'briefing:BRIEFING_OBJECTIVE_SMALL' : 'briefing:BRIEFING_OBJECTIVE'
                )}
                styleWrapper={css('inputWrapper')}
                styleLabel={css(null, 'objectiveMedium')}
                styleInput={css('input', 'objectiveMedium')}
                styleLabelHelper={css(null, 'objectiveHelperMedium')}
                id={INPUT_OBJECTIVE}
                onBlur={this._handleBlur}
                onFocus={() => this._handleFocus(1)}
                onChange={this._handleChangeObjective}
                enablesReturnKeyAutomatically
                returnKeyType="next"
                onSubmitEditing={this._handleContinue}
                autoCorrect={false}
              />
            </View>
            <View style={css(['section', currentStep === 2 && 'sectionActive'], 'sectionMedium')}>
              <Input
                component={RichInput}
                label={t('briefing:BRIEFING_BRIEFING')}
                styleWrapper={css(['inputWrapper', 'description'])}
                styleInput={css('input')}
                id={INPUT_BRIEFING}
                setRef={ref => (this._briefing = ref)}
                shouldKeepFocusWhenHasValue
                multiline
                numberOfLines={6}
                onBlur={this._handleBlur}
                onFocus={() => this._handleFocus(2)}
                onChange={this._handleChangeBriefing}
                autoCorrect={false}
              />
            </View>
            {currentStep === 3 && (
              <View style={css(['section', 'sectionActive'], 'hidden')}>
                <Review
                  title={title}
                  objective={objective}
                  briefing={briefing}
                  onPressStep={step => this._gotoStep(step)}
                  onSubmit={this._handleSubmit}
                />
              </View>
            )}
            <ButtonSubmit
              style={css('hidden', 'submitWrapper')}
              labelStyle={css('submit')}
              arrowLeftStyle={css('submitArrowLeft')}
              arrowRightStyle={css('submitArrowRight')}
              onPress={this._handleSubmit}
            >
              {t('briefing:BRIEFING_SUBMIT')}
            </ButtonSubmit>
          </Column>
        </Row>
        <ControlsBar style={css(['controlsBar', isReviewing && 'hidden'])}>
          {isLast && (
            <RichInputControls
              style={css('format')}
              iconStyle={css('formatIcon')}
              toggleBold={this._briefing && this._briefing.toggleBold}
              toggleItalic={this._briefing && this._briefing.toggleItalic}
              toggleUnderline={this._briefing && this._briefing.toggleUnderline}
            />
          )}
          <View style={css('controlButtonWrapper')}>
            <ButtonForward
              isLastStep={isLast}
              onPress={this._handleContinue}
              disabled={!isButtonEnabled}
            >
              {t(isLast ? 'briefing:BRIEFING_SUBMIT' : 'briefing:BRIEFING_CONTINUE')}
            </ButtonForward>
          </View>
        </ControlsBar>
        <TooltipBar
          tips={tips}
          currentIndex={currentStep}
          openIndex={tooltipOpenIndex}
          shouldStartOpen={breakpoint === SMALL}
        />
      </ScrollView>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
        paddingLeft: MARGIN_HORIZONTAL,
      },

      navigation: {
        alignItems: 'center',
        flexDirection: 'row',
        height: 48,
        justifyContent: 'space-between',
        marginTop: 4,
      },

      navigationBack: {
        left: -6,
      },

      navigationNext: {
        marginLeft: 'auto',
        right: -6,
      },

      navigationIcon: {
        color: Colors.BLACK,
        fontSize: 24,
        marginHorizontal: 4,
      },

      icon: {
        color: Colors.BLACK,
        fontSize: 100,
        marginLeft: -16,
        marginTop: 0,
      },

      description: {
        marginTop: 24,
        minHeight: 96,
        maxHeight: 196,
      },

      inputWrapper: {
        backgroundColor: Colors.WHITE,
      },

      input: { color: Colors.BLACK },

      titleMedium: TextStyles.h1,

      titleHelperMedium: {
        borderLeftWidth: 30,
        borderTopWidth: 15,
        borderBottomWidth: 15,
        top: 56,
      },

      objectiveMedium: {
        ...TextStyles.h2,
        lineHeight: 66,
      },

      objectiveHelperMedium: {
        borderLeftWidth: 22,
        borderTopWidth: 11,
        borderBottomWidth: 11,
        top: 52,
      },

      submitWrapper: {
        alignSelf: 'flex-start',
        display: 'flex',
        marginTop: 24,
      },

      submit: { color: Colors.GRAY },

      submitArrowLeft: { borderLeftColor: Colors.GRAY },

      submitArrowRight: { borderRightColor: Colors.GRAY },

      title: {
        ...TextStyles.h4,
        marginBottom: 24,
        paddingRight: 16,
      },

      section: {
        display: 'none',
      },

      sectionActive: { display: 'flex' },

      sectionMedium: {
        display: 'flex',
        paddingTop: 32,
      },

      controlsBar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        zIndex: 0,
        marginTop: 'auto',
        marginLeft: -MARGIN_HORIZONTAL,
      },

      controlButtonWrapper: {
        marginLeft: 'auto',
        paddingLeft: 0,
      },

      format: {
        flexDirection: 'row',
      },

      formatIcon: {
        color: Colors.WHITE,
        fontSize: 30,
      },

      formatMediumColumn: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: 370,
      },

      formatMedium: {
        flexDirection: 'column',
      },

      formatIconMedium: {
        color: Colors.BLACK,
        fontSize: 30,
      },

      hidden: { display: 'none' },
    };
  }
}

export default compose(
  withDimensions,
  withLocalization,
  withRouter,
  withTheme,
  lifecycle({
    componentDidMount() {
      const { changeTheme } = this.props;

      changeTheme(THEME_WHITE);
    },
  })
)(Briefing);
