import React from 'react';
import PropTypes from 'prop-types';
import { Dimensions, View, Text, TouchableWithoutFeedback } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { compose } from 'recompose';

import loading from 'assets/animations/publish/loading.json';
import error from 'assets/animations/publish/error.json';
import success from 'assets/animations/publish/success.json';

import { PureComponent } from 'components/BaseComponent';
import Lottie from 'components/Lottie';
import { withGlobalsContext } from 'components/GlobalsProvider';

import Routes from 'constants/Routes';
import { MARGIN_HORIZONTAL } from 'constants/Layout';

import { Colors } from 'theme/Colors';
import { TextStyles } from 'theme/TextStyles';

import withLocalization from 'utils/withLocalization';

const size = 148;

class Publish extends PureComponent {
  static propTypes = {
    // functions
    t: PropTypes.func.isRequired,
    toggleFooter: PropTypes.func.isRequired,
  };

  state = {
    isLoading: true,
    hasSucceeded: false,
  };

  constructor(props) {
    super(props);

    this._animations = [];
    this._mainAnimation = null;

    const { width: windowWidth, height: windowHeight } = Dimensions.get('window');

    this.numberOfBlocksHorizontal = Math.ceil(windowHeight / size);
    this.numberOfBlocksVertical = Math.ceil(windowWidth / size);
    const isHorizontalOdd = Boolean(this.numberOfBlocksHorizontal % 2);
    const isVerticalOdd = Boolean(this.numberOfBlocksVertical % 2);

    if (!isHorizontalOdd) {
      this.numberOfBlocksHorizontal += 1;
    }

    if (!isVerticalOdd) {
      this.numberOfBlocksVertical += 1;
    }
  }

  _handlePress = () => {
    const { isLoading } = this.state;

    if (isLoading) {
      return;
    }

    Navigation.popToRoot(Routes.BRIEFING);
  };

  componentDidMount() {
    if (!this._animations.length) return;

    this.props.toggleFooter(false);

    // Animates loading randomly
    this._animations.forEach(animation => {
      setTimeout(() => {
        animation.play && animation.play();
      }, Math.ceil(Math.random() * 5000));
    });

    // Simulates response
    setTimeout(() => {
      this.setState({
        isLoading: false,
        hasSucceeded: Math.random() > 0.5,
      });
      this._mainAnimation.play && this._mainAnimation.play();
      this.props.toggleFooter(true);
    }, 7000);
  }

  template(css) {
    const { t } = this.props;
    const { isLoading, hasSucceeded } = this.state;

    const resultAnimation = hasSucceeded ? success : error;
    const mainRow = Math.floor(this.numberOfBlocksVertical / 2);
    const mainColumn = Math.floor(this.numberOfBlocksHorizontal / 2);
    const message = hasSucceeded ? t('briefing:SUCCESS') : t('briefing:ERROR');

    return (
      <View style={css('root')}>
        <View style={css('wrapper')}>
          {[...Array(this.numberOfBlocksVertical)].map((_, rowIndex) => (
            <View style={css('row')} key={rowIndex}>
              {[...Array(this.numberOfBlocksHorizontal)].map((_, index) => {
                const animationIndex = rowIndex * this.numberOfBlocksHorizontal + index;
                const isMain = mainRow === rowIndex && mainColumn === index;
                const animationData = isMain && !isLoading ? resultAnimation : loading;

                return (
                  <View style={css('animation')} key={animationIndex}>
                    <Lottie
                      ref={animation => {
                        this._animations[animationIndex] = animation;
                        if (isMain) {
                          this._mainAnimation = animation;
                        }
                      }}
                      style={css('animation')}
                      source={animationData}
                      autoPlay={false}
                      loop={isLoading || isMain}
                    />
                  </View>
                );
              })}
            </View>
          ))}
        </View>
        <View style={css('messageWrapper')}>
          <Text style={css('message')}>{isLoading ? t('briefing:PUBLISHING') : message}</Text>
        </View>
        <View style={css('touchableWrapper')}>
          <TouchableWithoutFeedback onPress={this._handlePress} style={css('touchable')}>
            <View style={css('touchable')} />
          </TouchableWithoutFeedback>
        </View>
      </View>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
        overflow: 'hidden',
      },

      wrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: -32,
      },

      animation: {
        width: size,
        height: size,
      },

      row: {
        flexDirection: 'column',
        justifyContent: 'center',
      },

      messageWrapper: {
        alignSelf: 'center',
        marginHorizontal: MARGIN_HORIZONTAL,
        position: 'absolute',
        left: -16,
        right: -16,
        top: 178,
        bottom: 0,
        justifyContent: 'center',
        alignItems: 'center',
      },

      message: {
        ...TextStyles.h5,
        backgroundColor: Colors.BLACK,
        color: Colors.WHITE,
        textAlign: 'center',
        padding: 16,
        paddingBottom: 22,
      },

      touchableWrapper: {
        top: 32,
        right: 0,
        bottom: -32,
        left: 0,
        position: 'absolute',
      },

      touchable: {
        flex: 1,
      },
    };
  }
}

export default compose(
  withGlobalsContext,
  withLocalization
)(Publish);
