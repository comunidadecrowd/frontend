export default [
  {
    slug: 'design',
    subcategories: [
      {
        slug: 'branding',
      },
      {
        slug: 'art-direction',
      },
      {
        slug: 'web-design',
      },
      {
        slug: 'ux',
      },
      {
        slug: 'ilustration',
      },
      {
        slug: 'others',
      },
    ],
  },
  {
    slug: 'technology',
    subcategories: [
      {
        slug: 'others',
      },
    ],
  },
  {
    slug: 'content',
    subcategories: [
      {
        slug: 'others',
      },
    ],
  },
  {
    slug: 'performance',
    subcategories: [
      {
        slug: 'others',
      },
    ],
  },
];
