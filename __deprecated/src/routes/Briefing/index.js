import React from 'react';
import { View } from 'react-native';
import { Switch, Route } from 'react-router-native';

import { PureComponent } from 'components/BaseComponent';

import { BriefingRoutes } from 'constants/Routes';

import { Colors } from 'theme/Colors';

import Briefing from './Briefing';
import Category from './Category';
import Publish from './Publish';
import SubCategory from './SubCategory';

class BriefingRouter extends PureComponent {
  template(css) {
    return (
      <View style={css('root')}>
        <Switch>
          <Route exact path={BriefingRoutes.PUBLISH} component={Publish} />
          <Route exact path={BriefingRoutes.CATEGORY} component={Category} />
          <Route exact path={BriefingRoutes.BRIEFING} component={Briefing} />
          <Route exact path={BriefingRoutes.SUBCATEGORY} component={SubCategory} />
        </Switch>
      </View>
    );
  }

  styles() {
    return {
      root: {
        backgroundColor: Colors.BLACK,
        flex: 1,
      },
    };
  }
}

export default BriefingRouter;
