# Includes

Components For Certain Route / URL

## Folders / Files

### Tree

+Route1
  +components (optional)
  +Route1-1 (optional)
  +Route1-2 (optional)
  _constants.js (optional)
  index.js
+Route2
  ...

### Files Description

Every component has a root folder with the proper screen name.  
`components` is a folder to keep the components to be used to render this route(all common components to be used under this route). Each component can be a single file or have a structure same to this (and nested again if necessary) - components, index.js. When the component is defined as a single file (ie. Header), you need to define a test file under __tests__ of this route.  
`Route1-1` is a sub folder to keep the nested route. If url is users/:userId, structure will look like /Users/UserDetils. It can be also just a simple file if it doesn't include any complicated structure in it.  
`_constants.js` should include the constants required for this route.  
`index.js` is where we define a container/page.  
