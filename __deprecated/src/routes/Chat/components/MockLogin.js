/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Dimensions, Text, TouchableOpacity, View, Platform } from 'react-native';
import { noop } from 'lodash';
import { connect } from 'react-redux';
import { compose, setDisplayName } from 'recompose';
import { Navigation } from 'react-native-navigation';

import immutableToJS from 'utils/immutableToJS';
import { getChatToken, setContacts, setCurrentUser } from 'redux/chat/actions';
import { selectState, getSelector } from 'redux/selectors';

import { APP_SWORKER } from '../_storageWorker';
import usersData from '../__user.json';

const { width } = Dimensions.get('window');

const styles = {
  contactButtonWrapper: {
    margin: 30,
    borderRadius: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.2)',
    overflow: 'hidden',
  },
  contactButton: {
    paddingVertical: 20,
    width: width - 60,
    color: 'white',
    textAlign: 'center',
  },
};

class MockLogin extends Component {
  static propTypes = {
    token: PropTypes.shape({
      token: PropTypes.string,
    }),
    tokenRequesting: PropTypes.bool,
    getChatToken: PropTypes.func,
    setContacts: PropTypes.func,
    setCurrentUser: PropTypes.func,
    currentUser: PropTypes.shape({}),
  };

  static defaultProps = {
    token: {},
    tokenRequesting: false,
    getChatToken: noop,
    setContacts: noop,
    setCurrentUser: noop,
    currentUser: {},
  };

  state = {
    init: false,
    requesting: false,
  };

  componentDidUpdate(prevProps) {
    const { token, currentUser } = this.props;

    if (token.token !== prevProps.token.token) {
      this.pickUser(currentUser.userID);
    }
  }

  pickUser = async user => {
    const { token } = this.props;
    let chatInitialized = false;

    if (token.token) {
      this.setState({
        requesting: true,
        init: true,
      });
      chatInitialized = await APP_SWORKER.init(token.token);

      if (!chatInitialized) {
        getChatToken({ uid: user });
        this.setState({ requesting: false });
        return;
      }
      this.setState({ requesting: false });
      Navigation.push('/chat', {
        component: {
          name: '/channels',
          id: '/channels',
          options: {
            bottomTabs: {
              backgroundColor: '#000000',
              visible: true,
              ...Platform.select({ android: { drawBehind: true } }),
            },
            topBar: {
              visible: false,
              ...Platform.select({ android: { drawBehind: true } }),
            },
          },
        },
      });
    }
  };

  onPressContact = user => {
    const { setContacts, setCurrentUser, getChatToken, token, currentUser } = this.props;

    if (!token.token || currentUser.userID !== usersData[user].userID) {
      getChatToken({ uid: usersData[user].userID });
      setContacts([usersData[user === 'user' ? 'friend' : 'user']]);
    } else {
      this.pickUser(usersData[user].userID);
      setContacts([usersData[user === 'user' ? 'friend' : 'user']]);
    }

    setCurrentUser(usersData[user]);
  };

  renderContactButton = (user, label) => {
    const { tokenRequesting } = this.props;

    return (
      <TouchableOpacity
        style={{
          ...styles.contactButtonWrapper,
          opacity: tokenRequesting || this.state.requesting ? 0.5 : 1,
        }}
        disabled={tokenRequesting || this.state.requesting}
        onPress={() => this.onPressContact(user)}
      >
        <Text style={styles.contactButton}>{label}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        {this.renderContactButton('user', 'David Lima')}
        {this.renderContactButton('friend', 'Ingrid Dujord’hui')}
      </View>
    );
  }
}

/* istanbul ignore next */
const mapStateToProps = state => ({
  ...selectState('chat', 'token')(state, 'token'),
  currentUser: getSelector('chat', 'currentUser')(state),
});

/* istanbul ignore next */
const mapDispatchToProps = dispatch => ({
  getChatToken: payload => dispatch(getChatToken(payload)),
  setContacts: payload => dispatch(setContacts(payload)),
  setCurrentUser: payload => dispatch(setCurrentUser(payload)),
});

export default compose(
  setDisplayName('MockLogin'),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  immutableToJS
)(MockLogin);
