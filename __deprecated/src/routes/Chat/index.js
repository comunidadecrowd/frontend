/* eslint-disable */
import React from 'react';
import { View, Text } from 'react-native';

import { PureComponent } from 'components/BaseComponent';

import { Navigation } from 'react-native-navigation';
import Routes from 'root//Routes';

// REFACT:  Arrumando imports das rotas para o novo formato PAGES
  // import { Route, Switch } from 'react-router-native';
  // import InitRouter from 'pages/Init';
  // import Channel from './Channel';
  // import Channels from './Channels';
  // import MockLogin from './components/MockLogin';
// REFACT: 


class ChatRouter extends PureComponent {

  constructor(props) {
    super(props);
    // console.log('ChatRouter constructor this.props', this.props)
  }

  componentDidMount() {}

  template(css) {

    return (
      <View style={css('root')}>
        <Text style={{color: '#fff'}}>Usuário Logado | Iniciando Chat....</Text>
        {/* <Switch> */}
          {/* <Route exact path="/" component={MockLogin} /> // TODO:  Remover após a nova lógica de rotas (realizada pelo Alex && Bruno) */}
          {
            /* 
              // REFACT:  Estruturando para o novo formato de PAGES
                <Route exact path="/" component={InitRouter} />
                <Route exact path="/channels" component={Channels} />
                <Route exact path="/channel" component={Channel} />
              // REFACT: 
              */
          }
        {/* </Switch> */}
      </View>
    );
  }

  styles() {
    return {
      root: {
        flex: 1,
        backgroundColor: 'black',
      },
    };
  }
}

export default ChatRouter;
