import { Dimensions, StyleSheet } from 'react-native';
const { width: windowW, height: windowH } = Dimensions.get('window');

export default StyleSheet.create({
  main: {
    flex: undefined,
    height: windowH,
    paddingBottom: 20,
  },
  row: {
    flexDirection: 'row',
  },
  fullCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    width: '100%',
    height: 80,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    position: 'absolute',
    paddingVertical: 20,
    zIndex: 2,
    top: -25,
  },
  nameWrapper: {
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: -1,
  },
  profileWrapper: {
    height: windowH / 2,
  },
  profileImage: {
    width: '100%',
    height: '100%',
  },
  imageGradient: {
    position: 'absolute',
  },
  userInfos: {
    position: 'absolute',
    bottom: '13%',
    width: '100%',
  },
  mainChat: {
    backgroundColor: '#fff',
    minHeight: windowH,
    paddingVertical: 30,
    paddingHorizontal: 15,
  },
  chatFooter: {
    width: '100%',
    height: 60,
    backgroundColor: '#000',
    paddingHorizontal: 18,
    alignItems: 'center',
  },
  chatFooterButton: {
    width: 27,
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    width: windowW - (18 * 2 + 27 * 3),
    color: '#fff',
    fontSize: 14,
    fontFamily: 'Inter Ui',
    paddingHorizontal: 10,
    paddingTop: 8,
  },
  messageWrapper: {
    minHeight: 36,
    paddingLeft: 48,
    marginBottom: 20,
  },
  userMessage: {
    alignSelf: 'flex-end',
    maxWidth: '85%',
    color: '#000',
  },
  friendMessage: {
    alignSelf: 'flex-start',
    maxWidth: '85%',
  },
  headerActionRight: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  headerActionLeft: {
    position: 'absolute',
    top: 5,
    left: 5,
  },
});
