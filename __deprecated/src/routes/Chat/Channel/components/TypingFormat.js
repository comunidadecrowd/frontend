import React from 'react';
import { View } from 'react-native';
import { Ellipse, G, Svg } from 'react-native-svg';

import styles from './styles';

const TypingFormat = () => {
  const { typingFormatWrapper, row } = styles;

  return (
    <View style={[typingFormatWrapper, row]}>
      {(() => {
        let i;
        const max = 3;
        const dots = [];

        for (i = 0; i <= max; i++) {
          dots.push(
            <Svg key={i} width={7} height={7} style={{ marginRight: 9 }}>
              <G stroke="none" fill="none" fillRule="evenodd">
                <G transform="translate(-76.000000, -722.000000)" fill={i === 0 ? '#000' : '#CCC'}>
                  <G transform="translate(76.000000, 722.000000)">
                    <Ellipse cx="3.42461538" cy="3.36" rx="3.42461538" ry="3.36" />
                  </G>
                </G>
              </G>
            </Svg>
          );
        }
        return dots;
      })()}
    </View>
  );
};

export default TypingFormat;
