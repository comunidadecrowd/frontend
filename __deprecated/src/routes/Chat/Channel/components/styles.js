import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  userImage: {
    width: 36,
    height: 36,
    borderWidth: 1,
    borderRadius: 18,
    position: 'absolute',
    top: 0,
    left: -55,
  },
  userMessageWrapper: {
    minHeight: 16,
    backgroundColor: '#F2F2F2',
    paddingVertical: 7,
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: '#F2F2F2',
  },
  userProposeWrapper: {
    alignSelf: 'flex-end',
    width: '85%',
    color: '#000',
  },
  friendProposeWrapper: {
    alignSelf: 'flex-start',
    width: '85%',
    color: '#000',
  },
  messageAnswerWrapper: {
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: '#000',
    marginLeft: 23,
    alignItems: 'flex-end',
    paddingRight: 50,
  },
  proposeWrapper: {
    backgroundColor: 'rgba(0,0,0,0)',
    borderColor: '#000',
    marginLeft: 23,
    alignItems: 'flex-end',
    paddingRight: 30,
  },
  typingFormatWrapper: {
    minHeight: 16,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
  },
  audioFormatWrapper: {
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 36,
  },
  audioFormatBarWrapper: {
    width: '100%',
    height: 1,
    backgroundColor: '#E4E4E4',
  },
  audioFormatBar: {
    height: '100%',
    position: 'absolute',
    zIndex: 1,
    top: 0,
    left: 0,
  },
  clearBorder: {
    width: 2,
    height: 12,
    backgroundColor: '#ffffff',
    position: 'absolute',
    top: 7.35,
    right: 0,
    zIndex: 1,
  },
  iconBacktick: {
    position: 'absolute',
    bottom: 9,
    right: 10,
  },
});
