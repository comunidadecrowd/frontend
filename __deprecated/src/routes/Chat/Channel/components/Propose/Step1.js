import React, { Component, Fragment } from 'react';
import { Text, View, Dimensions } from 'react-native';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import Carousel, { Pagination } from 'react-native-snap-carousel';

import ButtonSubmit from 'components/ButtonSubmit';
import { DeliveryMan, Buy, Interview } from 'components/Icon/CarouselPropose';

const sliderFirstItem = 0;

const styles = {
  textWrapper: {
    paddingTop: 15,
    paddingHorizontal: 25,
    height: 150,
    color: 'white',
  },
  text: {
    color: 'white',
    textAlign: 'center',
    paddingVertical: 10,
  },
  indicator: {
    color: 'white',
  },
  buttonText: {
    color: 'lightblue',
    alignSelf: 'center',
  },
  paginatorWrapper: {
    alignItems: 'center',
  },
  paginationDot: {
    marginHorizontal: 2,
    bottom: 8,
  },
};

class Step1 extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
  };

  static defaultProps = {
    onSubmit: noop,
  };

  numberOfSteps = 3;

  state = {
    tutorialStep: 1,
    entries: [
      'Mollit proident fugiat minim in sit nostrud cillum consequat esse consequat anim cupidatat laboris officia.',
      'Anim ex cupidatat exercitation non aliquip tempor dolore eu consequat. Id proident et minim amet veniam reprehenderit excepteur dolor voluptate nulla dolore.',
      'Sunt mollit ullamco consequat aliquip nisi amet excepteur nostrud labore voluptate eu ut eiusmod.',
    ],
    sliderActiveItem: sliderFirstItem,
  };

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.textWrapper} key={index}>
        {index === 0 && (
          <Interview height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 1 && <Buy height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />}
        {index === 2 && (
          <DeliveryMan height={60} width={70} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        <Text style={styles.text}>{item}</Text>
      </View>
    );
  };

  render() {
    const { onSubmit } = this.props;
    const { entries, sliderActiveItem } = this.state;
    const { width: itemWidth } = Dimensions.get('window');

    return (
      <Fragment>
        <View style={{ height: 'auto' }}>
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            data={entries}
            renderItem={this.renderItem}
            sliderWidth={itemWidth}
            itemWidth={itemWidth}
            swipeThreshold={10}
            horizontal
            firstItem={sliderFirstItem}
            onSnapToItem={index => this.setState({ sliderActiveItem: index })}
          />
          <Pagination
            dotStyle={styles.paginationDot}
            inactiveDotColor={'#FFFFFF'}
            activeDotIndex={sliderActiveItem}
            dotColor={'lightblue'}
            dotsLength={entries.length}
            containerStyle={styles.stepIndicatorWrapper}
            carouselRef={this._carousel}
            inactiveDotScale={0.9}
          />
          <ButtonSubmit
            style={{
              marginHorizontal: 'auto',
              bottom: 30,
            }}
            onPress={onSubmit}
            labelColor="lightblue"
            activeColor="lightblue"
          >
            nova proposta
          </ButtonSubmit>
        </View>
      </Fragment>
    );
  }
}

export default Step1;
