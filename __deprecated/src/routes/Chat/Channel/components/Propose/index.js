/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Dimensions, TouchableOpacity, Keyboard, Animated } from 'react-native';
import { noop } from 'lodash';
import Interactable from 'react-native-interactable';
import { compose, setDisplayName } from 'recompose';
import { connect } from 'react-redux';

import immutableToJS from 'utils/immutableToJS';
import { selectState, getSelector } from 'redux/selectors';
import { listProposes, updatePropose } from 'redux/propose/actions';

import { Restore } from 'components/Icon/ProposeIcons';
import IconPlus from 'components/Icon/IconPlus';
import { IconBackButton } from 'components/Icon/ChatHeader';
import { TextStyles, Colors } from 'theme';

import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';

const { height } = Dimensions.get('window');

const styles = {
  wrapperInteractable: {
    position: 'absolute',
    width: '100%',
    height,
  },
  wrapperContent: {
    marginTop: 2,
    backgroundColor: 'black',
    borderRadius: 15,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
  },
  text: {
    color: 'white',
  },
  titleWrapper: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    ...TextStyles.h5,
    color: 'white',
  },
  icon: {
    alignSelf: 'center',
  },
  iconsWrapper: {
    alignSelf: 'center',
    flexDirection: 'row',
  },
  leftIconsWrapper: {
    width: 18,
    justifyContent: 'center',
  },
  archiveIcon: {
    alignSelf: 'center',
    marginRight: 10,
    marginLeft: 0,
  },
  handleIconClosePanel: {
    width: '30%',
    height: 8,
    backgroundColor: Colors.GRAY_DARK,
    borderRadius: 50,
    marginLeft: 'auto',
    marginRight: 'auto',
  },
};

class Propose extends Component {
  static propTypes = {
    getRef: PropTypes.func,
    onSubmitPropose: PropTypes.func.isRequired,
    channel: PropTypes.any,
    listProposes: PropTypes.func.isRequired,
    proposes: PropTypes.arrayOf(
      PropTypes.shape({
        body: PropTypes.shape({
          title: PropTypes.string,
          value: PropTypes.number,
          daysTerm: PropTypes.number,
        }),
        proposeId: PropTypes.string,
        userId: PropTypes.string,
        status: PropTypes.number,
      })
    ),
    proposesRequesting: PropTypes.bool,
  };

  static defaultProps = {
    getRef: noop,
    channel: {},
    proposesRequesting: false,
    proposes: [],
  };

  state = {
    title: '',
    value: '',
    term: '',
    step: 1,
    currentSnapIndex: 0,
    proposesList: [],
    archivedList: [],
    displayArchived: false,
    proposesRequesting: false,
  };

  delta = new Animated.Value(0);

  async componentDidMount() {
    const { listProposes, channel: channelSid, proposesRequesting } = this.props;

    // listing is called here to optmize the loading time of proposes list
    await listProposes({ channelSid });

    if (this.props.proposes && this.props.proposes.length && !proposesRequesting) {
      this.setState({ step: 3 });
    }
  }

  componentDidUpdate(prevProps) {
    const { proposes } = this.props;

    if (prevProps.proposes && !prevProps.proposes.length && proposes && proposes.length) {
      this.setState({ step: 3 });
    }
  }

  // componentDidUpdate(prevProps) {
  //   const { archivedList } = this.props;

    // if (prevProps.archivedList && prevProps.archivedList.length && archivedList && archivedList.length === 0) {
    //   this.setState({ displayArchived: false });
    // }
  // }

  onUpdateValue = (name, value) => {
    this.setState({ [name]: value });
  };

  submitEnabled = () => {
    const { title, value, term } = this.state;

    return title !== '' && (value !== '' && value !== 'R$0,00') && term !== '';
  };

  handleSubmitPropose = () => {
    const { onSubmitPropose } = this.props;
    const { title, value, term } = this.state;

    if (!this.submitEnabled()) return;

    onSubmitPropose(title, value, term);
    this.onClickNextStep();
    Keyboard.dismiss();
  };

  onClickNextStep = () => {
    const { step } = this.state;

    this.setState({ step: step + 1 });
  };

  onClickPlus = () => {
    const { step } = this.state;

    if (step !== 2) {
      this.setState({ step: 2 });
    }
  };

  onClickBack = () => {
    const { step } = this.state;

    if (step > 1) {
      this.setState({ step: step - 1 });
    }
  };

  onSnap = ({ nativeEvent: { index } }) => {
    const { proposes } = this.props;

    if (index === 0 && proposes && !proposes.length) {
      this.setState({
        title: '',
        value: '',
        term: '',
        step: 1,
      });
    }
    if (index === 0) {
      this.setState({
        title: null,
        value: null,
        term: null,
        step: 3,
      });
    }

    this.setSnapIndex(index);
  };

  setSnapIndex = index => {
    this.setState({ currentSnapIndex: index });
  };

  showArchived = () => this.setState({ displayArchived: true });
  hideArchived = () => this.setState({ displayArchived: false });

  render() {
    const { getRef, proposes, updatePropose, channel: channelSID } = this.props;
    const { step, currentSnapIndex, displayArchived } = this.state;

    return (
      <Interactable.View
        verticalOnly
        initialPosition={{
          x: 0,
          y: height + 50,
        }}
        ref={c => {
          this.view = c;
          getRef(c);
        }}
        snapPoints={[{ y: height + 50 }, { y: height - height / 1.65 }]}
        style={styles.wrapperInteractable}
        onSnap={this.onSnap}
        currentSnapIndex={currentSnapIndex}
        boundaries={{ top: height - height / 1.6 }}
      >
        <View style={styles.handleIconClosePanel} />
        <View style={styles.wrapperContent}>
          <View style={styles.titleWrapper}>
            <View style={styles.leftIconsWrapper}>
              {step === 2 && (
                <TouchableOpacity style={{ alignSelf: 'center' }} onPress={this.onClickBack}>
                  <IconBackButton
                    style={{ alignSelf: 'center' }}
                    width={18}
                    height={18}
                    color="#fff"
                  />
                </TouchableOpacity>
              )}
              {step === 3 && (
                <Animated.View
                  style={[
                    styles.archiveIcon,
                    {
                      // transform: [{ scaleX: this.delta }, { scaleY: this.delta }],
                    },
                  ]}
                >
                  <TouchableOpacity onPress={!displayArchived ? this.showArchived : this.hideArchived}>
                    <Restore
                      width={18}
                      height={18}
                      color={displayArchived ? '#ADD8E6' : '#FFFFFF'}
                    />
                  </TouchableOpacity>
                </Animated.View>
              )}
            </View>
            <View style={{ alignItems: 'center' }}>
              <Text style={styles.title}>Propostas</Text>
            </View>
            <View style={styles.iconsWrapper}>
              <TouchableOpacity style={styles.icon} onPress={this.onClickPlus}>
                <IconPlus width={18} height={18} color="#fff" />
              </TouchableOpacity>
            </View>
          </View>
          {step === 1 && <Step1 onSubmit={this.onClickNextStep} />}
          {step === 2 && (
            <Step2
              onSubmit={this.handleSubmitPropose}
              disabled={!this.submitEnabled()}
              onUpdate={this.onUpdateValue}
            />
          )}
          {step === 3 && (
            <Step3
              proposes={proposes}
              channel={channelSID}
              isArchiveList={displayArchived}
              updatePropose={updatePropose}
            />
          )}
        </View>
      </Interactable.View>
    );
  }
}

/* istanbul ignore next */
const mapStateToProps = state => ({
  ...selectState('propose', 'proposes')(state, 'proposes'),
  archivedList: getSelector('propose', 'archivedList')(state),
});

/* istanbul ignore next */
const mapDispatchToProps = dispatch => ({
  listProposes: payload => dispatch(listProposes(payload)),
  approvePropose: payload => dispatch(approvePropose(payload)),
});

export default compose(
  setDisplayName('Propose'),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  immutableToJS
)(Propose);
