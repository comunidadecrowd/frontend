/* eslint-disable */
import React, { Component } from 'react';
import { View, Dimensions, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { setDisplayName, compose } from 'recompose';
import { connect } from 'react-redux';

import { selectState, getSelector } from 'redux/selectors';
import { listProposes, approvePropose } from 'redux/propose/actions';
import immutableToJS from 'utils/immutableToJS';

import ProposeList from './ProposeList';

const { height } = Dimensions.get('window');

const styles = {
  root: {
    height: height / 2.35,
    backgroundColor: 'black',
  },
};

class Step3 extends Component {
  static propTypes = {
    proposes: PropTypes.shape({
      body: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.number,
        daysTerm: PropTypes.number,
      }),
      userId: PropTypes.string,
      status: PropTypes.number,
      proposeId: PropTypes.string,
    }),
    isArchiveList: PropTypes.bool,
    proposes: PropTypes.arrayOf(PropTypes.shape({})),
    proposeCreated: PropTypes.shape({
      body: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.number,
        daysTerm: PropTypes.number,
      }),
      userId: PropTypes.string,
      status: PropTypes.number,
      proposeId: PropTypes.string,
    }),
    approvePropose: PropTypes.func.isRequired,
    proposesRequesting: PropTypes.bool,
  };

  static defaultProps = {
    proposes: [],
    isArchiveList: false,
    proposesRequesting: false,
    proposeCreated: {},
  };

  state = {
    proposeList: [],
    archivedList: [],
  };

  componentDidMount() {
    const { proposes, currentUser: { userID } } = this.props;
    const activeProposeList = proposes.length ? proposes.filter(propose => propose.userId == userID && propose.status >= 200) : [];
    const archivedProposeList = proposes.length ? proposes.filter(propose => propose.userId == userID && propose.status === 100) : [];
    console.log('# activeProposeList', activeProposeList.filter(propose => propose.proposeId == 5731283548241920));
    this.setState({ proposeList: activeProposeList, archivedList: archivedProposeList });
  }

  approvePropose = proposeId => {
    console.log('# approvePropose', proposeId);
    const { approvePropose, currentUser: { userID } } = this.props;
    // const { proposeList, archivedList } = this.state;
    // const newArchivedList = archivedList;
    // const newProposeList = proposeList;

    // newArchivedList.push(...proposeList.filter(proposeObj => proposeObj.proposeId === proposeId));

    // for (let i in proposeList) {
    //   if (proposeList[i].proposeId === proposeId) {
    //     newProposeList.splice(i, 1);
    //   }
    // }

    // this.setState({
    //   proposeList: newProposeList,
    //   archivedList: newArchivedList,
    // });
    approvePropose({
      proposeId,
      approverUserId: userID
    });
  };

  archivePropose = proposeId => {
    console.log('# archivePropose', proposeId);
    // const { approvePropose } = this.props;
    // const { proposeList, archivedList } = this.state;
    // const newArchivedList = archivedList;
    // const newProposeList = proposeList;

    // newArchivedList.push(...proposeList.filter(proposeObj => proposeObj.proposeId === proposeId));

    // for (let i in proposeList) {
    //   if (proposeList[i].proposeId === proposeId) {
    //     newProposeList.splice(i, 1);
    //   }
    // }

    // this.setState({
    //   proposeList: newProposeList,
    //   archivedList: newArchivedList,
    // });
    // approvePropose({
    //   proposeId,
    //   status: 100,
    // });
  };

  restorePropose = proposeId => {
    console.log('# restorePropose', proposeId);
    // const { approvePropose } = this.props;
    // const { proposeList, archivedList } = this.state;
    // const newProposeList = proposeList;
    // const newArchivedList = archivedList;

    // for (let i in archivedList) {
    //   if (archivedList[i].proposeId === proposeId) {
    //     newArchivedList.splice(i, 1);
    //   }
    // }

    // newProposeList.push(...archivedList.filter(proposeObj => proposeObj.proposeId === proposeId));

    // this.setState({
    //   proposeList: newProposeList,
    //   archivedList: newArchivedList,
    // });

    // approvePropose({
    //   proposeId,
    //   status: 210,
    // });
  };

  render() {
    const { activeProposeList, archivedProposeList, isArchiveList, currentUser: { userID } } = this.props;
    const { proposeList, archivedList } = this.state;

    return (
      <View style={styles.root}>
        <ScrollView>
          {isArchiveList && archivedList.length ? (
            <ProposeList list={archivedList} swipeAction={this.restorePropose} />
          ) : (
              <ProposeList list={proposeList} swipeAction={this.archivePropose} approvePropose={this.approvePropose} />
          )}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  currentUser: getSelector('chat', 'currentUser')(state),
//   ...selectState('propose', 'proposes')(state, 'proposes'),
//   ...selectState('propose', 'archivedProposes')(state, 'archivedProposes'),
//   ...selectState('propose', 'proposeCreated')(state, 'proposeCreated'),
//   ...selectState('propose', 'proposeUpdated')(state, 'proposeUpdated'),
});

// /* istanbul ignore next */
const mapDispatchToProps = dispatch => ({
  listProposes: payload => dispatch(listProposes(payload)),
//   listArchived: payload => dispatch(listArchived(payload)),
  approvePropose: payload => dispatch(approvePropose(payload)),
});

export default compose(
  setDisplayName('Step3'),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  immutableToJS
)(Step3);
