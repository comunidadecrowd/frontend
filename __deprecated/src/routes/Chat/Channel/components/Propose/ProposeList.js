/* eslint-disable */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Animated, Dimensions } from 'react-native';
import Interactable from 'react-native-interactable';
import { setDisplayName, compose } from 'recompose';
import { connect } from 'react-redux';

import { selectState } from 'redux/selectors';
import immutableToJS from 'utils/immutableToJS';

import IconBacktick from 'components/Icon/IconBacktick';
import { Trash, Restore } from 'components/Icon/ProposeIcons';

const { width } = Dimensions.get('window');

const styles = {
  horizontalLine: {
    borderTopColor: 'white',
    borderTopWidth: 0.3,
    backgroundColor: 'black',
  },
  text: {
    color: 'white',
  },
  itemContainer: {
    height: 65,
    backgroundColor: 'black',
    borderTopColor: 'white',
    borderTopWidth: 0.3,
  },
  interactableContainer: {
    width: '100%',
    backgroundColor: 'black',
    padding: 20,
    flexDirection: 'row',
    alignItems: 'center',
    height: '100%',
  },
  proposeText: {
    color: 'white',
    fontFamily: 'Inter UI',
    fontSize: 18,
  },
  actionText: {
    color: 'black',
    fontFamily: 'Inter UI',
    fontSize: 14,
  },
  rightIconsWrapper: {
    position: 'absolute',
    right: 0,
    padding: 20,
    paddingTop: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIconsWrapper: {
    position: 'absolute',
    left: 0,
    padding: 20,
    flexDirection: 'column',
    backgroundColor: 'gray',
  },
};

class ProposeList extends Component {
  static propTypes = {
    list: PropTypes.arrayOf(
      PropTypes.shape({
        body: PropTypes.shape({
          title: PropTypes.string,
          price: PropTypes.number,
          daysTerm: PropTypes.number,
        }),
        proposeId: PropTypes.string,
        status: PropTypes.number,
        userId: PropTypes.string,
      })
    ).isRequired,
    swipeAction: PropTypes.func.isRequired,
    isArchiveList: PropTypes.bool,
    proposeCreatedRequesting: PropTypes.bool,
  };

  static defaultProps = {
    isArchiveList: false,
    proposeCreatedRequesting: false,
  };

  cards = [];

  deltaX = null;
  deltaY = null;
  transit = new Animated.Value(0);
  transitOpacity = new Animated.Value(0.1);

  onArchiveAnim = index => {
    Animated.timing(this.deltaY[index], {
      toValue: 0,
      duration: 200,
    }).start();
  };

  componentDidMount() {
    const { proposeCreatedRequesting } = this.props;

    proposeCreatedRequesting &&
      Animated.loop(
        Animated.spring(this.transit, {
          toValue: width / 2.5 - 30,
          duration: 2000,
        })
      ).start();
  }

  componentDidUpdate() {
    const { list } = this.props;

    if (list.length > this.deltaX.length) {
      this.deltaX.push(new Animated.Value(0));
    }
    if (list.length > this.deltaY.length) {
      this.deltaY.push(new Animated.Value(1));
    }
  }
  onDrag = (event, proposeIndex, proposeId) => {
    const { swipeAction, approvePropose } = this.props;
    const snapPointPos = event.nativeEvent.x;

    console.log('# snapPointPos', snapPointPos);

    if (snapPointPos > width / 5) {
      console.log('# onDrag', width / 5);
      this.cards[proposeIndex].snapTo({ index: 3 });
      this.onArchiveAnim(proposeIndex);
      setTimeout(() => {
        Animated.timing(this.deltaY[proposeIndex], {
          toValue: 1,
          duration: 0,
        }).start();
        this.deltaY.splice(proposeIndex, 1);
        this.deltaX.splice(proposeIndex, 1);
        swipeAction(proposeId);
        // since react recycles the views
        // this set the card to original position when removing an element
        // from the propose array
        this.cards[proposeIndex] && this.cards[proposeIndex].changePosition({ x: 0 });
      }, 500);
    } else if (snapPointPos < width / 5) {
      this.cards[proposeIndex].snapTo({ index: 3 });
      this.onArchiveAnim(proposeIndex);
      setTimeout(() => {
        Animated.timing(this.deltaY[proposeIndex], {
          toValue: 1,
          duration: 0,
        }).start();
        this.deltaY.splice(proposeIndex, 1);
        this.deltaX.splice(proposeIndex, 1);
        approvePropose(proposeId);
        // since react recycles the views
        // this set the card to original position when removing an element
        // from the propose array
        this.cards[proposeIndex] && this.cards[proposeIndex].changePosition({ x: 0 });
      }, 500);
    }
  };

  setStatusColor = status => {
    return status === 200 ? 'rgba(0,0,0,.5)' : status === 210 ? 'rgba(0,0,0,1)' : '#86C7ED';
  };

  setStatusText = status => {
    return status === 200
      ? 'Enviando'
      : status === 210
        ? 'Enviada'
        : status === 220
          ? 'Contratada'
          : '';
  };

  render() {
    const { list, isArchiveList, proposeCreatedRequesting } = this.props;

    this.deltaX = list.map(() => new Animated.Value(0));
    this.deltaY = list.map(() => new Animated.Value(1));

    return (
      <Fragment>
        {list.length > 0 &&
          list.map((propose, index) => (
            <Animated.View
              style={[
                styles.itemContainer,
                {
                  transform: [{ scaleY: this.deltaY[index] }],
                },
              ]}
              key={`parentView-${index}`}
            >
              <Animated.View
                style={[
                  styles.leftIconsWrapper,
                  {
                    opacity: this.deltaX[index].interpolate({
                      inputRange: [5, 50],
                      outputRange: [0, 1],
                      extrapolateLeft: 'clamp',
                      extrapolateRight: 'clamp',
                    }),
                  },
                ]}
              >
                {!isArchiveList ? (
                  <Trash width={24} height={24} />
                ) : (
                  <Restore width={24} height={24} />
                )}
              </Animated.View>
              <Animated.View
                style={[
                  styles.rightIconsWrapper,
                  {
                    opacity: this.deltaX[index].interpolate({
                      inputRange: [-50, -5],
                      outputRange: [1, 0],
                      extrapolateLeft: 'clamp',
                      extrapolateRight: 'clamp',
                    }),
                    /*eslint-disable-next-line*/
                    backgroundColor: 'lightblue'
                    /*eslint-enable*/
                  },
                ]}
              >
                <IconBacktick width={24} height={24} color="#000000" />
                <Text style={styles.actionText}>{this.setStatusText(propose.status)}</Text>
              </Animated.View>
              <Interactable.View
                animatedValueX={this.deltaX[index]}
                key={index}
                ref={c => {
                  this.cards[index] = c;
                }}
                horizontalOnly
                boundaries={{
                  left: -width / 3,
                  bounce: 0.7,
                }}
                snapPoints={[
                  {
                    x: 0,
                    id: 'centered',
                  },
                  {
                    x: width / 6,
                    id: 'archiveOpen',
                  },
                  {
                    x: -width / 6,
                    id: 'actions',
                  },
                  {
                    x: width / 2,
                    id: 'archived',
                  },
                ]}
                onDrag={event => this.onDrag(event, index, propose.proposeId)}
                style={styles.interactableContainer}
              >
                <View style={{ width: '50%' }}>
                  <Text style={[styles.proposeText, { textAlign: 'left' }]}>
                    {propose.body.title || ''}
                  </Text>
                </View>
                <View style={{ width: '50%' }}>
                  <Text style={[styles.proposeText, { textAlign: 'right' }]}>
                    {propose.body.price || ''}
                  </Text>
                </View>
              </Interactable.View>
            </Animated.View>
          ))}
        {proposeCreatedRequesting && (
          <View
            style={{
              width: '100%',
              height: 65,
              flexDirection: 'row',
              borderTopColor: 'white',
              borderTopWidth: 0.3,
            }}
          >
            <View
              style={{
                backgroundColor: '#34352F',
                width: '40%',
                height: 8,
                borderRadius: 50,
                marginLeft: 'auto',
                alignSelf: 'center',
              }}
            >
              <Animated.View
                style={{
                  backgroundColor: '#272822',
                  position: 'absolute',
                  width: 30,
                  left: this.transit,
                  height: 8,
                  borderRadius: 50,
                  alignSelf: 'center',
                }}
              />
            </View>
            <View
              style={{
                backgroundColor: '#34352F',
                width: '40%',
                height: 8,
                borderRadius: 50,
                marginLeft: 10,
                marginRight: 'auto',
                alignSelf: 'center',
              }}
            >
              <Animated.View
                style={{
                  backgroundColor: '#272822',
                  position: 'absolute',
                  width: 30,
                  left: this.transit,
                  height: 8,
                  borderRadius: 50,
                  alignSelf: 'center',
                }}
              />
            </View>
          </View>
        )}
        {list.length > 0 && <View style={styles.horizontalLine} />}
      </Fragment>
    );
  }
}

/* istanbul ignore next */
const mapStateToProps = state => ({
  ...selectState('propose', 'proposeCreated')(state, 'proposeCreated'),
});

export default compose(
  setDisplayName('ProposeList'),
  connect(mapStateToProps),
  immutableToJS
)(ProposeList);
