import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import Input from 'components/Input';
import { TextInputMask } from 'react-native-masked-text';
import ButtonSubmit from 'components/ButtonSubmit';

const styles = {
  root: {
    paddingTop: 0,
    padding: 25,
  },
  inputWrapper: {
    marginTop: 15,
  },
  activeInput: {
    paddingBottom: 15,
  },
  input: {
    paddingBottom: 8,
  },
  submit: {
    marginHorizontal: 'auto',
    marginTop: 20,
  },
};

class Step2 extends Component {
  static propTypes = {
    disabled: PropTypes.bool,
    onUpdate: PropTypes.func.isRequired,
    onSubmit: PropTypes.func.isRequired,
  };

  static defaultProps = {
    disabled: false,
  };

  state = {
    activeInputIndex: null,
  };

  textInputRef = [];

  onFocusInput = index => {
    this.setState({ activeInputIndex: index });
  };

  onBlur = () => {
    if (!this.state.activeInputIndex) {
      this.setState({ activeInputIndex: null });
    }
  };

  render() {
    const { onUpdate, onSubmit, disabled } = this.props;
    const { activeInputIndex } = this.state;

    return (
      <View style={styles.root}>
        <Input
          setRef={c => {
            this.textInputRef[0] = c;
          }}
          blurOnSubmit={false}
          styleWrapper={styles.inputWrapper}
          styleInput={activeInputIndex === 0 ? styles.activeInput : styles.input}
          label="Título"
          onChange={value => onUpdate('title', value)}
          returnKeyType="next"
          id="inputTitle"
          enablesReturnKeyAutomatically
          autoFocus
          onFocus={() => this.onFocusInput(0)}
          onSubmitEditing={() => this.textInputRef[1]._component.focus()}
        />
        <Input
          setRef={c => {
            this.textInputRef[1] = c;
          }}
          styleWrapper={styles.inputWrapper}
          styleInput={activeInputIndex === 1 ? styles.activeInput : styles.input}
          label="Valor"
          onChange={value => onUpdate('value', value)}
          returnKeyType="next"
          id="inputValue"
          enablesReturnKeyAutomatically
          onFocus={() => this.onFocusInput(1)}
          onSubmitEditing={() => this.textInputRef[2]._component.focus()}
          blurOnSubmit={false}
          type={'money'}
          component={TextInputMask}
        />
        <Input
          setRef={c => {
            this.textInputRef[2] = c;
          }}
          styleWrapper={styles.inputWrapper}
          styleInput={activeInputIndex === 2 ? styles.activeInput : styles.input}
          label="Prazo em dias"
          onChange={value => onUpdate('term', value)}
          returnKeyType="send"
          id="inputTerm"
          enablesReturnKeyAutomatically
          onFocus={() => this.onFocusInput(2)}
          onSubmitEditing={onSubmit}
          type={'only-numbers'}
          component={TextInputMask}
        />
        <ButtonSubmit
          style={styles.submit}
          onPress={onSubmit}
          disabled={disabled}
          type={'submit'}
          activeColor="lightblue"
        >
          enviar proposta
        </ButtonSubmit>
      </View>
    );
  }
}

export default Step2;
