/* eslint-disable */
import React, { Fragment } from 'react';
import { View } from 'react-native';

import { FriendBubble } from 'components/Icon/ChatBubbles';
import RichMessage from 'components/RichMessage';

import TextFormat from './TextFormat';
import TypingFormat from './TypingFormat';
import UserImage from './UserImage';
import styles from './styles';

const MessageType = (item, content, onAccept) => {
  if (item.attributes) {
    if (item.attributes.type === 'propose') {
      return (
        <RichMessage from="friend" item={item} template={item.attributes.type} onPress={() => onAccept(item)} />
      );
    }
  }
  return <TextFormat>{content}</TextFormat>;
};

const FriendMessage = props => {
  const { user, onAccept } = props;
  const { payload } = props.data;
  const { userMessageWrapper, friendProposeWrapper } = styles;

  return payload.map((item, index) => {
    const { content, status, attributes } = item;

    return (
      <View key={index}>
        {index === payload.length - 1 && (
          <Fragment>
            <UserImage category={user.category} imageURL={user.image} />
            <FriendBubble
              width={15}
              height={15}
              style={{
                position: 'absolute',
                left: -15,
                top: 7,
              }}
            />
          </Fragment>
        )}
        <View
          style={[
            userMessageWrapper,
            index !== payload.length - 1 && { marginBottom: 10 },
            props.style,
            attributes && attributes.type === 'propose' ? friendProposeWrapper : props.style,
          ]}
        >
          {status === 'typing' ? <TypingFormat /> : MessageType(item, content, onAccept)}
        </View>
      </View>
    );
  });
};

export default FriendMessage;
