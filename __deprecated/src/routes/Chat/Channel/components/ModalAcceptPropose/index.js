import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import { noop } from 'lodash';

import Interactable from 'react-native-interactable';
import CrowdModal from 'components/CrowdModal';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { DeliveryMan, Buy, Interview } from 'components/Icon/CarouselPropose';
import ButtonSubmit from 'components/ButtonSubmit';
import { FontRatioNormalize } from 'theme';
const { width, height } = Dimensions.get('window');

const styles = {
  wrapperInteractable: {
    position: 'absolute',
    width: width - 10,
    height: height - 135,
    paddingTop: 22,
    top: 50,
    left: 5,
    alignItems: 'center',
    zIndex: 1,
  },

  touchableCloseModal: {
    position: 'absolute',
    width: width,
    height: height - 135,
    alignItems: 'center',
    zIndex: 1,
  },

  carouselWrapper: {
    width: width - 30,
    height: '100%',
    marginHorizontal: 'auto',
  },

  textWrapper: {
    paddingTop: 60,
    paddingHorizontal: 25,
    height: 160,
    color: 'white',
  },

  text: {
    color: 'white',
    textAlign: 'center',
    paddingVertical: 10,
  },

  paginatorWrapper: {
    alignItems: 'center',
  },

  paginationDot: {
    marginHorizontal: 10,
    bottom: 30,
    width: 9,
    height: 9,
    borderRadius: 20,
  },
};

class ModalAcceptPropose extends Component {
  static propTypes = {
    getRef: PropTypes.func,
    onAcceptPropose: PropTypes.func,
  };

  static defaultProps = {
    getRef: noop,
    onAcceptPropose: noop,
  };

  state = {
    currentSnapIndex: 0,
    tutorialStep: 1,
    entries: [
      'Mollit proident fugiat minim in sit nostrud cillum consequat esse consequat anim cupidatat laboris officia. Sunt mollit ullamco consequat aliquip nisi...',
      'Anim ex cupidatat exercitation non aliquip tempor dolore eu consequat. Id proident et minim amet veniam reprehenderit excepteur dolor voluptate nulla dolore.',
      'Sunt mollit ullamco consequat aliquip nisi amet excepteur nostrud labore voluptate eu ut eiusmod.',
    ],
    sliderActiveItem: 0,
  };

  renderItem = ({ item, index }) => {
    return (
      <View style={styles.textWrapper} key={index}>
        {index === 0 && (
          <Interview height={80} width={90} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        {index === 1 && <Buy height={80} width={90} color="#fff" style={{ alignSelf: 'center' }} />}
        {index === 2 && (
          <DeliveryMan height={80} width={90} color="#fff" style={{ alignSelf: 'center' }} />
        )}
        <Text style={styles.text}>{item}</Text>
      </View>
    );
  };

  onSnap = ({ nativeEvent: { index } }) => {
    this.setSnapIndex(index);
  };

  setSnapIndex = index => {
    this.setState({ currentSnapIndex: index });
  };

  handleCloseModal = () => {
    this.view.snapTo({ index: 0 });
  };

  submitPropose = () => {
    const { onAcceptPropose } = this.props;

    onAcceptPropose();
    this.handleCloseModal();
  };

  render() {
    const { getRef } = this.props;
    const { entries, sliderActiveItem, currentSnapIndex } = this.state;

    return (
      <Interactable.View
        verticalOnly
        initialPosition={{
          x: 0,
          y: height,
        }}
        ref={c => {
          this.view = c;
          getRef(c);
        }}
        snapPoints={[{ y: height }, { y: 5 }]}
        style={styles.wrapperInteractable}
        onSnap={this.onSnap}
        currentSnapIndex={currentSnapIndex}
      >
        <TouchableOpacity
          style={styles.touchableCloseModal}
          activeOpacity={1}
          onPress={this.handleCloseModal}
        />
        <CrowdModal handleCloseModal={this.handleCloseModal}>
          <Carousel
            ref={c => {
              this._carousel = c;
            }}
            data={entries}
            renderItem={this.renderItem}
            sliderWidth={width}
            itemWidth={width}
            swipeThreshold={10}
            horizontal
            firstItem={0}
            onSnapToItem={index => this.setState({ sliderActiveItem: index })}
          />
          <Pagination
            dotStyle={styles.paginationDot}
            inactiveDotColor={'#FFFFFF'}
            activeDotIndex={sliderActiveItem}
            dotColor={'lightblue'}
            dotsLength={entries.length}
            containerStyle={styles.stepIndicatorWrapper}
            carouselRef={this._carousel}
            inactiveDotScale={0.9}
          />
          <ButtonSubmit
            noAnimation
            style={{
              marginHorizontal: 'auto',
              bottom: 0,
            }}
            textSize={FontRatioNormalize(18)}
            onPress={this.submitPropose}
          >
            contratar proposta
          </ButtonSubmit>
        </CrowdModal>
      </Interactable.View>
    );
  }
}

export default ModalAcceptPropose;
