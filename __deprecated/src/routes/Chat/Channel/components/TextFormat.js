import React from 'react';
import PropTypes from 'prop-types';

import { P } from 'theme/ChatTheme';

const TextFormat = ({ children, ...props }) => {
  return (
    <P {...props} color="#000000">
      {children}
    </P>
  );
};

TextFormat.propTypes = {
  children: PropTypes.node.isRequired,
};

export default TextFormat;
