import React, { Fragment } from 'react';
import { View } from 'react-native';

import IconBacktick from 'components/Icon/IconBacktick';
import TextFormat from './TextFormat';
import { UserBubble } from 'components/Icon/ChatBubbles';

import RichMessage from 'components/RichMessage';

import styles from './styles';

const MessageType = item => {
  if (item.attributes.type === 'propose') {
    return <RichMessage item={item} template={item.attributes.type} />;
  }
};

const UserMessage = props => {
  const {
    userMessageWrapper,
    messageAnswerWrapper,
    iconBacktick,
    clearBorder,
    userProposeWrapper,
    proposeWrapper,
  } = styles;
  const { payload } = props.data;

  return payload.map((item, index) => {
    const { status, content, attributes } = item;

    return (
      <View key={index}>
        {index === payload.length - 1 && (
          <Fragment>
            <View style={clearBorder} />
            <UserBubble
              width={15}
              height={15}
              style={{
                position: 'absolute',
                right: -14,
                top: 6,
              }}
            />
          </Fragment>
        )}
        <View
          style={[
            userMessageWrapper,
            attributes && attributes.type === 'propose' ? proposeWrapper : messageAnswerWrapper,
            index !== payload.length - 1 && { marginBottom: 10 },
            attributes && attributes.type === 'propose' ? userProposeWrapper : props.style,
          ]}
        >
          {!attributes ? <TextFormat>{content}</TextFormat> : MessageType(item)}
          <IconBacktick
            style={iconBacktick}
            width={10}
            height={8}
            color={
              status === 'send' ? 'rgba(0,0,0,.5)' : status === 'sent' ? 'rgba(0,0,0,1)' : '#86C7ED'
            }
          />
        </View>
      </View>
    );
  });
};

export default UserMessage;
