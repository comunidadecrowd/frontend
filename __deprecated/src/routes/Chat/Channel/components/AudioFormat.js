import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { G, Polygon, Svg } from 'react-native-svg';

import { P } from 'theme/ChatTheme';
import { userColors } from 'utils/chatUtils';

import styles from './styles';

const AudioFormat = ({ children, category, ...props }) => {
  const { audioFormatWrapper, row, audioFormatBarWrapper, audioFormatBar } = styles;

  return (
    <Fragment>
      <View style={[audioFormatWrapper, row]}>
        <View style={[row, { alignItems: 'center' }]}>
          <Svg width={8} height={10} style={{ marginRight: 8 }}>
            <G stroke="none" fill="none" fillRule="evenodd">
              <G transform="translate(-77.000000, -567.000000)" fill="#000000">
                <Polygon points="77 567 85 572.001043 77 577" />
              </G>
            </G>
          </Svg>
          <P color="#000">Mensagem de voz</P>
        </View>
        <P color="#000">{props.children}</P>
      </View>
      <View style={audioFormatBarWrapper}>
        <View
          style={[
            audioFormatBar,
            {
              width: '75%',
              backgroundColor: userColors(category),
            },
          ]}
        />
      </View>
    </Fragment>
  );
};

AudioFormat.propTypes = {
  category: PropTypes.string,
  children: PropTypes.node.isRequired,
};

AudioFormat.defaultProps = {
  category: '',
};

export default AudioFormat;
