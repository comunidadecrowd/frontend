import React from 'react';
import Svg, { Polygon } from 'react-native-svg';

const SendButton = props => (
  <Svg {...props}>
    <Polygon points="24,24 12,0 0,24" fill="#84C9F2" stroke="none" />
  </Svg>
);

export default SendButton;
