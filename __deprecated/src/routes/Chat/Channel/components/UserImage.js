import React from 'react';
import PropTypes from 'prop-types';
import { Image } from 'react-native';

import { userColors } from 'utils/chatUtils';

import styles from './styles';

const UserImage = ({ category, imageURL }) => {
  const { userImage } = styles;

  return (
    <Image style={[userImage, { borderColor: userColors(category) }]} source={{ uri: imageURL }} />
  );
};

UserImage.propTypes = {
  category: PropTypes.string,
  imageURL: PropTypes.string.isRequired,
};

UserImage.defaultProps = {
  category: '',
};

export default UserImage;
