import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';

import { P } from 'theme/ChatTheme';

const styles = {
  main: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  label: {
    paddingBottom: 5,
    paddingTop: 5,
  },
  attributes: {
    borderBottomWidth: 1,
    borderBottomColor: 'lightblue',
    paddingBottom: 5,
    marginBottom: 15,
  },
};

const ProposeMessage = props => {
  // props.data is here so code doesn't break when redering old proposes
  // with different object structure.
  // It will be removed after enough messages disappear from current history
  const { title, value, term } = props.data.body || props.data;

  return (
    <View style={styles.main}>
      <P style={styles.label} color="#CCCCCC">
        Titulo
      </P>
      <View style={styles.attributes}>
        <P color="#000000">{title}</P>
      </View>
      <P style={styles.label} color="#CCCCCC">
        Valor
      </P>
      <View style={styles.attributes}>
        <P color="#000000">{value}</P>
      </View>
      <P style={styles.label} color="#CCCCCC">
        Prazo
      </P>
      <View style={styles.attributes}>
        <P color="#000000">{term}</P>
      </View>
    </View>
  );
};

ProposeMessage.propTypes = {
  data: PropTypes.shape({
    type: PropTypes.string.isRequired,
    body: PropTypes.shape({
      title: PropTypes.string,
      value: PropTypes.string,
      term: PropTypes.string,
    }),
  }).isRequired,
};

export default ProposeMessage;
