/* eslint-disable */
import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
  ActivityIndicator,
  BackHandler,
  Keyboard,
} from 'react-native';
import { connect } from 'react-redux';
import { compose, setDisplayName } from 'recompose';
import { IconBackButton, IconCallUser, IconOpenMenu, IconStar } from 'components/Icon/ChatHeader';
import { isEmpty } from 'lodash';

import immutableToJS from 'utils/immutableToJS';
import { getSelector, selectState } from 'redux/selectors';
import { createPropose, updatePropose, hirePropose } from 'redux/propose/actions';

import { userColors } from 'utils/chatUtils';
import { APP_SWORKER } from '../_storageWorker';
import { Main, H5, H3E, P } from 'theme/ChatTheme';

import FriendMessage from './components/FriendMessage';
import IconAudio from 'components/Icon/IconAudio';
import IconMoney from 'components/Icon/IconMoney';
import IconPlus from 'components/Icon/IconPlus';
import IconStatus from 'components/Icon/IconStatus';

import ImageGradient from './components/ImageGradient';
import Propose from './components/Propose';
import UserMessage from './components/UserMessage';
import SendButton from './components/SendButton';

import ModalAcceptPropose from './components/ModalAcceptPropose';

import styles from './styles';

const Header = View;
const Profile = View;
const UserInfos = View;
const ChatFooter = View;
const MessageItem = View;
const MainChat = View;

class Channel extends Component {
  static propTypes = {
    data: PropTypes.shape({
      channelInfos: PropTypes.shape({}),
      userData: PropTypes.shape({}),
    }),
    currentUser: PropTypes.object,
    handleBack: PropTypes.func.isRequired,
    createPropose: PropTypes.func.isRequired,
    hirePropose: PropTypes.func.isRequired,
  };

  static defaultProps = {
    data: {},
    currentUser: {},
    proposes: [],
    proposeCreated: {},
    proposesRequesting: false,
  };

  chatChannel = null;
  storageChannel = null;
  friend = null;

  state = {
    init: false,
    headerStatus: false,
    userStatus: 'Offline',
    messages: [],
    proposeId: null,
    typing: false,
    loading: true,
    inputHeight: 0,
    newMessage: '',
    isKeyboardOpen: false,
  };

  async componentDidMount() {
    const { data, handleBack } = this.props;

    BackHandler.addEventListener('hardwareBackPress', handleBack);
    Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
    Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    await APP_SWORKER.goToChannel(data.channelInfos.SID).then(res => (this.chatChannel = res));
    await APP_SWORKER.Chat.getUserDescriptor(data.userData.userID).then(res => (this.friend = res));

    // LISTENERS
    APP_SWORKER.Chat.on('messageAdded', message => this.messageAddOnChannel(message));
    APP_SWORKER.Chat.on('memberUpdated', () => this.setStatusMessages(-1, 'read'));
    APP_SWORKER.Chat.on('userUpdated', updatedReasons => console.log(updatedReasons));
    await this.chatChannel.on('typingStarted', this.typingStarted);
    await this.chatChannel.on('typingEnded', this.typingEnded);

    const messages = (await this.chatChannel.getMessages()).items;

    this.setState({
      messages: messages.map(({ state: { author, body, attributes } }) => ({
        from: data.userData.userID === parseInt(author) ? 'friend' : 'user',
        body,
        status: data.userData.userID === parseInt(author) ? '' : 'read',
        // if the messages fetched from twilio have attributes
        // then set the state messages these attributes for
        // correct rendering
        attributes,
      })),
      loading: false,
    });
    this.showHeader();
  }

  keyboardDidShow = () => this.setState({ isKeyboardOpen: true });
  keyboardDidHide = () => this.setState({ isKeyboardOpen: false });

  onTogglePropose = () => {
    const { isKeyboardOpen } = this.state;

    if (isKeyboardOpen) {
      Keyboard.dismiss();
    }
    this.propose.snapTo({ index: 1 });
  };

  showHeader = () => {
    setTimeout(() => this.setState({ headerStatus: true }), 1500);
  };

  userUpdated = () => {
    console.log('user updated');
  };

  getMessageGroups = () => {
    const { messages, typing } = this.state;
    const messageGroups = [];
    let messageGroup = {};
    let user = '';

    messages.forEach(message => {
      if (message.from === user) {
        messageGroup.payload.push({
          status: message.status,
          content: message.body,
          // so there is no empty objects
          attributes: isEmpty(message.attributes) ? undefined : message.attributes,
        });
      } else {
        messageGroup = {
          from: message.from,
          payload: [
            {
              status: message.status,
              content: message.body,
              attributes: isEmpty(message.attributes) ? undefined : message.attributes,
            },
          ],
        };
        messageGroups.push(messageGroup);
        user = message.from;
      }
    });
    if (typing) {
      messageGroups.push({
        from: 'friend',
        payload: [{ status: 'typing' }],
      });
    }
    return messageGroups;
  };

  typingMessage = text => {
    this.chatChannel.typing();
    this.setState({ newMessage: text });
  };

  typingStarted = () => {
    this.setState({ typing: true });
  };

  typingEnded = () => {
    this.setState({ typing: false });
  };

  onSubmitPropose = (title, value, term) => {
    const { data, createPropose, currentUser } = this.props;

    // As some channel information are required to send
    // a propoosal to the backend
    // this api action will be called in this file
    createPropose({
      body: {
        title,
        price: Number(value.replace(/[^0-9-.]/g, '')),
        daysTerm: Number(term),
      },
      status: 200,
      userId: `${currentUser.userID}`,
      channelSid: data.channelInfos.SID,
    });
  };

  toggleModalAcceptPropose = message => {
    const { proposeId } = message.attributes;

    this.setState({ proposeId: proposeId });
    this.acceptProposeModal.snapTo({ index: 1 });
  };

  onAcceptPropose = () => {
    const { hirePropose, data } = this.props;
    const { proposeId } = this.state;

    hirePropose({
      proposeId: proposeId,
      contractorUserId: data.userData.userID,
    });
  };

  sendMessage = () => {
    const { messages, newMessage } = this.state;

    if (!newMessage) return;

    this.textInputREF.setNativeProps({ text: '' });

    this.setState(
      {
        messages: [
          ...messages,
          {
            from: 'user',
            body: newMessage,
            status: 'send',
          },
        ],
      },
      () => {
        this.chatChannel
          .sendMessage(newMessage)
          .then(() => this.setStatusMessages(messages.length))
          .catch(e => console.log(e));
        this.setState({ newMessage: '' });
      }
    );
  };

  setStatusMessages = (messageIndex, type = undefined) => {
    const { messages } = this.state;

    if (type === 'read') {
      this.setState({
        messages: messages.map(
          message =>
            message.status === 'sent'
              ? {
                  ...message,
                  status: 'read',
                }
              : message
        ),
      });
      return;
    }

    messages[messageIndex].status = 'sent';
    this.setState({
      messages: [...messages],
    });
  };

  messageAddOnChannel = async message => {
    const { data, currentUser } = this.props;
    const { body, author } = message;
    const { messages } = this.state;

    if (data.userData.userID !== parseInt(author) && message.attributes.type === 'propose') {
      this.setState({
        messages: [
          ...messages,
          {
            from: 'user',
            body,
            attributes: isEmpty(message.attributes) ? undefined : message.attributes,
          },
        ],
      });
      return;
    } else if (parseInt(author) !== parseInt(currentUser.userID)) {
      this.setState({
        messages: [
          ...messages,
          {
            from: 'friend',
            body,
            attributes: isEmpty(message.attributes) ? undefined : message.attributes,
          },
        ],
      });
    }
    this.chatChannel.updateLastConsumedMessageIndex(message.index);
  };

  setInputHeight = height => {
    this.setState({ inputHeight: height });
  };

  componentWillUnmount() {
    BackHandler.removeEventListener('hardwareBackPress', this.props.handleBack);
    Keyboard.removeListener('keyboardDidShow', this.keyboardDidShow);
    Keyboard.removeListener('keyboardDidHide', this.keyboardDidHide);
  }

  render() {
    const {
      main,
      row,
      fullCenter,
      header,
      nameWrapper,
      profileWrapper,
      profileImage,
      imageGradient,
      userInfos,
      mainChat,
      chatFooter,
      chatFooterButton,
      textInput,
      messageWrapper,
      userMessage,
      friendMessage,
    } = styles;

    const { data, handleBack } = this.props;
    const { headerStatus, loading, inputHeight, newMessage, typing } = this.state;
    const { category, image: userIMG, jobTitle, name: userNAME } = data.userData;
    const headerBackground = headerStatus ? '#000' : 'rgba(0, 0, 0, 0)';
    const messages = this.getMessageGroups();

    return (
      <View style={{ flex: 1 }}>
        <KeyboardAvoidingView behavior="position" keyboardVerticalOffset={0}>
          <Fragment>
            <Main style={main}>
              <ScrollView
                ref={ref => (this.scrollMessage = ref)}
                onContentSizeChange={() => this.scrollMessage.scrollToEnd({ animated: false })}
              >
                <TouchableOpacity
                  activeOpacity={1}
                  onPress={() =>
                    !loading &&
                    this.propose.props.currentSnapIndex > 0 &&
                    this.propose.snapTo({ index: 0 })
                  }
                >
                  <Profile style={profileWrapper}>
                    <View>
                      <Image style={profileImage} source={{ uri: userIMG }} />
                      <ImageGradient style={imageGradient} width="100%" height="100%" />
                    </View>
                    <UserInfos style={userInfos}>
                      <View style={fullCenter}>
                        <IconStatus
                          width={6}
                          height={6}
                          userStatus={!loading && this.friend.online}
                        />
                        <H3E color={userColors(category)}>{userNAME}</H3E>
                      </View>
                      <P center color="#9B9B9B">
                        {jobTitle}
                      </P>
                      <P
                        center
                        medium
                        style={{
                          marginTop: 10,
                          marginBottom: 5,
                        }}
                      >
                        São Paulo, SP
                      </P>
                      <View style={fullCenter}>
                        <IconStar style={{ marginRight: 8 }} width={18} height={18} color="#fff" />
                        <P center medium size={18}>
                          4.98
                        </P>
                      </View>
                    </UserInfos>
                  </Profile>
                  {!loading ? (
                    <MainChat style={mainChat} ref={ref => (this.mainChatREF = ref)}>
                      {messages.map((item, index) => {
                        return item.from === 'user' ? (
                          <MessageItem key={index} style={messageWrapper}>
                            <UserMessage style={userMessage} data={item} />
                          </MessageItem>
                        ) : (
                          <MessageItem key={index} style={messageWrapper}>
                            <FriendMessage
                              onAccept={this.toggleModalAcceptPropose}
                              style={friendMessage}
                              data={item}
                              user={data.userData}
                            />
                          </MessageItem>
                        );
                      })}
                    </MainChat>
                  ) : (
                    <View style={mainChat}>
                      <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                  )}
                </TouchableOpacity>
              </ScrollView>
              {!loading && (
                <ChatFooter style={[row, chatFooter]}>
                  <TouchableOpacity style={chatFooterButton}>
                    <IconPlus width={19} height={19} />
                  </TouchableOpacity>
                  <TextInput
                    ref={ref => (this.textInputREF = ref)}
                    placeholder="Escreva uma mensagem"
                    placeholderTextColor="#fff"
                    style={[textInput, { height: Math.max(35, inputHeight) }]}
                    value={newMessage}
                    onChange={event => this.typingMessage(event.nativeEvent.text)}
                    onContentSizeChange={e => this.setInputHeight(e.nativeEvent.contentSize.height)}
                    clearButtonMode="always"
                    editable={true}
                    keyboardType="default"
                    blurOnSubmit={false}
                    multiline={true}
                  />
                  {!newMessage.length ? (
                    <Fragment>
                      <TouchableOpacity style={chatFooterButton} onPress={this.onTogglePropose}>
                        <IconMoney width={10.01} height={18.95} />
                      </TouchableOpacity>
                      <TouchableOpacity style={chatFooterButton}>
                        <IconAudio width={15} height={19} />
                      </TouchableOpacity>
                    </Fragment>
                  ) : (
                    <TouchableOpacity onPress={this.sendMessage} style={{ marginLeft: 'auto' }}>
                      <SendButton width={24} height={20} />
                    </TouchableOpacity>
                  )}
                </ChatFooter>
              )}
            </Main>
            {!loading && (
              <Propose
                getRef={c => {
                  this.propose = c;
                }}
                onSubmitPropose={this.onSubmitPropose}
                channel={data.channelInfos.SID}
              />
            )}
            {!loading && (
              <ModalAcceptPropose
                getRef={c => {
                  this.acceptProposeModal = c;
                }}
                onAcceptPropose={this.onAcceptPropose}
              />
            )}
          </Fragment>
        </KeyboardAvoidingView>
      </View>
    );
  }
}

/* istanbul ignore next */
const mapStateToProps = state => ({
  currentUser: getSelector('chat', 'currentUser')(state),
  ...selectState('propose', 'proposeUpdated')(state, 'proposeUpdated'),
  ...selectState('propose', 'propose')(state, 'propose'),
});

/* istanbul ignore next */
const mapDispatchToProps = dispatch => ({
  createPropose: payload => dispatch(createPropose(payload)),
  updatePropose: payload => dispatch(updatePropose(payload)),
  hirePropose: payload => dispatch(hirePropose(payload)),
});

export default compose(
  setDisplayName('Channel'),
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  immutableToJS
)(Channel);
