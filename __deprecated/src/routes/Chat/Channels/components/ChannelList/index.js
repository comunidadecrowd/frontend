/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { APP_SWORKER } from '_storageWorker';

import { H3E } from 'theme/ChatTheme';
import { ListItem } from './views/ListItem';
import { ImportContacts } from './views/ImportContacts';
class ChannelList extends Component {
  propTypes = {
    currentUser: PropTypes.any.isRequired,
    channels: PropTypes.arrayOf(PropTypes.shape({})),
    handleGoToChannel: PropTypes.func.isRequired,
    setLoading: PropTypes.func.isRequired,
    joining: PropTypes.bool,
  };

  defaultProps = {
    joining: false,
    data: [],
    contacts: {},
  };

  onClickChannel = channel => {
    const { handleGoToChannel, currentUser, setLoading } = this.props;

    APP_SWORKER.createChannel(currentUser, channel, channelData => handleGoToChannel(channelData));
    setLoading();
  };

  render() {
    const { channels, joining } = this.props;

    return (
      <ListContainer>
        <ListTitle>Mensagens</ListTitle>
        {channels && channels.length ?
          channels.map((channel, index) => (
            <ListItem
              key={index}
              disabled={joining}
              onClickChannel={() => this.onClickChannel(channel)}
              channel={channel}
            />
          )) : (
            <ImportContacts handleUpdateContacts={() => false} />
        )}
      </ListContainer>
    );
  }
}
export default ChannelList;

const ListContainer = styled.ScrollView``;
const ListTitle = styled(H3E)`
  text-align: center;
  margin-bottom: 20px;
`;
