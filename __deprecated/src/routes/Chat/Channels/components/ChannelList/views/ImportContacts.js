/* eslint-disable */
import React from 'react';
import { H5, Button } from 'theme/ChatTheme';
import styled from 'styled-components/native';

export const ImportContacts = ({ handleUpdateContacts }) => {
  return (
    <Container>
      <Title center>{`Você ainda não iniciou nenhuma conversa.\nImporte os seus contatos!`}</Title>
      <ButtonAction color="#84C9F2" onPress={handleUpdateContacts}>Importar Contatos</ButtonAction>
    </Container>
  );
};

const Container = styled.View`
  padding: 20px;
`;

const Title = styled(H5)``;

const ButtonAction = styled(Button)`
  margin-top: 20px;
`;
