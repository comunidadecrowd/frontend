/* eslint-disable */
import React from 'react';
import { Text } from 'react-native';
import styled from 'styled-components/native';
import { H3E, P } from 'theme/ChatTheme';
import { categoryColor } from 'utils/Helpers';

import Avatar from 'components/Avatar';

export const ListItem = ({
  onClickChannel,
  disabled,
  channel: {
    contactPhotoPath,
    contactName,
    contactTitle,
    contactCategory,
    lastMessage,
    unreadMessagesCount,
  },
}) => {
  return (
    <_ChannelItem onPress={onClickChannel} disabled={disabled}>
      <Avatar image={contactPhotoPath} color={categoryColor(contactCategory)} />
      <_ChannelUserInfos>
        <_ChannelName color={categoryColor(contactCategory)}>{contactName}</_ChannelName>
        <_ChannelCategory>{contactTitle}</_ChannelCategory>
        <_ChannelLastMessage>{lastMessage}</_ChannelLastMessage>
      </_ChannelUserInfos>
      <_ChannelInfos>
        <_ChannelDateLastMessage>Ontem</_ChannelDateLastMessage>
        <_ChannelCountMessagesUnread>
          <Text>{unreadMessagesCount}</Text>
        </_ChannelCountMessagesUnread>
      </_ChannelInfos>
    </_ChannelItem>
  );
};

const _ChannelItem = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-content: flex-start;
  justify-content: space-between;
  padding: 0 10px;
  margin-bottom: 30px;
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
`;

const _ChannelUserInfos = styled.View`
  display: flex;
  flex-direction: column;
  flex-grow: 2;
`;

const _ChannelName = styled(H3E)`
  color: ${({ color }) => color};
  font-size: 16px;
  margin-left: 10px;
`;

const _ChannelCategory = styled(H3E)`
  color: #666;
  font-size: 16px;
  margin: 0 0 3px 10px;
`;

const _ChannelLastMessage = styled(P)`
  text-align: left;
  color: #fff;
  font-size: 16px;
  margin-left: 10px;
`;

const _ChannelInfos = styled.View`
  display: flex;
  justify-content: center;
  align-content: center;
`;

const _ChannelDateLastMessage = styled(P)`
  display: flex;
  margin-bottom: 8px;
  text-transform: uppercase;
`;

const _ChannelCountMessagesUnread = styled.View`
  display: flex;
  width: 25px;
  height: 25px;
  border-radius: 30px;
  background-color: #fff;
  color: #000;
  align-items: center;
  justify-content: center;
  align-self: center;
`;
