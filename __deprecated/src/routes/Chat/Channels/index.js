/* eslint-disable */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getSelector } from 'redux/selectors';
import immutableToJS from 'utils/immutableToJS';
import { compose, setDisplayName } from 'recompose';
import { StyleSheet, View, Dimensions, Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';

import { Main } from 'theme/ChatTheme';
import ChannelList from './components/ChannelList';
// import Contacts from './Contacts';

const { height: windowH } = Dimensions.get('window');

class Channels extends Component {
  
  static propTypes = {
    contacts: PropTypes.arrayOf(
      PropTypes.shape({
        userId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      })
    ),
  };

  static defaultProps = {
    contacts: [],
  };

  state = {
    channels: undefined,
    channelsStatus: 'initialized',
    channel: undefined,
    joining: false,
  };

  componentDidMount() {
    const { contacts } = this.props;
    let channels = [];

    contacts.map(contact => {
      contact['contactPhotoPath'] = contact.image;
      contact['contactName'] = contact.name;
      contact['contactCategory'] = contact.category;
      contact['contactTitle'] = contact.jobTitle;
      contact['unreadMessagesCount'] = 3;
      contact['lastMessage'] = 'Oi. (:';
      channels.push(contact);
    });
    this.setState({ channels });
  }

  goToChannel = channelData => {
    let channel = channelData;

    this.setState(
      {
        channel,
        joining: false,
      },
      () => {
        Navigation.push('/channels', {
          component: {
            name: '/channel',
            id: '/channel',
            passProps: {
              data: channel,
              handleBack: this.goBackToChannels,
            },
            options: {
              bottomTabs: {
                visible: false,
                ...Platform.select({ android: { drawBehind: true } }),
              },
              topBar: {
                visible: false,
                ...Platform.select({ android: { drawBehind: true } }),
              },
            },
          },
        });
      }
    );
  };

  goBackToChannels = () => {
    this.setState({ channel: undefined });
    Navigation.popTo('/channels');
  };

  isLoading = () => {
    this.setState({ joining: true });
  };

  render() {
    const { currentUser } = this.props;
    const { joining, channels } = this.state;

    return (
      <Main theme="black">
        <View style={styles.content}>
          {/*<Contacts
            data={contacts}
            loading={this.isLoading}
            handleGoToChannel={this.goToChannel}
            joining={joining}
          />*/}
          <ChannelList
            currentUser={currentUser}
            channels={channels}
            setLoading={this.isLoading}
            joining={joining}
            handleGoToChannel={this.goToChannel}
          />
        </View>
      </Main>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    height: windowH - 25,
  },
});

const mapStateToProps = state => ({
  currentUser: getSelector('chat', 'currentUser')(state),
  contacts: getSelector('chat', 'contacts')(state),
});

export default compose(
  setDisplayName('Channels'),
  connect(mapStateToProps),
  immutableToJS
)(Channels);
