import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Image, View, TouchableOpacity, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { compose, setDisplayName } from 'recompose';

import immutableToJS from 'utils/immutableToJS';
import { getSelector } from 'redux/selectors';

import { H5 } from 'theme/ChatTheme';
import { userColors } from 'utils/chatUtils';

import { APP_SWORKER } from '../_storageWorker';

class Contacts extends Component {
  static propTypes = {
    contacts: PropTypes.arrayOf(PropTypes.shape({})),
    joining: PropTypes.bool,
    handleGoToChannel: PropTypes.func.isRequired,
    currentUser: PropTypes.any.isRequired,
    loading: PropTypes.func.isRequired,
  };

  static defaultProps = {
    joining: false,
    data: [],
    contacts: {},
  };

  onClickContact = contact => {
    const { handleGoToChannel, currentUser } = this.props;

    APP_SWORKER.createChannel(currentUser, contact, channelData => handleGoToChannel(channelData));
  };

  render() {
    const { contacts, joining, loading } = this.props;

    return (
      <View theme="black" style={styles.container}>
        <H5
          style={styles.title}
          color="#86C7ED"
          upper
          center
        >{`Inicie uma conversa clicando\nem algum dos seus amigos abaixo.`}</H5>
        <ScrollView>
          {contacts.map(contact => {
            const color = userColors(contact.category);

            return (
              <Fragment key={contact.userID}>
                <TouchableOpacity
                  style={[styles.userWrapper, { opacity: joining ? 0.5 : 1 }]}
                  onPress={() => {
                    this.onClickContact(contact);
                    loading();
                  }}
                  disabled={joining}
                >
                  <Image
                    source={{ uri: contact.image }}
                    style={[styles.userImage, { borderColor: color }]}
                  />
                  <View>
                    <H5 color={color}>{contact.name}</H5>
                    <H5 color="#666666" style={styles.jobTitleStyle}>
                      {contact.jobTitle}
                    </H5>
                  </View>
                </TouchableOpacity>
              </Fragment>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  title: {
    marginBottom: 20,
  },
  userWrapper: {
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  userImage: {
    width: 52,
    height: 52,
    marginRight: 10,
    borderRadius: 26,
    marginVertical: 10,
    borderWidth: 1,
  },
  jobTitleStyle: {},
});

/* istanbul ignore next */
const mapStateToProps = state => ({
  currentUser: getSelector('chat', 'currentUser')(state),
  contacts: getSelector('chat', 'contacts')(state),
});

export default compose(
  setDisplayName('Contacts'),
  connect(mapStateToProps),
  immutableToJS
)(Contacts);
