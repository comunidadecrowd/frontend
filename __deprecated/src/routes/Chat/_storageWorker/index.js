import { Client as TwilioChatClient } from 'twilio-chat';

const APP_SWORKER = {
  
  Chat: null,

  init: async token => {
    let getInitChat = await APP_SWORKER.initChat(token);

    if (getInitChat) return true;
  },

  initChat: async token => {
    const Chat = await TwilioChatClient.create(token);

    // LISTENERS
    Chat.on('channelInvited', channel => APP_SWORKER.channelInvited(channel));

    APP_SWORKER.Chat = Chat;
    APP_SWORKER.channelDescriptors = (await APP_SWORKER.getUserChannels()).items;

    if (Chat) return true;
  },

  getUserChannels: async () => {
    return APP_SWORKER.Chat ? await APP_SWORKER.Chat.getUserChannelDescriptors() : null;
  },

  channelInvited: async channel => {
    await channel.join();
  },

  createChannel: async (userA, userB, callbackFn) => {
    const uniqueName = `direct-${[userA.userID, userB.userID].sort().join('-')}`;

    const channelDescriptor = APP_SWORKER.channelDescriptors.find(
      channel => channel.uniqueName === uniqueName
    );
    let channel = channelDescriptor && (await channelDescriptor.getChannel());

    try {
      if (!channel) {
        channel = await APP_SWORKER.Chat.createChannel({
          uniqueName,
          isPrivate: true,
        });
        await channel.join();
        await channel.invite(String(userB.userID));
      } else {
        if (channelDescriptor && channelDescriptor.status !== 'joined') {
          await channel.join();
        }
      }
    } catch (e) {
      console.log(e);
    }
    const storageData = {
      type: 'created',
      userData: userB,
      channelInfos: {
        SID: channel.sid,
        uniqueName,
      },
    };

    callbackFn(storageData);
  },

  deleteChannel: async channelSID => {
    const channel = await APP_SWORKER.Chat.getChannelBySid(channelSID);

    await channel.delete();
  },

  goToChannel: async SID => {
    const initChannel = await APP_SWORKER.Chat.getChannelBySid(SID);

    if (initChannel) {
      return initChannel;
    }
  },
};

export { APP_SWORKER };
