export const login = {
  auth: false,
  query: `
    mutation LoginMutation($email: String!, $password: String!) {
      login(email: $email, password: $password) {
        tokenInfo
      }
    }
  `,
};
