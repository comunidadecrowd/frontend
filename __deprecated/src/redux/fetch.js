/* istanbul ignore file */
import 'whatwg-fetch';
import { GraphQLClient } from 'graphql-request';
import { get as lodashGet, curry } from 'lodash';
import { API_URL } from 'react-native-dotenv';
import { stringify } from 'qs';

export function makeGraphQLClient(endpoint, auth) {
  return new GraphQLClient(
    endpoint,
    auth
      ? {
          headers: {
            authorization: `Bearer ${lodashGet(global, 'tokenInfo.id')}`,
          },
        }
      : {}
  );
}

function processError({ endpoint /* , query, auth, url, method */ }) {
  /* const metadata = {
    endpoint,
    query,
    url,
    auth,
    method
  };

  const name = `API Error: ${method} ${endpoint}`;
  const severity = 'error'; */

  return error => ({
    error: error.message || `${error.response.status} error calling ${endpoint}`,
  });
}

async function processResponse(body) {
  return {
    response: {
      body,
      status: 'ok',
    },
  };
}

export async function apiCall(
  { query, endpoint = API_URL, auth = true, normalize },
  payload,
  service
) {
  const fullUrl = endpoint + service + '/graphql';
  const client = makeGraphQLClient(fullUrl, auth);

  try {
    const data = await client.request(query, payload);

    return processResponse(normalize ? normalize(data) : data);
  } catch (e) {
    return processError({
      fullUrl,
      query,
    })(e);
  }
}

/* REST API */
function headersWithAuth(auth = true) {
  if (!auth) return {};
  const accessToken = global['@@CrowdStorage'].tokenInfo.accessToken;

  if (accessToken) {
    return { accessToken };
  }
  return {};
}

export function makeApiUrl(endpoint, query) {
  const queryString = stringify(query, {
    encode: false,
    arrayFormat: 'brackets',
  });

  return `${endpoint}${queryString ? '?' : ''}${queryString}`;
}

function checkStatus(response) {
  if (response.status < 400) {
    return response;
  }
  const error = new Error(response.statusText);

  error.response = response;
  throw error;
}

async function restProcessResponse(response) {
  const body = await response.json();

  return {
    response: {
      headers: response.headers,
      body,
      status: response.status,
    },
  };
}

export async function get(endpoint, query, auth = true) {
  const url = makeApiUrl(
    endpoint,
    {
      ...query,
      ...headersWithAuth(auth),
    },
    auth
  );

  try {
    const res = await fetch(url);
    const status = checkStatus(res);
    const processedResponse = await restProcessResponse(status);

    return processedResponse;
  } catch (e) {
    return processError({
      endpoint,
      query,
      url,
      method: 'GET',
    })(e);
  }
}
async function postOrPutOrDelete(method, endpoint, { query, auth = true, requestBody = {} } = {}) {
  const url = makeApiUrl(endpoint, {
    ...query,
    ...headersWithAuth(auth),
  });

  try {
    const res = await fetch(url, {
      method,
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        ...requestBody,
      }),
    });
    const status = checkStatus(res);
    const processedResponse = await restProcessResponse(status);

    return processedResponse;
  } catch (e) {
    return processError({
      endpoint,
      query,
      url,
      auth,
      method: 'GET',
    })(e);
  }
}

export const post = curry(postOrPutOrDelete)('POST');
export const put = curry(postOrPutOrDelete)('PUT');
export const del = curry(postOrPutOrDelete)('DELETE');
