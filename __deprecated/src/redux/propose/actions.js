import { createApiAction } from 'redux/redux-actions';

import {
  LIST_PROPOSES,
  CREATE_PROPOSE,
  UPDATE_PROPOSE,
  HIRE_PROPOSE,
  REQUEST_APPROVAL,
  APPROVE_PROPOSE,
} from 'redux/constants';

import {
  listProposes as listProposesApi,
  createPropose as createProposeApi,
  updatePropose as updateProposeApi,
  hirePropose as hireProposeApi,
  requestApproval as requestApprovalApi,
  approvePropose as approveProposeApi,
} from './apis';

export const listProposes = createApiAction(LIST_PROPOSES, listProposesApi);
export const updatePropose = createApiAction(UPDATE_PROPOSE, updateProposeApi);
export const createPropose = createApiAction(CREATE_PROPOSE, createProposeApi);
export const hirePropose = createApiAction(HIRE_PROPOSE, hireProposeApi);
export const requestApproval = createApiAction(REQUEST_APPROVAL, requestApprovalApi);
export const approvePropose = createApiAction(APPROVE_PROPOSE, approveProposeApi);
