import generateHandleActions from 'redux/state-handler';
import {
  GET_PROPOSE,
  LIST_PROPOSES,
  CREATE_PROPOSE,
  UPDATE_PROPOSE,
  HIRE_PROPOSE,
} from 'redux/constants';

const apiStates = [
  {
    type: GET_PROPOSE,
    name: 'propose',
    kind: 'object',
  },
  {
    type: LIST_PROPOSES,
    name: 'proposes',
  },
  {
    type: CREATE_PROPOSE,
    name: 'proposeCreated',
    kind: 'object',
  },
  {
    type: UPDATE_PROPOSE,
    name: 'proposeUpdated',
    kind: 'object',
  },
  {
    type: HIRE_PROPOSE,
    name: 'propose',
    kind: 'object',
  },
];

const instantStates = [];
const storage = {};

const listValues = ['proposes'];

export default generateHandleActions({
  apiStates,
  instantStates,
  storage,
  listValues,
});
