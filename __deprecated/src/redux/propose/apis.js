import { get } from 'lodash';

export const hirePropose = {
  query: `
    mutation hirePropose($proposeId: ID!, $contractorUserId: ID!) {
      contractPropose(proposeId: $proposeId, contractorUserId: $contractorUserId) {
        dueDate,
        body {
          title,
          price,
        },
      }
    }
  `,
  normalize: response => get(response, 'contractPropose'),
};

export const requestApproval = {
  query: `
    mutation requestApproval(proposeId: ID!){
      requestApprovalPropose(proposeId: $proposeId) {
        dueDate,
        proposeId,
        channelSid,
        status,
        body {
          title,
          price,
          daysTerm
        }
      }
    }
  `,
  normalize: response => get(response, 'requestApprovalPropose'),
};

export const approvePropose = {
  query: `
    mutation approveDelivery($proposeId: ID!, $approverUserId: ID!) {
      approvePropose(proposeId: $proposeId, approverUserId: $approverUserId) {
        dueDate,
        proposeId,
        channelSid,
        status,
        body {
          title,
          price,
          daysTerm,
        }
      }
    }
  `,

  normalize: response => get(response, 'approvePropose'),
};

export const listProposes = {
  query: `
    query listProposes($channelSid: ID!) {
      listProposeByChannelSid(channelSid: $channelSid) {
        body {
          title,
          price,
          daysTerm
        },
        proposeId,
        status,
        userId,
      }
    }
  `,
  normalize: response => get(response, 'listProposeByChannelSid'),
};

export const getPropose = {
  query: `
    query getPropose($proposeId: ID!) {
      listProposeById(proposeId: $proposeId) {
        body {
          title,
          price,
          daysTerm
        }
      }
    }
  `,
  normalize: response => get(response, 'listProposeById'),
};

export const createPropose = {
  query: `
    mutation createPropose($body: bodyInput!, $channelSid: String!, $userId: ID!, $status: Int!)
    {
      insertPropose(
        body: $body,
        channelSid: $channelSid,
        userId: $userId,
        status: $status
      ) {
        body {
          title,
          price,
          daysTerm
        },
        proposeId,
        status,
        userId,
      }
    }
  `,
  normalize: response => get(response, 'insertPropose'),
};

export const updatePropose = {
  query: `
    mutation updateProposeById($proposeId: ID!, $status: Int!) {
      updateProposeByProposeId(proposeId: $proposeId, status: $status) {
        body {
          title,
          price,
          daysTerm
        },
        proposeId,
        status,
        userId,
      }
    }
  `,
  normalize: response => get(response, 'updateProposeByProposeId'),
};
