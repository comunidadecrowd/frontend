// Auth
export const LOGIN = 'auth/LOGIN';
export const SET_TOKEN_INFO = 'auth/SET_TOKEN_INFO';

// ui
export const GLOBAL_NOTIFICATION = 'ui/GLOBAL_NOTIFICATION';
export const SET_HEADER_TITLE = 'ui/SET_HEADER_TITLE';
export const CLEAR_HEADER_TITLE = 'ui/CLEAR_HEADER_TITLE';

// chat
export const GET_CHAT_TOKEN = 'chat/GET_CHAT_TOKEN';
export const SET_CHAT_TOKEN = 'chat/SET_CHAT_TOKEN';
export const SET_CONTACTS = 'chat/SET_CONTACTS';
export const SET_CURRENT_USER = 'chat/SET_CURRENT_USER';

// propose
export const CREATE_PROPOSE = 'propose/CREATE_PROPOSE';
export const GET_PROPOSE = 'propose/GET_PROPOSE';
export const LIST_PROPOSES = 'propose/LIST_PROPOSES';
export const UPDATE_PROPOSE = 'propose/UPDATE_PROPOSE';
export const LIST_ARCHIVED = 'propose/LIST_ARCHIVED';
export const HIRE_PROPOSE = 'propose/HIRE_PROPOSE';
export const REQUEST_APPROVAL = 'propose/REQUEST_APPROVAL';
export const APPROVE_PROPOSE = 'propose/APPROVE_PROPOSE';
