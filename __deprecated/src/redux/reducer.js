import { combineReducers } from 'redux-immutable';

import auth from 'redux/auth/reducer';
import chat from 'redux/chat/reducer';
import ui from 'redux/ui/reducer';
import propose from 'redux/propose/reducer';

const rootReducer = combineReducers({
  auth,
  chat,
  ui,
  propose,
});

export default rootReducer;
