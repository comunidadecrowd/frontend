import { AsyncStorage } from 'react-native';

const initStorage = async () => {
  if (global.CrowdStorage) return;
  const keys = await AsyncStorage.getAllKeys();

  global.CrowdStorage = {};

  for (let i = 0; i < keys.length; i++) {
    const key = keys[i];

    global.CrowdStorage[key] = JSON.parse(await AsyncStorage.getItem(key));
  }
};

export default initStorage;
