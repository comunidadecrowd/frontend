/* istanbul ignore file */
import { apiCall } from 'redux/fetch';
import { createAction as reduxCreateAction } from 'redux-actions';
import { omit } from 'lodash';

export const createAction = reduxCreateAction;

export const apiTypes = type => [
  `${type}_REQUEST`,
  `${type}_SUCCESS`,
  `${type}_FAILURE`,
  `${type}_CLEAR`,
];

export const createClearAction = type => reduxCreateAction(`${type}_CLEAR`);

/**
 * Create an action creator that creates actions with types and apiCall keys. Heavily inspired
 * by the createAction function found in redux-actions
 * @param  {Array} types             An array of types that will be called based on api
 *                                   request. Must be in the following order:
 *                                   REQUEST, SUCCESS, FAILURE
 * @param  {Function} apiCallObject  An object having query, auth (optional), and endpoint (optional)
 * @param  {Function} payloadCreator A function that defines how the payload is created.
 * @return {Function} A function that creates actions.
 */
export function createApiAction(type, apiCallObject, notification = null) {
  const service = type.substring(0, type.indexOf('/')) || '';
  const types = apiTypes(type).slice(0, 3);

  const actionCreator = payload => {
    const action = {
      types,
      apiCall: () => apiCall(apiCallObject, payload, service),
      notification,
    };

    if (payload instanceof Error) {
      action.error = true;
    }

    if (payload !== undefined) {
      action.payload = omit(payload, 'onSuccess');
      action.onSuccess = payload.onSuccess;
    }
    return action;
  };

  return actionCreator;
}
