import generateHandleActions from 'redux/state-handler';
import { GET_CHAT_TOKEN, SET_CONTACTS, SET_CURRENT_USER } from 'redux/constants';

const apiStates = [
  {
    type: GET_CHAT_TOKEN,
    name: 'token',
    kind: 'object',
  },
];
const instantStates = [
  {
    type: SET_CONTACTS,
    name: 'contacts',
  },
  {
    type: SET_CURRENT_USER,
    name: 'currentUser',
    kind: 'object',
  },
];
const storage = {
  token: 'token',
  currentUser: 'currentUser',
};
const listValues = ['contacts'];

export default generateHandleActions({
  apiStates,
  instantStates,
  storage,
  listValues,
});
