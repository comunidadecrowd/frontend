import { createAction, createApiAction, createClearAction } from 'redux/redux-actions';

import { GET_CHAT_TOKEN, SET_CONTACTS, SET_CURRENT_USER } from '../constants';

import { getToken as getTokenApi } from './apis';

export const getChatToken = createApiAction(GET_CHAT_TOKEN, getTokenApi);
export const clearTokenInfo = createClearAction(GET_CHAT_TOKEN);

export const setContacts = createAction(SET_CONTACTS);
export const setCurrentUser = createAction(SET_CURRENT_USER);
