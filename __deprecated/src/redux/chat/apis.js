import { get } from 'lodash';

export const getToken = {
  auth: false,
  query: `
    query getChatToken($uid: String!){
      requestAccessToken(uid: $uid) {
        token
      }
    }
  `,
  normalize: response => get(response, 'requestAccessToken'),
};
