import fetch from 'axios';

const apiUrl = process.env.API_URL || 'https://api-login-dot-crowd-hm.appspot.com/api/v1';

export const login = (email, password) =>
  new Promise((resolve, reject) =>
    fetch({
      url: `${apiUrl}/login/legado`,
      method: 'POST',
      data: {
        login: email,
        senha: password,
      },
    })
      .then(response => {
        resolve(response);
      })
      .catch(({ response }) => {
        reject(response.data);
      })
  );
