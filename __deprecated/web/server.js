var express = require('express');
var compression = require('compression');
var basicAuth = require('basic-auth-connect');
var app = express();

app.use(compression());

app.use(basicAuth('root', 'verde'));

app.use(express.static(__dirname + '/public'));

app.listen(process.env.PORT || 3000);
