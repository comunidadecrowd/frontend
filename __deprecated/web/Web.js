import React from 'react';
import ReactDOM from 'react-dom';
import 'styles/index.css';
import 'styles/fonts.css';
import App from 'routes/App';
import registerServiceWorker from 'utils/registerServiceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

registerServiceWorker();
