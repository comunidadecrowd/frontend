const path = require('path');
const appDirectory = path.resolve(__dirname);

module.exports = {
  
  mode: process.env.NODE_ENV || 'production',
  context: __dirname,
  entry: ['./index.web.js'],

  output: {
    path: path.resolve(appDirectory, 'web'),
    filename: 'bundle.js'
  },

  devServer: {
    // host: '10.11.1.20',
    // host: '0.0.0.0',
    historyApiFallback: true,
    // port: 3000,
    disableHostCheck: true
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        // exclude: /node_modules/,
        include: [
          path.resolve(appDirectory, './index.web.js'),
          path.resolve(appDirectory, 'src'),
          // path.resolve(appDirectory, 'node_modules/react-router-native'),
          // path.resolve(appDirectory, 'node_modules/react-navigation'),
          // path.resolve(appDirectory, 'node_modules/react-native-tab-view'),
          // path.resolve(appDirectory, 'node_modules/react-native-paper'),
          // path.resolve(appDirectory, 'node_modules/react-native-vector-icons'),
          // path.resolve(appDirectory, 'node_modules/react-native-safe-area-view'),
          // path.resolve(appDirectory, 'node_modules/@expo/samples'),
          // path.resolve(appDirectory, 'node_modules/@expo/vector-icons'),
          // path.resolve(appDirectory, 'node_modules/react-native-platform-touchable'),
        ],
        use: {
          loader: 'babel-loader',
          options: {
            // cacheDirectory: false,
            // babelrc: false,
            plugins: [
              '@babel/plugin-transform-runtime'
              // 'react-native-web',
              // 'transform-decorators-legacy',
              // [
              //   'transform-runtime',
              //   {
              //     helpers: false,
              //     polyfill: false,
              //     regenerator: true,
              //   }
              // ]
            ],
            presets: ['module:metro-react-native-babel-preset']
            // presets: ['react-native']
          }
        }
      }
    ]
  },

  resolve: {
    alias: {
      'react-native': 'react-native-web',
    },
    extensions: [ '.web.js', '.js', ]
  }

}